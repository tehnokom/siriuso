"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.utils import timezone
from django.db import transaction
from django.utils.translation import gettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from .schema import EnciklopedioPagxoTipoNode, EnciklopedioPagxoNode, EnciklopedioKategorioTipoNode, EnciklopedioKategorioNode
from ..models import *


class RedaktuEnciklopedioKategorioTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kategorio_tipo = graphene.Field(EnciklopedioKategorioTipoNode, description=_('Созданная/изменённая запись типа категории Энциклопедии'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название типа'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('enciklopedio.povas_krei_kategorian_tipon'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            EnciklopedioKategorioTipo.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except EnciklopedioKategorioTipo.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            tipo = EnciklopedioKategorioTipo.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            tipo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tipo = EnciklopedioKategorioTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('enciklopedio.povas_forigi_kategorion')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('enciklopedio.povas_shanghi_kategorion'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                tipo.kodo = kwargs.get('kodo', tipo.kodo)
                                tipo.forigo = kwargs.get('forigo', tipo.forigo)
                                tipo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tipo.arkivo = kwargs.get('arkivo', tipo.arkivo)
                                tipo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tipo.publikigo = kwargs.get('publikigo', tipo.publikigo)
                                tipo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                tipo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except EnciklopedioKategorioTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuEnciklopedioKategorioTipo(status=status, message=message, kategorio_tipo=tipo)

class RedaktuEnciklopedioKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kategorio = graphene.Field(EnciklopedioKategorioNode, description=_('Созданная/изменённая категории Академии'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        tipo_kodo = graphene.String(description=_('Код типа категории'))
        kodo = graphene.String(description=_('Код'))
        nomo = graphene.String(description=_('Название категории'))
        priskribo = graphene.String(description=_('Описание категории'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kategorio = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('enciklopedio.povas_krei_kategorion'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('tipo_kodo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'tipo_kodo')
                        else:
                            try:
                                tipo = EnciklopedioKategorioTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                            except EnciklopedioKategorioTipo.DoesNotExist:
                                message = _('Неверный тип страницы')

                        if not (kwargs.get('priskribo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'priskribo')

                        if not (kwargs.get('kodo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            kategorio = EnciklopedioKategorio.objects.create(
                                kodo=kwargs.get('kodo'),
                                autoro=uzanto,
                                tipo=tipo,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(kategorio.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            set_enhavo(kategorio.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                            kategorio.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('priskribo', False) or kwargs.get('tipo_kodo', False)
                            or kwargs.get('nomo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False) or kwargs.get('kodo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'tipo_kodo' in kwargs and not message:
                        try:
                            tipo = EnciklopedioKategorioTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except EnciklopedioKategorioTipo.DoesNotExist:
                            message = _('Неверный тип страницы')

                    # Ищем запись для изменения
                    if not message:
                        try:
                            kategorio = EnciklopedioKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('enciklopedio.povas_forigi_kategorion')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('enciklopedio.povas_shanghi_kategorion'):
                                message = _('Недостаточно прав для изменения')
                            else:

                                update_fields = [
                                    'tipo', 'kodo', 'forigo', 'foriga_dato', 'arkivo', 'arkiva_dato',
                                    'publikigo', 'publikiga_dato', 'lasta_autoro', 'lasta_dato',
                                ]

                                kategorio.tipo = tipo or kategorio.tipo
                                kategorio.kodo = kwargs.get('kodo', kategorio.kodo)
                                kategorio.forigo = kwargs.get('forigo', kategorio.forigo)
                                kategorio.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kategorio.arkivo = kwargs.get('arkivo', kategorio.arkivo)
                                kategorio.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kategorio.publikigo = kwargs.get('publikigo', kategorio.publikigo)
                                kategorio.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kategorio.lasta_autoro = uzanto
                                kategorio.lasta_dato = timezone.now()

                                if kwargs.get('priskribo', False):
                                    set_enhavo(kategorio.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('priskribo')

                                if kwargs.get('nomo', False):
                                    set_enhavo(kategorio.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('nomo')

                                kategorio.save(update_fields=update_fields)
                                status = True
                                message = _('Запись успешно изменена')
                        except EnciklopedioKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuEnciklopedioKategorio(status=status, message=message, kategorio=kategorio)

class RedaktuEnciklopedioPagxoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    pagxo_tipo = graphene.Field(EnciklopedioPagxoTipoNode, description=_('Созданная/изменённая запись типа страницы Энциклопедии'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название типа'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('enciklopedio.povas_krei_pagxan_tipon'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            EnciklopedioPagxoTipo.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except EnciklopedioPagxoTipo.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            tipo = EnciklopedioPagxoTipo.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            tipo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tipo = EnciklopedioPagxoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('enciklopedio.povas_forigi_pagxan_tipon')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('enciklopedio.povas_shanghi_pagxan_tipon'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                tipo.kodo = kwargs.get('kodo', tipo.kodo)
                                tipo.forigo = kwargs.get('forigo', tipo.forigo)
                                tipo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tipo.arkivo = kwargs.get('arkivo', tipo.arkivo)
                                tipo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tipo.publikigo = kwargs.get('publikigo', tipo.publikigo)
                                tipo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                tipo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except EnciklopedioPagxoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuEnciklopedioPagxoTipo(status=status, message=message, pagxo_tipo=tipo)

class RedaktuEnciklopedioPagxo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    pagxo = graphene.Field(EnciklopedioPagxoNode, description=_('Созданная/изменённая страницы Академии'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        tipo_kodo = graphene.String(description=_('Код типа страницы'))
        kategorioj_id = graphene.List(graphene.String, 
            description=_('Список id категорий энциклопедии'))
        kodo = graphene.String(description=_('Код'))
        nomo = graphene.String(description=_('Название страницы'))
        teksto = graphene.String(description=_('Текст страницы'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        pagxo = None
        tipo = None
        novo_kategorioj = []
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('enciklopedio.povas_krei_pagxan'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('tipo_kodo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'tipo_kodo')
                        else:
                            try:
                                tipo = EnciklopedioPagxoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                            except EnciklopedioPagxoTipo.DoesNotExist:
                                message = _('Неверный тип страницы')

                        if not (kwargs.get('teksto', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'teksto')

                        if not (kwargs.get('kodo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        # проверяем наличие записей с таким id в списке
                        if not message:
                            if kwargs.get('kategorioj_id', False):
                                for kategorioj in kwargs.get('kategorioj_id'):
                                    try:
                                        novo_kategorioj.append(EnciklopedioKategorio.objects.get(id=kategorioj, forigo=False,
                                                                     arkivo=False, publikigo=True))
                                    except EnciklopedioKategorio.DoesNotExist:
                                        message = _('Неверная категория '+kategorioj)

                        if not message:
                            pagxo = EnciklopedioPagxo.objects.create(
                                kodo=kwargs.get('kodo'),
                                autoro=uzanto,
                                tipo=tipo,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            update_fields = []

                            if kwargs.get('teksto', False):
                                set_enhavo(pagxo.teksto, kwargs.get('teksto'), info.context.LANGUAGE_CODE)
                                update_fields.append('teksto')
                            if kwargs.get('nomo', False):
                                set_enhavo(pagxo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                                update_fields.append('nomo')
                            pagxo.kategorio.set(novo_kategorioj)
                            pagxo.save(update_fields=update_fields)

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('teksto', False) or kwargs.get('tipo_kodo', False)
                            or kwargs.get('nomo', False) or kwargs.get('kategorioj_kodo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False) or kwargs.get('kodo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'tipo_kodo' in kwargs and not message:
                        try:
                            tipo = EnciklopedioPagxoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except EnciklopedioPagxoTipo.DoesNotExist:
                            message = _('Неверный тип страницы')

                    # проверяем наличие записей с таким кодом в списке
                    if not message:
                        if kwargs.get('kategorioj_id', False):
                            for kategorioj in kwargs.get('kategorioj_id'):
                                try:
                                    novo_kategorioj.append(EnciklopedioKategorio.objects.get(id=kategorioj, forigo=False,
                                                                arkivo=False, publikigo=True))
                                except EnciklopedioKategorio.DoesNotExist:
                                    message = _('Неверная категория '+kategorioj)

                    # Ищем запись для изменения
                    if not message:
                        try:
                            pagxo = EnciklopedioPagxo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('enciklopedio.povas_forigi_pagxan')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('enciklopedio.povas_shanghi_pagxan'):
                                message = _('Недостаточно прав для изменения')
                            else:

                                update_fields = [
                                    'tipo', 'kodo', 'forigo', 'foriga_dato', 'arkivo', 'arkiva_dato',
                                    'publikigo', 'publikiga_dato', 'lasta_autoro', 'lasta_dato',
                                ]

                                pagxo.tipo = tipo or pagxo.tipo
                                pagxo.kodo = kwargs.get('kodo', pagxo.kodo)
                                pagxo.forigo = kwargs.get('forigo', pagxo.forigo)
                                pagxo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                pagxo.arkivo = kwargs.get('arkivo', pagxo.arkivo)
                                pagxo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                pagxo.publikigo = kwargs.get('publikigo', pagxo.publikigo)
                                pagxo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                pagxo.lasta_autoro = uzanto
                                pagxo.lasta_dato = timezone.now()

                                if kwargs.get('teksto', False):
                                    set_enhavo(pagxo.teksto, kwargs.get('teksto'), info.context.LANGUAGE_CODE)
                                    update_fields.append('teksto')

                                if kwargs.get('nomo', False):
                                    set_enhavo(pagxo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('nomo')

                                pagxo.kategorio.set(novo_kategorioj)
                                
                                pagxo.save(update_fields=update_fields)
                                status = True
                                message = _('Запись успешно изменена')
                        except EnciklopedioPagxo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuEnciklopedioPagxo(status=status, message=message, pagxo=pagxo)


class EnciklopedioMutations(graphene.ObjectType):
    redaktu_enciklopedio_kategorio_tipo = RedaktuEnciklopedioKategorioTipo.Field(
        description=_('''Создаёт или редактирует типы категорий Энциклопедии''')
    )
    redaktu_enciklopedio_kategorio = RedaktuEnciklopedioKategorio.Field(
        description=_('''Создаёт или редактирует категорию Энциклопедии''')
    )
    redaktu_enciklopedio_pagxo_tipo = RedaktuEnciklopedioPagxoTipo.Field(
        description=_('''Создаёт или редактирует типы страниц Энциклопедии''')
    )
    redaktu_enciklopedio_pagxo = RedaktuEnciklopedioPagxo.Field(
        description=_('''Создаёт или редактирует страницы Энциклопедии''')
    )

