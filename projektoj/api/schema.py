"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene  # сам Graphene
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from ..models import *  # модели приложения


# Модель владельцев задач
class ProjektojTaskoPosedantoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = ProjektojTaskoPosedanto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'tasko__uuid': ['exact'],
            'posedanto_uzanto__id': ['exact'],
            'posedanto_organizo__uuid': ['exact'],
            'statuso__id': ['exact'],
            'tipo__id': ['exact'],
            'posedanto_objekto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель задач
class ProjektojTaskoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    # владельцы (какое именно оружие выполняет задачу выстрела)
    posedanto =  SiriusoFilterConnectionField(ProjektojTaskoPosedantoNode,
        description=_('Выводит владельцев проектов'))

    class Meta:
        model = ProjektojTasko
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'objekto__uuid': ['exact'],
            'statuso__id': ['exact','in'],
            'tipo__id': ['exact'],
            'kategorio__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_posedanto(self, info, **kwargs):
        return ProjektojTaskoPosedanto.objects.filter(tasko=self, forigo=False, arkivo=False, 
            publikigo=True)


# Модель категорий проектов
class ProjektojProjektoKategorioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ProjektojProjektoKategorio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов проектов
class ProjektojProjektoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ProjektojProjektoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов проектов
class ProjektojProjektoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ProjektojProjektoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель владельцев проектов
class ProjektojProjektoPosedantoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = ProjektojProjektoPosedanto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'projekto__uuid': ['exact'],
            'posedanto_uzanto__id': ['exact'],
            'posedanto_organizo__uuid': ['exact'],
            'posedanto_komunumo__id': ['exact'],
            'posedanto_komunumo__uuid': ['exact'],
            'tipo__id': ['exact'],
            'statuso__id': ['exact'],
            'posedanto_objekto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель проектов
class ProjektojProjektoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    # задача
    tasko =  SiriusoFilterConnectionField(ProjektojTaskoNode,
        description=_('Выводит задачи, привязанные к проекту'))

    # владельцы
    posedanto =  SiriusoFilterConnectionField(ProjektojProjektoPosedantoNode,
        description=_('Выводит владельцев проектов'))

    class Meta:
        model = ProjektojProjekto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'tipo__id': ['exact'],
            'statuso__id': ['exact','in'],
            'objekto__uuid': ['exact'],
            'projektojprojektoposedanto__posedanto_komunumo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_tasko(self, info, **kwargs):
        return ProjektojTasko.objects.filter(projekto=self, forigo=False, arkivo=False, 
            publikigo=True)

    def resolve_posedanto(self, info, **kwargs):
        return ProjektojProjektoPosedanto.objects.filter(projekto=self, forigo=False, arkivo=False, 
            publikigo=True)


# Модель типов владельцев проектов
class ProjektojProjektoPosedantoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ProjektojProjektoPosedantoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов владельца проекта
class ProjektojProjektoPosedantoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ProjektojProjektoPosedantoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов связей проектов между собой
class ProjektojProjektoLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ProjektojProjektoLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель связей проектов между собой
class ProjektojProjektoLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = ProjektojProjektoLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'ligilo__uuid': ['exact'],
            'tipo__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель категорий задач
class ProjektojTaskoKategorioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = ProjektojTaskoKategorio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов задач
class ProjektojTaskoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = ProjektojTaskoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов задач
class ProjektojTaskoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = ProjektojTaskoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов владельцев задач
class ProjektojTaskoPosedantoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = ProjektojTaskoPosedantoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов владельца задач
class ProjektojTaskoPosedantoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = ProjektojTaskoPosedantoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов связей задач между собой
class ProjektojTaskoLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ProjektojTaskoLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель связей задач между собой
class ProjektojTaskoLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = ProjektojTaskoLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'ligilo__uuid': ['exact'],
            'tipo__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


class ProjektojQuery(graphene.ObjectType):
    projektoj_projekto_kategorio = SiriusoFilterConnectionField(
        ProjektojProjektoKategorioNode,
        description=_('Выводит все доступные модели категорий проектов')
    )
    projektoj_projekto_tipo = SiriusoFilterConnectionField(
        ProjektojProjektoTipoNode,
        description=_('Выводит все доступные модели типов проектов')
    )
    projektoj_projekto_statuso = SiriusoFilterConnectionField(
        ProjektojProjektoStatusoNode,
        description=_('Выводит все доступные модели статусов проектов')
    )
    projektoj_projekto = SiriusoFilterConnectionField(
        ProjektojProjektoNode,
        description=_('Выводит все доступные модели проектов')
    )
    projektoj_projekto_posedantoj_tipo = SiriusoFilterConnectionField(
        ProjektojProjektoPosedantoTipoNode,
        description=_('Выводит все доступные модели типов владельцев проектов')
    )
    projektoj_projekto_posedantoj_statuso = SiriusoFilterConnectionField(
        ProjektojProjektoPosedantoStatusoNode,
        description=_('Выводит все доступные модели статусов владельца проекта')
    )
    projektoj_projekto_posedantoj = SiriusoFilterConnectionField(
        ProjektojProjektoPosedantoNode,
        description=_('Выводит все доступные модели владельцев проектов')
    )
    projektoj_projekto_ligilo_tipo = SiriusoFilterConnectionField(
        ProjektojProjektoLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей проектов между собой')
    )
    projektoj_projekto_ligilo = SiriusoFilterConnectionField(
        ProjektojProjektoLigiloNode,
        description=_('Выводит все доступные модели связей проектов между собой')
    )
    projektoj_tasko_kategorio = SiriusoFilterConnectionField(
        ProjektojTaskoKategorioNode,
        description=_('Выводит все доступные модели категорий задач')
    )
    projektoj_tasko_tipo = SiriusoFilterConnectionField(
        ProjektojTaskoTipoNode,
        description=_('Выводит все доступные модели типов задач')
    )
    projektoj_tasko_statuso = SiriusoFilterConnectionField(
        ProjektojTaskoStatusoNode,
        description=_('Выводит все доступные модели статусов задач')
    )
    projektoj_tasko = SiriusoFilterConnectionField(
        ProjektojTaskoNode,
        description=_('Выводит все доступные модели задач')
    )
    projektoj_tasko_posedantoj_tipo = SiriusoFilterConnectionField(
        ProjektojTaskoPosedantoTipoNode,
        description=_('Выводит все доступные модели типов владельцев задач')
    )
    projektoj_tasko_posedantoj_statuso = SiriusoFilterConnectionField(
        ProjektojTaskoPosedantoStatusoNode,
        description=_('Выводит все доступные модели статусов владельца задач')
    )
    projektoj_tasko_posedanto = SiriusoFilterConnectionField(
        ProjektojTaskoPosedantoNode,
        description=_('Выводит все доступные модели владельцев задач')
    )
    projektoj_tasko_ligilo_tipo = SiriusoFilterConnectionField(
        ProjektojTaskoLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей задач между собой')
    )
    projektoj_tasko_ligilo = SiriusoFilterConnectionField(
        ProjektojTaskoLigiloNode,
        description=_('Выводит все доступные модели связей задач между собой')
    )
