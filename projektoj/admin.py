"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from django import forms
from django.utils.translation import gettext_lazy as _
import json

from .models import *
from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget
from universo_bazo.models import Realeco
from siriuso.utils.admin_mixins import TekstoMixin


# Форма категорий проектов
class ProjektojProjektoKategorioFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=Realeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = ProjektojProjektoKategorio
        fields = [field.name for field in ProjektojProjektoKategorio._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Категорий проектов
@admin.register(ProjektojProjektoKategorio)
class ProjektojProjektoKategorioAdmin(admin.ModelAdmin, TekstoMixin):
    form = ProjektojProjektoKategorioFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = ProjektojProjektoKategorio


# Форма типов проектов
class ProjektojProjektoTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=Realeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = ProjektojProjektoTipo
        fields = [field.name for field in ProjektojProjektoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы проектов
@admin.register(ProjektojProjektoTipo)
class ProjektojProjektoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ProjektojProjektoTipoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = ProjektojProjektoTipo


# Форма статусов проектов
class ProjektojProjektoStatusoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = ProjektojProjektoStatuso
        fields = [field.name for field in ProjektojProjektoStatuso._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Статусы проектов
@admin.register(ProjektojProjektoStatuso)
class ProjektojProjektoStatusoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ProjektojProjektoStatusoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = ProjektojProjektoStatuso


# Владелец модели объекта Универсо
class ProjektojProjektoPosedantoInline(admin.TabularInline):
    model = ProjektojProjektoPosedanto
    fk_name = 'projekto'
    extra = 1

# Форма проектов
class ProjektojProjektoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    kategorio = forms.ModelMultipleChoiceField(
        label=_('категория проектов Универсо'),
        queryset=ProjektojProjektoKategorio.objects.filter(forigo=False, publikigo=True)
    )
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = ProjektojProjekto
        fields = [field.name for field in ProjektojProjekto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Проекты
@admin.register(ProjektojProjekto)
class ProjektojProjektoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ProjektojProjektoFormo
    list_display = ('id','krea_dato','statuso','nomo_teksto','priskribo_teksto','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('id',)
    inlines = (ProjektojProjektoPosedantoInline,)
    list_filter = ('sxablono_sistema',)
    class Meta:
        model = ProjektojProjekto


# Форма типов владельцев проектов
class ProjektojProjektoPosedantoTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = ProjektojProjektoPosedantoTipo
        fields = [field.name for field in ProjektojProjektoPosedantoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы владельцев проектов
@admin.register(ProjektojProjektoPosedantoTipo)
class ProjektojProjektoPosedantoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ProjektojProjektoPosedantoTipoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = ProjektojProjektoPosedantoTipo


# Форма статусов владельцев проектов
class ProjektojProjektoPosedantoStatusoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = ProjektojProjektoPosedantoStatuso
        fields = [field.name for field in ProjektojProjektoPosedantoStatuso._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Статусы владельцев проектов
@admin.register(ProjektojProjektoPosedantoStatuso)
class ProjektojProjektoPosedantoStatusoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ProjektojProjektoPosedantoStatusoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = ProjektojProjektoPosedantoStatuso


# Форма проектов
class ProjektojProjektoPosedantoFormo(forms.ModelForm):
    
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = ProjektojProjektoPosedanto
        fields = [field.name for field in ProjektojProjektoPosedanto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Проекты
@admin.register(ProjektojProjektoPosedanto)
class ProjektojProjektoPosedantoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ProjektojProjektoPosedantoFormo
    list_display = ('uuid','projekto','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema',)
    class Meta:
        model = ProjektojProjektoPosedanto


# Форма типов связей проектов между собой
class ProjektojProjektoLigiloTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = ProjektojProjektoLigiloTipo
        fields = [field.name for field in ProjektojProjektoLigiloTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы связей проектов между собой
@admin.register(ProjektojProjektoLigiloTipo)
class ProjektojProjektoLigiloTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ProjektojProjektoLigiloTipoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = ProjektojProjektoLigiloTipo


# Форма связей проектов между собой
class ProjektojProjektoLigiloFormo(forms.ModelForm):
    
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = ProjektojProjektoLigilo
        fields = [field.name for field in ProjektojProjektoLigilo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Связи проектов между собой
@admin.register(ProjektojProjektoLigilo)
class ProjektojProjektoLigiloAdmin(admin.ModelAdmin, TekstoMixin):
    form = ProjektojProjektoLigiloFormo
    list_display = ('posedanto','tipo','ligilo','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema',)
    class Meta:
        model = ProjektojProjektoLigilo


# Форма категорий задач
class ProjektojTaskoKategorioFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=Realeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = ProjektojTaskoKategorio
        fields = [field.name for field in ProjektojTaskoKategorio._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Категорий задач
@admin.register(ProjektojTaskoKategorio)
class ProjektojTaskoKategorioAdmin(admin.ModelAdmin, TekstoMixin):
    form = ProjektojTaskoKategorioFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = ProjektojTaskoKategorio


# Форма типов задач
class ProjektojTaskoTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=Realeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = ProjektojTaskoTipo
        fields = [field.name for field in ProjektojTaskoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы задач
@admin.register(ProjektojTaskoTipo)
class ProjektojTaskoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ProjektojTaskoTipoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = ProjektojTaskoTipo


# Форма статусов задач
class ProjektojTaskoStatusoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = ProjektojTaskoStatuso
        fields = [field.name for field in ProjektojTaskoStatuso._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Статус задач
@admin.register(ProjektojTaskoStatuso)
class ProjektojTaskoStatusoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ProjektojTaskoStatusoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = ProjektojTaskoStatuso


# Форма задач
class ProjektojTaskoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    kategorio = forms.ModelMultipleChoiceField(
        label=_('Kategorio de taskoj'),
        queryset=ProjektojTaskoKategorio.objects.filter(forigo=False, publikigo=True)
    )
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = ProjektojTasko
        fields = [field.name for field in ProjektojTasko._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Владелец задачи Универсо
class ProjektojTaskoPosedantoInline(admin.TabularInline):
    model = ProjektojTaskoPosedanto
    fk_name = 'tasko'
    extra = 1

# Задачи
@admin.register(ProjektojTasko)
class ProjektojTaskoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ProjektojTaskoFormo
    list_display = ('id','krea_dato','statuso','nomo_teksto','projekto','priskribo_teksto','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    inlines = (ProjektojTaskoPosedantoInline,)
    list_filter = ('sxablono_sistema',)
    class Meta:
        model = ProjektojTasko


# Форма типов владельцев задач
class ProjektojTaskoPosedantoTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = ProjektojTaskoPosedantoTipo
        fields = [field.name for field in ProjektojTaskoPosedantoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы владельцев задач
@admin.register(ProjektojTaskoPosedantoTipo)
class ProjektojTaskoPosedantoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ProjektojTaskoPosedantoTipoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = ProjektojTaskoPosedantoTipo


# Форма статусов владельцев задач
class ProjektojTaskoPosedantoStatusoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = ProjektojTaskoPosedantoStatuso
        fields = [field.name for field in ProjektojTaskoPosedantoStatuso._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Статусы владельцев задач
@admin.register(ProjektojTaskoPosedantoStatuso)
class ProjektojTaskoPosedantoStatusoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ProjektojTaskoPosedantoStatusoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = ProjektojTaskoPosedantoStatuso


# Форма владельцев задач
class ProjektojTaskoPosedantoFormo(forms.ModelForm):

    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = ProjektojTaskoPosedanto
        fields = [field.name for field in ProjektojTaskoPosedanto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Владельцы задач
@admin.register(ProjektojTaskoPosedanto)
class ProjektojTaskoPosedantoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ProjektojTaskoPosedantoFormo
    list_display = ('uuid','tasko','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema',)
    class Meta:
        model = ProjektojTaskoPosedanto


# Форма типов связей задач между собой
class ProjektojTaskoLigiloTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = ProjektojTaskoLigiloTipo
        fields = [field.name for field in ProjektojTaskoLigiloTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы связей задач между собой
@admin.register(ProjektojTaskoLigiloTipo)
class ProjektojTaskoLigiloTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ProjektojTaskoLigiloTipoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = ProjektojTaskoLigiloTipo


# Форма связей задач между собой
class ProjektojTaskoLigiloFormo(forms.ModelForm):
    
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = ProjektojTaskoLigilo
        fields = [field.name for field in ProjektojTaskoLigilo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Связи задач между собой
@admin.register(ProjektojTaskoLigilo)
class ProjektojTaskoLigiloAdmin(admin.ModelAdmin, TekstoMixin):
    form = ProjektojTaskoLigiloFormo
    list_display = ('posedanto','tipo','ligilo','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema',)
    class Meta:
        model = ProjektojTaskoLigilo
