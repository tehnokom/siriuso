"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import Permission
from django.db.models import Max, Q

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from universo_bazo.models import UniversoBazaMaks, Realeco
from main.models import Uzanto


# Категории уведомлений, использует абстрактный класс UniversoBazaMaks
class SciigoKategorio(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='sciigoj_kategorioj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'sciigoj_kategorioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kategorio de sciigoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kategorioj de sciigoj')
        # права
        permissions = (
            ('povas_vidi_sciigoj_kategorioj', _('Povas vidi kategorioj de sciigoj')),
            ('povas_krei_sciigoj_kategorioj', _('Povas krei kategorioj de sciigoj')),
            ('povas_forigi_sciigoj_kategorioj', _('Povas forigu kategorioj de sciigoj')),
            ('povas_sxangxi_sciigoj_kategorioj', _('Povas ŝanĝi kategorioj de sciigoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(SciigoKategorio, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                                   update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('sciigoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'sciigoj.povas_vidi_sciigoj_kategorioj',
                'sciigoj.povas_krei_sciigoj_kategorioj',
                'sciigoj.povas_forigi_sciigoj_kategorioj',
                'sciigoj.povas_sxangxi_sciigoj_kategorioj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='sciigoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('sciigoj.povas_vidi_sciigoj_kategorioj')
                    or user_obj.has_perm('sciigoj.povas_vidi_sciigoj_kategorioj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('sciigoj.povas_vidi_sciigoj_kategorioj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы уведомлений, использует абстрактный класс UniversoBazaMaks
class SciigoTipo(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='sciigoj_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'sciigoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de sciigoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de sciigoj')
        # права
        permissions = (
            ('povas_vidi_sciigoj_tipoj', _('Povas vidi tipoj de sciigoj')),
            ('povas_krei_sciigoj_tipoj', _('Povas krei tipoj de sciigoj')),
            ('povas_forigi_sciigoj_tipoj', _('Povas forigu tipoj de sciigoj')),
            ('povas_sxangxi_sciigoj_tipoj', _('Povas ŝanĝi tipoj de sciigoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(SciigoTipo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                              update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('sciigoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'sciigoj.povas_vidi_sciigoj_tipoj',
                'sciigoj.povas_krei_sciigoj_tipoj',
                'sciigoj.povas_forigi_sciigoj_tipoj',
                'sciigoj.povas_sxangxi_sciigoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='sciigoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('sciigoj.povas_vidi_sciigoj_tipoj')
                    or user_obj.has_perm('sciigoj.povas_vidi_sciigoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('sciigoj.povas_vidi_sciigoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Уведомления, использует абстрактный класс UniversoBazaMaks
class Sciigo(UniversoBazaMaks):

    # владелец (пользователь, для которого создана сущность)
    posedanto = models.ForeignKey(Uzanto, verbose_name=_('Posedanto'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # категория уведомлений
    kategorio = models.ManyToManyField(SciigoKategorio, verbose_name=_('Kategorio'),
                                       db_table='sciigoj_kategorioj_ligiloj')

    # тип уведомлений
    tipo = models.ForeignKey(SciigoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='sciigoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'sciigoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Sciigo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Sciigoj')
        # права
        permissions = (
            ('povas_vidi_sciigoj', _('Povas vidi sciigoj')),
            ('povas_krei_sciigoj', _('Povas krei sciigoj')),
            ('povas_forigi_sciigoj', _('Povas forigu sciigoj')),
            ('povas_sxangxi_sciigoj', _('Povas ŝanĝi sciigoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(Sciigo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                              update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('sciigoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'sciigoj.povas_vidi_sciigoj',
                'sciigoj.povas_krei_sciigoj',
                'sciigoj.povas_forigi_sciigoj',
                'sciigoj.povas_sxangxi_sciigoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='sciigoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('sciigoj.povas_vidi_sciigoj')
                    or user_obj.has_perm('sciigoj.povas_vidi_sciigoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('sciigoj.povas_vidi_sciigoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond
