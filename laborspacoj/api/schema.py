"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene  # сам Graphene
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from ..models import *  # модели приложения


# Модель категорий рабочих пространств
class LaborspacoKategorioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = LaborspacoKategorio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов рабочих пространств
class LaborspacoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = LaborspacoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов рабочих пространств
class LaborspacoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = LaborspacoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель владельцев рабочих пространств
class LaborspacoPosedantoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = LaborspacoPosedanto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'laborspaco__uuid': ['exact'],
            'posedanto_uzanto__id': ['exact'],
            'posedanto_organizo__uuid': ['exact'],
            'posedanto_komunumo__id': ['exact'],
            'posedanto_komunumo__uuid': ['exact'],
            'tipo__id': ['exact'],
            'statuso__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель владельцев кластеров
class KlasteroPosedantoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = KlasteroPosedanto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'klastero__uuid': ['exact'],
            'posedanto_uzanto__id': ['exact'],
            'posedanto_organizo__uuid': ['exact'],
            'statuso__id': ['exact'],
            'tipo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


class KlasteroLaborspacoLigiloNode(SiriusoAuthNode, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = KlasteroLaborspacoLigilo
        filter_fields = {
        }
        interfaces = (graphene.relay.Node,)


# Модель кластеров
# class KlasteroNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
class KlasteroNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    # владельцы
    posedanto =  SiriusoFilterConnectionField(KlasteroPosedantoNode,
        description=_('Выводит владельцев рабочих пространств'))

    class Meta:
        model = Klastero
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'statuso__id': ['exact','in'],
            'tipo__id': ['exact'],
            'kategorio__id': ['exact'],
            'laborspacoj_klasteroposedanto_klastero__posedanto_uzanto__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_posedanto(self, info, **kwargs):
        return KlasteroPosedanto.objects.filter(klastero=self, forigo=False, arkivo=False, 
            publikigo=True)


# Модель рабочих пространств
class LaborspacoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    # кластер
    klastero =  SiriusoFilterConnectionField(KlasteroNode,
        description=_('Выводит кластеры, привязанные к рабочему пространству'))

    # владельцы
    posedanto =  SiriusoFilterConnectionField(LaborspacoPosedantoNode,
        description=_('Выводит владельцев рабочих пространств'))

    class Meta:
        model = Laborspaco
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'tipo__id': ['exact'],
            'statuso__id': ['exact','in'],
            'laborspacoj_laborspacoposedanto_laborspaco__posedanto_komunumo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_klastero(self, info, **kwargs):
        return Klastero.objects.filter(laborspaco=self, forigo=False, arkivo=False, 
            publikigo=True)

    def resolve_posedanto(self, info, **kwargs):
        return LaborspacoPosedanto.objects.filter(laborspaco=self, forigo=False, arkivo=False, 
            publikigo=True)


# Модель типов владельцев рабочих пространств
class LaborspacoPosedantoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = LaborspacoPosedantoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов владельца рабочего пространства
class LaborspacoPosedantoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = LaborspacoPosedantoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов связей рабочих пространств между собой
class LaborspacoLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = LaborspacoLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель связей рабочих пространств между собой
class LaborspacoLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = LaborspacoLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'ligilo__uuid': ['exact'],
            'tipo__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель категорий кластеров
class KlasteroKategorioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = KlasteroKategorio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов кластеров
class KlasteroTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = KlasteroTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов кластеров
class KlasteroStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = KlasteroStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов владельцев кластеров
class KlasteroPosedantoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = KlasteroPosedantoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов владельца кластера
class KlasteroPosedantoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = KlasteroPosedantoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)
        

# Модель типов связей кластеров между собой
class KlasteroLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = KlasteroLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель связей кластеров между собой
class KlasteroLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = KlasteroLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'ligilo__uuid': ['exact'],
            'tipo__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


class LaborspacojQuery(graphene.ObjectType):
    laborspaco_kategorio = SiriusoFilterConnectionField(
        LaborspacoKategorioNode,
        description=_('Выводит все доступные модели категорий рабочих пространств')
    )
    laborspaco_tipo = SiriusoFilterConnectionField(
        LaborspacoTipoNode,
        description=_('Выводит все доступные модели типов рабочих пространств')
    )
    laborspaco_statuso = SiriusoFilterConnectionField(
        LaborspacoStatusoNode,
        description=_('Выводит все доступные модели статусов рабочих пространств')
    )
    laborspaco = SiriusoFilterConnectionField(
        LaborspacoNode,
        description=_('Выводит все доступные модели рабочих пространств')
    )
    laborspaco_posedantoj_tipo = SiriusoFilterConnectionField(
        LaborspacoPosedantoTipoNode,
        description=_('Выводит все доступные модели типов владельцев рабочих пространств')
    )
    laborspaco_posedantoj_statuso = SiriusoFilterConnectionField(
        LaborspacoPosedantoStatusoNode,
        description=_('Выводит все доступные модели статусов владельца рабочего пространства')
    )
    laborspaco_posedantoj = SiriusoFilterConnectionField(
        LaborspacoPosedantoNode,
        description=_('Выводит все доступные модели владельцев рабочих пространств')
    )
    laborspaco_ligilo_tipo = SiriusoFilterConnectionField(
        LaborspacoLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей рабочих пространств между собой')
    )
    laborspaco_ligilo = SiriusoFilterConnectionField(
        LaborspacoLigiloNode,
        description=_('Выводит все доступные модели связей рабочих пространств между собой')
    )
    laborspacoj_klastero_kategorio = SiriusoFilterConnectionField(
        KlasteroKategorioNode,
        description=_('Выводит все доступные модели категорий кластеров')
    )
    laborspacoj_klastero_tipo = SiriusoFilterConnectionField(
        KlasteroTipoNode,
        description=_('Выводит все доступные модели типов кластеров')
    )
    laborspacoj_klastero_statuso = SiriusoFilterConnectionField(
        KlasteroStatusoNode,
        description=_('Выводит все доступные модели статусов кластеров')
    )
    laborspacoj_klastero = SiriusoFilterConnectionField(
        KlasteroNode,
        description=_('Выводит все доступные модели кластеров')
    )
    laborspacoj_klastero_posedantoj_tipo = SiriusoFilterConnectionField(
        KlasteroPosedantoTipoNode,
        description=_('Выводит все доступные модели типов владельцев кластеров')
    )
    laborspacoj_klastero_posedantoj_statuso = SiriusoFilterConnectionField(
        KlasteroPosedantoStatusoNode,
        description=_('Выводит все доступные модели статусов владельцев кластеров')
    )
    laborspacoj_klastero_posedanto = SiriusoFilterConnectionField(
        KlasteroPosedantoNode,
        description=_('Выводит все доступные модели владельцев кластеров')
    )
    laborspacoj_klastero_ligilo_tipo = SiriusoFilterConnectionField(
        KlasteroLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей кластеров между собой')
    )
    laborspacoj_klastero_ligilo = SiriusoFilterConnectionField(
        KlasteroLigiloNode,
        description=_('Выводит все доступные модели связей кластеров между собой')
    )
