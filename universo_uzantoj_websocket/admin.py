"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django import forms

from .models import *


# Форма для пользователей на канале
class UniversoUzantojWebsocketFormo(forms.ModelForm):
    
    class Meta:
        model = UniversoUzantojWebsocket
        fields = [field.name for field in UniversoUzantojWebsocket._meta.fields if field.name not in ('krea_dato', 'uuid')]


# Чаты
@admin.register(UniversoUzantojWebsocket)
class UniversoUzantojWebsocketAdmin(admin.ModelAdmin):
    form = UniversoUzantojWebsocketFormo
    list_display = ('uuid','posedanto','online')
    list_filter = ('online','realeco')
    exclude = ('uuid',)

    class Meta:
        model = UniversoUzantojWebsocket

