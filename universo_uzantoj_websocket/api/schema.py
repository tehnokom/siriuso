"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene  # сам Graphene
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoPermissions  # Миксины для описания типов  Graphene
from ..models import *  # модели приложения


# Модель владельцев задач Универсо
class UniversoUzantojWebsocketNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = UniversoUzantojWebsocket
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


class UniversoUzantojWebsocketQuery(graphene.ObjectType):
    universo_uzantoj_websocket = SiriusoFilterConnectionField(
        UniversoUzantojWebsocketNode,
        description=_('Выводит все ранее зарегестрированные соединения по вебсокету')
    )

