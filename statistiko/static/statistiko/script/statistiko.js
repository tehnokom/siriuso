$(document).ready(function () {
    let datetime_format = "DD.MM.YYYY HH:mm";

    $('#komenco, #fino').datetimepicker({
            locale: 'ru',
            format: datetime_format
        });

    $('#accept').on('click', function (e) {
        let komenco = moment($('#komenco').val(), datetime_format);
        let fino = moment($('#fino').val(), datetime_format);
        let models = ['main.Uzanto', 'komunumoj.KomunumojSociaprojekto', 'komunumoj.KomunumojSoveto',
            'komunumoj.KomunumojGrupo', 'komunumoj.KomunumojOrganizo', 'muroj.MurojUzantoEnskribo',
            'muroj.MurojSociaprojektoEnskribo', 'muroj.MurojSovetoEnskribo', 'muroj.MurojGrupoEnskribo',
            'muroj.MurojOrganizoEnskribo'];
        let kom_tuta = 0;
        let kom_gajno = 0;
        let kom_procento = 0.0;
        let enskribo_tuta = 0;
        let enskribo_gajno = 0;
        let enskribo_procento = 0.0;

        for(let i = 0; i<models.length; ++i) {
            let model = models[i];
            $.ajax({
                url: '/api/v1.1/',
                type: 'POST',
                async: false,
                cache: false,
                dataType: 'json',
                processData: true,
                data: {
                    csrfmiddlewaretoken: getCookie('csrftoken'),
                    query: 'query ($modelo: String!, $komenco: DateTime, $fino: DateTime){' +
                    'statistiko(modelo: $modelo, komenco: $komenco, fino: $fino){' +
                    'tuta, modelo, komencoPeriodo, finoPeriodo, procento, gajno} }',
                    variables: JSON.stringify({
                        modelo: model,
                        komenco: komenco.isValid() ? komenco.format() : null,
                        fino: fino.isValid() ? fino.format() : null
                    })
                }
            })
                .done(function(response){
                    switch (response.data.statistiko.modelo) {
                        case 'main.Uzanto':
                            $('#uzantoj_tuta').text(response.data.statistiko.tuta);
                            $('#uzantoj_gajno').text(response.data.statistiko.gajno);
                            $('#uzantoj_procento').text(response.data.statistiko.procento.toFixed(2) + '%');
                            break;
                        case 'komunumoj.KomunumojSociaprojekto':
                        case 'komunumoj.KomunumojSoveto':
                        case 'komunumoj.KomunumojGrupo':
                        case 'komunumoj.KomunumojOrganizo':
                            kom_tuta += parseInt(response.data.statistiko.tuta);
                            kom_gajno += parseInt(response.data.statistiko.gajno);
                            break;
                        case 'muroj.MurojUzantoEnskribo':
                        case 'muroj.MurojSociaprojektoEnskribo':
                        case 'muroj.MurojSovetoEnskribo':
                        case 'muroj.MurojGrupoEnskribo':
                        case 'muroj.MurojOrganizoEnskribo':
                            enskribo_tuta += parseInt(response.data.statistiko.tuta);
                            enskribo_gajno += parseInt(response.data.statistiko.gajno);
                            break;
                        default:
                            break;
                    }
                })
                .fail(function (jqXHR, textStatus) {
                    console.log(textStatus);
                });
        }

        kom_procento = 100 * kom_gajno / (kom_tuta - kom_gajno);
        kom_procento = isFinite(kom_procento) && !isNaN(kom_procento) ? kom_procento : 100.00;

        enskribo_procento = 100 * enskribo_gajno / (enskribo_tuta - enskribo_gajno);
        enskribo_procento = isFinite(enskribo_procento) && !isNaN(enskribo_procento) ? enskribo_procento : 100.0;

        $('#komunumoj_tuta').text(kom_tuta);
        $('#komunumoj_gajno').text(kom_gajno);
        $('#komunumoj_procento').text(kom_procento.toFixed(2) + '%');
        $('#enskriboj_tuta').text(enskribo_tuta);
        $('#enskriboj_gajno').text(enskribo_gajno);
        $('#enskriboj_procento').text(enskribo_procento.toFixed(2) + '%');
    }).trigger('click');

    $('#reset').on('click', function (e) {
       $('#komenco, #fino').val('');
       $('#accept').trigger('click');
    });
});