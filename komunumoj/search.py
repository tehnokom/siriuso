"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, Text, Date, Nested, Object, Search
# from django.db.models.signals import post_save, post_delete
# from django.dispatch import receiver

# connections.create_connection(hosts='elasticsearch', timeout=20)


# @receiver(post_save, sender=KomunumojSociaprojekto)
# def index_create(sender, instance, **kwargs):
#     instance.indexing()

# @receiver(post_delete, sender=models.KomunumojSociaprojekto)
# def index_delete(sender, instance, **kwargs):
#     instance.es_model.delete()

# @receiver(post_migrate, sender=models.KomunumojSociaprojekto)
# def index_migrate(sender, instance, **kwargs):
#     KomunumojSociaprojektoIndex.init()


class KomunumojSociaprojektoIndex(DocType):
    nomo = Object()
    priskribo = Object()

    class Meta:
        index = 'komunumoj-sociaprojekto-index'
