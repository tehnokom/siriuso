"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db.models import F, Q, Count
from komunumoj.models import *
from itertools import chain

def context_organizo_statistikoj(request, **kwargs):
    context = {}

    if 'organizo_id' in kwargs or 'kom_id' in kwargs:
        org_id = kwargs.get('organizo_id') or kwargs.get('kom_id')
        organizo = KomunumojOrganizo.objects\
            .values('uuid', 'tipo__kodo', 'nomo__enhavo', 'priskribo__enhavo', 'id', 'komunumojorganizoavataro__bildo',
                    'komunumojorganizoavataro__bildo_min', 'komunumojorganizokovrilo__bildo')\
            .filter(id=org_id, forigo=False)

        if organizo.count():
            organizo = organizo[0]
            queryset_total = KomunumojOrganizoMembro.objects.values('posedanto__id')

            total = queryset_total.filter(posedanto__id=org_id, forigo=False, autoro__konfirmita=True,
                                          autoro__is_superuser=False).aggregate(total=Count('posedanto__id'))['total']

            procentoj = 100 * total / 100000

            if not procentoj:
                procentoj = 1
            elif procentoj < 2:
                procentoj = 2
            else:
                procentoj = "%.2f" % (procentoj)

            context['organizo'] = organizo
            context['statistikoj'] = {'tuta': total,
                                      'procentoj': procentoj,}

            if request.user.is_authenticated:
                try:
                    uzanto_aligis = (KomunumojOrganizoMembro.objects
                                     .get(autoro_id=request.user.id, posedanto__id=org_id, forigo=False))
                    uzanto_aligis = [aligis.kodo for aligis in uzanto_aligis.muro_sciigo.all()]
                except KomunumojOrganizoMembro.DoesNotExist:
                    uzanto_aligis = False

                context['statistikoj']['uzanto_aligis'] = uzanto_aligis

                # Временное!!!
                # Определяем права пользователя для сообщества
                try:
                    uzanto_aliro = KomunumojOrganizoMembro.objects \
                        .values('tipo__kodo') \
                        .get(autoro_id=request.user.id, posedanto_id=organizo['uuid'], forigo=False)
                    context['uzanto_aliro'] = uzanto_aliro['tipo__kodo']
                    context['tipoj_membroj'] = ['membro', 'membro-adm', 'membro-mod']
                    context['adm_membroj'] = ['membro-adm', 'membro-mod', 'administranto', 'moderiganto']
                except KomunumojOrganizoMembro.DoesNotExist:
                    pass

    return context


def context_organizo_kohereco(request, **kwargs):
    context = {}

    if 'organizo_id' in kwargs:
        def make_query(obj, nomo_enhavo, avataro_bildo_min, filter):
            return (obj.objects
                    .annotate(nomo__enhavo=nomo_enhavo,
                              avataro_bildo_min=avataro_bildo_min)
                    .filter(filter, forigo=False))

        projektoj_dict = {
            'nomo_enhavo': F('nomo__enhavo'),
            'avataro_bildo_min': F('komunumojsociaprojektoavataro__bildo_min'),
            'filter': Q(
                komunumoj_komunumojkohereco_kohera_sociaprojekto__posedanto_organizo__id=kwargs['organizo_id'])
        }
        projektoj = make_query(KomunumojSociaprojekto, **projektoj_dict)

        sovetoj_dict = {
            'nomo_enhavo': F('nomo__enhavo'),
            'avataro_bildo_min': F('komunumojsovetoavataro__bildo_min'),
            'filter': Q(komunumoj_komunumojkohereco_kohera_soveto__posedanto_organizo__id=kwargs['organizo_id'])
        }
        sovetoj = make_query(KomunumojSoveto, **sovetoj_dict)

        organizo_dict = {
            'nomo_enhavo': F('nomo__enhavo'),
            'avataro_bildo_min': F('komunumojorganizoavataro__bildo_min'),
            'filter': Q(komunumoj_komunumojkohereco_kohera_organizo__posedanto_organizo__id=kwargs['organizo_id'])
        }
        organizoj = make_query(KomunumojOrganizo, **organizo_dict)

        grupoj_dict = {
            'nomo_enhavo': F('nomo__enhavo'),
            'avataro_bildo_min': F('komunumojgrupoavataro__bildo_min'),
            'filter': Q(komunumoj_komunumojkohereco_kohera_grupo__posedanto_organizo__id=kwargs['organizo_id'])
        }
        grupoj = make_query(KomunumojGrupo, **grupoj_dict)

        context['komunumoj'] = list(chain(sovetoj, organizoj, projektoj, grupoj))

    return context