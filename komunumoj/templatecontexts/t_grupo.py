"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""


from .grupo import context_grupo_statistikoj


def context_mobile_grupo_statistikoj(request, **kwargs):
    return context_grupo_statistikoj(request, **kwargs)