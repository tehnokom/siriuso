"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db.models import F, Q, Count
from komunumoj.models import *
from itertools import chain


def context_soveto_statistikoj(request, **kwargs):
    context = {}

    if 'soveto_id' in kwargs or 'kom_id' in kwargs:
        sov_id = kwargs.get('soveto_id') or kwargs.get('kom_id')
        soveto = KomunumojSoveto.objects\
            .values('uuid', 'nomo__enhavo', 'priskribo__enhavo', 'id', 'lando__nomo__enhavo', 'regiono__nomo__enhavo',
                    'lando__kodo', 'regiono__kodo', 'komunumojsovetoavataro__bildo',
                    'komunumojsovetoavataro__bildo_min', 'komunumojsovetokovrilo__bildo')\
            .filter(id=sov_id, forigo=False)

        if soveto.count():
            soveto = soveto[0]
            queryset_total = KomunumojSovetoMembro.objects.values('posedanto__id')

            total = queryset_total.filter(posedanto__id=sov_id, forigo=False, autoro__konfirmita=True,
                                          autoro__is_superuser=False).aggregate(total=Count('posedanto__id'))['total']

            procentoj = 100 * total / 100000

            if not procentoj:
                procentoj = 1
            elif procentoj < 2:
                procentoj = 2
            else:
                procentoj = "%.2f" % (procentoj)

            context['soveto'] = soveto
            context['statistikoj'] = {'tuta': total,
                                      'procentoj': procentoj,}

            if request.user.is_authenticated:
                try:
                    uzanto_aligis = (KomunumojSovetoMembro.objects
                                     .get(autoro_id=request.user.id, posedanto__id=sov_id, forigo=False))
                    uzanto_aligis = [aligis.kodo for aligis in uzanto_aligis.muro_sciigo.all()]
                except KomunumojSovetoMembro.DoesNotExist:
                    uzanto_aligis = False

                context['statistikoj']['uzanto_aligis'] = uzanto_aligis

                # Временное!!!
                # Определяем права пользователя для сообщества
                try:
                    uzanto_aliro = KomunumojSovetoMembro.objects \
                        .values('tipo__kodo') \
                        .get(autoro_id=request.user.id, posedanto_id=soveto['uuid'], forigo=False)
                    context['uzanto_aliro'] = uzanto_aliro['tipo__kodo']
                    context['tipoj_membroj'] = ['membro', 'membro-adm', 'membro-mod']
                    context['adm_membroj'] = ['membro-adm', 'membro-mod', 'administranto', 'moderiganto']
                except KomunumojSovetoMembro.DoesNotExist:
                    pass

    return context


def context_soveto_kohereco(request, **kwargs):
    context = {}

    if 'soveto_id' in kwargs:
        def make_query(obj, nomo_enhavo, avataro_bildo_min, filter):
            return (obj.objects
                    .annotate(nomo__enhavo=nomo_enhavo,
                              avataro_bildo_min=avataro_bildo_min)
                    .filter(filter, forigo=False))

        projektoj_dict = {
            'nomo_enhavo': F('nomo__enhavo'),
            'avataro_bildo_min': F('komunumojsociaprojektoavataro__bildo_min'),
            'filter': Q(
                komunumoj_komunumojkohereco_kohera_sociaprojekto__posedanto_soveto__id=kwargs['soveto_id'])
        }
        projektoj = make_query(KomunumojSociaprojekto, **projektoj_dict)

        sovetoj_dict = {
            'nomo_enhavo': F('nomo__enhavo'),
            'avataro_bildo_min': F('komunumojsovetoavataro__bildo_min'),
            'filter': Q(komunumoj_komunumojkohereco_kohera_soveto__posedanto_soveto__id=kwargs['soveto_id'])
        }
        sovetoj = make_query(KomunumojSoveto, **sovetoj_dict)

        organizo_dict = {
            'nomo_enhavo': F('nomo__enhavo'),
            'avataro_bildo_min': F('komunumojorganizoavataro__bildo_min'),
            'filter': Q(komunumoj_komunumojkohereco_kohera_organizo__posedanto_soveto__id=kwargs['soveto_id'])
        }
        organizoj = make_query(KomunumojOrganizo, **organizo_dict)

        grupoj_dict = {
            'nomo_enhavo': F('nomo__enhavo'),
            'avataro_bildo_min': F('komunumojgrupoavataro__bildo_min'),
            'filter': Q(komunumoj_komunumojkohereco_kohera_grupo__posedanto_soveto__id=kwargs['soveto_id'])
        }
        grupoj = make_query(KomunumojGrupo, **grupoj_dict)

        context['komunumoj'] = list(chain(projektoj, sovetoj, organizoj, grupoj))

    return context