"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""


from .sociaprojekto import context_projekto_statistikoj


def context_mobile_projekto_statistikoj(request, **kwargs):
    return context_projekto_statistikoj(request, **kwargs)