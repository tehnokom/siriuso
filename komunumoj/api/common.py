"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from komunumoj import models


__komunumoj_models = (models.Komunumo, )

def get_komunumo(uuid):
    """
    Принимает uuid сообщества, возвращает кортеж (tuple),
    где первый объект - найденное сообщество, а второй -
    текстовый индентификатор типа сообщества.
    Если сообщество не найдено, то возвращает None
    """
    komunumo = None
    kom_tipo = None

    for kom in __komunumoj_models:
        try:
            komunumo = kom.objects.get(uuid=uuid)
            kom_tipo = kom._meta.model_name
        except kom.DoesNotExist:
            pass
        if komunumo:
            break

    return (komunumo, kom_tipo) if komunumo else None


def get_komunumo_by_id(id, kom_tipo):
    """
    Принимает id сообщества, возвращает найденное сообщество.
    Если сообщество не найдено, то возвращает None
    """
    komunumo = None
    kom_tipo = None

    for kom in __komunumoj_models:
        if kom_tipo == kom._meta.model_name:
            try:
                komunumo = kom.objects.get(id=id)
            except kom.DoesNotExist:
                pass
            break

    return komunumo
