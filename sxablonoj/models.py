"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from django.db import models
from django.db.models import Max, Q
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import Permission

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from universo_bazo.models import UniversoBazaMaks
from rajtoj.models import RajtoRajtigo
from organizoj.models import OrganizoMembro
from objektoj.models import Objekto
from dokumentoj.models import Dokumento
from mono.models import MonoKonto, MonoFakturo
from taskoj.models import TaskojProjekto
from scipovoj.models import ScipovoObjekto
from sciigoj.models import Sciigo
from main.models import Uzanto


# Типы глобальных шаблонов, использует абстрактный класс UniversoBazaMaks
class SxablonoTipo(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'sxablonoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de ŝablonoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de ŝablonoj')
        # права
        permissions = (
            ('povas_vidi_sxablonoj_tipoj', _('Povas vidi tipoj de ŝablonoj')),
            ('povas_krei_sxablonoj_tipoj', _('Povas krei tipoj de ŝablonoj')),
            ('povas_forigi_sxablonoj_tipoj', _('Povas forigu tipoj de ŝablonoj')),
            ('povas_shanghi_sxablonoj_tipoj', _('Povas ŝanĝi tipoj de ŝablonoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(SxablonoTipo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                              update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('sxablonoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'sxablonoj.povas_vidi_sxablonoj_tipoj',
                'sxablonoj.povas_krei_sxablonoj_tipoj',
                'sxablonoj.povas_forigi_sxablonoj_tipoj',
                'sxablonoj.povas_shanghi_sxablonoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='sxablonoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('sxablonoj.povas_vidi_sxablonoj_tipoj')
                    or user_obj.has_perm('sxablonoj.povas_vidi_sxablonoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('sxablonoj.povas_vidi_sxablonoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Глобальные шаблоны, использует абстрактный класс UniversoBazaMaks
class Sxablono(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # тип глобальных шаблонов
    tipo = models.ForeignKey(SxablonoTipo, verbose_name=_('Tipo'), default=None,
                             on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # связанные шаблоны прав
    rajto = models.ManyToManyField(RajtoRajtigo, verbose_name=_('Ŝablonoj de rajtoj'), blank=True,
                                   db_table='sxablonoj_rajtoj_sxablonoj_ligiloj',
                                   limit_choices_to={'sxablono_sistema': True})

    # связанные шаблоны членства в организациях
    organizo = models.ManyToManyField(OrganizoMembro, verbose_name=_('Ŝablonoj de membroj de organizoj'),
                                      blank=True, db_table='sxablonoj_organizoj_sxablonoj_ligiloj',
                                      limit_choices_to={'sxablono_sistema': True})

    # связанные шаблоны объектов
    objekto = models.ManyToManyField(Objekto, verbose_name=_('Ŝablonoj de objektoj'), blank=True,
                                     db_table='sxablonoj_objectoj_sxablonoj_ligiloj',
                                     limit_choices_to={'sxablono_sistema': True})

    # связанные шаблоны документов
    dokumento = models.ManyToManyField(Dokumento, verbose_name=_('Ŝablonoj de dokumentoj'), blank=True,
                                       db_table='sxablonoj_dokumentoj_sxablonoj_ligiloj',
                                       limit_choices_to={'sxablono_sistema': True})

    # связанные шаблоны счетов (кошельков)
    konto = models.ManyToManyField(MonoKonto, verbose_name=_('Ŝablonoj de kontoj'), blank=True,
                                   db_table='sxablonoj_kontoj_sxablonoj_ligiloj',
                                   limit_choices_to={'sxablono_sistema': True})

    # связанные шаблоны инвойсов
    fakturo = models.ManyToManyField(MonoFakturo, verbose_name=_('Ŝablonoj de fakturoj'), blank=True,
                                     db_table='sxablonoj_fakturoj_sxablonoj_ligiloj',
                                     limit_choices_to={'sxablono_sistema': True})

    # связанные шаблоны проектов
    projekto = models.ManyToManyField(TaskojProjekto, verbose_name=_('Ŝablonoj de projektoj'), blank=True,
                                      db_table='sxablonoj_projektoj_sxablonoj_ligiloj',
                                      limit_choices_to={'sxablono_sistema': True})

    # связанные шаблоны навыков
    scipovo = models.ManyToManyField(ScipovoObjekto, verbose_name=_('Ŝablonoj de scipovoj'), blank=True,
                                     db_table='sxablonoj_scipovoj_sxablonoj_ligiloj',
                                     limit_choices_to={'sxablono_sistema': True})

    # связанные шаблоны уведомлений
    sciigo = models.ManyToManyField(Sciigo, verbose_name=_('Ŝablonoj de sciigoj'), blank=True,
                                    db_table='sxablonoj_sciigoj_sxablonoj_ligiloj',
                                    limit_choices_to={'sxablono_sistema': True})

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'sxablonoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ŝablono')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ŝablono')
        # права
        permissions = (
            ('povas_vidi_sxablonoj', _('Povas vidi ŝablonoj')),
            ('povas_krei_sxablonoj', _('Povas krei ŝablonoj')),
            ('povas_forigi_sxablonoj', _('Povas forigi ŝablonoj')),
            ('povas_shangxi_sxablonoj', _('Povas ŝanĝi ŝablonoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(Sxablono, self).save(force_insert=force_insert, force_update=force_update,
                                           using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('sxablonoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'sxablonoj.povas_vidi_sxablonoj',
                'sxablonoj.povas_krei_sxablonoj',
                'sxablonoj.povas_forigi_sxablonoj',
                'sxablonoj.povas_shangxi_sxablonoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='sxablonoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('sxablonoj.povas_vidi_sxablonoj')
                    or user_obj.has_perm('sxablonoj.povas_vidi_sxablonoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('sxablonoj.povas_vidi_sxablonoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond
