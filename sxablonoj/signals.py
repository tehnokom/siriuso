"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from copy import deepcopy

from profiloj.models import Profilo
from .models import *


def get_related_managers(self):
    managers = [
        f for f in self._meta.get_fields()
    ]
    return managers

"""
modelo - шаблон объекта (модели), копию которой будем делать
foreign - запись модели, на которую будет ссылаться созданная
modelo_key - имя поля ссылки
shablono - список всех объектов шаблонов
modeloj - список моделей, на которые заменены шаблоны
"""
def create_model(modelo, foreign, modelo_key, posedanto_uzanto, shablono, modeloj,thisback=True):
    modelo_old = deepcopy(modelo)
    shablono.append(modelo_old)
    uuid = modelo_old.uuid
    modelo.uuid = None
    modelo.pk = None
    modelo.publikiga_dato = timezone.now()
    modelo.sxablono_sistema = False
    modelo.sxablono_sistema_id = None
    modelo.sxablono_sistema_priskribo = default_lingvo
    if thisback:
        setattr(modelo,modelo_key,foreign)
    if hasattr(modelo,'posedanto_uzanto'):
        modelo.posedanto_uzanto=posedanto_uzanto
    if hasattr(modelo,'autoro'):
        modelo.autoro=posedanto_uzanto        
    modelo.save()
    modeloj.append(modelo)
    for mas in get_related_managers(modelo):
        if not mas:
            pass
        elif (mas.many_to_one or mas.one_to_one): # указатель на анализируемую далее таблицу по прямой связи
            kwarg = {mas.remote_field.name: modelo_old,
                'forigo':False,
                'arkivo':False,
                'publikigo':True}
            forei = mas.related_model.objects.filter(**kwarg)
            # если массив пустой, то 
            for f in forei:
                if f in shablono:# если по прямой ссылке стоит шаблон, который у нас уже заменён на модель, мы должны поменять указатель на нашу модель
                    setattr(modelo,mas.name,modeloj[shablono.index(f)])
                elif hasattr(f,'sxablono_sistema'):
                    if f.sxablono_sistema:# если это шаблон, то нужно создать копию с указанием на новый объект
                        setattr(modelo,mas.name,create_model(f,modelo,mas.name,posedanto_uzanto,shablono,modeloj,False))
        elif mas.one_to_many: # указатель на базу, исследование по обратной связи
            kwarg = {mas.remote_field.name: modelo_old,
                'forigo':False,
                'arkivo':False,
                'publikigo':True}
            forei = mas.related_model.objects.filter(**kwarg)
            for f in forei:
                if f in shablono:
                    pass
                elif f.sxablono_sistema:# если это шаблон, то нужно создать копию с указанием на новый объект
                    create_model(f,modelo,mas.remote_field.name,posedanto_uzanto,shablono,modeloj)  
    modelo.save()
    return modelo


@receiver(post_save, sender=Profilo)
def create_sxablonoj(sender, instance, **kwargs):
    """Создаём объект на основании шаблонов"""
    if kwargs.get('created', True):# проверяем, новый ли это пользователь
        try:
            shablono = Sxablono.objects.get(id=1, forigo=False, #id=1 - Для новых пользователей
                                                    arkivo=False, publikigo=True)

            # идём по спискам шаблонов объектов
            for obj in shablono.objekto.all():
                shablono = []
                modeloj = []
                create_model(obj,obj,'',instance,shablono,modeloj,False)

            # без нового запроса далее работать не хочет ???
            shablono = Sxablono.objects.get(id=1, forigo=False, #id=1 - Для новых пользователей
                                                    arkivo=False, publikigo=True)

            # идём по спискам шаблонов счетов
            for obj in shablono.konto.all():
                shablono = []
                modeloj = []
                create_model(obj,obj,'',instance,shablono,modeloj,False)

            # без нового запроса далее работать не хочет ???
            shablono = Sxablono.objects.get(id=1, forigo=False, #id=1 - Для новых пользователей
                                                    arkivo=False, publikigo=True)

            # идём по спискам шаблонов проектов
            for obj in shablono.projekto.all():
                shablono = []
                modeloj = []
                create_model(obj,obj,'',instance,shablono,modeloj,False)

        except Sxablono.DoesNotExist:
            message = '{} "{}"'.format(_('Неверный Шаблоны пользователей Универсо'),'Sxablono')
