"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from django.contrib.auth.models import User
from django.contrib.gis.gdal import DataSource
from geo.models import PlanetOsmPolygon
from django.contrib.gis.db.models.functions import AsGeoJSON
from django.core.serializers import serialize, deserialize
from rest_framework import serializers
from django.http import JsonResponse, HttpResponse
import json

class RegionShapeSerializer(serializers.Serializer):
    json = serializers.JSONField()


class RegionShape(APIView):
    def get(self, request, format=None):
        border = (PlanetOsmPolygon.objects.using('osm')
                  .filter(tags__contains={'ISO3166-2': 'RU-KGD'})
                  .annotate(json=AsGeoJSON('way'))
                  )

        feature_collection = serialize('geojson', border,
                                       geometry_field='way',
                                       fields=('name',))

        # print(feature_collection)
        # serializer = RegionShapeSerializer(border, many=True)
        # print(serializer.data)
        return Response(json.loads(feature_collection))
