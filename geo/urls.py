"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.urls import path, re_path, include
from geo.views import RegionShape

urlpatterns = [
    path('border', RegionShape.as_view()),
]