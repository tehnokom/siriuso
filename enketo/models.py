"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from django.db import models
from django.db.models import Q, Max
from django.utils.translation import gettext_lazy as _
from main.models import SiriusoBazaAbstrakta3, \
    SiriusoBazaAbstraktaKomunumoj, SiriusoTipoAbstrakta, Uzanto
from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, perms
from django.contrib.auth.models import Permission


# Анкета

# Типы Анкет, использует абстрактный класс SiriusoTipoAbstrakta
class EnketoTipo(SiriusoTipoAbstrakta):

    # многоязычное название в JSON формате
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'enketo_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de enketo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de enketo')
        # права
        permissions = (
            ('povas_vidi_tipon', _('Povas vidi tipon')),
            ('povas_krei_tipon', _('Povas krei tipon')),
            ('povas_forigi_tipon', _('Povas forigu tipon')),
            ('povas_shanghi_tipon', _('Povas ŝanĝi tipon')),
        )

    @staticmethod
    def _get_perm_cond(user_obj):
        return Q()


# Шаблон анкеты
class Enketo(SiriusoBazaAbstraktaKomunumoj):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # тип страниц Анкеты
    tipo = models.ForeignKey(EnketoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'enketo_enketo'
        # читабельное название модели, в единственном числе
        verbose_name = _('Enketo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Enketo')
        # права
        permissions = (
            ('povas_vidi_enketo', _('Povas vidi enketo')),
            ('povas_krei_enketo', _('Povas krei enketo')),
            ('povas_forigi_enketo', _('Povas forigu enketo')),
            ('povas_shanghi_enketo', _('Povas ŝanĝi enketo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле id этой модели
        return '{}'.format(self.id)

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(Enketo, self).save(force_insert=force_insert, force_update=force_update,
                                                  using=using, update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if self.autoro == user_obj:
                all_perms = set(perms.user_registrita_perms(apps=('enketo',)))
            else:
                all_perms = set()

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'enketo.povas_vidi_enketo', 'enketo.povas_krei_enketo',
                'enketo.povas_forigi_enketo', 'enketo.povas_shanghi_enketo'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='enketo', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('enketo.povas_vidi_enketo')
                    or user_obj.has_perm('enketo.povas_vidi_enketo')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('enketo.povas_vidi_enketo'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


def datumo_default():
    # return {'datumo': None}
    return None

# Поля анкеты
class Informoj(SiriusoBazaAbstrakta3):
    tipo_choices = (
        ('int', _('Integer')),
        ('bool', _('Boolean')),
        ('str', _('String')),
        ('date', _('Date and Time')),
        ('dec', _('Decimal')),
        ('interval', _('interval or Duration')),
        ('email', _('Email')),
        ('float', _('Float'))
    )

    tipo_choices_nomo = (
        'int',
        'bool',
        'str',
        'date',
        'dec',
        'interval',
        'email',
        'float',
    )

    # Анкета
    enketo = models.ForeignKey(Enketo, verbose_name=_('Enketo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # ID записи для вывода в разрезе анкеты
    id = models.IntegerField(_('ID'), default=0)

    # название
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                encoder=CallableEncoder)

    # тип данного поля
    tipo = models.CharField(_('Tipo'), max_length=12, choices=tipo_choices, blank=True,
                                   null=False, default='bool')

    # значение поля по умолчанию в JSON формате - храняться сами данные согласно типа поля
    datumo = models.JSONField(verbose_name=_('Datumo'), blank=True, null=True, default=datumo_default,
                                encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'enketo_informoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Informoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Informoj')
        # права
        permissions = (
            ('povas_vidi_enketo_informoj', _('Povas vidi informoj de enketo')),
            ('povas_krei_enketo_informoj', _('Povas krei informoj de enketo')),
            ('povas_forigi_enketo_informoj', _('Povas forigu informoj de enketo')),
            ('povas_shanghi_enketo_informoj', _('Povas ŝanĝi informoj de enketo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле id этой модели
        return '{}::{}'.format(self.enketo.id, self.tipo)

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.filter(enketo=self.enketo).aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(Informoj, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                        update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if self.autoro == user_obj:
                all_perms = set(perms.user_registrita_perms(apps=('enketo',)))
            else:
                all_perms = set()

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'enketo.povas_vidi_enketo_informoj', 'enketo.povas_krei_enketo_informoj',
                'enketo.povas_forigi_enketo_informoj', 'enketo.povas_shanghi_enketo_informoj'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='enketo', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('enketo.povas_vidi_enketo_informoj')
                    or user_obj.has_perm('enketo.povas_vidi_enketo_informoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('enketo.povas_vidi_enketo_informoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Заполненные анкеты
class Enketi(SiriusoBazaAbstrakta3):

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None, on_delete=models.CASCADE)

    # Анкета
    enketo = models.ForeignKey(Enketo, verbose_name=_('Enketo'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'enketo_enketi'
        # читабельное название модели, в единственном числе
        verbose_name = _('Enketi')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Enketi')
        # права
        permissions = (
            ('povas_vidi_enketi', _('Povas vidi enketi')),
            ('povas_krei_enketi', _('Povas krei enketi')),
            ('povas_forigi_enketi', _('Povas forigu enketi')),
            ('povas_shanghi_enketi', _('Povas ŝanĝi enketi')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле id этой модели
        return '{}'.format(self.uuid)

    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if self.autoro == user_obj:
                all_perms = set(perms.user_registrita_perms(apps=('enketo',)))
            else:
                all_perms = set()

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'enketo.povas_vidi_enketi', 'enketo.povas_krei_enketi',
                'enketo.povas_forigi_enketi', 'enketo.povas_shanghi_enketi'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='enketo', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('enketo.povas_vidi_enketi')
                    or user_obj.has_perm('enketo.povas_vidi_enketi')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('enketo.povas_vidi_enketi'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Поля заполненных анкет
class EnketiInformoj(SiriusoBazaAbstrakta3):

    # Заполняемая анкета
    enketi = models.ForeignKey(Enketi, verbose_name=_('Enketi'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # Поле анкеты
    informoj = models.ForeignKey(Informoj, verbose_name=_('Informoj'), blank=False, default=None, on_delete=models.CASCADE)

    # значение поля в JSON формате
    datumo = models.JSONField(verbose_name=_('Datumo'), blank=True, null=False, default=datumo_default,
                                encoder=CallableEncoder)
    # {datumo: ""}

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'enketo_enketi_informoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Enketi informo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Enketi informoj')
        # права
        permissions = (
            ('povas_vidi_enketi_informoj', _('Povas vidi enketi informoj')),
            ('povas_krei_enketi_informoj', _('Povas krei enketi informoj')),
            ('povas_forigi_enketi_informoj', _('Povas forigu enketi informoj')),
            ('povas_shanghi_enketi_informoj', _('Povas ŝanĝi enketi informoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле id этой модели
        return '{}'.format(self.uuid)

    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if self.autoro == user_obj:
                all_perms = set(perms.user_registrita_perms(apps=('enketo',)))
            else:
                all_perms = set()

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'enketo.povas_vidi_enketi_informoj', 'enketo.povas_krei_enketi_informoj',
                'enketo.povas_forigi_enketi_informoj', 'enketo.povas_shanghi_enketi_informoj'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='enketo', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('enketo.povas_vidi_enketi_informoj')
                    or user_obj.has_perm('enketo.povas_vidi_enketi_informoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('enketo.povas_vidi_enketi_informoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


