"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from django import forms
from django.utils.translation import gettext_lazy as _
import json

from .models import *
from siriuso.forms.widgets import LingvoInputWidget
from siriuso.utils.admin_mixins import TekstoMixin


# Форма для многоязычных названий типов Анкет
class EnketoTipoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=True)

    class Meta:
        model = EnketoTipo
        fields = [field.name for field in EnketoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы Анкет
@admin.register(EnketoTipo)
class EnketoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = EnketoTipoFormo
    list_display = ('nomo_teksto', 'kodo', 'uuid')
    exclude = ('nomo_teksto',)

    class Meta:
        model = EnketoTipo


# Форма для контента Анкет
class EnketoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=True)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=True)

    class Meta:
        model = Enketo
        fields = [field.name for field in Enketo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Поля анкеты для анкеты
class InformojInline(admin.TabularInline):
    model = Informoj
    fk_name = 'enketo'
    extra = 1


# Страницы Анкет
@admin.register(Enketo)
class EnketoAdmin(admin.ModelAdmin, TekstoMixin):
    form = EnketoFormo
    list_display = ('id', 'nomo_teksto')
    exclude = ('id',)
    inlines = (InformojInline, )

    class Meta:
        model = Enketo


# Форма для полей Анкет
class InformojFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=True)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=True)

    class Meta:
        model = Informoj
        fields = [field.name for field in Informoj._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Страницы полей Анкет
@admin.register(Informoj)
class InformojAdmin(admin.ModelAdmin, TekstoMixin):
    form = InformojFormo
    list_display = ('id', 'enketo', 'nomo_teksto')
    exclude = ('id',)

    class Meta:
        model = Informoj


# Форма для заполненных Анкет
class EnketiFormo(forms.ModelForm):

    class Meta:
        model = Enketi
        fields = [field.name for field in Enketi._meta.fields if field.name not in ('krea_dato', 'uuid')]


# Поля анкеты для анкеты
class EnketiInformojInline(admin.TabularInline):
    model = EnketiInformoj
    fk_name = 'enketi'
    extra = 1


# Страницы заполненных Анкет
@admin.register(Enketi)
class EnketiAdmin(admin.ModelAdmin, TekstoMixin):
    form = EnketiFormo
    list_display = ('uuid', 'enketo', 'autoro')
    exclude = ('uuid',)
    inlines = (EnketiInformojInline, )

    class Meta:
        model = Enketi


# Форма для заполненных Анкет
class EnketiInformojFormo(forms.ModelForm):

    class Meta:
        model = EnketiInformoj
        fields = [field.name for field in EnketiInformoj._meta.fields if field.name not in ('krea_dato', 'uuid')]


# Страницы заполненных Анкет
@admin.register(EnketiInformoj)
class EnketiInformojAdmin(admin.ModelAdmin, TekstoMixin):
    form = EnketiInformojFormo
    list_display = ('uuid', 'enketi', 'informoj')
    exclude = ('uuid',)

    class Meta:
        model = EnketiInformoj


