"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene  # сам Graphene
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from ..models import *  # модели приложения


# Модель связей категорий ресурсов между собой
class ResursoKategorioLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = ResursoKategorioLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__id': ['exact'],
            'ligilo__id': ['exact'],
            'tipo__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель категорий ресурсов
class ResursoKategorioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    # Связь категорий ресурсов между собой
    ligilo =  SiriusoFilterConnectionField(ResursoKategorioLigiloNode,
        description=_('Выводит связи категорий ресурсов между собой'))

    class Meta:
        model = ResursoKategorio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact'],
            'resursoj_resursokategorioligilo_ligilo': ['isnull'],
            'realeco__id':['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_ligilo(self, info, **kwargs):
        return ResursoKategorioLigilo.objects.filter(posedanto=self, forigo=False, arkivo=False, publikigo=True)


# Модель типов связей категорий ресурсов между собой
class ResursoKategorioLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ResursoKategorioLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов ресурсов
class ResursoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ResursoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель классов ресурсов
class ResursoKlasoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ResursoKlaso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов характеристик модификаций ресурсов
class ResursoModifoKarakterizadoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ResursoModifoKarakterizadoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель характеристик модификаций ресурсов
class ResursoModifoKarakterizadoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = ResursoModifoKarakterizado
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель модификации ресурсов
class ResursoModifoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    # Связь объектов между собой
    karakterizado =  SiriusoFilterConnectionField(ResursoModifoKarakterizadoNode,
        description=_('Выводит характеристики модификации ресурса'))

    class Meta:
        model = ResursoModifo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__id': ['exact'],
            'posedanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_karakterizado(self, info, **kwargs):
        # находим перечень задач, владельцами которых является объект
        karakterizadoj = ResursoModifoKarakterizadoLigilo.objects.filter(modifo=self, forigo=False, arkivo=False, 
            publikigo=True).values('karakterizado')
        return ResursoModifoKarakterizado.objects.filter(uuid__in=karakterizadoj, forigo=False, arkivo=False, 
            publikigo=True)


# Модель связи модификаций с характеристиками модификаций ресурсов
class ResursoModifoKarakterizadoLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = ResursoModifoKarakterizadoLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'modifo__uuid': ['exact'],
            'karakterizado__uuid': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель состояния модификаций ресурсов
class ResursoModifoStatoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ResursoModifoStato
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов мест хранения ресурсов
class ResursoStokejoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ResursoStokejoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель логических мест хранения ресурсов
class ResursoStokejoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ResursoStokejo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'tipo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов цен ресурсов
class ResursoPrezoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ResursoPrezoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель видов цен ресурсов
class ResursoPrezoSpecoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ResursoPrezoSpeco
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов цен ресурсов
class ResursoPrezoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ResursoPrezoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель цен ресурсов
class ResursoPrezoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = ResursoPrezo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель типов связей ресурсов между собой
class ResursoLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ResursoLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель связей ресурсов между собой
class ResursoLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ResursoLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'posedanto_stokejo__uuid': ['exact'],
            'ligilo__uuid': ['exact'],
            'tipo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель ресурсов
class ResursoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    # Связь ресурсов между собой
    ligilo =  SiriusoFilterConnectionField(ResursoLigiloNode,
        description=_('Выводит связи ресурсов между собой, владельцами которых является ресурс'))

    class Meta:
        model = Resurso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'klaso__id': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact'],
            'kategorio__id': ['exact'],
            'tipo__id': ['exact'],
            'klaso__id': ['exact'],
            'realeco__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_ligilo(self, info, **kwargs):
        return ResursoLigilo.objects.filter(posedanto=self, forigo=False, arkivo=False, 
            publikigo=True)


class ResursojQuery(graphene.ObjectType):
    resurso_kategorio = SiriusoFilterConnectionField(
        ResursoKategorioNode,
        description=_('Выводит все доступные модели категорий ресурсов')
    )
    resurso_kategorio_ligiloj_tipoj = SiriusoFilterConnectionField(
        ResursoKategorioLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей категорий ресурсов между собой')
    )
    resurso_kategorio_ligiloj = SiriusoFilterConnectionField(
        ResursoKategorioLigiloNode,
        description=_('Выводит все доступные модели связей категорий ресурсов между собой')
    )
    resurso_tipo = SiriusoFilterConnectionField(
        ResursoTipoNode,
        description=_('Выводит все доступные модели типов ресурсов')
    )
    resurso_klaso = SiriusoFilterConnectionField(
        ResursoKlasoNode,
        description=_('Выводит все доступные модели классов ресурсов')
    )
    resurso_modifo = SiriusoFilterConnectionField(
        ResursoModifoNode,
        description=_('Выводит все доступные модели модификации ресурсов')
    )
    resurso_modifo_karakterizadoj_tipo = SiriusoFilterConnectionField(
        ResursoModifoKarakterizadoTipoNode,
        description=_('Выводит все доступные модели типов характеристик модификаций ресурсов')
    )
    resurso_modifo_karakterizado = SiriusoFilterConnectionField(
        ResursoModifoKarakterizadoNode,
        description=_('Выводит все доступные модели характеристик модификаций ресурсов')
    )
    resurso_modifo_karakterizado_ligilo = SiriusoFilterConnectionField(
        ResursoModifoKarakterizadoLigiloNode,
        description=_('Выводит все доступные связи модификаций с характеристиками модификаций ресурсов')
    )
    resurso_modifo_stato = SiriusoFilterConnectionField(
        ResursoModifoStatoNode,
        description=_('Выводит все доступные модели состояния модификаций ресурсов')
    )
    resurso_stokejo_tipo = SiriusoFilterConnectionField(
        ResursoStokejoTipoNode,
        description=_('Выводит все доступные модели типов мест хранения ресурсов')
    )
    resurso_stokejo = SiriusoFilterConnectionField(
        ResursoStokejoNode,
        description=_('Выводит все доступные модели логических мест хранения ресурсов')
    )
    resurso_prezo_tipo = SiriusoFilterConnectionField(
        ResursoPrezoTipoNode,
        description=_('Выводит все доступные модели типов цен ресурсов')
    )
    resurso_prezo_speco = SiriusoFilterConnectionField(
        ResursoPrezoSpecoNode,
        description=_('Выводит все доступные модели видов цен ресурсов')
    )
    resurso_prezo_statuso = SiriusoFilterConnectionField(
        ResursoPrezoStatusoNode,
        description=_('Выводит все доступные модели статусов цен ресурсов')
    )
    resurso_prezo = SiriusoFilterConnectionField(
        ResursoPrezoNode,
        description=_('Выводит все доступные модели цен ресурсов')
    )
    resurso = SiriusoFilterConnectionField(
        ResursoNode,
        description=_('Выводит все доступные ресурсы')
    )
    resurso_ligilo_tipo = SiriusoFilterConnectionField(
        ResursoLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей ресурсов между собой')
    )
    resurso_ligilo = SiriusoFilterConnectionField(
        ResursoLigiloNode,
        description=_('Выводит все доступные модели связей ресурсов между собой')
    )
    # (находящийся в более высоком положении; находящийся поверх чего-л.; тж. перен.) supera
    # выбираем все категории, у которых нет вышестоящих связей
    supera_resurso_kategorio = SiriusoFilterConnectionField(
        ResursoKategorioNode,
        description=_('Выводит все доступные модели категорий ресурсов, у которых нет вышестоящих связей')
    )

    @staticmethod
    def resolve_supera_resurso_kategorio(root, info, **kwargs):
        kategorioj = ResursoKategorio.objects.filter(resursoj_resursokategorioligilo_ligilo__forigo=False,
            resursoj_resursokategorioligilo_ligilo__arkivo=False,
            resursoj_resursokategorioligilo_ligilo__publikigo=True,).values_list('uuid', flat=True)
        return ResursoKategorio.objects.exclude(uuid__in=kategorioj)
