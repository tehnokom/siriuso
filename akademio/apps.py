"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class AkademioConfig(AppConfig):
    name = 'akademio'
    verbose_name = _('Akademio')

    # def ready(self):
    #     import Akademio.signals
