"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django import forms
from django.utils.translation import gettext_lazy as _


class FullTextSearchForm(forms.Form):
    search_field = forms.CharField(widget=forms.TextInput(), max_length=50, required=False,
                                   label=_('Поиск'),
                                   help_text=_('Поиск по всему сайту'))