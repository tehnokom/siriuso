"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django import template
from django.urls import reverse
from django.utils.translation import ugettext as _


register = template.Library()


@register.inclusion_tag('main/pagination.html')
def pagination(args):
    pagination_context = {}
    pages = []

    if 'total' in args and args['total'] > 0 \
            and 'offset' in args and args['offset'] > 0\
            and 'view_name' in args\
            and 'view_args' in args and type(args['view_args']) == dict\
            and 'view_offset' in args and type(args['view_offset']) == str:
        total_pages = int(int(args['total'])/int(args['offset'])) + (1 if int(args['total'])%int(args['offset']) else 0)
        current_page = int(args['current_offset']/int(args['offset'])) + 1

        # start_page = {'num': '<<', 'title': _('Reen al la supro'),
        #              'url': reverse(view, kwargs=view_kwargs) }
        # pages.append(start_page)

        for page in range(0, total_pages):
            if page:
                args['view_args'][args['view_offset']] = page * args['offset']

            cur = {'num': page + 1, 'title': page + 1, 'url': reverse(args['view_name'],
                                                                             kwargs=args['view_args'])}

            if current_page == page + 1:
                cur['is_current_page'] = True

            pages.append(cur)

        pagination_context['pages'] = pages

    return pagination_context
