$(document).ready(function($){
    $('button').on('click', function (e) {
        $(this).prop('disabled', 'disabled');
        $.ajax({
        url: lastapaso_url,
        type: 'POST',
        async: true,
        cache: false,
        dataType: 'json',
        processData: true,
        data: {
            csrfmiddlewaretoken: getCookie('csrftoken'),
            action: 'confirmationcode'
        }
    })
        .done(function(response){
            if(response.status === 'ok') {
                alert(response.msg);
            } else {
                alert(response.msg);
            }
        })

        .fail(function(jqXHR, textStatus){
            console.log("Request failed: " + textStatus);
        })
    });
});
