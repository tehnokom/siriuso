$(function(){


	//Выбор нескольких чекбоксов
	//=================================================================
	$('.registrado_chefa_bloko').each(function(){
	
		var	$e = $(this),
			$checkboxes = $e.find('.registrado_selection_checkbox[data-required="true"]'),
			$button = $e.find('#registrado_selection_submit');

		function checkSelection(){
			if(!$e.find('input.bad-value').length && $e.find('.registrado_selection_checkbox[data-required="true"] > input[type="checkbox"]:checked').length == $e.find('.registrado_selection_checkbox[data-required="true"]').length)
				$button.removeAttr('disabled');
			else
				$button.attr('disabled', 'disabled');
		}
			
		checkSelection();
			
		$checkboxes.each(function(){
			
			var	$c = $(this);
			
			$c.change(function(){
				checkSelection();
			});
			
		});
	
	});
	
	
});