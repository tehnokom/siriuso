"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene

from django.contrib import auth
from django.utils.translation import gettext_lazy as _
from django.middleware.csrf import rotate_token as rotate_csrf_token
from django.conf import settings

from graphene_django import DjangoObjectType
from graphene import relay, types
from django.db.models import Q
from django.utils import timezone

from graphql_jwt.shortcuts import get_token

from siriuso.api.types import ErrorNode, SiriusoLingvo
from siriuso.api.filters import SiriusoFilterConnectionField
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions
from siriuso.utils import (generate_user_token, cancel_token, set_token_session, default_lingvo, check_reCaptcha,
                           lingvo_kodo_normaligo)
from uzantoj.api.schema import UzantoNode, UzantoProprietoj
from uzantoj.models import Uzanto, UzantojGekamaradoj, UzantojStatuso
from fotoj.models import FotojFotoDosiero, FotojUzantojKovrilo, FotojUzantojAvataro
from fotoj.api.schema import FotojFotoDosieroNode
from informiloj.api.schema import InformilojLingvoNode

from informiloj.models import InformilojLingvo, InformilojLando, InformilojRegiono
from komunumoj.models import Komunumo, KomunumoMembro, KomunumoMembroTipo
from komunumoj.api.schema import KomunumoNode
from profiloj.models import Profilo
from profiloj.api.schema import ProfiloNode
from profiloj.api.mutations import RedaktuProfilo
from ..tasks import send_confirm_code

import secrets
import re


class Ensaluti(graphene.Mutation):
    class Arguments:
        login = graphene.String(required=True)
        password = graphene.String(required=True)

    status = graphene.Boolean()
    message = graphene.String()
    token = graphene.String()
    uzanto = types.Field(UzantoNode)
    csrfToken = graphene.String()
    konfirmita = graphene.Boolean()
    jwt_token = graphene.String()

    def mutate(self, info, login, password):
        message = None
        konfirmita = None
        uzanto = auth.authenticate(username=login.lower(), password=password)

        if uzanto:
            if not uzanto.konfirmita:
                message = _('Учётная запись ещё не подтверждена')
                konfirmita = False
            elif info.context.user.is_authenticated and info.context.user.id != uzanto.id:
                message = _('Вы уже авторизированы')

            if message:
                return Ensaluti(status=False, message=message, konfirmita=konfirmita)

            token = generate_user_token(uzanto, request=info.context)
            jwt_token = get_token(uzanto)
            # Избегаем перезатирание текущей сессии
            if not info.context.user.is_authenticated:
                auth.login(info.context, uzanto)
                set_token_session(token, info.context)
                message = _('Успешный вход')
            else:
                message = _('Ключ успешно обновлен')

            return Ensaluti(status=True, token=token, csrfToken=info.context.META.get('CSRF_COOKIE'), uzanto=uzanto,
                            konfirmita=uzanto.konfirmita, jwt_token=jwt_token, message=message)
        else:
            message = _('Неверный логин или пароль')

        return Ensaluti(status=False, message=message)


class Elsaluti(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()

    def mutate(self, info):
        status = False
        message = _('Требуется авторизация')

        if info.context.user.is_authenticated:
            if 'HTTP_X_AUTH_TOKEN' in info.context.META:
                status = cancel_token(info.context)
                message = _('Токен не обнаружен') if not status else _('Токен аннулирован')
            else:
                status = True
                message = _('Аутентификация анулирована')

            auth.logout(info.context)

        return Elsaluti(status=status, message=message)


class CSRF(graphene.ObjectType):
    status = graphene.Boolean()
    csrf_token = graphene.String()
    message = graphene.String()


class Registrado(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)

    class Arguments:
        unua_nomo = graphene.String(required=True)
        familinomo = graphene.String(required=True)
        chefa_retposhto = graphene.String(required=True)
        pasvorto = graphene.String(required=True)
        chefa_telefonanumero = graphene.String()
        sekso = graphene.String()
        naskighdato = graphene.Date()
        lingvo = graphene.String()
        lando = graphene.String()
        regiono = graphene.String()
        chefa_organizo = graphene.Int()
        re_captcha = graphene.String(required=True)
        retnomo = graphene.String(description=_('никнейм'))

    def mutate(self, info, unua_nomo, familinomo, chefa_retposhto, pasvorto, re_captcha, **kwargs):
        status = False
        errors = []
        message = None

        email = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"
        tel = r"(^\+[0-9]{1,5}[0-9*#]{3,10}$)"

        inf_dict = {}
        chefa_organizo = None

        if info.context.user.is_authenticated and not (info.context.user.is_admin or info.context.user.is_superuser):
            message = _('Вы уже зарегистрированы')
            return Registrado(status=status, message=message, errors=errors)
        
        # Проверяем reCaptcha
        # if not check_reCaptcha(re_captcha, info.context.get_host().split(':')[0]): google
        if not check_reCaptcha(re_captcha, info.context.META['REMOTE_ADDR']):
            message = _('Ошибка при регистрации')
            errors.append(ErrorNode(field='reCaptcha', message=_('Не пройдена проверка reCaptcha')))

        # Проверяем формат e-mail, телефона и пароль
        if re.search(email, chefa_retposhto) is None:
            message = _('Ошибка при регистрации')
            errors.append(ErrorNode(field='chefaRetposhto', message=_('Не является адресом электронной почты')))

        if kwargs.get('chefa_telefonanumero') and re.search(tel, kwargs.get('chefa_telefonanumero')) is None:
            message = _('Ошибка при регистрации')
            errors.append(ErrorNode(field='chefaTelefonanumero', message=_('Не является номером телефона')))

        if len(pasvorto) < 6 or len(pasvorto) > 32:
            message = _('Ошибка при регистрации')
            errors.append(ErrorNode(field='pasvorto', message=_('Длина пароля должна быть от 6 до 32 символов')))

        if 'sekso' in kwargs and kwargs.get('sekso').lower() not in ('vira', 'virina'):
            message = _('Ошибка при регистрации')
            errors.append(ErrorNode(field='sekso', message=_("Не соответвует возможным значениям 'vira' или 'virina'")))

        if message:
            return Registrado(status=status, message=message, errors=errors)

        # Проверяем наличие пользователя с такими же e-mail и номером телефона
        q = Q(chefa_retposhto=chefa_retposhto)
        q = q | Q(chefa_telefonanumero=kwargs.get('chefa_telefonanumero')) if kwargs.get('chefa_telefonanumero') else q

        uzanto = Uzanto.objects.filter(q)

        if uzanto.count():
            message = _('Пользователь с такими учетными данныеми уже существует')

            for cur in uzanto:
                if (kwargs.get('chefa_telefonanumero')
                        and cur.chefa_telefonanumero == kwargs.get('chefa_telefonanumero')):
                    errors.append(
                        ErrorNode(field='chefaTelefonanumero', message=_('Такой номер телефона уже используется')))

                if cur.chefa_retposhto == chefa_retposhto:
                    errors.append(
                        ErrorNode(field='chefaRetposhto', message=_('Такой адрес электронной почты уже используется')))

        if kwargs.get('retnomo', False):
            # проверяем никнейм при его наличии в момент создания
            try:
                Profilo.objects.get(retnomo=kwargs.get('retnomo'))
                message = _('Такой никнейм уже используется')
            except:
                pass

        if not message:
            # Проверяем корректность остальных полей
            if 'naskighdato' in kwargs:
                if kwargs.get('naskighdato') < timezone.datetime(1900, 1, 1).date():
                    message = _('Ошибка при регистрации')
                    errors.append(ErrorNode(field='naskighdato', message=_('Мне бы такое здоровье...')))
                elif kwargs.get('naskighdato') > timezone.now().date() - timezone.timedelta(days=365):
                    message = _('Ошибка при регистрации')
                    errors.append(ErrorNode(field='naskighdato',
                                            message=_('А твои родители в курсе, что ты хочешь стать коммунистом?')))

            for key, val in {'unua_nomo': unua_nomo, 'familinomo': familinomo}.items():
                if len(val) > 150:
                    errors.append(ErrorNode(field=key, message=_('Длина поля не может быть больше 150 символов')))

            for key, model in {'lingvo': InformilojLingvo,
                               'lando': InformilojLando,
                               'regiono': InformilojRegiono}.items():
                if key in kwargs:
                    try:
                        inf_dict.update({key: model.objects.get(kodo=kwargs.get(key), forigo=False)})
                    except model.DoesNotExist:
                        message = _('Ошибка при регистрации')
                        errors.append(ErrorNode(field=key, message=_('Не соответвует возможным значениям')))

            if kwargs.get('chefa_organizo'):
                try:
                    chefa_organizo = Komunumo.objects.get(id=kwargs.get('chefa_organizo'),
                                                          tipo__kodo='entrepreno',
                                                          speco__kodo='popolaentrepreno',
                                                          forigo=False)
                except Komunumo.DoesNotExist:
                    message = _('Ошибка при регистрации')
                    errors.append(ErrorNode(field='chefa_organizo', message=_('Народное предприятие не найдено')))

            if message is None:
                # Регистрируем пользователя
                lingvo = inf_dict.get(
                    'lingvo',
                    InformilojLingvo.objects.get(kodo=lingvo_kodo_normaligo(settings.LANGUAGE_CODE), forigo=False)
                )

                json_unua_nomo = default_lingvo()
                json_unua_nomo['chefa_varianto'] = lingvo.kodo
                json_unua_nomo['lingvo'][lingvo.kodo] = 0
                json_unua_nomo['enhavo'].append(unua_nomo)

                json_familinomo = default_lingvo()
                json_familinomo['chefa_varianto'] = lingvo.kodo
                json_familinomo['lingvo'][lingvo.kodo] = 0
                json_familinomo['enhavo'].append(familinomo)

                uzanto = Uzanto.objects.create(unua_nomo=json_unua_nomo, familinomo=json_familinomo,
                                               chefa_retposhto=chefa_retposhto.lower(),
                                               chefa_telefonanumero=kwargs.get('chefa_telefonanumero'),
                                               sekso=kwargs.get('sekso', 'vira').lower(),
                                               naskighdato=kwargs.get('naskighdato'),
                                               chefa_lingvo=inf_dict.get('lingvo'),
                                               loghlando=inf_dict.get('lando'),
                                               loghregiono=inf_dict.get('regiono'),
                                               is_active=True,
                                               konfirmita=False,
                                               konfirmita_hash=secrets.token_hex(nbytes=4).upper())
                uzanto.set_password(pasvorto)
                uzanto.save()

                if chefa_organizo:
                    membro_tipo = KomunumoMembroTipo.objects.get(kodo='komunumano', forigo=False)
                    KomunumoMembro.objects.create(posedanto=chefa_organizo, autoro=uzanto, tipo=membro_tipo)

                send_confirm_code.delay(uzanto.id, False)
                
                # при наличии параметра никнейма - создаём его
                if kwargs.get('retnomo', False):
                    status, message, errors2, profilo = RedaktuProfilo.create(self, uzanto, info, **kwargs)
                    if status:
                        message = _('Пользователь успешно зарегистрирован')
                    else:
                        errors.append(errors2)
                else:
                    status = True
                    message = _('Пользователь успешно зарегистрирован')

        errors = list() if not len(errors) else errors
        return Registrado(status=status, message=message, errors=errors)


class RegistradoMini(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)

    class Arguments:
        unua_nomo = graphene.String(required=True)
        familinomo = graphene.String()
        chefa_retposhto = graphene.String(required=True)
        pasvorto = graphene.String(required=True)
        chefa_telefonanumero = graphene.String()
        sekso = graphene.String()
        naskighdato = graphene.Date()
        lingvo = graphene.String()
        lando = graphene.String()
        regiono = graphene.String()
        chefa_organizo = graphene.Int()
        re_captcha = graphene.String(required=True)

    def mutate(self, info, unua_nomo, familinomo, chefa_retposhto, pasvorto, re_captcha, **kwargs):
        status = False
        errors = []
        message = None

        email = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"
        tel = r"(^\+[0-9]{1,5}[0-9*#]{3,10}$)"

        inf_dict = {}
        chefa_organizo = None

        if info.context.user.is_authenticated and not (info.context.user.is_admin or info.context.user.is_superuser):
            message = _('Вы уже зарегистрированы')
            return RegistradoMini(status=status, message=message, errors=errors)

        # Проверяем reCaptcha
        if not check_reCaptcha(re_captcha, info.context.get_host().split(':')[0]):
            message = _('Ошибка при регистрации1')
            errors.append(ErrorNode(field='reCaptcha', message=_('Не пройдена проверка reCaptcha')))

        # Проверяем формат e-mail, телефона и пароль
        if re.search(email, chefa_retposhto) is None:
            message = _('Ошибка при регистрации2')
            errors.append(ErrorNode(field='chefaRetposhto', message=_('Не является адресом электронной почты')))

        if 'chefa_telefonanumero' in kwargs and re.search(tel, kwargs.get('chefa_telefonanumero')) is None:
            message = _('Ошибка при регистрации3')
            errors.append(ErrorNode(field='chefaRetposhto', message=_('Не является номером телефона')))

        if len(pasvorto) < 6 or len(pasvorto) > 32:
            message = _('Ошибка при регистрации4')
            errors.append(ErrorNode(field='pasvorto', message=_('Длина пароля должна быть от 6 до 32 символов')))

        if 'sekso' in kwargs and kwargs.get('sekso').lower() not in ('vira', 'virina'):
            message = _('Ошибка при регистрации5')
            errors.append(ErrorNode(field='sekso', message=_("Не соответвует возможным значениям 'vira' или 'virina'")))

        if message:
            return RegistradoMini(status=status, message=message, errors=errors)

        # Проверяем наличие пользователя с такими же e-mail и номером телефона
        q = Q(chefa_retposhto=chefa_retposhto)
        q = (q | Q(chefa_telefonanumero=kwargs.get('chefa_telefonanumero'))) if 'chefa_telefonanumero' in kwargs else q

        uzanto = Uzanto.objects.filter(q)

        if uzanto.count():
            message = _('Пользователь с такими учетными данныеми уже существует')

            for cur in uzanto:
                if ('chefa_telefonanumero' in kwargs
                        and cur.chefa_telefonanumero == kwargs.get('chefa_telefonanumero')):
                    errors.append(
                        ErrorNode(field='chefaTelefonanumero', message=_('Такой номер телефона уже используется')))

                if cur.chefa_retposhto == chefa_retposhto:
                    errors.append(
                        ErrorNode(field='chefaRetposhto', message=_('Такой адрес электронной почты уже используется')))

        if not message:
            # Проверяем корректность остальных полей
            if 'naskighdato' in kwargs:
                if kwargs.get('naskighdato') < timezone.datetime(1900, 1, 1).date():
                    message = _('Ошибка при регистрации6')
                    errors.append(ErrorNode(field='naskighdato', message=_('Мне бы такое здоровье...')))
                elif kwargs.get('naskighdato') > timezone.now().date() - timezone.timedelta(days=365):
                    message = _('Ошибка при регистрации7')
                    errors.append(ErrorNode(field='naskighdato',
                                            message=_('А твои родители в курсе, что ты хочешь стать коммунистом?')))

            for key, val in {'unua_nomo': unua_nomo, 'familinomo': familinomo}.items():
                if len(val) > 150:
                    errors.append(ErrorNode(field=key, message=_('Длина поля не может быть больше 150 символов')))

            for key, model in {'lingvo': InformilojLingvo,
                               'lando': InformilojLando,
                               'regiono': InformilojRegiono}.items():
                if key in kwargs:
                    try:
                        inf_dict.update({key: model.objects.get(kodo=kwargs.get(key), forigo=False)})
                    except model.DoesNotExist:
                        message = _('Ошибка при регистрации8')
                        errors.append(ErrorNode(field=key, message=_('Не соответвует возможным значениям')))

            if (kwargs.get('chefa_organizo',False)):
                try:
                    chefa_organizo = Komunumo.objects.get(id=kwargs.get('chefa_organizo'),
                                                        tipo__kodo='entrepreno',
                                                        speco__kodo='popolaentrepreno',
                                                        forigo=False)
                except Komunumo.DoesNotExist:
                    message = _('Ошибка при регистрации9')
                    errors.append(ErrorNode(field='chefa_organizo', message=_('Народное предприятие не найдено')))

            if message is None:
                # Регистрируем пользователя
                lingvo = inf_dict.get(
                    'lingvo',
                    InformilojLingvo.objects.get(kodo=lingvo_kodo_normaligo(settings.LANGUAGE_CODE), forigo=False)
                )

                json_unua_nomo = default_lingvo()
                json_unua_nomo['chefa_varianto'] = lingvo.kodo
                json_unua_nomo['lingvo'][lingvo.kodo] = 0
                json_unua_nomo['enhavo'].append(unua_nomo)

                json_familinomo = default_lingvo()
                json_familinomo['chefa_varianto'] = lingvo.kodo
                json_familinomo['lingvo'][lingvo.kodo] = 0
                json_familinomo['enhavo'].append(familinomo)

                uzanto = Uzanto.objects.create(unua_nomo=json_unua_nomo, familinomo=json_familinomo,
                                               chefa_retposhto=chefa_retposhto.lower(),
                                               chefa_telefonanumero=kwargs.get('chefa_telefonanumero'),
                                               sekso=kwargs.get('sekso', 'vira').lower(),
                                               naskighdato=kwargs.get('naskighdato',timezone.now()),
                                               chefa_lingvo=inf_dict.get('lingvo'),
                                               loghlando=inf_dict.get('lando'),
                                               loghregiono=inf_dict.get('regiono'),
                                               is_active=True,
                                               konfirmita=False,
                                               konfirmita_hash=secrets.token_hex(nbytes=4).upper())
                uzanto.set_password(pasvorto)
                uzanto.save()

                if kwargs.get('chefa_organizo',False):
                    membro_tipo = KomunumoMembroTipo.objects.get(kodo='komunumano', forigo=False)
                    KomunumoMembro.objects.create(posedanto=chefa_organizo, autoro=uzanto, tipo=membro_tipo)
                send_confirm_code.delay(uzanto.id, False)
                status = True
                message = _('Пользователь успешно зарегистрирован')

        errors = list() if not len(errors) else errors
        return RegistradoMini(status=status, message=message, errors=errors)


class RegistraKonfirmo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()

    class Arguments:
        ensalutu = graphene.String(required=True)
        pasvorto = graphene.String(required=True)
        konfirma_kodo = graphene.String(required=True)
        uzanto_id = graphene.Int()

    def mutate(self, info, ensalutu, pasvorto, konfirma_kodo, **kwargs):
        status = False
        message = None
        uzanto = None

        if info.context.user.is_authenticated:
            # Администратор активирует пользователя
            if info.context.user.is_admin or info.context.user.is_superuser:
                if 'uzanto_id' not in kwargs:
                    message = _('Необходимо указать ID пользователя')
                elif info.context.user.id == kwargs.get('uzanto_id'):
                    message = _('Ваша регистрация уже подтверждена')

                if message:
                    return RegistraKonfirmo(status=status, message=message)

                try:
                    uzanto = Uzanto.objects.get(id=kwargs.get('uzanto_id'), konfirmita=False, is_active=True)
                except Uzanto.DoesNotExist:
                    message = _('Пользователь с тиким ID не найден, либо заблокирован')
            else:
                message = '%s \'%s\'' % (_('Не достаточно прав для использования аргумента'), 'uzanto_id')
        else:
            uzanto = auth.authenticate(username=ensalutu.lower(), password=pasvorto)

        if uzanto:
            if uzanto.konfirmita_hash == konfirma_kodo:
                uzanto.konfirmita = True
                uzanto.konfirmita_hash = None
                uzanto.save()
                status = True
                message = _('Регистрация успешно подтверждена')
            else:
                message = _('Код подтверждения не верен')
        else:
            message = _('Пользователь не определён')

        return RegistraKonfirmo(status=status, message=message)


class NovaKonfirmaKodo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()

    class Arguments:
        uzanto_id = graphene.Int()
        ensalutu = graphene.String()
        pasvorto = graphene.String()

    def mutate(self, info, **kwargs):
        status = False
        message = None
        uzanto = None

        if info.context.user.is_authenticated:
            if 'uzanto_id' in kwargs:
                if info.context.user.is_superuser or info.context.user.is_admin:
                    try:
                        uzanto = Uzanto.objects.get(id=kwargs.get('uzanto_id'), is_active=True)
                    except:
                        message = _('Пользователь с тиким ID не найден, либо заблокирован')
                else:
                    message = '%s \'%s\'' % (_('Не достаточно прав для использования аргумента'), 'uzanto_id')
            else:
                message = _('Ваша регистрация уже подтверждена')

        if not message:
            if 'uzanto_id' not in kwargs:
                if not len(kwargs.get('ensalutu', '')) and not len(kwargs.get('pasvorto', '')):
                    message = '%s \'%s\'' % (_('Не переданы обязательные аргументы: '),
                                             '\'ensalutu\', \'pasvorto\'')
                elif not len(kwargs.get('ensalutu', '')):
                    message = '%s \'%s\'' % (_('Не передан обязательный аргумент'), 'ensalutu')
                elif not len(kwargs.get('pasvorto', '')):
                    message = '%s \'%s\'' % (_('Не передан обязательный аргумент'), 'pasvorto')

        if message:
            return NovaKonfirmaKodo(status=status, message=message)

        if uzanto is None:
            uzanto = auth.authenticate(username=kwargs.get('ensalutu').lower(), password=kwargs.get('pasvorto'))

        if uzanto is None:
            message = _('Пользователь не определён')
        elif not uzanto.konfirmita:
            uzanto.konfirmita_hash = secrets.token_hex(nbytes=4).upper()
            uzanto.save()
            send_confirm_code.delay(uzanto.id)
            status = True
            message = _('Новый код отправлен')
        else:
            message = _('Ваша регистрация уже подтверждена')

        return NovaKonfirmaKodo(status=status, message=message)


class Restarigo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()

    class Arguments:
        ensalutu = graphene.String(required=True)
        konfirma_kodo = graphene.String()
        pasvorto = graphene.String()

    def mutate(self, info, ensalutu, **kwargs):
        status = False
        message = None

        if not info.context.user.is_authenticated:
            # Проверяем аргументы
            if 'konfirma_kodo' in kwargs and not len(kwargs.get('konfirma_kodo')) :
                message = _('Необходимо указать код подвтерждения')
            elif 'pasvorto' in kwargs:
                if not len(kwargs.get('pasvorto')):
                    message = _('Необходимо указать пароль')
                elif len(kwargs.get('pasvorto')) < 6 or len(kwargs.get('pasvorto')) > 32:
                    message = _('Пароль должен быть от 6 до 32 символов')

            if message:
                return Restarigo(status=status, message=message)

            # Определяем пользователя
            try:
                uzanto = Uzanto.objects.get(Q(chefa_retposhto=ensalutu.lower()) | Q(chefa_telefonanumero=ensalutu),
                                            is_active=True, konfirmita=True)

                # Определяем по параметрам, на каком шаге восстановления доступа находимся
                if 'konfirma_kodo' not in kwargs and 'pasvorto' not in kwargs:
                    # Генерируем и отправляем код на почту
                    uzanto.agordoj['restariga_kodo'] = secrets.token_hex(nbytes=4).upper()
                    uzanto.save()
                    send_confirm_code.delay(uzanto.id, True)
                    status = True
                    message = _('Код подтверждения отправлен')
                elif 'konfirma_kodo' in kwargs and 'pasvorto' not in kwargs:
                    # Проверяем корректность кода
                    if uzanto.agordoj.get('restariga_kodo') == kwargs.get('konfirma_kodo'):
                        status = True
                        message = _('Код подтверждения верен')
                    else:
                        message = _('Код подтверждения не верен')
                elif 'konfirma_kodo' in kwargs and 'pasvorto' in kwargs:
                    if uzanto.agordoj.get('restariga_kodo') == kwargs.get('konfirma_kodo'):
                        # Меняем пароль на указанный
                        uzanto.set_password(kwargs.get('pasvorto'))
                        del uzanto.agordoj['restariga_kodo']
                        uzanto.save()
                        status = True
                        message = _('Пароль успешно изменён')
                    else:
                        message = _('Код подтверждения не верен')

            except Uzanto.DoesNotExist:
                message = _('Пользователь не определён')
        else:
            message = _('Вы уже авторизованы')

        return Restarigo(status=status, message=message)


class MiUzantoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    avataro = types.Field(FotojFotoDosieroNode)
    kovrilo = types.Field(FotojFotoDosieroNode)
    unua_nomo = types.Field(SiriusoLingvo)
    dua_nomo = types.Field(SiriusoLingvo)
    familinomo = types.Field(SiriusoLingvo)
    statuso = types.Field(SiriusoLingvo)
    statistiko = types.Field(UzantoProprietoj)
    kontakta_informo = graphene.String()
    sekso = graphene.String()
    kandidatoj_tuta = graphene.Int()
    gekamaradoj_tuta = graphene.Int()
    chefa_organizo = types.Field(KomunumoNode)
    chefa_lingvo = types.Field(InformilojLingvoNode)
    sovetoj = SiriusoFilterConnectionField(KomunumoNode)
    sindikatoj = SiriusoFilterConnectionField(KomunumoNode)
    administritaj_komunumoj = SiriusoFilterConnectionField(KomunumoNode)
    rajtoj = graphene.List(graphene.String)
    universo_uzanto = types.Field(ProfiloNode)
    profilo = types.Field(ProfiloNode)

    class Meta:
        model = Uzanto
        fields = ['id', 'uuid', 'is_active', 'konfirmita', 'malbona_retposhto', 'unua_nomo', 'familinomo',
                       'dua_nomo', 'sekso', 'chefa_retposhto', 'chefa_telefonanumero', 'chefa_lingvo',
                       'naskighdato', 'loghlando', 'loghregiono', 'agordoj', 'is_admin']
        filter_fields = {
            'id': ['exact'],
            'uuid': ['exact'],
            'is_active': ['exact'],
            'konfirmita': ['exact'],
            'malbona_retposhto': ['exact'],
            'sekso': ['exact'],
        }
        interfaces = (relay.Node,)

    def resolve_avataro(self, info):
        try:
            avataro = FotojUzantojAvataro.objects.get(posedanto_id=getattr(self, 'id'), forigo=False, chefa_varianto=True)

            if avataro.avataro is None:
                raise FotojUzantojAvataro.DoesNotExist()

        except (FotojUzantojAvataro.DoesNotExist, FotojUzantojAvataro.MultipleObjectsReturned):
            avataro = FotojFotoDosiero(posedanto_id=getattr(self, 'id'), forigo=False)
            avataro.bildo_baza = ('/static/main/images/virina-avataro.png' if getattr(self, 'sekso') == 'virina'
                       else '/static/main/images/vira-avataro.png')
            avataro.bildo_f = ('/static/main/images/virina-avataro-min.png' if getattr(self, 'sekso') == 'virina'
                       else '/static/main/images/vira-avataro-min.png')
            return avataro

        return avataro.avataro.dosiero

    def resolve_kovrilo(self, info):
        try:
            kovrilo = FotojUzantojKovrilo.objects.get(posedanto_id=getattr(self, 'id'), forigo=False, chefa_varianto=True)

            if kovrilo.kovrilo is None:
                raise FotojUzantojKovrilo.DoesNotExist()

        except (FotojUzantojKovrilo.DoesNotExist, FotojUzantojKovrilo.MultipleObjectsReturned):
            kovrilo = FotojFotoDosiero(posedanto_id=getattr(self, 'id'), forigo=False)
            kovrilo.bildo = '/static/main/images/defaulte-kovrilo.png'
            return kovrilo

        return kovrilo.kovrilo.dosiero

    def resolve_statuso(self, info):
        try:
            statuso = UzantojStatuso.objects.get(posedanto_id=getattr(self, 'id'), forigo=False, arkivo=False)
            statuso = statuso.teksto
        except (UzantojStatuso.DoesNotExist, UzantojStatuso.MultipleObjectsReturned):
            statuso = None
        return statuso

    def resolve_statistiko(self, info):
        return UzantoProprietoj(parent=self)

    def resolve_rajtoj(self, info, **kwargs):
        return list(filter(lambda x: x.find('povas') > -1, info.context.user.get_all_permissions()))

    def resolve_sovetoj(self, info, **kwargs):
        uzanto_id = info.context.user.id
        membroj = ['komunumano', 'komunumano-adm', 'komunumano-mod']

        q = (Komunumo.objects.filter(forigo=False, tipo__kodo='soveto',
                                     komunumoj_komunumomembro_posedanto__autoro_id=uzanto_id,
                                     komunumoj_komunumomembro_posedanto__forigo=False,
                                     komunumoj_komunumomembro_posedanto__tipo__kodo__in=membroj)
             )
        return q

    def resolve_sindikatoj(self, info, **kwargs):
        uzanto_id = info.context.user.id
        membroj = ['komunumano', 'komunumano-adm', 'komunumano-mod']

        q = (Komunumo.objects.filter(forigo=False, tipo__kodo='sindikato',
                                              komunumoj_komunumomembro_posedanto__autoro_id=uzanto_id,
                                              komunumoj_komunumomembro_posedanto__forigo=False,
                                              komunumoj_komunumomembro_posedanto__tipo__kodo__in=membroj)
             )
        return q

    def resolve_kandidatoj_tuta(self, info):
        uzanto_id = info.context.user.id

        q = (UzantojGekamaradoj.objects
             .filter(posedanto__is_active=True, posedanto__konfirmita=True, akceptis1=True, gekamarado_id=uzanto_id,
                     akceptis2__isnull=True, forigo=False))

        return q.count()

    def resolve_gekamaradoj_tuta(self, info):
        uzanto_id = info.context.user.id

        q = (UzantojGekamaradoj.objects
             .filter(Q(posedanto_id=uzanto_id, gekamarado__is_active=True, gekamarado__konfirmita=True)
                     | Q(gekamarado_id=uzanto_id, posedanto__is_active=True, posedanto__konfirmita=True),
                     akceptis1=True, akceptis2=True, forigo=False))

        return q.count()

    def resolve_chefa_organizo(self, info):
        uzanto_id = info.context.user.id

        try:
            membro_tipo = KomunumoMembroTipo.objects.get(kodo='komunumano', forigo=False)
            organizo = Komunumo.objects.get(tipo__kodo='entrepreno',
                                            speco__kodo='popolaentrepreno',
                                            komunumoj_komunumomembro_posedanto__autoro=uzanto_id,
                                            komunumoj_komunumomembro_posedanto__forigo=False,
                                            komunumoj_komunumomembro_posedanto__tipo=membro_tipo,
                                            forigo=False)
            return organizo
        except (KomunumoMembro.DoesNotExist, Komunumo.DoesNotExist):
            pass

        return None

    def resolve_administritaj_komunumoj(self, info, **kwargs):
        uzanto = info.context.user

        if uzanto.is_authenticated:
            return Komunumo.objects.filter(
                komunumoj_komunumomembro_posedanto__autoro=uzanto,
                komunumoj_komunumomembro_posedanto__forigo=False,
                komunumoj_komunumomembro_posedanto__tipo__kodo__in=(
                    'administranto', 'moderiganto', 'komunumano-adm', 'komunumano-adm'
                )
            )

        return Komunumo.objects.none()

    def resolve_universo_uzanto(self, info, **kwargs):
        try:
            return getattr(self, 'profilo')
        except self._meta.model.profilo.RelatedObjectDoesNotExist:
            return None

    def resolve_profilo(self, info, **kwargs):
        try:
            return getattr(self, 'profilo')
        except self._meta.model.profilo.RelatedObjectDoesNotExist:
            return None


class Query(graphene.ObjectType):
    # csrf_token = graphene.Field(CSRF)
    mi = types.Field(MiUzantoNode)

    def resolve_csrf_token(self, info):
        if info.context.user.is_authenticated:
            rotate_csrf_token(info.context)
            return CSRF(status=True, csrf_token=info.context.META['CSRF_COOKIE'],
                        message=_('Успешно'))
        return CSRF(status=False, csrf_token=None, message=_('Вы должны быть авторизованы'))

    def resolve_mi(self, info, **kwargs):
        if info.context.user.is_authenticated:
            try:
                return Uzanto.objects.get(id=info.context.user.id, is_active=True, konfirmita=True)
            except Uzanto.DoesNotExist:
                pass
        return None


class Mutations(graphene.ObjectType):
    ensaluti = Ensaluti.Field()
    elsaluti = Elsaluti.Field()
    registrado = Registrado.Field()
    registrado_mini = RegistradoMini.Field()
    registra_konfirmo = RegistraKonfirmo.Field()
    nova_konfirma_kodo = NovaKonfirmaKodo.Field()
    restarigo = Restarigo.Field()


mutations = graphene.Schema(query=Query, mutation=Mutations)
