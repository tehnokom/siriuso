"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from celery import shared_task
from django.conf import settings
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.utils.translation import gettext_lazy as _

from django.conf import settings
from siriuso.utils.modules import get_enhavo
from main.models import Uzanto

@shared_task
def send_confirm_code(uzanto_id, restarigo=False):
    try:
        uzanto = Uzanto.objects.get(id=uzanto_id, is_active=True)
    except Uzanto.DoesNotExist:
        return False
    data = {'uzanto': uzanto,
            'kodo': uzanto.konfirmita_hash if not restarigo else uzanto.agordoj.get('restariga_kodo'),
            'restarigo': restarigo,
            'abono_shlosilo': None,
            'poshto': None,
            'site_url': settings.SITE_URL}
    email_body = """%s, %s %s
            %s %s""" % (_('Saluton'), get_enhavo(uzanto.unua_nomo, uzanto.chefa_lingvo.kodo),
                        get_enhavo(uzanto.familinomo, uzanto.chefa_lingvo.kodo),
                        _(''),
                        uzanto.konfirmita_hash
                        )

    html_body = render_to_string('registrado/registrado_nova_konfirmaretposto.html', data)
    return send_mail(_('Konfirmo de la registrado') if not restarigo else
                     _('Восстановление доступа к учетной записи'),
              email_body,
              settings.EMAIL_NO_REPLY,
              [uzanto.chefa_retposhto],
              fail_silently=True,
              html_message=html_body
              )
