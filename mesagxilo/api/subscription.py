"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import channels_graphql_ws
import graphene

from graphql import GraphQLError
from uzantoj.api.schema import UzantoNode
from .schema import MesagxiloBabilejoNode, MesagxiloMesagxoNode, MesagxiloInvestojNode
from ..models import MesagxiloBabilejo, MesagxiloMesagxo, MesagxiloPartoprenanto
from uzantoj.models import Uzanto


# Реализация подписки для чатов
# Объединение возвращаемых объектов действий
class MesagxiloEventojUnion(graphene.Union):
    class Meta:
        # Типы объединения
        types = (
            UzantoNode,
            MesagxiloMesagxoNode,
        )


# Подписки для WebSocket
class MesagxiloEventoj(channels_graphql_ws.Subscription):
    """Подписка на действия в чатах"""
    evento = graphene.String(required=True)
    babilejo = graphene.relay.node.Field(MesagxiloBabilejoNode)
    objekto = graphene.Field(MesagxiloEventojUnion)

    class Arguments:
        """Тут аргументы, передающиеся для подписки на события чатов"""
        # Список ID чатов
        babilejoj = graphene.List(graphene.Int, required=True)

    def subscribe(self, info, babilejoj):
        """В этом методе определятся список чатов для подписки на их действия"""
        if info.context.user.is_authenticated:
            babilejoj_res = MesagxiloPartoprenanto.objects.filter(
                partoprenanto=info.context.user, forigo=False, arkivo=False, publikigo=True,
                babilejo__id__in=babilejoj
            )

            babilej_set = set(
                babilejoj_res.values_list('babilejo__id', flat=True)
            )

            if not babilejoj_res:
                raise GraphQLError(
                    'Невозможно отслеживать чаты c ID {}, они не сущствуют или не доступны'.format(
                        set(babilejoj) - babilej_set
                    )
                )

            # Тут можно сделать проверку на количество заявленных и фактически найденных чатов,
            # в которых пользователь участвует
            return list(map(lambda v: 'Mesagxilo_{}'.format(v), babilej_set)) or None

        raise GraphQLError('Необходима авторизация')

    @staticmethod
    def publish(payload, info, babilejoj):
        evento = payload.get('evento')

        try:
            babilejo = MesagxiloBabilejo.objects.get(
                uuid=payload.get('babilejo'),
                publikigo=True,
                forigo=False,
                arkivo=False
            )
            if evento in ('nova_mesagxo', 'redaktita_mesagxo'):
                objekto = MesagxiloMesagxo.objects.get(
                    uuid=payload.get('mesagxo'),
                    publikigo=True,
                    arkivo=False,
                    forigo=False
                )
                autoro = objekto.posedanto
            else:
                objekto = Uzanto.objects.get(
                    id=payload.get('uzanto')
                )
                autoro = objekto

            if autoro.id == info.context.user.id:
                # Пропускаем публикацию события для автора события :)
                return MesagxiloEventoj.SKIP

            return MesagxiloEventoj(evento=evento, babilejo=babilejo, objekto=objekto)
        except (MesagxiloBabilejo.DoesNotExist, Uzanto.DoesNotExist, MesagxiloMesagxo.DoesNotExist):
            pass

        return MesagxiloEventoj.SKIP

    @classmethod
    def publikigi(cls, babilejo, mesagxo):
        """Этот метод должен вызываться при публикации нового сообщения в чате"""
        cls.broadcast(
            group='Mesagxilo_{}'.format(babilejo.id),
            payload={
                'evento': 'nova_mesagxo',
                'babilejo': str(babilejo.uuid),
                'mesagxo': str(mesagxo.uuid)
            }
        )

    @classmethod
    def redakti(cls, babilejo, mesagxo):
        """Этот метод должен вызываться при редактировании сообщения в чате"""
        cls.broadcast(
            group='Mesagxilo_{}'.format(babilejo.id),
            payload={
                'evento': 'redaktita_mesagxo',
                'babilejo': str(babilejo.uuid),
                'mesagxo': str(mesagxo.uuid)
            }
        )

    @classmethod
    def presajoj(cls, babilejo, uzanto):
        """Этот метод должен вызываться при редактировании сообщения в чате"""
        cls.broadcast(
            group='Mesagxilo_{}'.format(babilejo.id),
            payload={
                'evento': 'presajoj',
                'babilejo': str(babilejo.uuid),
                'uzanto': str(uzanto.id)
            }
        )


class MesagxiloSubscription(graphene.ObjectType):
    mesagxilo_eventoj = MesagxiloEventoj.Field()
