# Установка бекенда Siriuso
[[_TOC_]]

Создать пользователя siriuso
```shell
sudo adduser siriuso
```

Добавить пользователя siriuso в группы sudo и www-data
```shell
sudo usermod -aG sudo www-data siriuso
```

# Клонирование файлов проекта
Установить Git
```shell
sudo apt install -y git
```
Клонировать код из Gitlab
```shell
cd /opt
sudo git clone https://gitlab.com/tehnokom/siriuso.git
```
Переключиться на ветку develop
```shell
cd siriuso
git checkout develop
```

# Установка Python
Установить приложение add-apt-repository для подключения сторонних репозиториев
```shell
sudo apt update
sudo apt install -y software-properties-common
```
Подключить PPA (Personal Package Archive) репозиторий неофициальных пакетов Python
(все пакеты PPA находятся на сервере ppa.launchpad.net , который поддерживает Canonical)
```shell
sudo add-apt-repository ppa:deadsnakes/ppa
```
Установить Python 3.8
```shell
sudo apt install -y python3.8
```

# Установка PostgreSQL
Установить необходимые пакеты
(под вопросом пакеты libpq-dev и postgresql-contrib)
```shell
sudo apt-get install libpq-dev postgresql postgresql-contrib
```
Подключиться к под УЗ postgres
```shell
sudo -u postgres psql
```
Создать базу данных, пользователя и дать пользователю полные права на базу
```sql
CREATE DATABASE siriuso;
CREATE USER siriuso WITH PASSWORD 'oGLOWo8nd3';
ALTER ROLE siriuso SET client_encoding TO 'utf8';
ALTER ROLE siriuso SET default_transaction_isolation TO 'read committed';
ALTER ROLE siriuso SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE siriuso TO siriuso;
```
Загрузить последний дамп БД в PostgreSQL
(где взять дамп?)
```shell
gunzip a.sql.gz
psql -h 127.0.0.1 -U siriuso -d siriuso -f ~/base/a.sql
```
