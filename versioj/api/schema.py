"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene
from graphene_django import DjangoObjectType
from graphene_permissions.permissions import AllowAny
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId
from ..models import *


class VersioUzantoEnskriboNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioUzantoEnskribo
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro.get('teksto')

    def resolve_priskribo(self, info):
        return self.valoro.get('priskribo')


class VersioKomunumoEnskriboNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioKomunumoEnskribo
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']


class VersioEnskribo(graphene.Union):
    class Meta:
        types = (VersioKomunumoEnskriboNode, VersioKomunumoEnskriboNode)


class VersioEnskriboUnionConnection(graphene.relay.Connection):
    class Meta:
        node = VersioEnskribo


class VersioAkademioPagxoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioAkademioPagxo
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']


class VersioAkademio(graphene.Union):
    class Meta:
        types = (VersioAkademioPagxoNode,)


class VersioAkademioUnionConnection(graphene.relay.Connection):
    class Meta:
        node = VersioAkademio


class VersioEnciklopedioPagxoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioEnciklopedioPagxo
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']


class VersioEnciklopedio(graphene.Union):
    class Meta:
        types = (VersioEnciklopedioPagxoNode,)


class VersioEnciklopedioPagxoUnionConnection(graphene.relay.Connection):
    class Meta:
        node = VersioEnciklopedio

class VersioKodoPagxoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioKodoPagxo
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioKodo(graphene.Union):
    class Meta:
        types = (VersioKodoPagxoNode,)

class VersioKodoPagxoUnionConnection(graphene.relay.Connection):
    class Meta:
        node = VersioKodo


class VersioKonferencojTemoKomentoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioKonferencojTemoKomento
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioKonferencojTemoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioKonferencojTemo
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioKonferencojKategorioNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioKonferencojKategorio
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioEsploradojTemoKomentoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioEsploradojTemoKomento
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioEsploradojTemoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioEsploradojTemo
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

class VersioEsploradojKategorioNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioEsploradojKategorio
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']


class VersioMesagxiloMesagxoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)

    valoro = graphene.String()
    priskribo = graphene.String()

    class Meta:
        model = VersioMesagxiloMesagxo
        filter_fields = {
            'uuid': ['exact', 'in', 'not_in'],
            'autoro__id': ['exact', 'in'],
            'posedanto__uuid': ['exact', 'in'],
            'lingvo__kodo': ['exact', 'in'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_valoro(self, info):
        return self.valoro['teksto']

