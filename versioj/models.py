"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import models
from django.contrib.auth.models import Permission
from django.utils.translation import gettext_lazy as _
from uuid import uuid4


class VersioLingvoAbstracta(models.Model):
    uuid = models.UUIDField(_('UUID'), primary_key=True, default=uuid4, editable=False)
    id = models.IntegerField(_('ID'), default=0)
    krea_dato = models.DateTimeField(auto_now_add=True, auto_now=False, blank=False)
    autoro = models.ForeignKey('main.Uzanto', verbose_name=_('Autoro'), blank=False, null=False,
                               on_delete=models.CASCADE)
    lingvo = models.ForeignKey('informiloj.InformilojLingvo', verbose_name=_('Lingvo'), blank=False, null=False,
                               on_delete=models.CASCADE)
    valoro = models.JSONField(_('Valoro'), blank=False, null=False)
    aktiva = models.BooleanField(_('Aktiva versio'), blank=False, default=False)

    class Meta:
        abstract = True


class VersioUzantoEnskribo(VersioLingvoAbstracta):
    posedanto = models.ForeignKey('muroj.MurojUzantoEnskribo', verbose_name=_('Originala Enskribo'), blank=False,
                                  null=False, on_delete=models.CASCADE)

    class Meta:
        db_table = 'versioj_uzantoj_enskriboj'
        index_together = (
            ('posedanto', 'id', 'lingvo'),
        )
        verbose_name = _('Versio uzanta enskribo')
        verbose_name_plural = _('Versioj uzantaj enskriboj')
        permissions = (
            ('povas_vidi_uzantan_enskribon_version', _('Povas vidi uzantan enskribon version')),
            ('povas_forigi_uzantan_enskribon_version', _('Povas forigi uzantan enskribon version')),
            ('povas_restarigi_uzantan_enskribon_version', _('Povas restarigi uzantan enskribon version')),
        )

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = self._meta.model
            next_id = model.objects.filter(
                posedanto=self.posedanto, lingvo=self.lingvo
            ).aggregate(models.Max('id'))['id__max']

            next_id = 1 if next_id is None else next_id + 1

            super().__setattr__('id', next_id)

        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)


class VersioKomunumoEnskribo(VersioLingvoAbstracta):
    posedanto = models.ForeignKey('muroj.MuroEnskribo', verbose_name=_('Originala Enskribo'),
                                  blank=False, null=False, on_delete=models.CASCADE)

    class Meta:
        db_table = 'versioj_komunumoj_enskriboj'
        index_together = (
            ('posedanto', 'id', 'lingvo'),
        )
        verbose_name = _('Versio komunuma enskribo')
        verbose_name_plural = _('Versioj komunumaj enskriboj')
        permissions = (
            ('povas_vidi_komunuman_enskribon_version', _('Povas vidi komunuman enskribon version')),
            ('povas_forigi_komunuman_enskribon_version', _('Povas forigi komunuman enskribon version')),
            ('povas_restarigi_komunuman_enskribon_version', _('Povas restarigi komunuman enskribon version')),
        )

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = self._meta.model
            next_id = model.objects.filter(
                posedanto=self.posedanto, lingvo=self.lingvo
            ).aggregate(models.Max('id'))['id__max']

            next_id = 1 if next_id is None else next_id + 1

            super().__setattr__('id', next_id)

        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)


class VersioAkademioPagxo(VersioLingvoAbstracta):
    posedanto = models.ForeignKey('akademio.AkademioPagxo', verbose_name=_('Originala akademio pagxo'),
                                  blank=False, null=False, on_delete=models.CASCADE)

    class Meta:
        # имя таблицы в БД
        db_table = 'versioj_akademioj_pagxoj'
        # индексируемые поля
        index_together = (
            ('posedanto', 'id', 'lingvo'),
        )
        # название объекта модели
        verbose_name = _('Versio akademio pagxo')
        # название объекта модели во множественном числе
        verbose_name_plural = _('Versioj akademioj pagxoj')
        # права на модель
        permissions = (
            ('povas_vidi_akademio_pagxo_version', _('Povas vidi akademion pagxon version')),
            ('povas_forigi_akademio_pagxo_version', _('Povas forigi akademion pagxon version')),
            ('povas_restarigi_akademio_pagxo_version', _('Povas restarigi akademion pagxon version')),
        )

    # реализуем автоинвремент поля id для конкретного родительского объекта
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = self._meta.model
            next_id = model.objects.filter(
                posedanto=self.posedanto, lingvo=self.lingvo
            ).aggregate(models.Max('id'))['id__max']

            next_id = 1 if next_id is None else next_id + 1

            super().__setattr__('id', next_id)

        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)


class VersioEnciklopedioPagxo(VersioLingvoAbstracta):
    posedanto = models.ForeignKey('enciklopedio.EnciklopedioPagxo', verbose_name=_('Originala enciklopedio pagxo'),
                                  blank=False, null=False, on_delete=models.CASCADE)

    class Meta:
        db_table = 'versioj_enciklopedioj_pagxoj'
        index_together = (
            ('posedanto', 'id', 'lingvo'),
        )
        verbose_name = _('Versio enciklopedio pagxo')
        verbose_name_plural = _('Versioj enciklopedioj pagxoj')
        permissions = (
            ('povas_vidi_enciklopedio_pagxo_version', _('Povas vidi enciklopedion pagxon version')),
            ('povas_forigi_enciklopedio_pagxo_version', _('Povas forigi enciklopedion pagxon version')),
            ('povas_restarigi_enciklopedio_pagxo_version', _('Povas restarigi enciklopedion pagxon version')),
        )

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = self._meta.model
            next_id = model.objects.filter(
                posedanto=self.posedanto, lingvo=self.lingvo
            ).aggregate(models.Max('id'))['id__max']

            next_id = 1 if next_id is None else next_id + 1

            super().__setattr__('id', next_id)

        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

class VersioKodoPagxo(VersioLingvoAbstracta):
    posedanto = models.ForeignKey('kodo.KodoPagxo', verbose_name=_('Originala kodo pagxo'),
                                  blank=False, null=False, on_delete=models.CASCADE)

    class Meta:
        db_table = 'versioj_kodoj_pagxoj'
        index_together = (
            ('posedanto', 'id', 'lingvo'),
        )
        verbose_name = _('Versio kodo pagxo')
        verbose_name_plural = _('Versioj kodoj pagxoj')
        permissions = (
            ('povas_vidi_kodo_pagxo_version', _('Povas vidi kodon pagxon version')),
            ('povas_forigi_kodo_pagxo_version', _('Povas forigi kodon pagxon version')),
            ('povas_restarigi_kodo_pagxo_version', _('Povas restarigi kodon pagxon version')),
        )

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = self._meta.model
            next_id = model.objects.filter(
                posedanto=self.posedanto, lingvo=self.lingvo
            ).aggregate(models.Max('id'))['id__max']

            next_id = 1 if next_id is None else next_id + 1

            super().__setattr__('id', next_id)

        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

class VersioKonferencojTemoKomento(VersioLingvoAbstracta):
    posedanto = models.ForeignKey('konferencoj.KonferencojTemoKomento', verbose_name=_('Originala konferencoj temo komento'),
                                  blank=False, null=False, on_delete=models.CASCADE)

    class Meta:
        db_table = 'versioj_konferencoj_temo_komento'
        index_together = (
            ('posedanto', 'id', 'lingvo'),
        )
        verbose_name = _('Versio konferencoj temo komento')
        verbose_name_plural = _('Versioj konferencoj temoj komentoj')
        permissions = (
            ('povas_vidi_konferencoj_temon_komenton_version', _('Povas vidi konferencoj temon komenton version')),
            ('povas_forigi_konferencoj_temon_komenton_version', _('Povas forigi konferencoj temon komenton version')),
            ('povas_restarigi_konferencoj_temon_komenton_version', _('Povas restarigi konferencoj temon komenton version')),
        )

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = self._meta.model
            next_id = model.objects.filter(
                posedanto=self.posedanto, lingvo=self.lingvo
            ).aggregate(models.Max('id'))['id__max']

            next_id = 1 if next_id is None else next_id + 1

            super().__setattr__('id', next_id)

        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

class VersioKonferencojTemo(VersioLingvoAbstracta):
    posedanto = models.ForeignKey('konferencoj.KonferencojTemo', verbose_name=_('Originala konferencoj temo'),
                                  blank=False, null=False, on_delete=models.CASCADE)

    class Meta:
        db_table = 'versioj_konferencoj_temo'
        index_together = (
            ('posedanto', 'id', 'lingvo'),
        )
        verbose_name = _('Versio konferencoj temo')
        verbose_name_plural = _('Versioj konferencoj temoj')
        permissions = (
            ('povas_vidi_konferencoj_temon_version', _('Povas vidi konferencoj temon version')),
            ('povas_forigi_konferencoj_temon_version', _('Povas forigi konferencoj temon version')),
            ('povas_restarigi_konferencoj_temon_version', _('Povas restarigi konferencoj temon version')),
        )

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = self._meta.model
            next_id = model.objects.filter(
                posedanto=self.posedanto, lingvo=self.lingvo
            ).aggregate(models.Max('id'))['id__max']

            next_id = 1 if next_id is None else next_id + 1

            super().__setattr__('id', next_id)

        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

class VersioKonferencojKategorio(VersioLingvoAbstracta):
    posedanto = models.ForeignKey('konferencoj.KonferencojKategorio', verbose_name=_('Originala konferencoj kategorio'),
                                  blank=False, null=False, on_delete=models.CASCADE)

    class Meta:
        db_table = 'versioj_konferencoj_kategorio'
        index_together = (
            ('posedanto', 'id', 'lingvo'),
        )
        verbose_name = _('Versio konferencoj kategorio')
        verbose_name_plural = _('Versioj konferencoj kategorioj')
        permissions = (
            ('povas_vidi_konferencoj_kategorion_version', _('Povas vidi konferencoj kategorion version')),
            ('povas_forigi_konferencoj_kategorion_version', _('Povas forigi konferencoj kategorion version')),
            ('povas_restarigi_konferencoj_kategorion_version', _('Povas restarigi konferencoj kategorion version')),
        )

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = self._meta.model
            next_id = model.objects.filter(
                posedanto=self.posedanto, lingvo=self.lingvo
            ).aggregate(models.Max('id'))['id__max']

            next_id = 1 if next_id is None else next_id + 1

            super().__setattr__('id', next_id)

        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

class VersioEsploradojTemoKomento(VersioLingvoAbstracta):
    posedanto = models.ForeignKey('esploradoj.EsploradojTemoKomento', verbose_name=_('Originala esploradoj temo komento'),
                                  blank=False, null=False, on_delete=models.CASCADE)

    class Meta:
        db_table = 'versioj_esploradoj_temo_komento'
        index_together = (
            ('posedanto', 'id', 'lingvo'),
        )
        verbose_name = _('Versio esploradoj temo komento')
        verbose_name_plural = _('Versioj esploradoj temoj komentoj')
        permissions = (
            ('povas_vidi_esploradoj_temon_komenton_version', _('Povas vidi esploradoj temon komenton version')),
            ('povas_forigi_esploradoj_temon_komenton_version', _('Povas forigi esploradoj temon komenton version')),
            ('povas_restarigi_esploradoj_temon_komenton_version', _('Povas restarigi esploradoj temon komenton version')),
        )

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = self._meta.model
            next_id = model.objects.filter(
                posedanto=self.posedanto, lingvo=self.lingvo
            ).aggregate(models.Max('id'))['id__max']

            next_id = 1 if next_id is None else next_id + 1

            super().__setattr__('id', next_id)

        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

class VersioEsploradojTemo(VersioLingvoAbstracta):
    posedanto = models.ForeignKey('esploradoj.EsploradojTemo', verbose_name=_('Originala esploradoj temo'),
                                  blank=False, null=False, on_delete=models.CASCADE)

    class Meta:
        db_table = 'versioj_esploradoj_temo'
        index_together = (
            ('posedanto', 'id', 'lingvo'),
        )
        verbose_name = _('Versio esploradoj temo')
        verbose_name_plural = _('Versioj esploradoj temoj')
        permissions = (
            ('povas_vidi_esploradoj_temon_version', _('Povas vidi esploradoj temon version')),
            ('povas_forigi_esploradoj_temon_version', _('Povas forigi esploradoj temon version')),
            ('povas_restarigi_esploradoj_temon_version', _('Povas restarigi esploradoj temon version')),
        )

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = self._meta.model
            next_id = model.objects.filter(
                posedanto=self.posedanto, lingvo=self.lingvo
            ).aggregate(models.Max('id'))['id__max']

            next_id = 1 if next_id is None else next_id + 1

            super().__setattr__('id', next_id)

        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

class VersioEsploradojKategorio(VersioLingvoAbstracta):
    posedanto = models.ForeignKey('esploradoj.EsploradojKategorio', verbose_name=_('Originala esploradoj kategorio'),
                                  blank=False, null=False, on_delete=models.CASCADE)

    class Meta:
        db_table = 'versioj_esploradoj_kategorio'
        index_together = (
            ('posedanto', 'id', 'lingvo'),
        )
        verbose_name = _('Versio esploradoj kategorio')
        verbose_name_plural = _('Versioj esploradoj kategorioj')
        permissions = (
            ('povas_vidi_esploradoj_kategorion_version', _('Povas vidi esploradoj kategorion version')),
            ('povas_forigi_esploradoj_kategorion_version', _('Povas forigi esploradoj kategorion version')),
            ('povas_restarigi_esploradoj_kategorion_version', _('Povas restarigi esploradoj kategorion version')),
        )

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = self._meta.model
            next_id = model.objects.filter(
                posedanto=self.posedanto, lingvo=self.lingvo
            ).aggregate(models.Max('id'))['id__max']

            next_id = 1 if next_id is None else next_id + 1

            super().__setattr__('id', next_id)

        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

class VersioMesagxiloMesagxo(VersioLingvoAbstracta):
    posedanto = models.ForeignKey('mesagxilo.MesagxiloMesagxo', verbose_name=_('Originala mesagxilo mesagxo'),
                                  blank=False, null=False, on_delete=models.CASCADE)

    class Meta:
        db_table = 'versioj_mesagxilo_mesagxo'
        index_together = (
            ('posedanto', 'id', 'lingvo'),
        )
        verbose_name = _('Versio mesagxilo mesagxo')
        verbose_name_plural = _('Versioj mesagxilo mesagxo')
        permissions = (
            ('povas_vidi_mesagxilon_mesagxon_version', _('Povas vidi mesagxilon mesagxon version')),
            ('povas_forigi_mesagxilon_mesagxon_version', _('Povas forigi mesagxilon mesagxon version')),
            ('povas_restarigi_mesagxilon_mesagxon_version', _('Povas restarigi mesagxilon mesagxon version')),
        )

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = self._meta.model
            next_id = model.objects.filter(
                posedanto=self.posedanto, lingvo=self.lingvo
            ).aggregate(models.Max('id'))['id__max']

            next_id = 1 if next_id is None else next_id + 1

            super().__setattr__('id', next_id)

        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)
