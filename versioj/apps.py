"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.apps import AppConfig


class VersiojConfig(AppConfig):
    name = 'versioj'

    def ready(self):
        import versioj.signals
