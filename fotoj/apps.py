"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class FotojConfig(AppConfig):
    name = 'fotoj'
    verbose_name = _('Fotoj')

    def ready(self):
        import fotoj.signals
