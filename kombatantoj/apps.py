"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class KombatantojConfig(AppConfig):
    name = 'kombatantoj'
    verbose_name = _('Kombatantoj')

    def ready(self):
        import kombatantoj.signals
