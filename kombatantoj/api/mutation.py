"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.db import transaction
import graphene
import requests
from django.conf import settings

from siriuso.api.types import ErrorNode
from siriuso.utils import set_enhavo
from komunumoj.models import Komunumo
from main.models import Uzanto
from .scheme import KombatantoNode, KombatantoResursoNomenklaturoNode, KombatantoResursoNode, KontaktoTipoNode
from ..models import *
from organizoj.models import Organizo


# Создание/редактирование моделей типов контактов
class RedaktuKontaktoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    kontakto_tipo = graphene.Field(KontaktoTipoNode,
        description=_('Созданный/изменённый модель типа контактов'))
    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        kodo = graphene.String(description=_('Код'))

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        kontakto_tipo = None
        user = info.context.user

        if user.has_perm('kombatantoj.povas_krei_kombatanton_tipo_kontakto'):
            tipo = None
            speco = None
            posedanto = None
            komunumo = None
            uzanto = None

            if not kwargs.get('kodo', False):
                errors.append(ErrorNode(
                    field='kodo',
                    message=_('Поле обязательно для заполнения')
                ))

            if not kwargs.get('nomo'):
                errors.append(ErrorNode(
                    field='nomo',
                    message=_('Поле обязательно для заполнения')
                ))

            if not len(errors):
                kontakto_tipo = KontaktoTipo.objects.create(
                    kodo=kwargs.get('kodo'),
                )

                if (kwargs.get('nomo', False)):
                    set_enhavo(kontakto_tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                kontakto_tipo.save()
                status = True
                message = _('kontakto_tipo sukcese kreiĝis')
                errors = list()
            else:
                message = _('Nevalida argumentvaloroj')
                for error in errors:
                    message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                        error.message, _('в поле'), error.field)
        else:
            message = _('Nesufiĉaj rajtoj')

        return status, message, errors, kontakto_tipo

    @staticmethod
    def edit(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        kontakto_tipo = None
        user = info.context.user

        if (user.has_perm('kombatantoj.povas_shangxi_kombatanton_tipo_kontakto')
                or user.has_perm('kombatantoj.povas_forigi_kombatanton_tipo_kontakto')):
            pass
        else:
            message = _('Nesufiĉaj rajtoj')

        return status, message, errors, kontakto_tipo

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        kontakto_tipo = None
        user = info.context.user

        if user.is_authenticated:
            if 'uuid' in kwargs:
                # Изменяем запись
                status, message, errors, kontakto_tipo = RedaktuKontaktoTipo.edit(root, info, **kwargs)
            else:
                # Создаём запись
                status, message, errors, kontakto_tipo = RedaktuKontaktoTipo.create(root, info, **kwargs)
        else:
            message = _('Bezonata rajtigo')

        return RedaktuKontaktoTipo(status=status, message=message, errors=errors, kontakto_tipo=kontakto_tipo)


class RedaktuKombatanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    kombatanto = graphene.Field(KombatantoNode)

    class Arguments:
        uuid = graphene.UUID()
        nomo = graphene.String()
        unua_nomo = graphene.String()
        dua_nomo = graphene.String()
        familinomo = graphene.String()
        sekso = graphene.String()
        publikigo = graphene.Boolean()
        arkivo = graphene.Boolean()
        forigo = graphene.Boolean()
        tipo_kodo = graphene.String()
        speco_kodo = graphene.String()
        posedanto_uuid = graphene.UUID()
        adreso = graphene.String()
        prezaj_politikoj = graphene.String()
        informoj = graphene.String()
        komunumo_uuid = graphene.UUID()
        uzanto_id = graphene.Int()

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        kombatanto = None
        user = info.context.user

        if user.has_perm('kombatantoj.povas_krei_kombatanton'):
            tipo = None
            speco = None
            posedanto = None
            komunumo = None
            uzanto = None

            # Проверяем тип
            if not kwargs.get('tipo_kodo', False):
                errors.append(ErrorNode(
                    field='tipo_kodo',
                    message=_('Bezonata kampo')
                ))
            else:
                try:
                    tipo = KombatantoTipo.objects.get(kodo=kwargs.get('tipo_kodo'))
                except KombatantoTipo.DoesNotExist:
                    errors.append(ErrorNode(
                        field='tipo_kodo',
                        message=_('Tajpu ĉi tiun kodon ne trovitan')
                    ))

            # Проверяем вид
            if not kwargs.get('speco_kodo', False):
                errors.append(ErrorNode(
                    field='speco_kodo',
                    message=_('Bezonata kampo')
                ))
            else:
                try:
                    speco = KombatantoSpeco.objects.get(kodo=kwargs.get('speco_kodo'))
                except KombatantoSpeco.DoesNotExist:
                    errors.append(ErrorNode(
                        field='speco_kodo',
                        message=_('Vido kun ĉi tiu kodo ne trovita')
                    ))

            # Проверяем владельца
            if 'posedanto_uuid' in kwargs:
                try:
                    posedanto = Kombatanto.objects.get(uuid=kwargs.get('posedanto_uuid'))
                except Kombatanto.DoesNotExist:
                    errors.append(ErrorNode(
                        field='posedanto_uuid',
                        message=_('Posedanto ne trovita')
                    ))

            # Проверяем сообщество
            if 'komunumo_uuid' in kwargs and 'uzanto_id' in kwargs:
                errors = errors + [
                    ErrorNode(
                        field='komunumo_uuid',
                        message='{} {}'.format(
                            _('Ne eblas specifi samtempe kun'),
                            'komunumo_uuid'
                        )
                    ),
                    ErrorNode(
                        field='uzanto_id',
                        message='{} {}'.format(
                            _('Ne eblas specifi samtempe kun'),
                            'uzanto_id'
                        )
                    )
                ]
            elif 'komunumo_uuid' in kwargs:
                try:
                    komunumo = Komunumo.objects.get(uuid=kwargs.get('komunumo_uuid'), forigo=False,
                                                    arkivo=False, publikigo=True)
                except Komunumo.DoesNotExist:
                    errors.append(ErrorNode(
                        field='komunumo_uuid',
                        message=_('Komunumo ne trovita')
                    ))
            elif 'uzanto_id' in kwargs:
                try:
                    uzanto = Uzanto.objects.get(uuid=kwargs.get('komunumo_uuid'), forigo=False,
                                                arkivo=False, publikigo=True)
                except Uzanto.DoesNotExist:
                    errors.append(ErrorNode(
                        field='komunumo_uuid',
                        message=_('Uzanto ne trovita')
                    ))

            if kwargs.get('forigo', False):
                errors.append(ErrorNode(
                    field='forigo',
                    message=_('Ne eblas forviŝi dum kreado')
                ))

            if kwargs.get('arkivo', False):
                errors.append(ErrorNode(
                    field='arkivo',
                    message=_('Ne eblas arkivi dum kreado')
                ))

            if 'prezaj_politikoj' in kwargs and kwargs.get('prezaj_politikoj').lower() not in (
                    'podetala', 'pogranda', 'filio'
            ):
                errors.append(ErrorNode(
                    field='prezaj_politikoj',
                    message=_('Nevalida valoro')
                ))

            if tipo:
                if tipo.kodo == 'homo':
                    if kwargs.get('nomo', False):
                        errors.append(ErrorNode(
                            field='nomo',
                            message=_('Ĉi tiu kampo ne validas por tipo "Homo"')
                        ))

                    if not kwargs.get('sekso', False):
                        errors.append(ErrorNode(
                            field='sekso',
                            message=_('Bezonata kampo')
                        ))
                    elif kwargs.get('sekso').lower() not in ('vira', 'virina'):
                        errors.append(ErrorNode(
                            field='sekso',
                            message=_('Nevalida valoro')
                        ))

                    if not kwargs.get('unua_nomo', False):
                        errors.append(ErrorNode(
                            field='unua_nomo',
                            message=_('Bezonata kampo')
                        ))

                    if not kwargs.get('familinomo', False):
                        errors.append(ErrorNode(
                            field='familinomo',
                            message=_('Bezonata kampo')
                        ))
                else:
                    if kwargs.get('unua_nomo', False):
                        errors.append(ErrorNode(
                            field='unua_nomo',
                            message=_('Ĉi tiu kampo estas nur por tipo "Homo"')
                        ))

                    if kwargs.get('familinomo', False):
                        errors.append(ErrorNode(
                            field='familinomo',
                            message=_('Ĉi tiu kampo estas nur por tipo "Homo"')
                        ))

                    if kwargs.get('sekso', False):
                        errors.append(ErrorNode(
                            field='sekso',
                            message=_('Ĉi tiu kampo estas nur por tipo "Homo"')
                        ))

                    if not kwargs.get('nomo', False):
                        errors.append(ErrorNode(
                            field='nomo',
                            message=_('Bezonata kampo')
                        ))

            if not len(errors):
                kombatanto = Kombatanto.objects.create(
                    autoro=user,
                    forigo=False,
                    arkivo=False,
                    publikigo=True,
                    publikiga_dato=timezone.now(),
                    tipo=tipo,
                    speco=speco,
                    posedanto=posedanto,
                    adreso=kwargs.get('adreso', ''),
                    prezaj_politikoj=kwargs.get('prezaj_politikoj', 'podetala').lower(),
                    komunumo=komunumo,
                    uzanto=uzanto
                )

                if tipo.kodo == 'homo':
                    set_enhavo(kombatanto.unua_nomo, kwargs.get('unua_nomo'), 'ru_RU')
                    set_enhavo(kombatanto.dua_nomo, kwargs.get('dua_nomo'), 'ru_RU')
                    set_enhavo(kombatanto.familinomo, kwargs.get('familinomo'), 'ru_RU')
                    kombatanto.sekso = kwargs.get('sekso').lower()  if 'sekso' in kwargs else None
                else:
                    set_enhavo(kombatanto.nomo, kwargs.get('nomo'), 'ru_RU')

                kombatanto.save()
                status = True
                message = _('Kombatanto sukcese kreiĝis')
                errors = list()
            else:
                message = _('Nevalida argumentvaloroj')
                for error in errors:
                    message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                        error.message, _('в поле'), error.field)
        else:
            message = _('Nesufiĉaj rajtoj')

        return status, message, errors, kombatanto

    @staticmethod
    def edit(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        kombatanto = None
        user = info.context.user

        if (user.has_perm('kombatantoj.povas_shanghi_kombatanton')
                or user.has_perm('kombatantoj.povas_forigi_kombatanton')):
            pass
        else:
            message = _('Nesufiĉaj rajtoj')

        return status, message, errors, kombatanto

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        kombatanto = None
        user = info.context.user

        if user.is_authenticated:
            if 'uuid' in kwargs:
                # Изменяем запись
                status, message, errors, kombatanto = RedaktuKombatanto.edit(root, info, **kwargs)
            else:
                # Создаём запись
                status, message, errors, kombatanto = RedaktuKombatanto.create(root, info, **kwargs)
        else:
            message = _('Bezonata rajtigo')

        return RedaktuKombatanto(status=status, message=message, errors=errors, kombatanto=kombatanto)


class RedaktuKombatantoNomenklaturo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    organizo_resurso_nomenklaturo = graphene.Field(KombatantoResursoNomenklaturoNode)

    class Arguments:
        uuid = graphene.UUID()
        organizo_uuid = graphene.UUID()
        stokado_uuid = graphene.UUID()
        minimuma = graphene.Float()
        maksimuma = graphene.Float()
        publikigo = graphene.Boolean()
        forigo = graphene.Boolean()

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = []
        organizo = None
        stokado = None
        nomenklaturo = None
        user = info.context.user

        if (user.has_perm('kombatantoj.povas_krei_resurson_nomenklaturon')
                or user.has_perm('kombatantoj.povas_shanghi_resurson_nomenklaturon')
                or user.has_perm('kombatantoj.povas_forigi_resurson_nomenklaturon')):
            # Создаём новую запись
            if 'uuid' not in kwargs:
                if user.has_perm('kombatantoj.povas_krei_resurson_nomenklaturon'):
                    if 'organizo_uuid' not in kwargs:
                        errors.append(
                            ErrorNode(
                                field='organizo_uuid',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    else:
                        try:
                            organizo = Kombatanto.objects.get(uuid=kwargs.get('organizo_uuid'), forigo=False, publikigo=True)
                        except Kombatanto.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='organizo_uuid',
                                    message=_('Организация не найдена')
                                )
                            )

                    if 'stokado_uuid' not in kwargs:
                        errors.append(
                            ErrorNode(
                                field='stokado_uuid',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    elif organizo is None:
                        errors.append(
                            ErrorNode(
                                field='stokado_uuid',
                                message=_('Невозможно определить место хранения')
                            )
                        )
                    else:
                        try:
                            stokado = Kombatanto.objects.get(uuid=kwargs.get('stokado_uuid'), posedanto=organizo,
                                                           publikigo=True, forigo=False)
                        except Kombatanto.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='stokado_uuid',
                                    message=_('Место хранения не найдено')
                                )
                            )

                    if 'minimuma' not in kwargs:
                        errors.append(
                            ErrorNode(
                                field='minimuma',
                                message=_('Поле обязательно для заполнения')
                            )
                        )
                    elif kwargs.get('minimuma') < 0:
                        errors.append(
                            ErrorNode(
                                field='minimuma',
                                message=_('Значени должно быть положительным')
                            )
                        )

                    if 'maksimuma' in kwargs:
                        if kwargs.get('maksimuma') < 0:
                            errors.append(
                                ErrorNode(
                                    field='maksimuma',
                                    message=_('Значени должно быть положительным')
                                )
                            )
                        elif kwargs.get('maksimuma') < kwargs.get('minimuma') and kwargs.get('maksimuma'):
                            errors.append(
                                ErrorNode(
                                    field='maksimuma',
                                    message=_('Значени должно быть больше минимального объёма')
                                )
                            )

                    if 'forigo' in kwargs and kwargs.get('forigo'):
                        errors.append(
                            ErrorNode(
                                field='forigo',
                                message=_('Нельзя удалить не созданный ресурс')
                            )
                        )

                    if not len(errors):
                        nomenklaturo = KombatantoResursoNomenklaturo.objects.create(
                            organizo=organizo,
                            stokado=stokado,
                            autoro=user,
                            minimuma=kwargs.get('minimuma'),
                            maksimuma=kwargs.get('maksimuma'),
                            publikigo=kwargs.get('publikigo', False),
                            forigo=kwargs.get('forigo', False)
                        )

                        status = True
                        message = _('Запись успешно создана')
                    else:
                        message = _('Неверные значения аргументов')
                        for error in errors:
                            message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                error.message, _('в поле'), error.field)
                else:
                    message = _('Недостаточно прав на создание записи')
            else:
                # Изменяем запись
                try:
                    nomenklaturo = KombatantoResursoNomenklaturo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                except:
                    errors.append(
                        ErrorNode(
                            field='uuid',
                            message=_('Запись не найдена')
                        )
                    )

                if kwargs.get('forigo', False) and not user.has_perm('kombatantoj.povas_forigi_resurson_nomenklaturon'):
                    errors.append(
                        ErrorNode(
                            field='forigo',
                            message=_('Недостаточно прав на удаление')
                        )
                    )

                if not user.has_perm('kombatantoj.povas_shanghi_resurson_nomenklaturon') and (
                    'organizo_uuid' in kwargs or 'publikigo' in kwargs
                ):
                    message = _('Недостаточно прав для изменения записи')

                else:
                    if 'organizo_uuid' in kwargs:
                        try:
                            organizo = Kombatanto.objects.get(uuid=kwargs.get('organizo_uuid'), forigo=False, publikigo=True)
                        except Kombatanto.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='organizo_uuid',
                                    message=_('Организация не найдена')
                                )
                            )
                    else:
                        organizo = nomenklaturo.organizo

                    if 'stokado_uuid' in kwargs:
                        try:
                            stokado = Kombatanto.objects.get(uuid=kwargs.get('stokado_uuid'), posedanto=organizo,
                                                           publikigo=True, forigo=False)
                        except Kombatanto.DoesNotExist:
                            errors.append(
                                ErrorNode(
                                    field='stokado_uuid',
                                    message=_('Место хранения не найдено')
                                )
                            )

                    if kwargs.get('minimuma', 0) < 0:
                        errors.append(
                            ErrorNode(
                                field='minimuma',
                                message=_('Значени должно быть положительным')
                            )
                        )

                    if 'maksimuma' in kwargs and nomenklaturo:
                        if kwargs.get('maksimuma') < 0:
                            errors.append(
                                ErrorNode(
                                    field='maksimuma',
                                    message=_('Значени должно быть положительным')
                                )
                            )
                        elif (kwargs.get('maksimuma') and
                              kwargs.get('maksimuma') < kwargs.get('minimuma', nomenklaturo.minimuma)
                              and kwargs.get('maksimuma')):
                            errors.append(
                                ErrorNode(
                                    field='maksimuma',
                                    message=_('Значени должно быть больше минимального объёма')
                                )
                            )

                    if not len(errors):
                        nomenklaturo.organizo = organizo or nomenklaturo.organizo
                        nomenklaturo.stokado = stokado or nomenklaturo.stokado
                        nomenklaturo.minimuma = kwargs.get('minimuma', nomenklaturo.minimuma)
                        nomenklaturo.maksimuma = kwargs.get('maksimuma', nomenklaturo.maksimuma) or None
                        nomenklaturo.publikigo = kwargs.get('publikigo', nomenklaturo.publikigo)
                        nomenklaturo.forigo = kwargs.get('forigo', nomenklaturo.forigo)
                        nomenklaturo.save()

                        status = True
                        message = _('Запись успешно изменена')
                    else:
                        nomenklaturo = None
                        message = _('Неверные значения аргументов')
                        for error in errors:
                            message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                error.message, _('в поле'), error.field)

        else:
            message = _('Недостаточно прав')

        return RedaktuKombatantoNomenklaturo(status=status, message=message, errors=errors,
                                           organizo_resurso_nomenklaturo=nomenklaturo)


class ForigiKombatantoResurso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    organizo_resurso = graphene.Field(KombatantoResursoNode)

    class Arguments:
        resurso_uuid = graphene.UUID(required=True)
        resanigi = graphene.Boolean()

    @staticmethod
    def mutate(root, info, resurso_uuid, **kwargs):
        status = False
        message = None
        resurso = None
        user = info.context.user
        homo = None

        if user.is_authenticated:
            with transaction.atomic():
                # не верно настроены права - нужно будет исправить данные права. Сейчас еще не настроен приём товара.
                # if not user.has_perm('dokumentoj.drinkejisto'):
                message = _('Недостаточно прав для списания товара')

                try:
                    resurso = KombatantoResurso.objects.get(uuid=resurso_uuid, publikigo=True, forigo=False)

                    if resurso.elcherpita and not kwargs.get('resanigi', False):
                        message = _('Товар уже списан')

                except KombatantoResurso.DoesNotExist:
                    message = _('Товар не найден')

                if not message and 'resanigi' in kwargs:
                    # не верно настроены права - нужно будет исправить данные права. Сейчас еще не настроен приём товара.
                    # if not user.has_perm('dokumentoj.organiza_administranto'):
                    message = _('Недостаточно прав для использования аргумента')

                if not message:
                    resurso.elcherpita = not kwargs.get('resanigi', False)
                    resurso.save()

                    try:
                        homo = Kombatanto.objects.get(uzanto=user)

                        if resurso.elcherpita:
                            KombatantoBonusPunkto.objects.create(
                                homo=homo,
                                resurso_nomenklaturo=resurso,
                                resurso=resurso.resurso,
                                bonusoj_punktoj=resurso.resurso.bonus_punkto,
                                publikigo=True,
                                forigo=False
                            )
                        else:
                            KombatantoBonusPunkto.objects.select_for_update().filter(
                                publikigo=True,
                                forigo=False,
                                homo=homo,
                                resurso_nomenklaturo=resurso,
                                resurso=resurso.resurso
                            ).update(forigo=True)
                    except Kombatanto.DoesNotExist:
                        pass

                    status = True
                    message = _('Товар успешно списан')

        else:
            message = _('Необходимо авторизироваться')

        return ForigiKombatantoResurso(status=status, message=message, organizo_resurso=resurso)


class ImportoKontakto1c(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    kombatanto = graphene.Field(KombatantoNode)

    class Arguments:
        adreso = graphene.String(description=_('Адрес сервера, откуда производить импорт данных'))

    # создание контактов через процедуру
    # def AldoniKontakto(tipo_retpowto, tipo_telefono, tipo_adreso, tipo_alia, tipo, speco):
    #     pass


    # готовим (подгото́в||ить prepari) объекты для создания контакта по умолчанию при импорте
    @staticmethod
    def PrepariKontakto(errors):
        tipo_retpowto = None
        tipo_telefono = None
        tipo_adreso = None
        tipo_alia = None
        # запоминаем тип почты
        try:
            tipo_retpowto = KontaktoTipo.objects.get(kodo='retpowto')
        except KontaktoTipo.DoesNotExist:
            errors.append(ErrorNode(
                field='retpowto',
                message=_('Ошибочный тип контакта - электронная почта')
            ))
        try:
            tipo_telefono = KontaktoTipo.objects.get(kodo='telefono')
        except KontaktoTipo.DoesNotExist:
            errors.append(ErrorNode(
                field='telefono',
                message=_('Ошибочный тип контакта - телефон')
            ))
        try:
            tipo_adreso = KontaktoTipo.objects.get(kodo='adreso')
        except KontaktoTipo.DoesNotExist:
            errors.append(ErrorNode(
                field='adreso',
                message=_('Ошибочный тип контакта - адрес')
            ))
        try:
            tipo_alia = KontaktoTipo.objects.get(kodo='alia')
        except KontaktoTipo.DoesNotExist:
            errors.append(ErrorNode(
                field='alia',
                message=_('Ошибочный тип контакта - другой')
            ))
        return errors, tipo_retpowto, tipo_telefono, tipo_adreso, tipo_alia

    @staticmethod
    def PrepariKombatanto(errors):
        tipo = None
        speco = None
        try:
            tipo = KombatantoTipo.objects.get(kodo='homo')
        except KombatantoTipo.DoesNotExist:
            errors.append(ErrorNode(
                field='tipo_kodo',
                message=_('Tajpu ĉi tiun kodon ne trovitan')
            ))
        # Проверяем вид
        try:
            speco = KombatantoSpeco.objects.get(kodo='fizikapersono')
        except KombatantoSpeco.DoesNotExist:
            errors.append(ErrorNode(
                field='speco_kodo',
                message=_('Vido kun ĉi tiu kodo ne trovita')
            ))
        return errors, tipo, speco

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        kombatanto = None
        user = info.context.user

        url = settings.ODATA_URL+'Catalog_КонтактныеЛица?$top=10000&$format=application/json&$inlinecount=allpages'
        login = settings.ODATA_USER
        password = settings.ODATA_PASSWORD

        if user.has_perm('kombatantoj.povas_krei_kombatanton'):
            posedanto = None
            komunumo = None
            uzanto = None
            errors = list()

            with transaction.atomic():
                r = requests.get(url,auth=(login, password), verify=False)
                i = 0
                kvanto = 0
                jsj = r.json()

                errors, tipo_retpowto, tipo_telefono, tipo_adreso, tipo_alia = ImportoKontakto1c.PrepariKontakto(errors)
                errors, tipo, speco = ImportoKontakto1c.PrepariKombatanto(errors)
                

                if not len(errors):
                    for js in jsj['value']:

                        uuid = js['Ref_Key']
                        try:
                            kombatanto = Kombatanto.objects.get(uuid=uuid, forigo=False,
                                                                arkivo=False, publikigo=True)
                            next # такой есть, идём к следующему
                        except Kombatanto.DoesNotExist:
                            pass

                            informoj = js['Code']
                            unua_nomo = js['Имя']
                            dua_nomo = js['Отчество']
                            familinomo = js['Фамилия']
                            nomo = ''
                            organizo = None
                            organizo_type = js['ОбъектВладелец_Type']
                            if organizo_type == 'StandardODATA.Catalog_Контрагенты':
                                organizo_uuid = js['ОбъектВладелец']
                                try:
                                    organizo = Organizo.objects.get(uuid=organizo_uuid, forigo=False,
                                                                        arkivo=False, publikigo=True)
                                except Organizo.DoesNotExist:
                                    message = "{} {}".format(_('Неверная организация с uuid='), organizo_uuid)

                            if not len(errors):
                                kombatanto = Kombatanto.objects.create(
                                    uuid=uuid,
                                    autoro=user,
                                    forigo=False,
                                    arkivo=False,
                                    publikigo=True,
                                    publikiga_dato=timezone.now(),
                                    tipo=tipo,
                                    speco=speco,
                                    organizo=organizo,
                                )
                                if tipo.kodo == 'homo':
                                    if unua_nomo:
                                        set_enhavo(kombatanto.unua_nomo, unua_nomo, 'ru_RU')
                                    if dua_nomo:
                                        set_enhavo(kombatanto.dua_nomo, dua_nomo, 'ru_RU')
                                    if familinomo:
                                        set_enhavo(kombatanto.familinomo, familinomo, 'ru_RU')
                                    # kombatanto.sekso = kwargs.get('sekso').lower()  if 'sekso' in kwargs else None
                                else:
                                    if nomo:
                                        set_enhavo(kombatanto.nomo, nomo, 'ru_RU')

                                if informoj:
                                    set_enhavo(kombatanto.informoj, informoj, 'ru_RU')

                                kombatanto.save()
                                kvanto += 1

                                tipo_kontakto = None
                                for kontakt in js['КонтактнаяИнформация']:
                                    if kontakt['Тип'] == 'АдресЭлектроннойПочты':
                                        tipo_kontakto = tipo_retpowto
                                    elif kontakt['Тип'] == 'Телефон':
                                        tipo_kontakto = tipo_telefono
                                    elif kontakt['Тип'] == 'Адрес':
                                        tipo_kontakto = tipo_adreso
                                    else:
                                        tipo_kontakto = tipo_alia #другой
                                    valoro = kontakt['Представление']
                                    if tipo_kontakto:
                                        kontakto = Kontakto.objects.create(
                                            autoro=user,
                                            forigo=False,
                                            arkivo=False,
                                            publikigo=True,
                                            publikiga_dato=timezone.now(),
                                            tipo=tipo_kontakto,
                                            valoro=valoro,
                                            kombatanto=kombatanto,
                                        )
                            else:
                                message = _('Nevalida argumentvaloroj')
                                for error in errors:
                                    message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                        error.message, _('в поле'), error.field)
                if not message:
                    status = True
                    errors = list()
                    if kvanto > 0:
                        message =  '{} {} {}'.format(_('Добавлено'), kvanto, _('записей'))
                    else:
                        message =  _('Не найдено новых объектов для добавления')
        else:
            message = _('Nesufiĉaj rajtoj')

        return status, message, errors, kombatanto

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        kombatanto = None
        user = info.context.user

        if user.is_authenticated:
            # Импортируем данные
            status, message, errors, kombatanto = ImportoKontakto1c.create(root, info, **kwargs)
        else:
            message = _('Bezonata rajtigo')

        return ImportoKontakto1c(status=status, message=message, errors=errors, kombatanto=kombatanto)


class KombatantoMutation(graphene.ObjectType):
    redaktu_kombatanto = RedaktuKombatanto.Field()
    redaktu_kombatanto_nomenklaturo = RedaktuKombatantoNomenklaturo.Field()
    forigi_kombatanto_resurso = ForigiKombatantoResurso.Field()
    importo_kontakto1c = ImportoKontakto1c.Field(
        description=_('''Импорт контактов из 1с''')
    )
    redaktu_kontakto_tipo = RedaktuKontaktoTipo.Field(
        description=_('''Создание/редактирование моделей типов контактов''')
    )
