"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.dispatch import receiver
from django.db.models.signals import post_save
from django.utils import timezone

from siriuso.utils import get_enhavo, set_enhavo
from main.models import Uzanto
from .models import Kombatanto, KombatantoTipo, KombatantoSpeco


#@receiver(post_save, sender=Kombatanto)
def create_magazeno_stokado(instance, **kwargs):
    if instance.tipo.kodo in ('organizo', 'magazeno'):
        sub_tipo = 'magazeno' if instance.tipo.kodo == 'organizo' else 'stokado'
        sub_orgs = Kombatanto.objects.filter(posedanto=instance, tipo__kodo=sub_tipo, forigo=False, aktiva=True)

        if not sub_orgs:
            try:
                print(sub_tipo)
                tipo = KombatantoTipo.objects.get(kodo=sub_tipo, forigo=False, aktiva=True)
                sub_tipo_nomo = 'Основной склад' if sub_tipo == 'magazeno' else 'Основное место хранения'
                sub_org = Kombatanto.objects.create(tipo=tipo, posedanto=instance, forigo=False, aktiva=True)
                set_enhavo(sub_org.nomo, sub_tipo_nomo, lingvo='ru_RU')
                sub_org.save()
            except KombatantoTipo.DoesNotExist:
                pass


@receiver(post_save, sender=Uzanto)
def create_uzanto_kombatanto(instance, **kwargs):
    if instance.is_active:
        try:
            kombatanto = Kombatanto.objects.get(uzanto=instance, forigo=False, arkivo=False, publikigo=True,
                                                tipo__kodo='homo', speco__kodo='fizikapersono')
        except Kombatanto.DoesNotExist:
            kombatanto = None

        if not kombatanto:
            try:
                tipo = KombatantoTipo.objects.get(kodo='homo')
                speco = KombatantoSpeco.objects.get(kodo='fizikapersono')
                Kombatanto.objects.create(
                    autoro=instance,
                    forigo=False,
                    arkivo=False,
                    publikigo=True,
                    publikiga_dato=timezone.now(),
                    tipo=tipo,
                    speco=speco,
                    uzanto=instance,
                    unua_nomo=instance.unua_nomo,
                    dua_nomo=instance.dua_nomo,
                    familinomo=instance.familinomo
                )
            except (KombatantoTipo.DoesNotExist, KombatantoSpeco.DoesNotExist):
                pass
        else:
            kombatanto.unua_nomo = instance.unua_nomo
            kombatanto.dua_nomo = instance.dua_nomo
            kombatanto.familinomo = instance.familinomo
            kombatanto.save()
