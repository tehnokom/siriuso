"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import models
from django.contrib.auth.models import Permission
from django.utils.translation import gettext_lazy as _
from main.models import SiriusoBazaAbstrakta, SiriusoBazaAbstraktaKomunumoj
from django.core.validators import MinValueValidator
from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, generate_hex_id, perms
import functools

from komunumoj.models import KomunumoMembro
from organizoj.models import Organizo


class KombatantoTipo(SiriusoBazaAbstrakta):
    kodo = models.CharField(_('Kodo'), null=False, max_length=32, blank=False)
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=False, default=default_lingvo,
                              encoder=CallableEncoder)

    class Meta:
        db_table = 'kombatantoj_tipoj'
        verbose_name = _('Tipo de kombatanto')
        verbose_name_plural = _('Tipoj de kombatantoj')

    def __str__(self):
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])


class KombatantoSpeco(SiriusoBazaAbstrakta):
    kodo = models.CharField(_('Kodo'), null=False, max_length=32, blank=False)
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=False, default=default_lingvo,
                              encoder=CallableEncoder)

    class Meta:
        db_table = 'kombatantoj_specoj'
        verbose_name = _('Speco de kombatanto')
        verbose_name_plural = _('Specoj de kombatantoj')

    def __str__(self):
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])


class KombatantoRolo(SiriusoBazaAbstraktaKomunumoj):
    posedanto = models.ForeignKey('kombatantoj.Kombatanto', verbose_name=_('Kombatanto'), blank=False,
                                  null=False, on_delete=models.CASCADE)
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=False, default=default_lingvo,
                              encoder=CallableEncoder)

    class Meta:
        db_table = 'kombatantoj_roloj'
        verbose_name = _('Rolo de kombatanto')
        verbose_name_plural = _('Roloj de kombatantoj')
        permissions = (
            ('povas_vidi_kombatantan_specon', _('Povas vidi kombatantan specon')),
            ('povas_krei_kombatantan_specon', _('Povas krei kombatantan specon')),
            ('povas_forigi_kombatantan_specon', _('Povas forigi kombatantan specon')),
            ('povas_shanghi_kombatantan_specon', _('Povas ŝanĝi kombatantan specon')),
        )


class KombatantoRoloRajto(models.Model):
    posedanto = models.ForeignKey(KombatantoRolo, verbose_name=_('Rolo'), blank=False, null=False,
                                  on_delete=models.CASCADE)
    rajtoj = models.ManyToManyField('auth.Permission', blank=False)

    class Meta:
        db_table = 'kombatantoj_roloj_rajtoj'
        verbose_name = _('Rajtoj rolo')
        verbose_name_plural = _('Rajtoj roloj')


class KombatantoRoloDokumentoRajto(models.Model):
    posedanto = models.ForeignKey(KombatantoRolo, verbose_name=_('Rolo'), blank=False, null=False,
                                  on_delete=models.CASCADE)
    rajtoj = models.ManyToManyField('auth.Permission', blank=False)

    class Meta:
        db_table = 'kombatantoj_roloj_dokumentoj_rajtoj'
        verbose_name = _('Rajtoj dokumenta rolo')
        verbose_name_plural = _('Rajtoj dokumentaj roloj')


class KombatantoKategorio(SiriusoBazaAbstrakta):
    kodo = models.CharField(_('Kodo'), null=False, max_length=32, blank=False)
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=False, default=default_lingvo,
                              encoder=CallableEncoder)

    class Meta:
        db_table = 'kombatantoj_kategorioj'
        verbose_name = _('Grupo de kombatanto')
        verbose_name_plural = _('Grupoj de kombatantoj')

    def __str__(self):
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])

    @staticmethod
    def _get_perm_cond(user):
        if user.is_staff or user.is_superuser:
            return models.Q()

        cond = models.Q()

        return cond


generate_kom_hid = functools.partial(generate_hex_id, 'kombatantoj.Kombatanto')


class Kombatanto(SiriusoBazaAbstraktaKomunumoj):
    prezaj_choices = (
        ('podetala', _('Podetala')),
        ('pogranda', _('Pogranda')),
        ('filio', _('Filia')),
    )

    sekso_choices = (
        ('vira', _('Vira')),
        ('virina', _('Virina'))
    )

    hid = models.CharField(_('Hex ID'), max_length=16, db_index=True, null=False, blank=False,
                           default=generate_kom_hid)
    tipo = models.ForeignKey(KombatantoTipo, verbose_name=_('Tipo de kombatanto'),
                             on_delete=models.CASCADE, null=False)
    speco = models.ForeignKey(KombatantoSpeco, verbose_name=_('Speco de kombatanto'),
                              on_delete=models.CASCADE, null=False)
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, default=default_lingvo,
                              encoder=CallableEncoder)
    # Фамилия, Имя, Отчество и пол в случае с человеком
    unua_nomo = models.JSONField(verbose_name=_('Unua nomo'), blank=True, default=default_lingvo,
                                   encoder=CallableEncoder)
    dua_nomo = models.JSONField(verbose_name=_('Dua nomo'), blank=True, default=default_lingvo,
                                  encoder=CallableEncoder)
    familinomo = models.JSONField(verbose_name=_('Familinomo'), blank=True, default=default_lingvo,
                                    encoder=CallableEncoder)
    # пол
    sekso = models.CharField(_('Sekso'), max_length=6, choices=sekso_choices, blank=True, null=True)

    # Родительский комбатант
    posedanto = models.ForeignKey('self', on_delete=models.DO_NOTHING, blank=True, null=True)
    adreso = models.CharField(_('Adresao'), max_length=2048, blank=True, null=True)
    informoj = models.JSONField(verbose_name=_('Pliaj informoj'), blank=False,
                                  default=default_lingvo, encoder=CallableEncoder)
    kategorioj = models.ManyToManyField(KombatantoKategorio, db_table='kombatantoj_kategorioj_ligiloj', blank=True)
    prezaj_politikoj = models.CharField(_('Politiko pri prezoj'), max_length=24, choices=prezaj_choices, blank=True,
                                        null=False, default='podetala')

    komunumo = models.ForeignKey('komunumoj.Komunumo', verbose_name=_('Komunuma Posedanto'), blank=True, default=None,
                                 null=True, on_delete=models.CASCADE)
    uzanto = models.ForeignKey('main.Uzanto', verbose_name=_('Posedanto'), blank=True, default=None,
                               null=True, on_delete=models.CASCADE, related_name='kombatantoj_uzantoj')
    kontrakto = models.JSONField(_('Kontrakto'), blank=True, null=True, default=dict)
    # к какой организации относится данный кобатант
    organizo = models.ForeignKey(Organizo, verbose_name=_('Organizo'), blank=True, default=None,
                               null=True, on_delete=models.CASCADE, related_name='kombatantoj_organizoj')

    class Meta:
        db_table = 'kombatantoj'
        verbose_name = _('Kombatanto')
        verbose_name_plural = _('Kombatantoj')
        # права
        permissions = (
            ('povas_vidi_kombatanton', _('Povas vidi kombatanton')),
            ('povas_krei_kombatanton', _('Povas krei kombatanton')),
            ('povas_forigi_kombatanton', _('Povas forigi kombatanton')),
            ('povas_shanghi_kombatanton', _('Povas ŝanĝi kombatanton')),
        )

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('kombatantoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'kombatantoj.povas_vidi_kombatanton',
                'kombatantoj.povas_krei_kombatanton',
                'kombatantoj.povas_forigi_kombatanton',
                'kombatantoj.povas_shanghi_kombatanton'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='kombatantoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            if user_obj.is_admin or user_obj.is_superuser:
                return models.Q()
            # Для авторизированного пользователя
            try:
                homo = Kombatanto.objects.get(uzanto=user_obj)
                cond = models.Q(uuid=homo.posedanto_id) | models.Q(posedanto=homo.posedanto, posedanto__isnull=False)

                if (perms.has_registrita_perm('kombatantoj.povas_vidi_kombatanton')
                        or user_obj.has_perm('kombatantoj.povas_vidi_kombatanton')):
                    # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                    return models.Q()
                else:
                    # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                    cond = models.Q(uuid__isnull=True)

                #Просмотр создателям 
                cond |= (models.Q(autoro=user_obj))
                # cond |= (models.Q(uzanto__isnull=True))

                # Просмотр участникам сообществ
                komunum = KomunumoMembro.objects.filter(
                    autoro=user_obj
                ).values_list('posedanto')
                cond |= models.Q(komunumo__in=komunum)

                return cond
            except Kombatanto.DoesNotExist:
                return models.Q(uuid__isnull=True)
        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('kombatantoj.povas_vidi_kombatanton'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond

    def __str__(self):
        if self.tipo.kodo == 'sekcio' and self.posedanto:
            return '{} ({})'.format(
                get_enhavo(self.nomo, empty_values=True)[0],
                get_enhavo(self.posedanto.nomo, empty_values=True)[0]
            )

        return '({} - {}) {}'.format(
            get_enhavo(self.tipo.nomo)[0],
            get_enhavo(self.speco.nomo)[0],
            (get_enhavo(self.nomo, empty_values=True)[0] or
             '{} {}'.format(
                 get_enhavo(self.familinomo, empty_values=True)[0],
                 get_enhavo(self.unua_nomo, empty_values=True)[0])
             )
        )


class KombatantoInterna(models.Model):
    posedanto = models.ForeignKey(Kombatanto, verbose_name=_('Posedanto Kombatanto'), blank=False, null=False,
                                  on_delete=models.CASCADE, related_name='kombatanto_interna_posedanto')
    kombatanto = models.ForeignKey(Kombatanto, verbose_name=_('Kombatanto'), blank=False, null=False,
                                   on_delete=models.CASCADE, related_name='kombatanto_interna_kombatanto')
    roloj = models.ManyToManyField(KombatantoRolo, verbose_name=_('Kombatantoj'), blank=True,
                                   db_table='kombatantoj_internaj_roloj')

    class Meta:
        db_table = 'kombatantoj_internaj'
        verbose_name = _('Interna kombatanto')
        verbose_name_plural = _('Internaj kombatantoj')
        permissions = (
            ('povas_vidi_internan_kombatanton', _('Povas vidi internan kombatanton')),
            ('povas_krei_internan_kombatanton', _('Povas krei internan kombatanton')),
            ('povas_forigi_internan_kombatanton', _('Povas forigi internan kombatanton')),
            ('povas_shanghi_internan_kombatanton', _('Povas ŝanĝi internan kombatanton')),
        )

    def __str__(self):
        return '{}: {} -> {}'.format(
            _('Interna kombaranto'),
            str(self.kombatanto),
            str(self.posedanto)
        )


class KombatantoEkstera(models.Model):
    posedanto = models.ForeignKey(Kombatanto, verbose_name=_('Posedanto Kombatanto'), blank=False, null=False,
                                  on_delete=models.CASCADE, related_name='kombatanto_ekstera_posedanto')
    kombatanto = models.ForeignKey(Kombatanto, verbose_name=_('Kombatanto'), blank=False, null=False,
                                   on_delete=models.CASCADE, related_name='kombatanto_ekstera_kombatanto')
    kategorioj = models.ManyToManyField(KombatantoKategorio, verbose_name=_('Kombatantoj'), blank=False,
                                   db_table='kombatantoj_ekstera_kategorioj')

    class Meta:
        db_table = 'kombatantoj_eksteraj'
        verbose_name = _('Ekstera kombatanto')
        verbose_name_plural = _('Ekstera kombatantoj')
        permissions = (
            ('povas_vidi_eksteran_kombatanton', _('Povas vidi eksteran kombatanton')),
            ('povas_krei_eksteran_kombatanton', _('Povas krei eksteran kombatanton')),
            ('povas_forigi_eksteran_kombatanton', _('Povas forigi eksteran kombatanton')),
            ('povas_shanghi_eksteran_kombatanton', _('Povas ŝanĝi eksteran kombatanton')),
        )

    def __str__(self):
        return '{}: {} -> {}'.format(
            _('Interna kombaranto'),
            str(self.kombatanto),
            str(self.posedanto)
        )


class KombatantoResursoNomenklaturo(SiriusoBazaAbstraktaKomunumoj):
    # склад хранения
    organizo = models.ForeignKey(Kombatanto, blank=False, null=False, on_delete=models.CASCADE)
    # место хранения
    stokado = models.ForeignKey(Kombatanto, blank=False, null=False, on_delete=models.CASCADE,
                                related_name='stokado')
    # resurso = models.ForeignKey('resursoj.Resurso', blank=False, null=False, on_delete=models.CASCADE)
    minimuma = models.FloatField(_('Ekvilibro pri minimuma resurso'), blank=False, null=False,
                                 validators=[MinValueValidator(0), ])
    maksimuma = models.FloatField(_('Maksimuma resurso ekvilibro'), blank=True, null=True)

    class Meta:
        db_table = 'kombatantoj_nomenklaturoj_resursoj'
        verbose_name = _('Komandanta Nomenclatura')
        verbose_name_plural = _('Komandantaj Nomenclaturaj')
        permissions = (
            ('povas_vidi_resurson_nomenklaturon', _('Povas vidi resurson nomenklaturon')),
            ('povas_krei_resurson_nomenklaturon', _('Povas krei resurson nomenklaturon')),
            ('povas_forigi_resurson_nomenklaturon', _('Povas forigi resurson nomenklaturon')),
            ('povas_shanghi_resurson_nomenklaturon', _('Povas ŝanĝi resurson nomenklaturon')),
        )

    def __str__(self):
        return '{}'.format(get_enhavo(self.organizo.nomo, empty_values=True)[0])


generate_kom_res_hid = functools.partial(generate_hex_id, 'kombatantoj.KombatantoResurso')


class KombatantoResurso(SiriusoBazaAbstraktaKomunumoj):
    hid = models.CharField(_('Hex ID'), max_length=16, db_index=True, null=False, blank=False,
                           default=generate_kom_res_hid)
    organizo = models.ForeignKey(Kombatanto, blank=False, null=False, on_delete=models.CASCADE)
    kvanto = models.FloatField(_('Kvanto de resurso'), blank=False, null=False)
    kosto = models.FloatField(_('Kosto de resurso'), blank=False, null=False)
    teknika = models.BooleanField(_('Teknika linio'), blank=True, default=False)
    rezervita = models.BooleanField(_('Rezerva resurso'), blank=True, default=False)
    elcherpita = models.BooleanField(_('Elĉerpita'), blank=True, default=False)

    class Meta:
        db_table = 'kombatantoj_resursoj'
        verbose_name = _('Kombatanta resurso')
        verbose_name_plural = _('Kombatantaj resursoj')

    def __str__(self):
        return '{}:'.format(get_enhavo(self.organizo.nomo, empty_values=True)[0])#,
                            #    get_enhavo(self.resurso.nomo, empty_values=True)[0])

    @staticmethod
    def _get_perm_cond(user):
        if user.is_staff or user.is_superuser:
            return models.Q()

        try:
            homo = Kombatanto.objects.get(uzanto=user)
            cond = models.Q(organizo__posedanto=homo.posedanto) | models.Q(organizo=homo.posedanto)
            
            #Просмотр создателям 
            cond |= (models.Q(autoro=user))

            # Просмотр участникам сообществ
            komunum = KomunumoMembro.objects.filter(
                autoro=user
            ).values_list('posedanto')
            cond |= models.Q(organizo__komunumo__in=komunum)

            return cond
        except Kombatanto.DoesNotExist:
            return models.Q()


# Модель типов контактов
class KontaktoTipo(SiriusoBazaAbstrakta):
    kodo = models.CharField(_('Kodo'), max_length=32, blank=False)
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=False, default=default_lingvo,
                              encoder=CallableEncoder)
    href = models.CharField(_('Hiperligilo por kontakti'), max_length=256, blank=True, null=True, default=None)

    class Meta:
        db_table = 'kontaktoj_tipoj'
        verbose_name = _('Tipo de kontakto')
        verbose_name_plural = _('Tipoj de kontaktoj')
        permissions = (
            ('povas_vidi_kombatanton_tipo_kontakto', _('Povas vidi kombatanton de kontakto de tipo')),
            ('povas_krei_kombatanton_tipo_kontakto', _('Povas krei kombatanton de kontakto de tipo')),
            ('povas_forigi_kombatanton_tipo_kontakto', _('Povas forigi kombatanton de kontakto de tipo')),
            ('povas_shangxi_kombatanton_tipo_kontakto', _('Povas ŝanĝi kombatanton de kontakto de tipo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        return '{}) {}'.format(self.kodo, get_enhavo(self.nomo, empty_values=True)[0])

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('kombatantoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'kombatantoj.povas_vidi_kombatanton_tipo_kontakto', 
                'kombatantoj.povas_krei_kombatanton_tipo_kontakto',
                'kombatantoj.povas_forigi_kombatanton_tipo_kontakto',
                'kombatantoj.povas_shangxi_kombatanton_tipo_kontakto'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='kombatantoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('kombatantoj.povas_vidi_kombatanton_tipo_kontakto')
                    or user_obj.has_perm('kombatantoj.povas_vidi_kombatanton_tipo_kontakto')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('kombatantoj.povas_vidi_kombatanton_tipo_kontakto'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond


# Модель контактов
class Kontakto(SiriusoBazaAbstraktaKomunumoj):
    tipo = models.ForeignKey(KontaktoTipo, on_delete=models.CASCADE, null=False)
    # Данные контакта, для телефона - номер телефона, для сайта - полная ссылка
    valoro = models.CharField(_('Valoro'), max_length=512, blank=False)

    # Связь с комбатантом
    kombatanto = models.ForeignKey(Kombatanto, on_delete=models.CASCADE, blank=True, null=True, default=None)

    class Meta:
        db_table = 'kontaktoj'
        verbose_name = _('Kontakto')
        verbose_name_plural = _('Kontaktoj')
        permissions = (
            ('povas_vidi_kontakton', _('Povas vidi kontakton')),
            ('povas_krei_kontakton', _('Povas krei kontakton')),
            ('povas_forigi_kontakton', _('Povas forigi kontakton')),
            ('povas_shanghi_kontakton', _('Povas ŝanĝi kontakton')),
        )

    def __str__(self):
        return '{}: {}'.format(get_enhavo(self.tipo.nomo, empty_values=True)[0], self.valoro)


# История действий
class Rakonto(SiriusoBazaAbstraktaKomunumoj):
    obj_uuid = models.UUIDField(_('Objekto UUID'), blank=False, null=False, db_index=True)
    ago = models.JSONField(_('Aga priskribo'), blank=False, null=False)

    class Meta:
        db_table = 'rakonto'
        verbose_name = _('Rekorda historio')
        verbose_name_plural = _('Rekordaj historioj')


class KombatantoBonusPunkto(SiriusoBazaAbstraktaKomunumoj):
    homo = models.ForeignKey(Kombatanto, blank=False, null=False, on_delete=models.CASCADE)
    # resurso = models.ForeignKey('resursoj.Resurso', blank=False, null=False, on_delete=models.CASCADE)
    resurso_nomenklaturo = models.ForeignKey(KombatantoResurso, blank=True, null=True,
                                             on_delete=models.SET_NULL)
    dato = models.DateTimeField(_('Bonuson pri dato kaj tempo akreditita'), auto_now_add=True)
    bonusoj_punktoj = models.FloatField(_('Punktoj aljuĝitaj'), blank=False, null=False)
