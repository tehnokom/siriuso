"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import models
from django.utils.translation import gettext_lazy as _
from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo
from main.models import SiriusoBazaAbstraktaKomunumoj, SiriusoKomentoAbstrakta, SiriusoTipoAbstrakta
from komunumoj.models import KomunumojAliro, Komunumo


# Функционал декретумов для сообществ
# Типы категорий декретумов, использует абстрактный класс SiriusoTipoAbstrakta
class DekretumojKategorioTipo(SiriusoTipoAbstrakta):

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'dekretumoj_kategorioj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de kategorioj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de kategorioj')



# Категории декретумов советов, использует абстрактный класс SiriusoBazaAbstraktaKomunumoj
class DekretumojKategorioSoveto(SiriusoBazaAbstraktaKomunumoj):

    # владелец (совет)
    posedanto = models.ForeignKey(Komunumo, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # тип категории
    tipo = models.ForeignKey(DekretumojKategorioTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название в таблице названий категорий, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание в таблице описаний категорий, от туда будет браться описание с нужным языковым тегом
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # доступ к категории
    aliro = models.ForeignKey(KomunumojAliro, verbose_name=_('Aliro'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'dekretumoj_kategorioj_sovetoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kategorio')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kategorioj')


# Типы декретумов (справочник), использует абстрактный класс SiriusoTipoAbstrakta
class DekretumojDekretumoTipo(SiriusoTipoAbstrakta):

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'dekretumoj_dekretumoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de dekretumoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de dekretumoj')


# Декретумы советов, использует абстрактный класс SiriusoBazaAbstraktaKomunumoj
class DekretumojDekretumoSoveto(SiriusoBazaAbstraktaKomunumoj):

    # владелец (совет)
    posedanto = models.ForeignKey(Komunumo, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # категория
    kategorio = models.ForeignKey(DekretumojKategorioSoveto, verbose_name=_('Kategorio'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # тип декретума
    tipo = models.ForeignKey(DekretumojDekretumoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название в таблице названий декретумов, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # текст в таблице текстов декретумов, от туда будет браться текст с нужным языковым тегом
    teksto = models.JSONField(verbose_name=_('Teksto'), blank=True, null=False, default=default_lingvo,
                                encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'dekretumoj_dekretumoj_sovetoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Dekretumo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Dekretumoj')
