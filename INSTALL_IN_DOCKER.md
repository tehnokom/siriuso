# Установка бекенда Siriuso
[[_TOC_]]

# Создание сети Docker
Создать сеть
```
sudo docker network create -d bridge siriuso-network
```

# Установка PostgreSQL
[Официальный образ PostgreSQL в Docker](https://hub.docker.com/_/postgres)

Создать хранилища файлов для БД и резервных копий
```
sudo docker volume create siriuso-postgresql-data-volume
sudo docker volume create siriuso-backup-volume
```
Создать переменную среды с паролем к БД
```
read -s -p "Введите пароль к БД: " POSTGRES_PASSWORD
```
Создать контейнер
```
sudo docker run \
  --name siriuso-dbms-postgresql \
  --volume siriuso-postgresql-data-volume:/var/lib/postgresql/data \
  --volume siriuso-backup-volume:/backup \
  --env POSTGRES_DB=siriuso \
  --env POSTGRES_USER=siriuso \
  --env POSTGRES_PASSWORD=$POSTGRES_PASSWORD \
  --publish 127.0.0.1:5432:5432 \
  --network siriuso-network \
  --detach \
  postgres
```
Описание процесса восстановления БД из дампа смотри в файле [BACKUP_DB.md](BACKUP_DB.md)

# Установка Redis
[Официальный образ Redis в Docker](https://hub.docker.com/_/redis)

```
docker run \
  --name siriuso-cache-redis \
  --publish 127.0.0.1:6379:6379 \
  --detach \
  redis:5-buster
```
Установить инструменты для работы с Redis
```
sudo apt install redis-tools
```
Проверить доступность сервера Redis
```
redis-cli -h localhost -p 6379 PING
```

# Установка RabbitMQ
[Официальный образ RabbitMQ в Docker](https://hub.docker.com/_/rabbitmq)

```
docker run \
  --name siriuso-mq-rabbitmq \
  --env RABBITMQ_DEFAULT_USER=siriuso \
  --env RABBITMQ_DEFAULT_PASS=siriuso \
  --publish 127.0.0.1:5672:5672 \
  --publish 127.0.0.1:15672:15672 \
  --detach \
  rabbitmq:3.8-management
```
Суффикс management используется для образов, в которых дополнительно установлен плагин для управления RabbitMQ

[Интерфейс управления RabbitMQ](http://127.0.0.1:15672/)

# Установка Siriuso (приложение на Django)
[Официальный образ Python в Docker](https://hub.docker.com/_/python)

Создать хранилища для статических и загружаемых файлов
```
sudo docker volume create siriuso-media-volume
sudo docker volume create siriuso-static-volume
```
Скачать и распаковать резервную копию статических файлов в хранилище
Подключиться по SFTP к t34.universo.pro
```
sftp t34.universo.pro
```
Скачать последнюю резервную копию статических файлов и отключиться от сервера
```
cd /backup
get 2022-09-18_static.tar.gz
exit
```
Запустить контейнер
```
sudo docker run \
  --name siriuso-app-django \
  --volume siriuso-media-volume:/opt/media \
  --volume siriuso-static-volume:/opt/static \
  --publish 127.0.0.1:8000:8000 \
  --network siriuso-network \
  -it \
  python:3.8-slim bash
```
Зайти внутрь контейнера
```
sudo docker exec -it siriuso-django /bin/bash
```
Перейти в директрию /usr/src и скачать проект в папку app через Git
```
# Внутри контейнера
cd /usr/src
git clone https://gitlab.com/tehnokom/siriuso.git app
```
Перейти в папку app
```
# Внутри контейнера
cd app
```
Установить виртуальное окружение Python ([Подробнее тут](https://docs.python.org/3/tutorial/venv.html))
Про virtualenv можно почитать [тут](https://virtualenv.pypa.io/en/latest/)
```
pip3 install virtualenv
```
Создать виртуальное окружение Python для Siriuso с интерпретатором python3.8 (совместим с Django 3 и Django 4)
```
virtualenv --python python3.8 /opt/siriuso-python-env
```
Активировать использование виртуального окружения
```
source /opt/siriuso-python-env/bin/activate
```
При этом изменяется приглашение командной строки. В её начало добавляется "(siriuso-python-env)"

Установить необходимые пакеты Python
```
pip3 install -r configs/docker/python/requirements.txt
```