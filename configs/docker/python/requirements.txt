pytz
psycopg2-binary
Pillow
packaging>=20.9,<21.0
tzdata
# celery==4.4.6
# django-celery-beat>=1.5.0,<1.6.0
# flower<1.0.0
celery>5, <6.0
django-celery-beat
flower<2.0.0 #1.2.0
geoip2
bcrypt
Django>=3.2.0,<4.0
# Django>=4.0.0,<4.2
djangorestframework>=3.7.7
django-cors-headers>=2.2.0
django-filter>=2.2.0
django-hosts>=3.0
django-recaptcha>=1.3.1
django-redis>=4.8.0
django-rosetta>=0.7.14
django-geoip>=0.5.2.1
django-embed-video
graphene-django==2.15.0
# graphene-django>=3.0.0
graphene-permissions>=1.1.1
channels-redis>=3.0.0,<4.0
# django-channels-graphql-ws>=0.4.2
django-channels-graphql-ws>=0.9.0
# django-channels-graphql-ws-django4
django-graphql-jwt==0.3.4
# # elasticsearch-dsl>=6.0.0,<7.0.0
elasticsearch-dsl
gunicorn
django-oauth-toolkit
django-crispy-forms
crispy-bootstrap4
django-environ
