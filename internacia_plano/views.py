"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.template.response import TemplateResponse
from django.http.response import Http404, JsonResponse
from siriuso.views import SiriusoTemplateView


# Международный план
@method_decorator(login_required, name='dispatch')
class InternaciaPlano(SiriusoTemplateView):

    template_name = 'internacia_plano/internacia_plano.html'
    mobile_template_name = 'internacia_plano/portebla/t_internacia_plano.html'


# Карта активности
@method_decorator(login_required, name='dispatch')
class PlanoMapo(SiriusoTemplateView):

    template_name = 'internacia_plano/plano_mapo.html'
    mobile_template_name = 'internacia_plano/portebla/t_plano_mapo.html'
