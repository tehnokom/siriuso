from selenium import webdriver
import unittest

class NewVisitorTest(unittest.TestCase):
    '''тест нового посетителя'''
    def setUp(self):
        '''установка'''
        self.browser = webdriver.Firefox()

    def tearDown(self):
        '''демонтаж'''
        self.browser.quit()

    def test1_named_well(self):
        '''тест: Регистрация на странице сообщество'''
        # Эдит слышала про крутое новое онлайн-приложение
        # о созадиний производственного сообщества. Она решает в нем
        # зарегистрироваться
        self.browser.get('http://localhost:8000')

        # Она видит, что заголовок и шапка страницы говорят о сообществе
        # (техноком )
        self.assertIn('Техноком', self.browser.title)
        self.fail('Закончить тест!')

        # ит.д. Нужно дополнить пустыми комментариями дальнейщие ее действия
        # а потом думать об осуществление заполнение командами
    def test2_named_well(self):#другие поведения пользователя
        pass

    def test3_named_well(self):#другие поведения пользователя(желательно конкретизировать: если сложное действие поделить)
        pass

    def test__named_well(self):#другие поведения пользователя(желательно охватить все возмодные поведения сайта)
        pass
if __name__ == '__main__':
    unittest.main(warnings='ignore')#Python проверяет,был ли он выполнен из командной строки, а не просто импортирован другим сценарием
