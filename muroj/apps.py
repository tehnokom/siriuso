"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class MuroConfig(AppConfig):
    name = 'muroj'
    verbose_name = _('Muroj')

    def ready(self):
        import muroj.signals
        import muroj.tasks
