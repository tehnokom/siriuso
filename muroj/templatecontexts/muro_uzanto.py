"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from uzantoj.templatecontexts.uzanto import get_kamaradoj_id
from main.models import Uzanto


def context_kamaradoj(request, **kwargs):
    context = {}

    if request.user.is_authenticated:
        context['mia_kamaradoj'] = get_kamaradoj_id(request.user.id)

    return context


def context_statistikoj(request, **kwargs):
    context = {}

    try:
        uzanto = Uzanto.objects.values('id', 'uuid', 'sekso', 'unua_nomo', 'familinomo',
                                            'fotoj_fotojuzantojavataro_posedanto__bildo', 'uzantojkovrilo__bildo',
                                            'uzantojstatuso__teksto').get(id=kwargs['uzanto_id'])
        context['uzanto'] = uzanto
        context['uzanto']['username'] = '{}_{}'.format(uzanto['unua_nomo'],
                                                       uzanto['familinomo']).lower()
    except:
        pass

    return context
