"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""


from django.urls import path, re_path, include
from .views import *
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'komunumoj', MurojEnskribojView,  base_name='muroj_komunumoj')
router.register(r'komunumo/(?P<komunumo_uuid>[a-zA-Z0-9-]+)', MurojEnskribojView, base_name='muro_komunumo')
router.register(r'komentoj', MurojKomentojView, base_name='muroj_komentoj')
router.register(r'komento/(?P<enskribo_uuid>[a-zA-Z0-9-]+)', MurojKomentojView, base_name='muro_komentoj')

urlpatterns = [
    path('', include(router.urls)),
]
