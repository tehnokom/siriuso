"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django import forms
from .models import *
from informiloj.models import InformilojLingvo
from komunumoj.models import KomunumojAliro
from django.utils import timezone
from embed_video.fields import EmbedVideoFormField
from muroj.tasks import email_post_notice


# Форма для записей на стене пользователей
class MurojUzantoEnskriboFormo(forms.ModelForm):
    teksto_formo = forms.CharField(widget=forms.Textarea(attrs={'cols': 64, 'rows': 8}), required=False)
    bildo_formo = forms.ImageField(required=False)
    video_formo = EmbedVideoFormField(required=False)

    class Meta:
        model = MurojUzantoEnskribo
        fields = ('teksto_formo', 'bildo_formo', 'aliro', 'komentado_aliro')

    def __init__(self, *args, **kwargs):
        super(MurojUzantoEnskriboFormo, self).__init__(*args, **kwargs)
        self.fields['aliro'].empty_label = None
        self.fields['komentado_aliro'].empty_label = None

        default = UzantojAliro.objects.filter(speciala=True)
        if default.count():
            self.fields['aliro'].initial = default[0]
            self.fields['komentado_aliro'].initial = default[0]

    def clean(self):
        data = self.cleaned_data
        if ('teksto_formo' not in data or 'teksto_formo' in data and data['teksto_formo'] == '')\
                and ('bildo_formo' not in data or 'bildo_formo' in data and data['bildo_formo'] is None)\
                and ('video_formo' not in data or 'video_formo' in data and data['video_formo'] == ''):
            raise forms.ValidationError('Хотя бы одно из полей должно быть заполнено')

        if 'bildo_formo' in data and data['bildo_formo'] is not None\
            and 'video_formo' in data and data['video_formo'] != '':
            raise forms.ValidationError('Поля "Изображение" и "Видео" не могут быть заполнены одновременно')

        return data

    def save(self, request, obj_id, commit=True):
        muro = MurojUzantoMuro.objects.filter(posedanto_id=obj_id)[0]
        self.instance.muro_id = muro.uuid
        self.instance.posedanto_id = request.user.id
        self.instance.publikigo = True
        self.instance.publikiga_dato = timezone.now()
        self.instance.video = self.cleaned_data['video_formo'] if self.cleaned_data['video_formo'] else ''

        enskribo = super(MurojUzantoEnskriboFormo, self).save(commit=False)
        super(MurojUzantoEnskriboFormo, self).save(commit=commit)

        if self.cleaned_data['bildo_formo'] is not None:
            bildo = MurojUzantoEnskriboBildo(bildo=self.cleaned_data['bildo_formo'],
                                             posedanto=enskribo)
            if commit:
                bildo.save()

        if commit:
            kodo = InformilojLingvo.objects.filter(kodo='ru_RU')

            if kodo is not None:
                teksto = MurojUzantoEnskriboTeksto(lingvo=kodo[0],
                                          posedanto_id=self.instance.uuid,
                                          enhavo=self.cleaned_data['teksto_formo'])
                teksto.save()

            # Рассылка оповещений
            # email_post_notice(self.instance._meta.model_name, self.instance.uuid)


# Форма для комментариев записей на стене пользователей
class MurojUzantoEnskriboKomentoFormo(forms.ModelForm):

    teksto_formo = forms.CharField(widget=forms.Textarea(attrs={'cols': 58, 'rows': 4}), required=True)

    class Meta:
        model = MurojUzantoEnskriboKomento
        fields = ('teksto_formo', )

    def save(self, request=None, enskribo=None, commit=True):
        self.instance.posedanto_id = request.POST['enskribo']
        self.instance.autoro = request.user
        super(MurojUzantoEnskriboKomentoFormo, self).save(commit=commit)

        if commit:
            kodo = InformilojLingvo.objects.filter(kodo='ru_RU')

            if not kodo is None:
                if not kodo is None:
                    teksto = MurojUzantoEnskriboKomentoTeksto(lingvo=kodo[0], posedanto_id=self.instance.uuid,
                                                              enhavo=self.cleaned_data['teksto_formo'])
                    teksto.save()


# Форма для записей на стене общественных проектов
class MurojSociaprojektoEnskriboFormo(forms.ModelForm):

    teksto_formo = forms.CharField(widget=forms.Textarea(attrs={'cols': 64, 'rows': 8}), required=False)
    bildo_formo = forms.ImageField(required=False)
    video_formo = EmbedVideoFormField(required=False)

    class Meta:
        model = MurojSociaprojektoEnskribo
        fields = ('teksto_formo', 'bildo_formo', 'autoro_montri', 'aliro', 'komentado_aliro')

    def __init__(self, *args, **kwargs):
        super(MurojSociaprojektoEnskriboFormo, self).__init__(*args, **kwargs)
        self.fields['aliro'].empty_label = None
        self.fields['komentado_aliro'].empty_label = None

        default = KomunumojAliro.objects.filter(speciala=True)
        if default.count():
            self.fields['aliro'].initial = default[0]
            self.fields['komentado_aliro'].initial = default[0]

    def clean(self):
        data = self.cleaned_data
        if ('teksto_formo' not in data or 'teksto_formo' in data and data['teksto_formo'] == '')\
                and ('bildo_formo' not in data or 'bildo_formo' in data and data['bildo_formo'] is None)\
                and ('video_formo' not in data or 'video_formo' in data and data['video_formo'] == ''):
            raise forms.ValidationError('Хотя бы одно из полей должно быть заполнено')

        if 'bildo_formo' in data and data['bildo_formo'] is not None\
            and 'video_formo' in data and data['video_formo'] != '':
            raise forms.ValidationError('Поля "Изображение" и "Видео" не могут быть заполнены одновременно')

        return data

    def save(self, request=None, obj_id=None, commit=True):
        muro = MurojSociaprojektoMuro.objects.filter(posedanto_id=obj_id).get()
        self.instance.autoro = request.user
        self.instance.muro_id = muro.uuid
        self.instance.posedanto_id = obj_id
        self.instance.publikigo = True
        self.instance.publikiga_dato = timezone.now()
        self.instance.video = self.cleaned_data['video_formo'] if self.cleaned_data['video_formo'] else ''

        enskribo = super(MurojSociaprojektoEnskriboFormo, self).save(commit=False)
        super(MurojSociaprojektoEnskriboFormo, self).save(commit=commit)

        if self.cleaned_data['bildo_formo'] is not None:
            bildo = MurojSociaprojektoEnskriboBildo(bildo=self.cleaned_data['bildo_formo'],
                                                    posedanto=enskribo,
                                                    autoro=enskribo.autoro)
            if commit:
                bildo.save()

        if commit:
            kodo = InformilojLingvo.objects.filter(kodo='ru_RU')

            if kodo is not None:
                MurojSociaprojektoEnskriboTeksto(lingvo=kodo[0],
                                                 posedanto_id=self.instance.uuid,
                                                 enhavo=self.cleaned_data['teksto_formo']).save()
            # Рассылка оповещений
            email_post_notice.delay(self.instance._meta.model_name, self.instance.uuid)


# Форма для комментариев записей на стене общественных проектов
class MurojSociaprojektoEnskriboKomentoFormo(forms.ModelForm):

    teksto_formo = forms.CharField(widget=forms.Textarea(attrs={'cols': 58, 'rows': 4}), required=True)

    class Meta:
        model = MurojSociaprojektoEnskriboKomento
        fields = ('teksto_formo', )

    def save(self, request=None, enskribo=None, commit=True):
        self.instance.posedanto_id = request.POST['enskribo']
        self.instance.autoro = request.user
        super(MurojSociaprojektoEnskriboKomentoFormo, self).save(commit=commit)

        if commit:
            kodo = InformilojLingvo.objects.filter(kodo='ru_RU')

            if not kodo is None:
                if not kodo is None:
                    teksto = MurojSociaprojektoEnskriboKomentoTeksto(lingvo=kodo[0], posedanto_id=self.instance.uuid,
                                                                     enhavo=self.cleaned_data['teksto_formo'])
                    teksto.save()


# Форма для записей на стене групп
class MurojGrupoEnskriboFormo(forms.ModelForm):

    teksto_formo = forms.CharField(widget=forms.Textarea(attrs={'cols': 64, 'rows': 8}), required=False)
    bildo_formo = forms.ImageField(required=False)
    video_formo = EmbedVideoFormField(required=False)

    class Meta:
        model = MurojGrupoEnskribo
        fields = ('teksto_formo', 'bildo_formo', 'autoro_montri', 'aliro', 'komentado_aliro')

    def __init__(self, *args, **kwargs):
        super(MurojGrupoEnskriboFormo, self).__init__(*args, **kwargs)
        self.fields['aliro'].empty_label = None
        self.fields['komentado_aliro'].empty_label = None

        default = KomunumojAliro.objects.filter(speciala=True)
        if default.count():
            self.fields['aliro'].initial = default[0]
            self.fields['komentado_aliro'].initial = default[0]

    def clean(self):
        data = self.cleaned_data
        if ('teksto_formo' not in data or 'teksto_formo' in data and data['teksto_formo'] == '')\
                and ('bildo_formo' not in data or 'bildo_formo' in data and data['bildo_formo'] is None)\
                and ('video_formo' not in data or 'video_formo' in data and data['video_formo'] == ''):
            raise forms.ValidationError('Хотя бы одно из полей должно быть заполнено')

        if 'bildo_formo' in data and data['bildo_formo'] is not None\
            and 'video_formo' in data and data['video_formo'] != '':
            raise forms.ValidationError('Поля "Изображение" и "Видео" не могут быть заполнены одновременно')

        return data

    def save(self, request=None, obj_id=None, commit=True):
        muro = MurojGrupoMuro.objects.filter(posedanto_id=obj_id)[0]
        self.instance.autoro = request.user
        self.instance.muro_id = muro.uuid
        self.instance.posedanto_id = obj_id
        self.instance.publikigo = True
        self.instance.publikiga_dato = timezone.now()
        self.instance.video = self.cleaned_data['video_formo'] if self.cleaned_data['video_formo'] else ''

        enskribo = super(MurojGrupoEnskriboFormo, self).save(commit=False)
        super(MurojGrupoEnskriboFormo, self).save(commit=commit)

        if self.cleaned_data['bildo_formo'] is not None:
            bildo = MurojGrupoEnskriboBildo(bildo=self.cleaned_data['bildo_formo'],
                                            posedanto=enskribo,
                                            autoro=enskribo.autoro)
            if commit:
                bildo.save()

        if commit:
            kodo = InformilojLingvo.objects.filter(kodo='ru_RU')

            if not kodo is None:
                teksto = MurojGrupoEnskriboTeksto(lingvo=kodo[0], posedanto_id=self.instance.uuid,
                                                  enhavo=self.cleaned_data['teksto_formo'])
                teksto.save()

            # Рассылка оповещений
            email_post_notice.delay(self.instance._meta.model_name, self.instance.uuid)


# Форма для комментариев записей на стене групп
class MurojGrupoEnskriboKomentoFormo(forms.ModelForm):

    teksto_formo = forms.CharField(widget=forms.Textarea(attrs={'cols': 58, 'rows': 4}), required=True)

    class Meta:
        model = MurojGrupoEnskriboKomento
        fields = ('teksto_formo', )

    def save(self, request=None, enskribo=None, commit=True):
        self.instance.posedanto_id = request.POST['enskribo']
        self.instance.autoro = request.user
        super(MurojGrupoEnskriboKomentoFormo, self).save(commit=commit)

        if commit:
            kodo = InformilojLingvo.objects.filter(kodo='ru_RU')

            if not kodo is None:
                if not kodo is None:
                    teksto = MurojGrupoEnskriboKomentoTeksto(lingvo=kodo[0], posedanto_id=self.instance.uuid,
                                                             enhavo=self.cleaned_data['teksto_formo'])
                    teksto.save()


# Форма для записей на стене советов
class MurojSovetoEnskriboFormo(forms.ModelForm):

    teksto_formo = forms.CharField(widget=forms.Textarea(attrs={'cols': 64, 'rows': 8}), required=False)
    bildo_formo = forms.ImageField(required=False)
    video_formo = EmbedVideoFormField(required=False)

    class Meta:
        model = MurojSovetoEnskribo
        fields = ('teksto_formo', 'bildo_formo', 'autoro_montri', 'aliro', 'komentado_aliro')

    def __init__(self, *args, **kwargs):
        super(MurojSovetoEnskriboFormo, self).__init__(*args, **kwargs)
        self.fields['aliro'].empty_label = None
        self.fields['komentado_aliro'].empty_label = None

        default = KomunumojAliro.objects.filter(speciala=True)
        if default.count():
            self.fields['aliro'].initial = default[0]
            self.fields['komentado_aliro'].initial = default[0]

    def clean(self):
        data = self.cleaned_data
        if ('teksto_formo' not in data or 'teksto_formo' in data and data['teksto_formo'] == '')\
                and ('bildo_formo' not in data or 'bildo_formo' in data and data['bildo_formo'] is None)\
                and ('video_formo' not in data or 'video_formo' in data and data['video_formo'] == ''):
            raise forms.ValidationError('Хотя бы одно из полей должно быть заполнено')

        if 'bildo_formo' in data and data['bildo_formo'] is not None\
            and 'video_formo' in data and data['video_formo'] != '':
            raise forms.ValidationError('Поля "Изображение" и "Видео" не могут быть заполнены одновременно')

        return data

    def save(self, request=None, obj_id=None, commit=True):
        muro = MurojSovetoMuro.objects.filter(posedanto_id=obj_id)[0]
        self.instance.autoro = request.user
        self.instance.muro_id = muro.uuid
        self.instance.posedanto_id = obj_id
        self.instance.publikigo = True
        self.instance.publikiga_dato = timezone.now()
        self.instance.video = self.cleaned_data['video_formo'] if self.cleaned_data['video_formo'] else ''

        enskribo = super(MurojSovetoEnskriboFormo, self).save(commit=False)
        super(MurojSovetoEnskriboFormo, self).save(commit=commit)

        if self.cleaned_data['bildo_formo'] is not None:
            bildo = MurojSovetoEnskriboBildo(bildo=self.cleaned_data['bildo_formo'],
                                             posedanto=enskribo,
                                             autoro=enskribo.autoro)
            if commit:
                bildo.save()

        if commit:
            kodo = InformilojLingvo.objects.filter(kodo='ru_RU')

            if not kodo is None:
                teksto = MurojSovetoEnskriboTeksto(lingvo=kodo[0], posedanto_id=self.instance.uuid,
                                                   enhavo=self.cleaned_data['teksto_formo'])
                teksto.save()

            # Рассылка оповещений
            email_post_notice.delay(self.instance._meta.model_name, self.instance.uuid)


# Форма для комментариев записей на стене советов
class MurojSovetoEnskriboKomentoFormo(forms.ModelForm):

    teksto_formo = forms.CharField(widget=forms.Textarea(attrs={'cols': 58, 'rows': 4}), required=True)

    class Meta:
        model = MurojSovetoEnskriboKomento
        fields = ('teksto_formo', )

    def save(self, request=None, enskribo=None, commit=True):
        self.instance.posedanto_id = request.POST['enskribo']
        self.instance.autoro = request.user
        super(MurojSovetoEnskriboKomentoFormo, self).save(commit=commit)

        if commit:
            kodo = InformilojLingvo.objects.filter(kodo='ru_RU')

            if not kodo is None:
                if not kodo is None:
                    teksto = MurojSovetoEnskriboKomentoTeksto(lingvo=kodo[0], posedanto_id=self.instance.uuid,
                                                              enhavo=self.cleaned_data['teksto_formo'])
                    teksto.save()


# Форма для записей на стене организаций
class MurojOrganizoEnskriboFormo(forms.ModelForm):

    teksto_formo = forms.CharField(widget=forms.Textarea(attrs={'cols': 64, 'rows': 8}), required=False)
    bildo_formo = forms.ImageField(required=False)
    video_formo = EmbedVideoFormField(required=False)

    class Meta:
        model = MurojOrganizoEnskribo
        fields = ('teksto_formo', 'bildo_formo', 'autoro_montri', 'aliro', 'komentado_aliro')

    def __init__(self, *args, **kwargs):
        super(MurojOrganizoEnskriboFormo, self).__init__(*args, **kwargs)
        self.fields['aliro'].empty_label = None
        self.fields['komentado_aliro'].empty_label = None

        default = KomunumojAliro.objects.filter(speciala=True)
        if default.count():
            self.fields['aliro'].initial = default[0]
            self.fields['komentado_aliro'].initial = default[0]

    def clean(self):
        data = self.cleaned_data
        if ('teksto_formo' not in data or 'teksto_formo' in data and data['teksto_formo'] == '')\
                and ('bildo_formo' not in data or 'bildo_formo' in data and data['bildo_formo'] is None)\
                and ('video_formo' not in data or 'video_formo' in data and data['video_formo'] == ''):
            raise forms.ValidationError('Хотя бы одно из полей должно быть заполнено')

        if 'bildo_formo' in data and data['bildo_formo'] is not None\
            and 'video_formo' in data and data['video_formo'] != '':
            raise forms.ValidationError('Поля "Изображение" и "Видео" не могут быть заполнены одновременно')

        return data

    def save(self, request=None, obj_id=None, commit=True):
        muro = MurojOrganizoMuro.objects.filter(posedanto_id=obj_id)[0]
        self.instance.autoro = request.user
        self.instance.muro_id = muro.uuid
        self.instance.posedanto_id = obj_id
        self.instance.publikigo = True
        self.instance.publikiga_dato = timezone.now()
        self.instance.video = self.cleaned_data['video_formo'] if self.cleaned_data['video_formo'] else ''

        enskribo = super(MurojOrganizoEnskriboFormo, self).save(commit=False)
        super(MurojOrganizoEnskriboFormo, self).save(commit=commit)

        if self.cleaned_data['bildo_formo'] is not None:
            bildo = MurojOrganizoEnskriboBildo(bildo=self.cleaned_data['bildo_formo'],
                                               posedanto=enskribo,
                                               autoro=enskribo.autoro)
            if commit:
                bildo.save()

        if commit:
            kodo = InformilojLingvo.objects.filter(kodo='ru_RU')

            if not kodo is None:
                teksto = MurojOrganizoEnskriboTeksto(lingvo=kodo[0], posedanto_id=self.instance.uuid,
                                                     enhavo=self.cleaned_data['teksto_formo'])
                teksto.save()

            # Рассылка оповещений
            email_post_notice.delay(self.instance._meta.model_name, self.instance.uuid)


# Форма для комментариев записей на стене организаций
class MurojOrganizoEnskriboKomentoFormo(forms.ModelForm):

    teksto_formo = forms.CharField(widget=forms.Textarea(attrs={'cols': 58, 'rows': 4}), required=True)

    class Meta:
        model = MurojOrganizoEnskriboKomento
        fields = ('teksto_formo', )

    def save(self, request=None, enskribo=None, commit=True):
        self.instance.posedanto_id = request.POST['enskribo']
        self.instance.autoro = request.user
        super(MurojOrganizoEnskriboKomentoFormo, self).save(commit=commit)

        if commit:
            kodo = InformilojLingvo.objects.filter(kodo='ru_RU')

            if not kodo is None:
                if not kodo is None:
                    teksto = MurojOrganizoEnskriboKomentoTeksto(lingvo=kodo[0], posedanto_id=self.instance.uuid,
                                                                enhavo=self.cleaned_data['teksto_formo'])
                    teksto.save()
