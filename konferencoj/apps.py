"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class KonferencojConfig(AppConfig):
    name = 'konferencoj'
    verbose_name = _('Konferencoj')

    def ready(self):
        import konferencoj.signals
        # Импортом сообщаем django, что надо проанализировать код в модуле tasks
        # При этом происходит регистрация функций заданий celery
        import konferencoj.tasks
