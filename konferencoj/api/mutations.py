"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.utils import timezone
from django.db import transaction  # создание и изменения будем делать в блокирующей транзакции
from django.utils.translation import gettext_lazy as _
import graphene

from siriuso.utils import set_enhavo, get_lang_kodo, perms
from konferencoj.tasks import sciigi_konferencoj, send_email_comment_publication
from .schema import KonferencojKategorioTipoNode, KonferencojKategorioNode, KonferencojTemoTipoNode, \
    KonferencojTemoNode, KonferencojTemoKomentoNode, KonferencojSciigojNode
from ..models import *
from ..tasks import send_email_enketo

class Enketo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    # enketo = graphene.Field(KonferencojKategorioTipoNode,
    #                                 description=_('Созданная/изменённая запись типа категории конференции'))

    class Arguments:
        nomo = graphene.String(description=_('Название'))
        telefono = graphene.String(description=_('Телефон'))
        organizo = graphene.String(description=_('Название организации'))
        retposhto = graphene.String(description=_('Электронная почта'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        uzanto = info.context.user

        with transaction.atomic():
            # Проверяем наличие всех полей
            if not kwargs.get('nomo', False):
                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

            if not message:
                # отправляем сообщение на почту
                send_email_enketo.delay(kwargs.get('nomo'),
                                     kwargs.get('telefono', None),
                                     kwargs.get('organizo', None),
                                     kwargs.get('retposhto', None),
                                     )

                status = True
                message = _('Сообщение отправлено')

        return Enketo(status=status, message=message)


class RedaktuKonferencojKategorioTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kategorio_tipo = graphene.Field(KonferencojKategorioTipoNode,
                                    description=_('Созданная/изменённая запись типа категории конференции'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название типа категории'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('konferencoj.povas_krei_kategorian_tipon'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            KonferencojKategorioTipo.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except KonferencojKategorioTipo.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            tipo = KonferencojKategorioTipo.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now(),
                            )

                            set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            tipo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tipo = KonferencojKategorioTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False
                                                                        )

                            if (not uzanto.has_perm('konferencoj.povas_forigi_kategorian_tipon')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('konferencoj.povas_shanghi_kategorian_tipon'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                tipo.kodo = kwargs.get('kodo', tipo.kodo)
                                tipo.forigo = kwargs.get('forigo', tipo.forigo)
                                tipo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tipo.arkivo = kwargs.get('arkivo', tipo.arkivo)
                                tipo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tipo.publikigo = kwargs.get('publikigo', tipo.publikigo)
                                tipo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                tipo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KonferencojKategorioTipo.DoesNotExist:
                            message = _('Запись не найдена')

        else:
            message = _('Требуется авторизация')

        return RedaktuKonferencojKategorioTipo(status=status, message=message, kategorio_tipo=tipo)


class RedaktuKonferencojKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kategorio = graphene.Field(KonferencojKategorioNode, description=_('Созданная/изменённая категории конференции'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        tipo_kodo = graphene.String(description=_('Код типа конференции'))
        posedanto_id = graphene.Int(description=_('Код владельца конференции'))
        aliro_kodo = graphene.String(description=_('Код разрешений доступа'))
        nomo = graphene.String(description=_('Название категории конференции'))
        priskribo = graphene.String(description=_('Описание категории конференции'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kategorio = None
        tipo = None
        posedanto = None
        aliro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    # Проверяем наличие всех полей
                    if kwargs.get('forigo', False) and not message:
                        message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                    if kwargs.get('arkivo', False) and not message:
                        message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                    if not message:
                        if not (kwargs.get('tipo_kodo', False)):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'tipo_kodo')
                        else:
                            try:
                                tipo = KonferencojKategorioTipo.objects.get(kodo=kwargs.get('tipo_kodo'),
                                                                            forigo=False,
                                                                            arkivo=False, publikigo=True)
                            except KonferencojKategorioTipo.DoesNotExist:
                                message = _('Неверный тип категории конференции')

                    if not message:
                        if not (kwargs.get('posedanto_id', False)):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'posedanto_id')
                        else:
                            try:
                                posedanto = Komunumo.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                 arkivo=False, publikigo=True)
                            except Komunumo.DoesNotExist:
                                message = _('Неверный владелец конференции')

                    if not message:
                        if not (kwargs.get('aliro_kodo', False)):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'aliro_kodo')
                        else:
                            try:
                                aliro = KomunumojAliro.objects.get(kodo=kwargs.get('aliro_kodo'), forigo=False,
                                                                   arkivo=False)
                            except KomunumojAliro.DoesNotExist:
                                message = _('Неверная настройка прав доступа к конференции')

                    if not (kwargs.get('nomo', False) or message):
                        message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                    if not (kwargs.get('priskribo', False) or message):
                        message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'priskribo')

                    if not message:
                        if (uzanto.has_perm('konferencoj.povas_krei_kategorion', posedanto)
                                or uzanto.has_perm('konferencoj.povas_krei_kategorion')):
                            kategorio = KonferencojKategorio.objects.create(
                                autoro=uzanto,
                                tipo=tipo,
                                posedanto=posedanto,
                                aliro=aliro,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            update_fields = ['nomo', 'priskribo']
                            set_enhavo(kategorio.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            set_enhavo(kategorio.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                            kategorio.save(update_fields=update_fields)

                            status = True
                            message = _('Запись создана')
                        else:
                            message = _('Недостаточно прав')

                else:
                    # Изменяем запись
                    if not (kwargs.get('posedanto_id', False) or kwargs.get('tipo_kodo', False)
                            or kwargs.get('aliro_kodo', False) or kwargs.get('nomo', False)
                            or kwargs.get('priskribo', False) or kwargs.get('publikigo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'tipo_kodo' in kwargs and not message:
                        try:
                            tipo = KonferencojKategorioTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                        except KonferencojKategorioTipo.DoesNotExist:
                            message = _('Неверный тип категории конференции')

                    if 'posedanto_id' in kwargs and not message:
                        try:
                            posedanto = Komunumo.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                             arkivo=False, publikigo=True)
                        except Komunumo.DoesNotExist:
                            message = _('Неверный владелец типа категории конференции')

                    if 'aliro_kodo' in kwargs and not message:
                        try:
                            aliro = KomunumojAliro.objects.get(kodo=kwargs.get('aliro_kodo'), forigo=False,
                                                               arkivo=False, publikigo=True)
                        except KomunumojAliro.DoesNotExist:
                            message = _('Неверный доступ к категории конференции')

                    # Ищем запись для изменения
                    if not message:
                        try:
                            kategorio = KonferencojKategorio.objects.get(
                                uuid=kwargs.get('uuid'), forigo=False
                            )
                            if (not (uzanto.has_perm('konferencoj.povas_shanghi_kategorion', kategorio.posedanto)
                                     or uzanto.has_perm('konferencoj.povas_shanghi_kategorion'))
                                    or (posedanto
                                        and not (uzanto.has_perm('konferencoj.povas_krei_kategorion', kategorio.posedanto))
                                                 or not uzanto.has_perm('konferencoj.povas_krei_kategorion'))
                                    or (kwargs.get('konferencoj.povas_forigi_kategorion', False)
                                        and not (uzanto.has_perm('konferencoj.povas_forigi_kategorion')
                                                 or uzanto.has_perm('konferencoj.povas_forigi_kategorion',
                                                                    kategorio.posedanto))
                                        )):
                                message = _('Недостаточно прав')
                            else:
                                kategorio.tipo = tipo or kategorio.tipo
                                kategorio.posedanto = posedanto or kategorio.posedanto
                                kategorio.aliro = aliro or kategorio.aliro
                                kategorio.forigo = kwargs.get('forigo', kategorio.forigo)
                                kategorio.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kategorio.arkivo = kwargs.get('arkivo', kategorio.arkivo)
                                kategorio.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kategorio.publikigo = kwargs.get('publikigo', kategorio.publikigo)
                                kategorio.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kategorio.lasta_autoro = uzanto
                                kategorio.lasta_dato = timezone.now()

                                update_fields = [
                                    'tipo', 'aliro',
                                    'posedanto', 'forigo', 'foriga_dato', 'arkivo', 'arkiva_dato',
                                    'publikigo', 'publikiga_dato', 'lasta_autoro', 'lasta_dato',
                                ]

                                if kwargs.get('nomo', False):
                                    set_enhavo(kategorio.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('nomo')
                                if kwargs.get('priskribo', False):
                                    set_enhavo(kategorio.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('priskribo')

                                kategorio.save(update_fields=update_fields)
                                status = True
                                message = _('Запись успешно изменена')
                        except KonferencojKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKonferencojKategorio(status=status, message=message, kategorio=kategorio)


class RedaktuKonferencojTemoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    temo_tipo = graphene.Field(KonferencojTemoTipoNode,
                               description=_('Созданная/изменённая запись типа темы конференции'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название типа категории'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('konferencoj.povas_krei_teman_tipon'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            KonferencojTemoTipo.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except KonferencojTemoTipo.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            tipo = KonferencojTemoTipo.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now(),
                            )

                            set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            tipo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                                or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                                or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tipo = KonferencojTemoTipo.objects.get(
                                    uuid=kwargs.get('uuid'), forigo=False
                                   )
                            if (not uzanto.has_perm('konferencoj.povas_forigi_teman_tipon')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('konferencoj.povas_shanghi_teman_tipon'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                tipo.kodo = kwargs.get('kodo', tipo.kodo)
                                tipo.forigo = kwargs.get('forigo', tipo.forigo)
                                tipo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tipo.arkivo = kwargs.get('arkivo', tipo.arkivo)
                                tipo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tipo.publikigo = kwargs.get('publikigo', tipo.publikigo)
                                tipo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                tipo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KonferencojTemoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKonferencojTemoTipo(status=status, message=message, temo_tipo=tipo)


class RedaktuKonferencojTemo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    temo = graphene.Field(KonferencojTemoNode, description=_('Созданная/изменённая тема конференции'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        tipo_kodo = graphene.String(description=_('Код типа темы конференции'))
        fermita = graphene.Boolean(description=_('Признак закрытой темы'))
        komentado_aliro_kodo = graphene.String(description=_('Код разрешений писать в закрытую тему'))
        kategorio_id = graphene.String(description=_('Код категории конференции'))
        nomo = graphene.String(description=_('Название темы конференции'))
        priskribo = graphene.String(description=_('Описание темы конференции'))
        teksto = graphene.String(description=_('Текст стартового комментария'))
        fiksa = graphene.Boolean(description=_('Признак закрепления темы'))
        fiksa_listo = graphene.Int(description=_('Позиция среди закреплённых тем'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kategorio = None
        tipo = None
        posedanto = None
        komentado_aliro = None
        temo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if (uzanto.has_perm('konferencoj.povas_krei_temon')
                            or perms.has_registrita_perm('konferencoj.povas_krei_temon')):
                        # Проверяем наличие всех обязательных полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not (kwargs.get('priskribo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'priskribo')

                        if not (kwargs.get('teksto', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'teksto')

                        if not message:
                            if not (kwargs.get('tipo_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'tipo_kodo')
                            else:
                                try:
                                    tipo = KonferencojTemoTipo.objects.get(
                                        kodo=kwargs.get('tipo_kodo'), forigo=False, arkivo=False, publikigo=True
                                    )
                                except KonferencojTemoTipo.DoesNotExist:
                                    message = _('Неверный тип темы конференции')

                        if not message:
                            if not (kwargs.get('kategorio_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kategorio_id')
                            else:
                                try:
                                    kategorio = KonferencojKategorio.objects.get(
                                        id=kwargs.get('kategorio_id'), forigo=False, arkivo=False, publikigo=True
                                    )
                                except KonferencojKategorio.DoesNotExist:
                                    message = _('Неверная категория конференции ')

                        if not message:
                            if not (kwargs.get('komentado_aliro_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'komentado_aliro_kodo')
                            else:
                                try:
                                    komentado_aliro = KomunumojAliro.objects.get(
                                        kodo=kwargs.get('komentado_aliro_kodo'), forigo=False, arkivo=False, publikigo=True
                                    )
                                except KomunumojAliro.DoesNotExist:
                                    message = _('Неверные права на доступ к закрытой теме')

                        if not message:
                            temo = KonferencojTemo.objects.create(
                                autoro=uzanto,
                                tipo=tipo,
                                posedanto=posedanto,
                                kategorio=kategorio,
                                komentado_aliro=komentado_aliro,
                                fermita=kwargs.get('fermita', False),
                                fiksa=kwargs.get('fiksa', False),
                                fiksa_listo=kwargs.get('fiksa_listo', "0"),
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            update_fields = ['nomo', 'priskribo']
                            set_enhavo(temo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            set_enhavo(temo.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)

                            temo.save(update_fields=update_fields)

                            komento = KonferencojTemoKomento.objects.create(
                                posedanto=temo,
                                autoro=uzanto,
                                forigo=False,
                                arkivo=False,
                                publikigo=True,
                                publikiga_dato=timezone.now()
                            )

                            update_fields = []
                            update_fields.append('teksto')
                            set_enhavo(komento.teksto, kwargs.get('teksto'), lingvo=get_lang_kodo(info.context))
                            komento.save(update_fields=update_fields)

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('tipo_kodo', False)
                                or kwargs.get('komentado_aliro_id', False) or kwargs.get('nomo', False)
                                or kwargs.get('priskribo', False) or kwargs.get('publikigo', False)
                                or kwargs.get('fermita', False) or kwargs.get('kategorio_id', False)
                                or kwargs.get('fiksa', False) or kwargs.get('fiksa_listo', False)
                                or kwargs.get('forigo', False) or kwargs.get('arkivo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'tipo_kodo' in kwargs and not message:
                        try:
                            tipo = KonferencojTemoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                        except KonferencojTemoTipo.DoesNotExist:
                            message = _('Неверный тип категории конференции')

                    if 'kategorio_id' in kwargs and not message:
                        try:
                            kategorio = KonferencojKategorio.objects.get(id=kwargs.get('kategorio_id'),
                                                                        forigo=False,
                                                                        arkivo=False, publikigo=True)
                        except KonferencojKategorio.DoesNotExist:
                            message = _('Неверная категория конференции')

                    if 'komentado_aliro_kodo' in kwargs and not message:
                        try:
                            komentado_aliro = KomunumojAliro.objects.get(
                                kodo=kwargs.get('komentado_aliro_kodo'), forigo=False, arkivo=False, publikigo=True
                            )
                        except KomunumojAliro.DoesNotExist:
                            message = _('Неверный доступ к категории конференции')

                    # Ищем запись для изменения
                    if not message:
                        try:
                            temo = KonferencojTemo.objects.get(uuid=kwargs.get('uuid'), forigo=False,
                                                              )
                            if (not uzanto.has_perm('konferencoj.povas_forigi_teman')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('konferencoj.povas_shanghi_teman'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                temo.tipo = tipo or temo.tipo
                                temo.kategorio = kategorio or temo.kategorio
                                temo.posedanto = posedanto or temo.posedanto
                                temo.komentado_aliro = komentado_aliro or temo.komentado_aliro
                                temo.fermita = kwargs.get('fermita', temo.forigo)
                                temo.fiksa = kwargs.get('fiksa', temo.fiksa)
                                temo.fiksa_listo = kwargs.get('fiksa_listo', temo.fiksa_listo)
                                temo.forigo = kwargs.get('forigo', temo.forigo)
                                temo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                temo.arkivo = kwargs.get('arkivo', temo.arkivo)
                                temo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                temo.publikigo = kwargs.get('publikigo', temo.publikigo)
                                temo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                temo.lasta_autoro = uzanto
                                temo.lasta_dato = timezone.now()

                                update_fields = [
                                    'fermita', 'fiksa', 'fiksa_listo', 'tipo', 'kategorio', 'komentado_aliro',
                                    'posedanto', 'forigo', 'foriga_dato', 'arkivo', 'arkiva_dato',
                                    'publikigo', 'publikiga_dato', 'lasta_autoro', 'lasta_dato',
                                ]

                                if kwargs.get('nomo', False):
                                    set_enhavo(temo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('nomo')
                                if kwargs.get('priskribo', False):
                                    set_enhavo(temo.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('priskribo')

                                temo.save(update_fields=update_fields)
                                status = True
                                message = _('Запись успешно изменена')
                        except KonferencojTemo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKonferencojTemo(status=status, message=message, temo=temo)


class RedaktuKonferencojTemoKomento(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    temo_komento = graphene.Field(KonferencojTemoKomentoNode,
                                  description=_('Созданная/изменённая запись комментария к теме конференции'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        teksto = graphene.String(description=_('Текст комментария конференции'))
        posedanto_id = graphene.String(description=_('Код владельца комментария'))
        komento_uuid = graphene.String(description=_('Код комментируемого сообщения'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        posedanto = None
        komento = None
        komentoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if (uzanto.has_perm('konferencoj.povas_krei_teman_komenton')
                            or perms.has_registrita_perm('konferencoj.povas_krei_teman_komenton')):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('teksto', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'teksto')

                        if not message:
                            if not (kwargs.get('posedanto_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'posedanto_id')
                            else:
                                try:
                                    posedanto = KonferencojTemo.objects.get(id=kwargs.get('posedanto_id'),
                                                                            forigo=False,
                                                                            arkivo=False, publikigo=True)
                                except KonferencojTemo.DoesNotExist:
                                    message = _('Неверная тема конференции')

                        if not message:
                            if (kwargs.get('komento_uuid', False)):
                                try:
                                    komentoj = KonferencojTemoKomento.objects.get(uuid=kwargs.get('komento_uuid'),
                                                                            forigo=False,
                                                                            arkivo=False, publikigo=True)
                                except KonferencojTemoKomento.DoesNotExist:
                                    message = _('Неверное комментируемое сообщение')

                        if not message:
                            komento = KonferencojTemoKomento.objects.create(
                                posedanto=posedanto,
                                autoro=uzanto,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now(),
                                komento=komentoj
                            )

                            update_fields = []
                            set_enhavo(komento.teksto, kwargs.get('teksto'), info.context.LANGUAGE_CODE)
                            update_fields.append('teksto')

                            komento.save(update_fields=update_fields)

                            status = True
                            message = _('Запись создана')

                            # Отправить сообщение о новой записи в чате всем остальным участникам чата 
                            sciigi_konferencoj.delay(komento.uuid)
                            # send_email_comment_publication.delay(komento.uuid)

                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('teksto', False) or kwargs.get('posedanto_id', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False) or kwargs.get('komento_uuid', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'posedanto_id' in kwargs and not message:
                        try:
                            posedanto = KonferencojTemo.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                        except KonferencojTemo.DoesNotExist:
                            message = _('Неверный тип категории конференции')

                    if not message:
                        if (kwargs.get('komento_uuid', False)):
                            try:
                                komentoj = KonferencojTemoKomento.objects.get(uuid=kwargs.get('komento_uuid'),
                                                                            forigo=False,
                                                                            arkivo=False, publikigo=True)
                            except KonferencojTemoKomento.DoesNotExist:
                                message = _('Неверное комментируемое сообщение')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            komento = KonferencojTemoKomento.objects.get(uuid=kwargs.get('uuid'), forigo=False,
                                                                        )
                            if (not uzanto.has_perm('konferencoj.povas_forigi_teman_komenton')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('konferencoj.povas_shanghi_teman_komenton'):
                                message = _('Недостаточно прав для изменения')
                            else:

                                update_fields = [
                                    'posedanto', 'forigo', 'foriga_dato', 'arkivo', 'arkiva_dato',
                                    'publikigo', 'publikiga_dato', 'lasta_autoro', 'lasta_dato',
                                    'komento',
                                ]

                                komento.posedanto = posedanto or komento.posedanto
                                komento.komento = komentoj or komento.komento
                                komento.forigo = kwargs.get('forigo', komento.forigo)
                                komento.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                komento.arkivo = kwargs.get('arkivo', komento.arkivo)
                                komento.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                komento.publikigo = kwargs.get('publikigo', komento.publikigo)
                                komento.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('teksto', False):
                                    set_enhavo(komento.teksto, kwargs.get('teksto'), info.context.LANGUAGE_CODE)
                                    update_fields.append('teksto')

                                komento.save(update_fields=update_fields)
                                status = True
                                message = _('Запись успешно изменена')
                        except KonferencojTemoKomento.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKonferencojTemoKomento(status=status, message=message, temo_komento=komento)


# Таблица подписок пользователей на Темы конференций
class RedaktuKonferencojSciigoj(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    sciigoj = graphene.Field(KonferencojSciigojNode, description=_('Созданная/изменённая подписка пользователя на тему конференции'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        temo_id = graphene.String(description=_('Код типа темы конференции'))
        sciigoj_id = graphene.List(graphene.String, required=True,
            description=_('Список кодов типов подписок для подписываемого пользователя на тему конференции'))
        autoro_id = graphene.String(description=_('Код подписываемого пользователя на тему конференции'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        inf_sciigoj = None
        novo_sciigoj = []
        temo = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('konferencoj.povas_krei_sciigoj'):
                        # Проверяем наличие всех обязательных полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('temo_id', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'temo_id')
                        else:
                            try:
                                temo = KonferencojTemo.objects.get(id=kwargs.get('temo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                            except KonferencojTemo.DoesNotExist:
                                message = _('Неверный тип темы конференции')

                        # Проверяем наличие записи с таким кодом
                        if not message:
                            if (kwargs.get('autoro_id', False)):
                                try:
                                    autoro = Uzanto.objects.get(id=kwargs.get('autoro_id'), is_active=True,
                                                konfirmita=True)
                                except Uzanto.DoesNotExist:
                                    message = _('Неверный пользователь конференции')
                            else:
                                autoro = uzanto

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            if kwargs.get('sciigoj_id', False):
                                for sciigo in kwargs.get('sciigoj_id'):
                                    try:
                                        novo_sciigoj.append(InformilojSciigoTipo.objects.get(kodo=sciigo, forigo=False))
                                        # novo_sciigoj.append(InformilojSciigoTipo.objects.filter(kodo__in=sciigo, forigo=False))
                                    except InformilojSciigoTipo.DoesNotExist:
                                        message = _('Неверный возможный вариант подписки'+sciigo)

                        if not message:
                            sciigoj = KonferencojSciigoj.objects.create(
                                autoro=autoro,
                                temo=temo,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            sciigoj.sciigoj.set(novo_sciigoj)

                            sciigoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('autoro_id', False) or kwargs.get('sciigoj_id', False)
                            or kwargs.get('temo_id', False) or kwargs.get('publikigo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not (kwargs.get('temo_id', False) or message):
                        message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'temo_id')
                    else:
                        try:
                            temo = KonferencojTemo.objects.get(id=kwargs.get('temo_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except KonferencojTemo.DoesNotExist:
                            message = _('Неверный тип темы конференции')

                    # Проверяем наличие записи с таким кодом
                    if not message:
                        if (kwargs.get('autoro_id', False)):
                            try:
                                autoro = Uzanto.objects.get(id=kwargs.get('autoro_id'), is_active=True,
                                            konfirmita=True)
                            except Uzanto.DoesNotExist:
                                message = _('Неверный пользователь конференции')
                        else:
                            autoro = uzanto

                    # проверяем наличие записей с таким кодом в списке
                    if not message:
                        if kwargs.get('sciigoj_id', False):
                            for sciigo in kwargs.get('sciigoj_id'):
                                try:
                                    novo_sciigoj.append(InformilojSciigoTipo.objects.filter(kodo__in=sciigo, forigo=False))
                                except InformilojSciigoTipo.DoesNotExist:
                                    message = _('Неверный возможный вариант подписки'+sciigo)

                    # Ищем запись для изменения
                    if not message:
                        try:
                            sciigoj = KonferencojSciigoj.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('konferencoj.povas_forigi_sciigoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('konferencoj.povas_shanghi_sciigoj'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                sciigoj.temo = temo or sciigoj.temo
                                sciigoj.autoro = autoro or sciigoj.autoro
                                sciigoj.forigo = kwargs.get('forigo', sciigoj.forigo)
                                sciigoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                sciigoj.arkivo = kwargs.get('arkivo', sciigoj.arkivo)
                                sciigoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                sciigoj.publikigo = kwargs.get('publikigo', sciigoj.publikigo)
                                sciigoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                sciigoj.lasta_autoro = uzanto
                                sciigoj.lasta_dato = timezone.now()

                                if kwargs.get('sciigoj_id', False):
                                    sciigoj.sciigoj.set(novo_sciigoj)

                                sciigoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KonferencojSciigoj.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKonferencojSciigoj(status=status, message=message, sciigoj=sciigoj)


class KonferencojMutations(graphene.ObjectType):
    redaktu_konferencoj_kategorio_tipo = RedaktuKonferencojKategorioTipo.Field(
        description=_('''Создаёт или редактирует типы категории конференции''')
    )
    redaktu_konferencoj_kategorio = RedaktuKonferencojKategorio.Field(
        description=_('''Создаёт или редактирует категории конференции''')
    )
    redaktu_konferencoj_temo_tipo = RedaktuKonferencojTemoTipo.Field(
        description=_('''Создаёт или редактирует типы темы конференции''')
    )
    redaktu_konferencoj_temo = RedaktuKonferencojTemo.Field(
        description=_('''Создаёт или редактирует типы темы конференции''')
    )
    redaktu_konferencoj_temo_komento = RedaktuKonferencojTemoKomento.Field(
        description=_('''Создаёт или редактирует комментарии конференции''')
    )
    redaktu_konferencoj_sciigoj = RedaktuKonferencojSciigoj.Field(
        description=_('''Создаёт или редактирует подписки пользователей на конференции''')
    )
    enketo = Enketo.Field(
        description=_('''Отправляет три строки по заранее заданному адресу''')
    )
