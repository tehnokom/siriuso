"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""


import django.dispatch

# Сигнал о публикации нового комментария
# konferenca_komento_poshtita = django.dispatch.Signal(providing_args=['komento', 'ago_uzanto'])
konferenca_komento_poshtita = django.dispatch.Signal()

# Сигнал об изменении опубликованного комментария
# konferenca_komento_modifita = django.dispatch.Signal(providing_args=['komento', 'ago_uzanto'])
konferenca_komento_modifita = django.dispatch.Signal()

# Сигнал об удалении комментария
# konferenca_komenti_forigita = django.dispatch.Signal(providing_args=['komento', 'ago_uzanto'])
konferenca_komenti_forigita = django.dispatch.Signal()
