"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from celery import shared_task
from django.core.mail import EmailMessage
from django.core import mail
from django.template.loader import get_template
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.contrib.sites.models import Site
from main.models import Uzanto

from datetime import timedelta


class HtmlEmailMessage(EmailMessage):
    content_subtype = 'html'


@shared_task
def send_email_confirmcode(mailto, code, email=True):
    email_body = get_template('uzantoj/emails/confirmcode.html')\
        .render({'confirmcode': code, 'site_url': Site.objects.get_current(), 'abono_shlosilo': False, 'poshto': email})

    email_message = {
        'subject': _('Код подтверждения действия на Техноком'),
        'body': email_body,
    }

    email = (HtmlEmailMessage(**email_message, to=(mailto,)),)

    with mail.get_connection() as connection:
        result = connection.send_messages(email)
    return result


@shared_task
def forigi_nekonfirmitajn_uzantojn(delta_hours=3):
    tt = timezone.now() - timedelta(hours=delta_hours)

    return Uzanto.objects.filter(
        konfirmita=False,
        krea_dato__lte=tt
    ).delete()
