"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from uzantoj.models import *


# class HomoContactInLine(admin.TabularInline):
#     model = HomoContact
#
#
# class HomoEducationInLine(admin.StackedInline):
#     model = HomoEducation


# Доступ (видимость) к различным сущностям привязанным к пользователю
@admin.register(UzantojAliro)
class UzantojAliroAdmin (admin.ModelAdmin):
    list_display = ('nomo',)
    exclude = ('nomo',)

    class Meta:
        model = UzantojAliro


# Статусы пользователей
@admin.register(UzantojStatuso)
class UzantojStatusoAdmin (admin.ModelAdmin):
    list_display = ('uuid', 'teksto')
    exclude = ('teksto',)
    search_fields = ('teksto',)
    ordering = ('uuid', 'teksto')

    class Meta:
        model = UzantojStatuso


# Типы товарищей
@admin.register(UzantojGekamaradojTipo)
class UzantojGekamaradojTipoAdmin (admin.ModelAdmin):
    list_display = ('nomo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = UzantojGekamaradojTipo


# Товарищи
@admin.register(UzantojGekamaradoj)
class UzantojGekamaradojAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'posedanto', 'gekamarado', 'tipo')

    class Meta:
        model = UzantojGekamaradoj
