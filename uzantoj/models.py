"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import models
from django.utils.translation import gettext_lazy as _

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo
from main.models import SiriusoBazaAbstraktaUzanto, SiriusoTipoAbstrakta, SiriusoKomentoAbstrakta
from main.models import Uzanto


# Варианты доступа (видимости) к сущностям пользователя, использует абстрактный класс SiriusoTipoAbstrakta
class UzantojAliro(SiriusoTipoAbstrakta):

    # Код доступа
    kodo = models.CharField(_('Kodo'), max_length=16)

    # Обозначения доступов
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'uzantoj_aliroj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Aliro')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Aliroj')


# Типы товарищей, использует абстрактный класс SiriusoTipoAbstrakta
class UzantojGekamaradojTipo(SiriusoTipoAbstrakta):

    # код товарища
    kodo = models.CharField(_('Kodo'), max_length=10, blank=False)

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'uzantoj_gekamaradoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de gekamaradoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de gekamaradoj')


# Товарищи, использует абстрактный класс SiriusoBazaAbstraktaUzanto
class UzantojGekamaradoj(SiriusoBazaAbstraktaUzanto):

    # тип товарища
    tipo = models.ForeignKey(UzantojGekamaradojTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             related_name='%(app_label)s_%(class)s_tipo',
                             on_delete=models.CASCADE)

    # # тип принят (первым пользователем)
    # akceptis_tipo1 = models.BooleanField(_('Akceptis tipo (de sendinto)'), blank=False, default=True)
    #
    # # тип принят (вторым пользователем)
    # akceptis_tipo2 = models.BooleanField(_('Akceptis tipo (de akceptinto)'), blank=False, default=False)
    
    # товарищ (пользователь)
    gekamarado = models.ForeignKey(Uzanto, verbose_name=_('Posedanto'), blank=False, default=None, 
                                   related_name='%(app_label)s_%(class)s_gekamarado', on_delete=models.CASCADE)

    # заявка принята подавшим (первым пользователем)
    akceptis1 = models.BooleanField(_('Akceptis (de sendinto)'), blank=False, default=True)

    # заявка принята принявшим (вторым пользователем)
    akceptis2 = models.BooleanField(_('Akceptis (de akceptinto)'), blank=True, default=None, null=True)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'uzantoj_gekamaradoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Gekamarado')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Gekamaradoj')

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)


# Статусы пользователей, использует абстрактный класс SiriusoBazaAbstraktaUzanto
class UzantojStatuso(SiriusoBazaAbstraktaUzanto):

    # текст в таблице текстов комментариев обложек, от туда будет браться текст с нужным языковым тегом
    teksto = models.JSONField(verbose_name=_('Teksto'), blank=True, null=False, default=default_lingvo,
                                encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'uzantoj_statusoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Statuso')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statusoj')

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле teksto этой модели
        return '{}'.format(get_enhavo(self.teksto, empty_values=True)[0])


# Уведомления для пользователей
class UzantojSciigoj(SiriusoBazaAbstraktaUzanto):
    # текстовое содержание уведомления
    teksto = models.TextField(_('Teksto'), blank=False, null=False)

    # атрибуты объектов события (если таковые есть)
    atributoj = models.JSONField(_('Atributoj'), blank=True, null=True)

    # дата прочтения
    leganta_dato = models.DateTimeField(_('Leganta dato'), blank=True, null=True)

    # признак прочтения уведомления
    vidita = models.BooleanField(_('Vidita'), blank=True, default=False)

    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'uzantoj_sciigoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Uzanto sciigo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Uzantoj sciigoj')

    def __str__(self):
        return str(self.uuid)
