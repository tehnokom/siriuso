"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.db.models import Q
from django.contrib.auth import authenticate, login, logout
from django.core.cache import cache
import graphene
import secrets
import re

from fotoj.api.mutations import InstaliAvataron, InstaliKovrilon, FotojUzantojAvataro, FotojUzantojKovrilo
from siriuso.api.types import ErrorNode, SiriusoLingvo
from siriuso.utils import set_enhavo, default_lingvo, set_token_session
from uzantoj.sciigoj import sendu_sciigon
from informiloj.api.schema import InformilojLingvoNode, InformilojLandoNode, InformilojRegionoNode
from informiloj.models import InformilojLingvo, InformilojLando, InformilojRegiono
from komunumoj.models import Komunumo, KomunumoMembro, KomunumoMembroTipo
from komunumoj.api.schema import KomunumoNode
from uzantoj.tasks import send_email_confirmcode
from uzantoj.models import (UzantojGekamaradoj, UzantojGekamaradojTipo, 
                            UzantojAliro, UzantojStatuso)
from .schema import UzantoProprietoj
from .sciigoj import UzantoSciigoNode, UzantojSciigoj
from fotoj.api.schema import FotojUzantoAvataroNode, FotojUzantoKovriloNode

from main.models import Uzanto


def _check_mail(email):
    status = False
    message = None

    if re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", email) is None:
        message = _('Не является Email')
    else:
        try:
            Uzanto.objects.get(chefa_retposhto=email.lower())
            message = _('Такой адрес электронной почты уже используется')
        except Uzanto.DoesNotExist:
            status = True

    return (status, message)


def _check_tel(tel):
    status = False
    message = None

    if re.match(r"(^\+[0-9]{1,5}[0-9*#]{3,10}$)", tel) is None:
        message = _('Не является номером телефона')
    else:
        try:
            Uzanto.objects.get(chefa_telefonanumero=tel)
            message = _('Такой номер телефона уже используется')
        except Uzanto.DoesNotExist:
            status = True

    return (status, message)


class ShanghiPoshtonTelefonon(graphene.Mutation):
    class Arguments:
        user_id = graphene.Int()
        konfirma_kodo = graphene.String()
        retposhto = graphene.String()
        telefona_nombro = graphene.String()

    status = graphene.Boolean()
    message = graphene.String()

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = _('Внутренняя ошибка сервера')
        user = info.context.user
        # тут надо будет доделать проверку на администратора,
        # который может поменять email пользователя, передав
        # параметр user_id
        user_id = user.id

        if user.is_authenticated:
            if 'retposhto' in kwargs and 'telefona_nombro' in kwargs:
                message = _('Должен быть задан только один изменяемый параметр')
            elif 'retposhto' in kwargs or 'telefona_nombro' in kwargs:
                if 'konfirma_kodo' in kwargs:
                    value = kwargs.get('retposhto').lower() if 'retposhto' in kwargs else kwargs.get('telefona_nombro')
                    key = 'change_mail' if 'retposhto' in kwargs else 'change_tel'
                    data = info.context.session.get(key)

                    if data is not None:
                        status = (data[2] > timezone.datetime.now() and data[1] == kwargs['konfirma_kodo'] and
                                  data[0] == value)

                        if status:
                            uzanto = Uzanto.objects.get(id=user_id)

                            if 'retposhto' in kwargs:
                                uzanto.chefa_retposhto = data[0]
                                message = _('Адрес электронной почты изменён')
                            else:
                                uzanto.chefa_telefonanumero = data[0]
                                message = _('Номер телефона изменён')

                            uzanto.malbona_retposhto = False
                            uzanto.save()
                            del info.context.session[key]
                            info.context.session.modified = True
                        else:
                            message = _('Hеверный код подтверждения или истекло время его действия')

                else:
                    if 'retposhto' in kwargs:
                        value = kwargs.get('retposhto').lower()
                        status, message = _check_mail(value)
                    else:
                        value = kwargs.get('telefona_nombro')
                        status, message = _check_tel(value)

                    new_key = secrets.token_hex(nbytes=4).upper()
                    end_date = timezone.datetime.now() + timezone.timedelta(seconds=600)

                    if status:
                        to_email = value if 'retposhto' in kwargs else user.chefa_retposhto
                        key = 'change_mail' if 'retposhto' in kwargs else 'change_tel'
                        info.context.session[key] = (value, new_key, end_date)
                        send_email_confirmcode.delay(to_email, new_key, 'retposhto' in kwargs)
                        message = _('Код подтверждения выслан на адрес') + ' <%s>' % to_email
            else:
                message = _('Не задан ни один из обязательных параметров')
        else:
            message = _('Требуется авторизация')

        return ShanghiPoshtonTelefonon(status=status, message=message)


class Malaboni(graphene.Mutation):
    class Arguments:
        email = graphene.String(required=True)
        token = graphene.String(required=True)

    status = graphene.Boolean()
    message = graphene.String()

    @staticmethod
    def mutate(root, info, email, token):
        status = False

        if re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", email) is None:
            message = _('Не является Email')
        else:
            try:
                uzanto = (Uzanto.objects
                          .only('agordoj')
                          .get(chefa_retposhto=email))

                if 'abono_shlosilo' in uzanto.agordoj:
                    if token == uzanto.agordoj['abono_shlosilo']:
                        del uzanto.agordoj['abono_shlosilo']
                        uzanto.save()
                        status = True
                        message = _('Адрес электронной почты успешно исключён из списков рассылки')
                    else:
                        message = _('Неверный код подтверждения действия')
                else:
                    message = _('Адрес электронной почты уже исключён из списков рассылки')

            except Uzanto.DoesNotExist:
                message = _('Адрес электронной почты в списках рассылки не найден')

        return Malaboni(status=status, message=message)


class AplikiAgordoj(graphene.Mutation):
    class Arguments:
        id = graphene.Int()
        poshto_sciigoj = graphene.Boolean()
        unua_nomo = graphene.String()
        dua_nomo = graphene.String()
        familinomo = graphene.String()
        sekso = graphene.String()
        statuso = graphene.String()
        naskighdato = graphene.Date()
        lingvo = graphene.String()
        lando = graphene.String()
        regiono = graphene.String()
        chefa_organizo = graphene.Int()
        avataro = graphene.String()
        kovrilo = graphene.String()

    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)

    poshto_sciigoj = graphene.Boolean()
    unua_nomo = graphene.Field(SiriusoLingvo)
    dua_nomo = graphene.Field(SiriusoLingvo)
    familinomo = graphene.Field(SiriusoLingvo)
    sekso = graphene.String()
    statuso = graphene.Field(SiriusoLingvo)
    naskighdato = graphene.Date()
    lingvo = graphene.Field(InformilojLingvoNode)
    lando = graphene.Field(InformilojLandoNode)
    regiono = graphene.Field(InformilojRegionoNode)
    chefa_organizo = graphene.Field(KomunumoNode)
    avataro = graphene.Field(FotojUzantoAvataroNode)
    kovrilo = graphene.Field(FotojUzantoKovriloNode)

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = []
        out = {}

        if not info.context.user.is_authenticated:
            message = _('Требуется авторизация')
        else:

            if (not len(kwargs) or (len(kwargs) == 1 and 'id' in kwargs)) \
                    and 'avataro' not in info.context.FILES and 'kovrilo' not in info.context.FILES:
                message = _('Не передан ни один аргумент для изменения')
            else:
                uzanto_id = info.context.user.id
                # тут надо будет доделать проверку на администратора,
                # который может поменять email пользователя, передав
                # параметр user_id

                try:
                    uzanto = Uzanto.objects.select_related('chefa_lingvo').get(id=uzanto_id)

                    for arg, val in kwargs.items():
                        if arg == 'poshto_sciigoj':
                            if not val:
                                if 'abono_shlosilo' in uzanto.agordoj:
                                    del uzanto.agordoj['abono_shlosilo']
                            elif 'abono_shlosilo' not in uzanto.agordoj:
                                uzanto.agordoj['abono_shlosilo'] = secrets.token_urlsafe(24)

                            out[arg] = val

                        elif arg in ('unua_nomo', 'dua_nomo', 'familinomo'):
                            if len(arg) > 150 and arg != 'statuso':
                                errors.append(ErrorNode(field=arg,
                                                        message=_('Длина поля не может быть больше 150 символов'))
                                              )
                            elif not val and arg not in ('dua_nomo', 'statuso'):
                                errors.append(ErrorNode(field=arg,
                                                        message=_('Поле обязательно для заполнения'))
                                              )
                            else:
                                enhavo = getattr(uzanto, arg)
                                set_enhavo(enhavo, teksto=val, lingvo=uzanto.chefa_lingvo.kodo)
                                setattr(uzanto, arg, enhavo)
                                out[arg] = enhavo

                        elif arg == 'statuso':
                            if len(arg) > 512:
                                errors.append(
                                    ErrorNode(field=arg,
                                              message=_('Длина поля не может быть больше 512 символов'))
                                )
                            elif not len(arg):
                                (UzantojStatuso.objects.filter(posedanto=uzanto, forigo=False)
                                 .update(publikigo=False, forigo=True, arkivo=True))
                            else:
                                (UzantojStatuso.objects.filter(posedanto=uzanto, forigo=False)
                                 .update(publikigo=False, forigo=True, arkivo=True))

                                enhavo = default_lingvo()
                                set_enhavo(enhavo, teksto=val, lingvo=uzanto.chefa_lingvo.kodo)

                                out['statuso'] = (UzantojStatuso.objects
                                                  .create(posedanto=uzanto, publikigo=True, arkivo=False,
                                                          forigo=False, teksto=enhavo)).teksto

                        elif arg == 'sekso':
                            if val.lower() not in ('vira', 'virina'):
                                errors.append(
                                    ErrorNode(field=arg,
                                              message='%s: %s'
                                                      % (_('Неверное значение поля. Допустимые значения'),
                                                         '\'vira\', \'virina\''))
                                )
                            else:
                                out[arg] = val
                                setattr(uzanto, arg, val.lower())

                        elif arg in ('lingvo', 'lando', 'regiono'):
                            try:
                                if arg == 'lingvo':
                                    uzanto.chefa_lingvo = InformilojLingvo.objects.get(kodo=val, forigo=False)
                                    out[arg] = uzanto.chefa_lingvo
                                elif arg == 'lando':
                                    uzanto.loghlando = InformilojLando.objects.get(kodo=val, forigo=False)
                                    out[arg] = uzanto.loghlando
                                else:
                                    uzanto.loghregiono = (InformilojRegiono.objects
                                                          .get(kodo=val, forigo=False, lando=uzanto.loghlando)
                                                          )
                                    out[arg] = uzanto.loghregiono
                            except:
                                errors.append(ErrorNode(field=arg, message='Неверное значение поля'))

                        elif arg == 'chefa_organizo':
                            try:
                                organizo = Komunumo.objects.get(tipo__kodo='entrepreno',
                                                                speco__kodo='popolaentrepreno',
                                                                id=val,
                                                                forigo=False)
                                membra_tipo = KomunumoMembroTipo.objects.get(kodo='komunumano', forigo=False)

                                (KomunumoMembro.objects
                                 .filter(forigo=False, autoro_id=uzanto.id, posedanto__tipo__kodo='entrepreno',
                                         posedanto__speco__kodo='popolaentrepreno',
                                         tipo__kodo__in=['komunumano', 'komunumano-adm', 'komunumano-mod'])
                                 .update(forigo=True, lasta_autoro_id=uzanto.id, lasta_dato=timezone.now(),
                                         foriga_dato=timezone.now()))

                                (KomunumoMembro.objects
                                 .create(posedanto=organizo, forigo=False, autoro_id=uzanto.id,
                                         tipo=membra_tipo))

                                out[arg] = organizo
                            except (Komunumo.DoesNotExist, KomunumoMembroTipo.DoesNotExist):
                                errors.append(ErrorNode(field=arg, message='Неверное значение поля'))

                        elif arg == 'naskighdato':
                            if val < timezone.datetime(1900, 1, 1).date():
                                errors.append(ErrorNode(field=arg, message=_('Мне бы такое здоровье...')))
                            elif val > timezone.now().date() - timezone.timedelta(days=365):
                                errors.append(
                                    ErrorNode(field='naskighdato',
                                              message=_('А твои родители в курсе, что ты хочешь стать коммунистом?'))
                                )
                            else:
                                setattr(uzanto, arg, val)
                                out[arg] = val

                        elif arg in ('avataro', 'kovrilo'):
                            if arg == 'avataro':
                                (FotojUzantojAvataro.objects.filter(posedanto=uzanto, forigo=False, chefa_varianto=True)
                                 .update(aktiva=False, chefa_varianto=False))
                            else:
                                (FotojUzantojKovrilo.objects.filter(posedanto=uzanto, forigo=False, chefa_varianto=True)
                                 .update(aktiva=False, chefa_varianto=False))

                    # Устарело!!! Используйте мутации приложения fotoj
                    # Для обратной совместимости сделал вызов мутаций из fotoj
                    uzanto_aliro = UzantojAliro.objects.get(kodo='chiuj')

                    if 'avataro' in info.context.FILES:
                        info.context.FILES['bildoBaza'] = info.context.FILES['avataro']
                        params = {
                            'uzanto_id': uzanto_id,
                        }

                        avataro = InstaliAvataron.mutate(root, info, **params)
                        if not avataro.status:
                            for err in avataro.errors:
                                errors.append(ErrorNode(field='avataro', message=err))

                    if 'kovrilo' in info.context.FILES:
                        info.context.FILES['bildoBaza'] = info.context.FILES['kovrilo']
                        params = {
                            'uzanto_id': uzanto_id,
                        }

                        kovrilo = InstaliKovrilon.mutate(root, info, **params)
                        if not kovrilo.status:
                            for err in kovrilo.errors:
                                errors.append(ErrorNode(field='kovrilo', message=err))

                    if not len(errors):
                        uzanto.save()
                        status = True
                        message = _('Настройки применены')
                    else:
                        message = _('Ошибка изменения настроек пользователя')

                except Uzanto.DoesNotExist:
                    message = _('Пользователь не найден')

        return AplikiAgordoj(status=status, message=message, errors=errors, **out)


# Рассылка уведомления о действиях товарищества
def sendu_gekamarado_sciigon(teksto, to_uzanto, from_uzanto):
    return sendu_sciigon(
        teksto,
        teksto_parametroj={
            'unua_nomo': {
                'obj': from_uzanto,
                'field': 'unua_nomo'
            },
            'familinomo': {
                'obj': from_uzanto,
                'field': 'familinomo'
            }
        },
        to=(to_uzanto.id,),
        objektoj=(from_uzanto,)
    )


class Engekamarado(graphene.Mutation):
    class Arguments:
        uzanto_id = graphene.Int()
        kandidato_id = graphene.Int(required=True)

    status = graphene.Boolean()
    message = graphene.String()
    statistiko = graphene.Field(UzantoProprietoj)

    @staticmethod
    def mutate(root, info, kandidato_id, **kwargs):
        status = False
        statistiko = None

        if not info.context.user.is_authenticated:
            message = _('Требуется авторизация')
        else:
            if 'uzanto_id' in kwargs and not (info.context.user.is_admin or info.context.user.is_superuser):
                message = '%s \'%s\'' % (_('Недостаточно прав для использования аргумента'), 'uzanto_id')
            else:
                uzanto_id = kwargs.get('uzanto_id', info.context.user.id)
                uzanto = None
                kandidato = None

                # Проверяем наличие пользователя-заявителя и пользователя контрегнта
                try:
                    uzanto = Uzanto.objects.get(id=uzanto_id, is_active=True,
                                                konfirmita=True)
                    kandidato = Uzanto.objects.get(id=kandidato_id, is_active=True,
                                                   konfirmita=True)

                    # Проверка на наличие товарищества
                    relation = UzantojGekamaradoj.objects.filter(
                        Q(posedanto=uzanto, gekamarado=kandidato) |
                        Q(posedanto=kandidato, gekamarado=uzanto),
                        akceptis1=True,
                        arkivo=False,
                        forigo=False
                    ).order_by('-krea_dato')

                    # Исправляем ошибку, если обнаружено более нескольких
                    # товарищеских связей
                    if relation.count() > 1:
                        for i in range(1, relation.count()):
                            relation[i].forigo = True
                            relation[i].save()

                    relation = relation[0] if relation else None

                    if relation and (relation.akceptis2 or relation.akceptis2 is None):
                        if relation.gekamarado == uzanto:
                            # Принимаем предложение товарищества
                            relation.akceptis2 = True
                            relation.save()
                            status = True

                            # Посылаем уведомление о принятии товарищества
                            sendu_gekamarado_sciigon(
                                _('<em>%(unua_nomo)s %(familinomo)s</em> akceptis vian kamaradpeton'),
                                to_uzanto=relation.posedanto,
                                from_uzanto=relation.gekamarado
                            )

                            # Удаляем значение кол-ва друзей для контрагента и заявителя из кэша
                            cache_key = 'api_tuta_gekamaradoj'
                            cache_changed = False
                            val = cache.get(cache_key)

                            if val and kandidato_id in val:
                                del val[kandidato_id]
                                cache_changed = True

                            if val and uzanto_id in val:
                                del val[uzanto_id]
                                cache_changed = True

                            if cache_changed:
                                cache.set(cache_key, val, timeout=3600 * 24 * 10)

                            statistiko = UzantoProprietoj(parent=kandidato)
                            message = _('Предложение товарищества принято.')
                        else:
                            # Пользователь уже предложил товарищество
                            message = _('Предложение товарищества уже отправлено.')
                    elif kandidato != uzanto:
                        if relation:
                            relation.forigo = True
                            relation.save()

                        cache_key = 'api_gekamaradoj_peto_uzanto_%s' % info.context.user.id
                        cache_kandidato_key = 'api_kandidatoj_gekamaradoj_uzanto_%s' % kandidato_id

                        type = UzantojGekamaradojTipo.objects.get(kodo='gekamarado')
                        relation = UzantojGekamaradoj.objects.create(posedanto=uzanto, akceptis1=True, tipo=type,
                                                                     gekamarado=kandidato, akceptis2=None)
                        status = True
                        cache.delete(cache_key)
                        cache.delete(cache_kandidato_key)
                        statistiko = UzantoProprietoj(parent=kandidato)
                        message = _('Предложение товарищества отправлено.')

                        # Отправляем уведомление о предложении товарищества
                        sendu_gekamarado_sciigon(
                            _('<em>%(unua_nomo)s %(familinomo)s</em> sendis kamaradpeton al vi'),
                            to_uzanto=relation.gekamarado,
                            from_uzanto=relation.posedanto
                        )
                    else:
                        message = _('Нельзя предложить товарищество самому себе')

                except Uzanto.DoesNotExist:
                    if uzanto:
                        message = _('Кандидат в товарищи не существует или ещё не активировал свою учетную запись.')
                    else:
                        message = _('Заявитель не существует или ещё не активировал свою учетную запись.')

        if status:
            # Удаляем значение кол-ва друзей для контрагента и заявителя из кэша
            cache_key = 'api_tuta_gekamaradoj'
            val = cache.get(cache_key)

            if kandidato_id in val:
                del val[kandidato_id]
                cache.set(cache_key, val, timeout=3600 * 24 * 10)

        return Engekamarado(status=status, message=message, statistiko=statistiko)


class Elgekamarado(graphene.Mutation):
    class Arguments:
        uzanto_id = graphene.Int()
        kandidato_id = graphene.Int(required=True)

    status = graphene.Boolean()
    message = graphene.String()
    statistiko = graphene.Field(UzantoProprietoj)

    @staticmethod
    def mutate(root, info, kandidato_id, **kwargs):
        status = False
        statistiko = None
        kandidato = None

        if not info.context.user.is_authenticated:
            message = _('Требуется авторизация')
        else:
            if 'uzanto_id' in kwargs and not (info.context.user.is_admin or info.context.user.is_superuser):
                message = '%s \'%s\'' % (_('Недостаточно прав для использования аргумента'), 'uzanto_id')
            else:
                uzanto_id = kwargs.get('uzanto_id', info.context.user.id)
                uzanto = None

                # Проверяем наличие пользователя-заявителя и пользователя контрегнта
                try:
                    uzanto = Uzanto.objects.get(id=uzanto_id, is_active=True,
                                                konfirmita=True)
                    kandidato = Uzanto.objects.get(id=kandidato_id, is_active=True,
                                                   konfirmita=True)

                    # Проверка на наличие товарищества
                    relation = UzantojGekamaradoj.objects.filter(
                        Q(posedanto=uzanto, gekamarado=kandidato) |
                        Q(posedanto=kandidato, gekamarado=uzanto),
                        akceptis1=True,
                        arkivo=False,
                        forigo=False
                    ).order_by('-krea_dato')

                    if relation.count():
                        # Исправляем ошибку, если обнаружено более нескольких
                        # товарищеских связей
                        if relation.count() > 1:
                            for i in range(1, relation.count()):
                                relation[i].arkivo = True
                                relation[i].save()
                        relation = relation[0]

                        # Отказываем в товариществе
                        if relation.gekamarado == uzanto and relation.akceptis2 is None:
                            relation.akceptis2 = False
                            relation.save()
                            message = _('Предложение товарищества отклонено.')
                            status = True
                        elif relation.posedanto == uzanto and relation.akceptis2 is None:
                            # Отменяем запрос пользователя к контрагенту
                            relation.forigo = True
                            relation.save()
                            status = True
                            message = _('Запрос товарищества отменён')

                            cache_key = 'api_gekamaradoj_peto_uzanto_%s' % uzanto_id
                            cache_kandidato_key = 'api_kandidatoj_gekamaradoj_uzanto_%s' % kandidato_id
                            cache.delete(cache_key)
                            cache.delete(cache_kandidato_key)
                        elif relation.akceptis1 and relation.akceptis2:
                            # Отменяем товарищество
                            if relation.posedanto == uzanto:
                                relation.akceptis1 = False
                            else:
                                relation.akceptis2 = False

                            relation.forigo = True
                            relation.save()
                            message = _('Пользователь исключен из товарищей.')
                            status = True

                            # Чистим кратковременный кэш
                            cache_gekamarado_uzanto_key = 'api_gekamaradoj_uzanto_%s' % uzanto_id
                            cache_gekamarado_kandidato_key = 'api_gekamaradoj_uzanto_%s' % kandidato_id
                            cache.delete(cache_gekamarado_uzanto_key)
                            cache.delete(cache_gekamarado_kandidato_key)

                            # Удаляем значение кол-ва друзей для контрагента и заявителя из кэша
                            cache_key = 'api_tuta_gekamaradoj'

                            cache_changed = False
                            val = cache.get(cache_key)

                            if val and kandidato_id in val:
                                del val[kandidato_id]
                                cache_changed = True

                            if val and uzanto_id in val:
                                del val[uzanto_id]
                                cache_changed = True

                            if cache_changed:
                                cache.set(cache_key, val, timeout=3600 * 24 * 10)
                        else:
                            # Товарищество уже отменено или отклонено
                            message = _('Товарищество с пользователем уже отменено.')
                    else:
                        # Не существует предложения или товарищества
                        message = _('Товарищеская связь или предложение товарищества не существует.')

                except Uzanto.DoesNotExist:
                    if uzanto:
                        message = _('Кандидат в товарищи не существует или ещё не активировал свою учетную запись.')
                    else:
                        message = _('Заявитель не существует или ещё не активировал свою учетную запись.')

        if status:
            # Собираем статистику по контрагенту
            statistiko = UzantoProprietoj(parent=kandidato)
            # Удаляем значение кол-ва друзей для контрагента и заявителя из кэша
            cache_key = 'api_tuta_gekamaradoj'
            val = cache.get(cache_key)

            if kandidato_id in val:
                del val[kandidato_id]
                cache.set(cache_key, val, timeout=3600 * 24 * 10)

        return Elgekamarado(status=status, message=message, statistiko=statistiko)


class ShanghiPasvorton(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    csrf_token = graphene.String()

    class Arguments:
        uzanto_id = graphene.Int()
        pasvorto = graphene.String()
        nova_pasvorto = graphene.String(required=True)

    @staticmethod
    def mutate(root, info, nova_pasvorto, **kwargs):
        status = False
        message = None
        csrf = None

        if info.context.user.is_authenticated:
            cur_uzanto_id = info.context.user.id
            has_admin_perms = info.context.user.is_admin or info.context.user.is_superuser

            if ('uzanto_id' in kwargs and has_admin_perms) or 'uzanto_id' not in kwargs:
                if 'uzanto_id' in kwargs:
                    try:
                        uzanto = Uzanto.objects.get(id=kwargs.get('uzanto_id'), is_active=True, konfirmita=True)
                    except Uzanto.DoesNotExist:
                        uzanto = None
                elif 'pasvorto' in kwargs:
                    uzanto = info.context.user
                else:
                    return ShanghiPasvorton(status=status, message=_('Не указан действующий пароль'))

                if uzanto:
                    auth_uzanto = authenticate(info.context,
                                               username=uzanto.chefa_retposhto, password=kwargs.get('pasvorto'))

                    if auth_uzanto:
                        if auth_uzanto.id == cur_uzanto_id:
                            logout(info.context)

                        auth_uzanto.set_password(nova_pasvorto)
                        auth_uzanto.save()

                        if auth_uzanto.id == cur_uzanto_id:
                            login(info.context, auth_uzanto)
                            set_token_session(info.context.META['HTTP_X_AUTH_TOKEN'], info.context)

                        status = True
                        csrf = info.context.META.get('CSRF_COOKIE')
                        message = _('Пароль пользователя успешно изменён')
                    else:
                        message = _('Неверный текущий пароль')

                else:
                    message = _('Пользователь не найден')
            else:
                message = _('Не достаточно прав для использования аргумента \'uzanto_id\'')
        else:
            message = _('Требуется авторизация')

        return ShanghiPasvorton(status=status, message=message, csrf_token=csrf)


class EtikedaSciigo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    sciigoj = graphene.List(of_type=UzantoSciigoNode)

    class Arguments:
        sciigoj_uuid = graphene.List(of_type=graphene.UUID)

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        uzanto = info.context.user
        sciigoj = list()

        if uzanto.is_authenticated:
            if 'sciigoj_uuid' in kwargs and len(kwargs.get('sciigoj_uuid')):
                params = {
                    'uuid__in': kwargs.get('sciigoj_uuid')
                }
            else:
                params = dict()

            sciigoj = UzantojSciigoj.objects.filter(
                posedanto=uzanto,
                forigo=False,
                arkivo=False,
                publikigo=True,
                vidita=False,
                **params
            )

            if sciigoj:
                if len(params):
                    # Проверяем количество найденных уведомлений
                    dif = set(kwargs.get('sciigoj_uuid')) - set(sciigoj.values_list('uuid', flat=True))

                    if len(dif):
                        message = _('Не найдены или уже помечены уведомления с UUID: %s') % ', '.join(dif)

                sciigoj.update(vidita=True)
                sciigoj = UzantojSciigoj.objects.filter(
                    posedanto=uzanto,
                    forigo=False,
                    arkivo=False,
                    publikigo=True,
                    **params
                )

                status = True
                message = message if message else _('Уведомления успешно помечены')
            else:
                message = _('Уведомления для пометки не найдены или уже помечены')

        else:
            message = _('Требуется авторизация')

        return EtikedaSciigo(status=status, message=message, sciigoj=(sciigoj if sciigoj else None))


class Mutations(graphene.ObjectType):
    shanghi_poshton_telefonon = ShanghiPoshtonTelefonon.Field()
    shanghi_pasvorton = ShanghiPasvorton.Field()
    malaboni = Malaboni.Field()
    apliki_agordoj = AplikiAgordoj.Field()
    engekamarado = Engekamarado.Field()
    elgekamarado = Elgekamarado.Field()
    etikeda_sciigo = EtikedaSciigo.Field()
