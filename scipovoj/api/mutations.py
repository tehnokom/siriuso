"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from .schema import *
from ..models import *


# Модель категорий навыков
class RedaktuScipovoKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    scipovoj_kategorioj = graphene.Field(ScipovoKategorioNode,
        description=_('Созданная/изменённая категория навыков'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        scipovoj_kategorioj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('scipovoj.povas_krei_scipovoj_kategorioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            scipovoj_kategorioj = ScipovoKategorio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(scipovoj_kategorioj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(scipovoj_kategorioj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    scipovoj_kategorioj.realeco.set(realeco)
                                else:
                                    scipovoj_kategorioj.realeco.clear()

                            scipovoj_kategorioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            scipovoj_kategorioj = ScipovoKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('scipovoj.povas_forigi_scipovoj_kategorioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('scipovoj.povas_shanghi_scipovoj_kategorioj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                scipovoj_kategorioj.forigo = kwargs.get('forigo', scipovoj_kategorioj.forigo)
                                scipovoj_kategorioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                scipovoj_kategorioj.arkivo = kwargs.get('arkivo', scipovoj_kategorioj.arkivo)
                                scipovoj_kategorioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                scipovoj_kategorioj.publikigo = kwargs.get('publikigo', scipovoj_kategorioj.publikigo)
                                scipovoj_kategorioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                scipovoj_kategorioj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(scipovoj_kategorioj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(scipovoj_kategorioj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        scipovoj_kategorioj.realeco.set(realeco)
                                    else:
                                        scipovoj_kategorioj.realeco.clear()

                                scipovoj_kategorioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ScipovoKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuScipovoKategorio(status=status, message=message, scipovoj_kategorioj=scipovoj_kategorioj)


# Модель типов навыков
class RedaktuScipovoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    scipovoj_tipoj = graphene.Field(ScipovoTipoNode, 
        description=_('Созданный/изменённый тип навыков'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        scipovoj_tipoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('scipovoj.povas_krei_scipovoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            scipovoj_tipoj = ScipovoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(scipovoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(scipovoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    scipovoj_tipoj.realeco.set(realeco)
                                else:
                                    scipovoj_tipoj.realeco.clear()

                            scipovoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            scipovoj_tipoj = ScipovoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('scipovoj.povas_forigi_scipovoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('scipovoj.povas_shanghi_scipovoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                scipovoj_tipoj.forigo = kwargs.get('forigo', scipovoj_tipoj.forigo)
                                scipovoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                scipovoj_tipoj.arkivo = kwargs.get('arkivo', scipovoj_tipoj.arkivo)
                                scipovoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                scipovoj_tipoj.publikigo = kwargs.get('publikigo', scipovoj_tipoj.publikigo)
                                scipovoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                scipovoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(scipovoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(scipovoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        scipovoj_tipoj.realeco.set(realeco)
                                    else:
                                        scipovoj_tipoj.realeco.clear()

                                scipovoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ScipovoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuScipovoTipo(status=status, message=message, scipovoj_tipoj=scipovoj_tipoj)


# Модель уровней навыков
class RedaktuScipovoNivelo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    scipovoj_tipoj = graphene.Field(ScipovoNiveloNode, 
        description=_('Созданный/изменённый уровень навыков'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nivelo = graphene.Int(description=_('Значение уровня'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        scipovoj_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('scipovoj.povas_krei_scipovoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            scipovoj_tipoj = ScipovoNivelo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                nivelo = kwargs.get('nivelo', 0),
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            scipovoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nivelo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            scipovoj_tipoj = ScipovoNivelo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('scipovoj.povas_forigi_scipovoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('scipovoj.povas_shanghi_scipovoj_tipoj'):
                                message = _('Недостаточно прав для изменения')
                            if not message:

                                scipovoj_tipoj.forigo = kwargs.get('forigo', scipovoj_tipoj.forigo)
                                scipovoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                scipovoj_tipoj.arkivo = kwargs.get('arkivo', scipovoj_tipoj.arkivo)
                                scipovoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                scipovoj_tipoj.publikigo = kwargs.get('publikigo', scipovoj_tipoj.publikigo)
                                scipovoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                scipovoj_tipoj.autoro = uzanto
                                scipovoj_tipoj.nivelo = kwargs.get('nivelo', scipovoj_tipoj.nivelo)

                                scipovoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ScipovoNivelo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuScipovoNivelo(status=status, message=message, scipovoj_tipoj=scipovoj_tipoj)


# Модель навыков
class RedaktuScipovo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    scipovoj = graphene.Field(ScipovoNode, 
        description=_('Созданный/изменённый навык'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий навыков'))
        tipo_id = graphene.Int(description=_('Код типа навыков'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        scipovoj = None
        realeco = Realeco.objects.none()
        kategorio = ScipovoKategorio.objects.none()
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('scipovoj.povas_krei_scipovoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одно значение'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            if 'kategorio' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                            elif not len(kwargs.get('kategorio')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одно значение'),'kategorio')
                            else:
                                kategorio_id = set(kwargs.get('kategorio'))

                                if len(kategorio_id):
                                    kategorio = ScipovoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие категория навыков'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = ScipovoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ScipovoTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип навыков'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип навыков'),'tipo_id')

                        if not message:
                            scipovoj = Scipovo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                tipo = tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(scipovoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(scipovoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    scipovoj.realeco.set(realeco)
                                else:
                                    scipovoj.realeco.clear()

                            if 'kategorio' in kwargs:
                                if kategorio:
                                    scipovoj.kategorio.set(kategorio)
                                else:
                                    scipovoj.kategorio.clear()

                            scipovoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False) or kwargs.get('kategorio', False)
                            or kwargs.get('tipo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            scipovoj = Scipovo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('scipovoj.povas_forigi_scipovoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('scipovoj.povas_shanghi_scipovoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))

                                    if len(kategorio_id):
                                        kategorio = ScipovoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие категория навыков'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = ScipovoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ScipovoTipo.DoesNotExist:
                                        message = _('Неверный тип навыков')
                            if not message:

                                scipovoj.forigo = kwargs.get('forigo', scipovoj.forigo)
                                scipovoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                scipovoj.arkivo = kwargs.get('arkivo', scipovoj.arkivo)
                                scipovoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                scipovoj.publikigo = kwargs.get('publikigo', scipovoj.publikigo)
                                scipovoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                scipovoj.autoro = uzanto
                                scipovoj.tipo = tipo if kwargs.get('tipo_id', False) else scipovoj.tipo

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(scipovoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(scipovoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        scipovoj.realeco.set(realeco)
                                    else:
                                        scipovoj.realeco.clear()
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        scipovoj.kategorio.set(kategorio)
                                    else:
                                        scipovoj.kategorio.clear()

                                scipovoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except Scipovo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuScipovo(status=status, message=message, scipovoj=scipovoj)


# Модель навыков объектов
class RedaktuScipovoObjekto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    scipovoj_objektoj = graphene.Field(ScipovoObjektoNode, 
        description=_('Созданный/изменённый навык объекта'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        objekto_uuid = graphene.String(description=_('UUID объекта'))
        nivelo_uuid = graphene.String(description=_('UUID уровня навыка'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        scipovo_id = graphene.Int(description=_('ID навыка'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        scipovoj_objektoj = None
        autoro = None
        objekto = None
        nivelo = None
        scipovo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('scipovoj.povas_krei_scipovoj_objektoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'scipovo_id' in kwargs:
                                try:
                                    scipovo = Scipovo.objects.get(id=kwargs.get('scipovo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Scipovo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный навык'),'scipovo_id')
                            else:
                                message = '{} "{}"'.format(_('Не указан навык'),'scipovo_id')

                        if not message:
                            if 'objekto_uuid' in kwargs:
                                try:
                                    objekto = Objekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Objekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект'),'objekto_uuid')
                            else:
                                message = '{} "{}"'.format(_('Не указан объект'),'objekto_uuid')

                        if not message:
                            if 'nivelo_uuid' in kwargs:
                                try:
                                    nivelo = ScipovoNivelo.objects.get(uuid=kwargs.get('nivelo_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ScipovoNivelo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный уровень навыка'),'nivelo_uuid')
                            else:
                                message = '{} "{}"'.format(_('Не указан уровень навыка'),'nivelo_uuid')

                        if not message:
                            scipovoj_objektoj = ScipovoObjekto.objects.create(
                                forigo=False,
                                arkivo=False,
                                objekto = objekto,
                                scipovo = scipovo,
                                nivelo = nivelo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            scipovoj_objektoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('scipovo_id', False) or kwargs.get('nivelo_uuid', False)
                            or kwargs.get('objekto_uuid', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            scipovoj_objektoj = ScipovoObjekto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('scipovoj.povas_forigi_scipovoj_objektoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('scipovoj.povas_shanghi_scipovoj_objektoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'scipovo_id' in kwargs:
                                    try:
                                        scipovo = Scipovo.objects.get(id=kwargs.get('scipovo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Scipovo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный навык'),'scipovo_id')

                            if not message:
                                if 'objekto_uuid' in kwargs:
                                    try:
                                        objekto = Objekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Objekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект'),'objekto_uuid')

                            if not message:
                                if 'nivelo_uuid' in kwargs:
                                    try:
                                        nivelo = ScipovoNivelo.objects.get(uuid=kwargs.get('nivelo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ScipovoNivelo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный уровень навыка'),'nivelo_uuid')

                            if not message:

                                scipovoj_objektoj.forigo = kwargs.get('forigo', scipovoj_objektoj.forigo)
                                scipovoj_objektoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                scipovoj_objektoj.arkivo = kwargs.get('arkivo', scipovoj_objektoj.arkivo)
                                scipovoj_objektoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                scipovoj_objektoj.publikigo = kwargs.get('publikigo', scipovoj_objektoj.publikigo)
                                scipovoj_objektoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                scipovoj_objektoj.scipovo = scipovo if kwargs.get('scipovo_id', False) else scipovoj_objektoj.scipovo
                                scipovoj_objektoj.objekto = objekto if kwargs.get('objekto_uuid', False) else scipovoj_objektoj.objekto
                                scipovoj_objektoj.nivelo = nivelo if kwargs.get('nivelo_uuid', False) else scipovoj_objektoj.nivelo

                                scipovoj_objektoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ScipovoObjekto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuScipovoObjekto(status=status, message=message, scipovoj_objektoj=scipovoj_objektoj)


class ScipovoMutations(graphene.ObjectType):
    redaktu_scipovo_kategorio = RedaktuScipovoKategorio.Field(
        description=_('''Создаёт или редактирует категории навыков''')
    )
    redaktu_scipovo_tipo = RedaktuScipovoTipo.Field(
        description=_('''Создаёт или редактирует типы навыков''')
    )
    redaktu_scipovo_nivelo = RedaktuScipovoNivelo.Field(
        description=_('''Создаёт или редактирует уровни навыков''')
    )
    redaktu_scipovo = RedaktuScipovo.Field(
        description=_('''Создаёт или редактирует навыки''')
    )
    redaktu_scipovo_objekto = RedaktuScipovoObjekto.Field(
        description=_('''Создаёт или редактирует навыки объектов''')
    )
