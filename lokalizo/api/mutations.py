"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.utils import timezone
from django.db import transaction
from django.utils.translation import gettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from .schema import LokalizoElementoTipoNode, LokalizoElementoNode, LokalizoTradukistoNode
from ..models import *


class RedaktuLokalizoTradukisto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    tradukisto = graphene.Field(LokalizoTradukistoNode, description=_('Созданная/изменённая запись типа элемента локализации'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        posedanto_id = graphene.Int(description=_('ID переводчика'))
        nivelo = graphene.Int(description=_('Уровень переводчика'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tradukisto = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('lokalizo.povas_krei_tradukiston'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('nivelo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nivelo')

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if not message:
                            tradukisto = LokalizoTradukisto.objects.create(
                                nivelo=kwargs.get('nivelo'),
                                posedanto=uzanto,
                                forigo=False,
                            )

                            tradukisto.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('nivelo', False) 
                                or kwargs.get('forigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tradukisto = LokalizoTradukisto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('lokalizo.povas_forigi_tradukiston')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('lokalizo.povas_shanghi_tradukiston'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                tradukisto.nivelo = kwargs.get('nivelo', tradukisto.nivelo)
                                tradukisto.forigo = kwargs.get('forigo', tradukisto.forigo)
                                tradukisto.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None

                                tradukisto.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except LokalizoTradukisto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuLokalizoTradukisto(status=status, message=message, tradukisto=tradukisto)

class RedaktuLokalizoElementoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    pagxo_tipo = graphene.Field(LokalizoElementoTipoNode, description=_('Созданная/изменённая запись типа элемента локализации'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название типа'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('lokalizo.povas_krei_elementon_tipon'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            LokalizoElementoTipo.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except LokalizoElementoTipo.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            tipo = LokalizoElementoTipo.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            tipo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                                or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                                or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tipo = LokalizoElementoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('lokalizo.povas_forigi_elementon_tipon')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('lokalizo.povas_shanghi_elementon_tipon'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                tipo.kodo = kwargs.get('kodo', tipo.kodo)
                                tipo.forigo = kwargs.get('forigo', tipo.forigo)
                                tipo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tipo.arkivo = kwargs.get('arkivo', tipo.arkivo)
                                tipo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tipo.publikigo = kwargs.get('publikigo', tipo.publikigo)
                                tipo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                tipo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except LokalizoElementoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuLokalizoElementoTipo(status=status, message=message, pagxo_tipo=tipo)


class RedaktuLokalizoElemento(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    pagxo = graphene.Field(LokalizoElementoNode, description=_('Созданный/изменённый элемент локализации'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        tipo_kodo = graphene.String(description=_('Код типа страницы'))
        teksto = graphene.String(description=_('Текст страницы'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        pagxo = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('lokalizo.povas_krei_elementon_tipon'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('tipo_kodo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'tipo_kodo')
                        else:
                            try:
                                tipo = LokalizoElementoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                            except LokalizoElementoTipo.DoesNotExist:
                                message = _('Неверный тип страницы')

                        if not (kwargs.get('teksto', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'teksto')

                        if not message:
                            pagxo = LokalizoElemento.objects.create(
                                autoro=uzanto,
                                tipo=tipo,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(pagxo.teksto, kwargs.get('teksto'), info.context.LANGUAGE_CODE)
                            pagxo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('teksto', False) or kwargs.get('tipo_kodo', False)
                                or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                                or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'tipo_kodo' in kwargs and not message:
                        try:
                            tipo = LokalizoElementoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except LokalizoElementoTipo.DoesNotExist:
                            message = _('Неверный тип страницы')

                    # Ищем запись для изменения
                    if not message:
                        try:
                            pagxo = LokalizoElemento.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('lokalizo.povas_forigi_elementon_tipon')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('lokalizo.povas_shanghi_elementon_tipon'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                pagxo.tipo = tipo or pagxo.tipo
                                pagxo.forigo = kwargs.get('forigo', pagxo.forigo)
                                pagxo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                pagxo.arkivo = kwargs.get('arkivo', pagxo.arkivo)
                                pagxo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                pagxo.publikigo = kwargs.get('publikigo', pagxo.publikigo)
                                pagxo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                pagxo.lasta_autoro = uzanto
                                pagxo.lasta_dato = timezone.now()

                                if kwargs.get('teksto', False):
                                    set_enhavo(pagxo.teksto, kwargs.get('teksto'), info.context.LANGUAGE_CODE)

                                pagxo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except LokalizoElemento.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuLokalizoElemento(status=status, message=message, pagxo=pagxo)


class LokalizoMutations(graphene.ObjectType):
    redaktu_lokalizo_tradukisto = RedaktuLokalizoTradukisto.Field(
        description=_('''Создаёт или редактирует уровни переводчиков''')
    )
    redaktu_lokalizo_pagxo_tipo = RedaktuLokalizoElementoTipo.Field(
        description=_('''Создаёт или редактирует типы элементов локализации''')
    )
    redaktu_lokalizo_pagxo = RedaktuLokalizoElemento.Field(
        description=_('''Создаёт или редактирует элементы локализации''')
    )
