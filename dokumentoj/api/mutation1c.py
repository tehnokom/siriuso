"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene
from django.conf import settings
from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
import requests
import json
from django.utils.translation import gettext_lazy as _
from datetime import datetime

from siriuso.api.types import ErrorNode
from .schema import DokumentoEkspedoNode
from organizoj.models import OrganizoMembro, Organizo
from ..models import DokumentoPosedantoStatuso, DokumentoPosedantoTipo, Dokumento, DokumentoEkspedo, DokumentoPosedanto, DokumentoContract,\
    DokumentoEkspedoKargo, DokumentoEkspedoKonservejoKargo, DokumentoEkspedoKonservejoKargoKondukisto, DokumentoKategorio, DokumentoTipo,\
    DokumentoSpeco
from .subscription import DokumentoEkspedoEventoj
from siriuso.utils import set_enhavo #, get_enhavo
from mono.models import MonoValuto
from kombatantoj.models import Kombatanto
from kombatantoj.api.mutation import ImportoKontakto1c
from objektoj.models import ObjektoTemperaturaReghimo, ObjektoTipoPakumo
from informiloj.models import InformilojUrboj
from taskoj.models import TaskojProjektoStatuso, TaskojTaskoStatuso, TaskojProjekto, \
    TaskojProjektoPosedanto, TaskojTasko, TaskojTaskoPosedanto
from taskoj.api.mutations import RedaktuKreiTaskojProjektoTaskojPosedanto
from universo_bazo.models import Realeco

# Добавление документа экспедирования в 1с 
class AldoniDokumentoEkspedo1c(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    aldoni = graphene.Field(DokumentoEkspedoNode)

    class Arguments:
        adreso = graphene.String(description=_('Адрес сервера, откуда производить импорт данных'))
        salutnomo = graphene.String(description=_('Адрес сервера, откуда производить импорт данных'))
        pasvorto = graphene.String(description=_('Адрес сервера, откуда производить импорт данных'))

    @staticmethod
    def krei(**kwargs):
        # создание без проверки пользователя
        status = False
        message = None
        errors = list()
        aldoni = None
        url = None
        login = None
        password = None
        if kwargs.get('adreso', False) and kwargs.get('salutnomo', False) and kwargs.get('pasvorto', False):
        # url = "https://192.168.99.5/skl_test_site/odata/standard.odata/Document_бит_ЗаявкаЭкспедированияММП(guid'eed47c08-3c81-11ec-8bc3-00155d091105')?$format=application/json"
            url = kwargs.get('adreso', False)
            login = kwargs.get('salutnomo', False)
            password = kwargs.get('pasvorto', False)
        else:
            url = settings.ODATA_URL+'Document_бит_ЗаявкаЭкспедированияММП?$format=application/json'
            login = settings.ODATA_USER
            password = settings.ODATA_PASSWORD

        with transaction.atomic():
            dato = timezone.now()
            tempo_s = None
            tempo_elsxargxado = None
            konservejo_ricevado_dato = None
            konservejo_ricevado_tempo_de = None
            konservejo_ricevado_tempo_en = None
            konservejo_fordono_dato = None
            konservejo_fordono_tempo_de = None
            konservejo_fordono_tempo_en = None
            if kwargs.get('tempo_s', False):
                tempo_s = kwargs.get('tempo_s').strftime("%Y-%m-%dT%H:%M:%S")
            if kwargs.get('tempo_elsxargxado', False):
                tempo_elsxargxado = kwargs.get('tempo_elsxargxado').strftime("%Y-%m-%dT%H:%M:%S")
            if kwargs.get('konservejo_ricevado_dato', False):
                konservejo_ricevado_dato = kwargs.get('konservejo_ricevado_dato').strftime("%Y-%m-%dT%H:%M:%S")
            if kwargs.get('konservejo_ricevado_tempo_de', False):
                konservejo_ricevado_tempo_de = kwargs.get('konservejo_ricevado_tempo_de').strftime("%Y-%m-%dT%H:%M:%S")
            if kwargs.get('konservejo_ricevado_tempo_en', False):
                konservejo_ricevado_tempo_en = kwargs.get('konservejo_ricevado_tempo_en').strftime("%Y-%m-%dT%H:%M:%S")
            if kwargs.get('konservejo_fordono_dato', False):
                konservejo_fordono_dato = kwargs.get('konservejo_fordono_dato').strftime("%Y-%m-%dT%H:%M:%S")
            if kwargs.get('konservejo_fordono_tempo_de', False):
                konservejo_fordono_tempo_de = kwargs.get('konservejo_fordono_tempo_de').strftime("%Y-%m-%dT%H:%M:%S")
            if kwargs.get('konservejo_fordono_tempo_en', False):
                konservejo_fordono_tempo_en = kwargs.get('konservejo_fordono_tempo_en').strftime("%Y-%m-%dT%H:%M:%S")
            fordono_kargo_dato = None
            if kwargs.get('fordono_kargo_dato', False):
                fordono_kargo_dato = kwargs.get('fordono_kargo_dato').strftime("%Y-%m-%dT%H:%M:%S")
            fordono_kargo_tempo = None
            if kwargs.get('fordono_kargo_tempo', False):
                fordono_kargo_tempo = kwargs.get('fordono_kargo_tempo').strftime("%Y-%m-%dT%H:%M:%S")
            trajno_venigo_dato = None
            if kwargs.get('trajno_venigo_dato', False):
                trajno_venigo_dato = kwargs.get('trajno_venigo_dato').strftime("%Y-%m-%dT%H:%M:%S")
            trajno_venigo_tempo = None
            if kwargs.get('trajno_venigo_tempo', False):
                trajno_venigo_tempo = kwargs.get('trajno_venigo_tempo').strftime("%Y-%m-%dT%H:%M:%S")
            trajno_finigho_dato = None
            if kwargs.get('trajno_finigho_dato', False):
                trajno_finigho_dato = kwargs.get('trajno_finigho_dato').strftime("%Y-%m-%dT%H:%M:%S")
            trajno_finigho_tempo = None
            if kwargs.get('trajno_finigho_tempo', False):
                trajno_finigho_tempo = kwargs.get('trajno_finigho_tempo').strftime("%Y-%m-%dT%H:%M:%S")
            plenumado_dato = None
            if kwargs.get('plenumado_dato', False):
                plenumado_dato = kwargs.get('plenumado_dato').strftime("%Y-%m-%dT%H:%M:%S")
            uid = None
            if kwargs.get('uid', None):
                uid = str(kwargs.get('uid'))
            datoj = {
                # "Date": "2021-11-01T08:53:36",
                "Date": dato.strftime("%Y-%m-%dT%H:%M:%S"),
                "УИД": uid,
                "СборныйГруз": kwargs.get('rendevuejo', False),
                "КонтактноеЛицоЗаказчика_Type": "StandardODATA.Catalog_КонтактныеЛица",
                "ТелефонЗаказчика": kwargs.get('telefono_kliento', ''),
                "ТелефонПоАдресу": kwargs.get('telefono_adreso', None),
                "ТелефонПоАдресуРазгрузки": kwargs.get('telefono_adreso_elsxargxado', None),
                "Объем": kwargs.get('volumeno', None),
                "Вес": kwargs.get('pezo', None),
                "ОписаниеГруза": kwargs.get('priskribo_kargo', None),
                "СтоимостьГруза": kwargs.get('kosto_kargo', None),
                "СуммаЗаказчика": kwargs.get('monsumo_kliento', None),
                "СуммаЗаказчикаУстановленаВручную": kwargs.get('monsumo_kliento_fiksa_mane', None),
                "СуммаПеревозчика": kwargs.get('monsumo_transportisto', None),
                "СуммаПеревозчикаУстановленаВручную": kwargs.get('monsumo_transportisto_fiksa_mane', None),
                "АдресЗагрузки": kwargs.get('adreso_kargado', None),
                "АдресЗагрузки_Type": "Edm.String",
                "АдресВыгрузки": kwargs.get('adreso_elsxargxado', None),
                "АдресВыгрузки_Type": "Edm.String",
                
                "ВремяС": tempo_s,
                "ВремяВыгрузкиС": tempo_elsxargxado,
                "СКЛ_ДатаПриемки": konservejo_ricevado_dato,
                "СКЛ_ВремяПриемкиС": konservejo_ricevado_tempo_de,
                "СКЛ_ВремяПриемкиПо": konservejo_ricevado_tempo_en,
                "СКЛ_ДатаПередачи": konservejo_fordono_dato,
                "СКЛ_ВремяПередачиС": konservejo_fordono_tempo_de,
                "СКЛ_ВремяПередачиПо": konservejo_fordono_tempo_en,
                "ОрганизацияПоАдресу": kwargs.get('organizo_adreso', None),
                "ОрганизацияПоАдресуРазгрузки": kwargs.get('organizo_adreso_elsxargxado', None),
                "КонтактноеЛицоПоАдресу": kwargs.get('kombatanto_adresoj', None),
                "КонтактноеЛицоПоАдресуРазгрузки": 	kwargs.get('kombatanto_adresoj_elsxargxado', None),
                "ЗаказчикИзвещен": kwargs.get('kliento_sciigi', None),
                "Комментарий": kwargs.get('komento', None),
                "КомментарийАдресаВыгрузки": kwargs.get('komento_adreso_elsxargxado', None),
                "КомментарийАдресаЗагрузки": kwargs.get('komento_adreso_kargado', None),
                "КомментарийОтмены": kwargs.get('komento_nuligo', None),
                "Факт": kwargs.get('fakto', None),
                "ЦенаЗаказчика": kwargs.get('kosto_kliento', None),
                "ЦенаПеревозчика": kwargs.get('kosto_transportisto', None),
                "АвансЗаказчика": kwargs.get('partopago_kliento', None),
                "АвансПеревозчику": kwargs.get('partopago_transportisto', None),
                "ДопУсловия": kwargs.get('aldona_kondicxaro', None),
                "СтавкаНДСЗаказчик": kwargs.get('impostokvoto_kliento', None),
                "СтавкаНДСПеревозчик": kwargs.get('impostokvoto_transportisto', None),
                "КомментарийРасчета": kwargs.get('priskribo_kalkulado', None),
                "НомерНакладнойСК": kwargs.get('numero_frajtoletero', None),
                "ВесФакт": kwargs.get('pezo_fakto', None),
                "ВесРасч": kwargs.get('pezo_kalkula', None),
                "ОпасныйГруз": kwargs.get('dangxera_kargo', None),
                "ОписаниеОпасногоГруза": kwargs.get('priskribo_dangxera_kargo', None),
                "КлассОпасности": kwargs.get('klaso_dangxera', None),
                "ВозвратТСД": kwargs.get('retropasxo_tsd', None),
                "ПриемДоставкаВНерабочееВремя": kwargs.get('akcepto_liveri_nelabora_tempo', None),
                "ДополнительнаяУпаковка": kwargs.get('aldone_pakumo', None),
                "ДатчикТемпературы": kwargs.get('indikatoro_temperatura', None),
                "ПредоставлениеТП": kwargs.get('livero_tp', None),
                "скл_ТемпературныйРежим1": kwargs.get('konservejo_temperatura_reghimo_1', None),
                "скл_ТемпературныйРежим2": kwargs.get('konservejo_temperatura_reghimo_2', None),
                "скл_ТемпературныйРежим3": kwargs.get('konservejo_temperatura_reghimo_3', None),
                "скл_ТемпературныйРежим4": kwargs.get('konservejo_temperatura_reghimo_4', None),
                "скл_ТемпературныйРежим5": kwargs.get('konservejo_temperatura_reghimo_5', None),
                "скл_ТемпературныйРежим6": kwargs.get('konservejo_temperatura_reghimo_6', None),
                "скл_ТемпературныйРежимИной": kwargs.get('konservejo_temperatura_reghimo_alia', None),
                "ПредоставлениеТП2": kwargs.get('Livero_tp_2', None),
                "ПредоставлениеТП3": kwargs.get('Livero_tp_3', None),
                "ПредоставлениеТП4": kwargs.get('Livero_tp_4', None),
                "ПредоставлениеТП5": kwargs.get('Livero_tp_5', None),
                "ПредоставлениеТПИной": kwargs.get('Livero_tp_alia', None),
                "ДопТребованияХЦ": kwargs.get('aldone_postulo_hc', None),
                "ИныеУслуги": kwargs.get('aliaj_servo', None),
                "ПередачаГрузаФИО": kwargs.get('fordono_kargo_fio', None),
                "ФПередачаГрузаДата": fordono_kargo_dato,
                "ФПередачаГрузаВремя": fordono_kargo_tempo,
                "ТребуетсяСтрахование": kwargs.get('asekurado', None),
                "КоличествоЧасовГрузчики": kwargs.get('horoj_sxargxistoj', None),
                "КоличествоГрузчиков": kwargs.get('kvanto_sxargxistoj', None),
                "КоличествоМест": kwargs.get('kvanto_pecoj', None),
                "ТребуютсяГрузчикиТакелажники": kwargs.get('sxargxisto_rigilaro', None),
                "Тяжеловес": kwargs.get('en_peza_kategorio', None),
                "НаименованиеГруза": kwargs.get('nomo_kargo', None),
                "скл_Задание": kwargs.get('konservejo_tasko', None),
                "скл_Комментарий": kwargs.get('konservejo_komento', None),
                "рл_ДатаПодачиТранспорт": trajno_venigo_dato,
                "рл_ВремяПодачиТранспорт": trajno_venigo_tempo,
                "рл_ДатаОкончанияТранспорт": trajno_finigho_dato,
                "рл_ВремяОкончанияТранспорт": trajno_finigho_tempo,
                "ДатчикТемпературы2": kwargs.get('sensoro_temperatura_2', None),
                "ДатчикТемпературы3": kwargs.get('sensoro_temperatura_3', None),
                "ДатчикТемпературы4": kwargs.get('sensoro_temperatura_4', None),
                "ДатчикТемпературы5": kwargs.get('sensoro_temperatura_5', None),
                "ДатчикТемпературыИной": kwargs.get('sensoro_temperatura_alia', None),
                "скл_КомментарийКлиента": kwargs.get('priskribo_kliento', None),
                "НомерНакладнойКлиента": kwargs.get('numero_frajtoletero_kliento', None),
                "EmailОтправителя": kwargs.get('email_ekspedinto', None),
                "EmailПолучателя": kwargs.get('email_ricevanto', None),
                "ДатаНаИсполнении": plenumado_dato,
                "рл_СтационарныйТермописец": kwargs.get('fiksa_termoskribi', None),
                "рл_СтационарныйТермописец2": kwargs.get('fiksa_termoskribi_2', None),
                "рл_СтационарныйТермописец3": kwargs.get('fiksa_termoskribi_3', None),
                "рл_СтационарныйТермописец4": kwargs.get('fiksa_termoskribi_4', None),
                "рл_СтационарныйТермописец5": kwargs.get('fiksa_termoskribi_5', None),
                "рл_СтационарныйТермописецИной": kwargs.get('fiksa_termoskribi_alia', None),
                "рл_ТипЗаявки": kwargs.get('tipo_mendo', None),
                "рл_ГрузчикиПогрузка": kwargs.get('sxargxistoj_sxargxado', None),
                "рл_ГрузчикиВыгрузка": kwargs.get('sxargxistoj_elsxargxado', None),
                "Маршрут": [],
                "ГрузыММП": [],
            }
            if kwargs.get('kliento', False):
                print('=== добавляем заказчика === ', kwargs.get('kliento_uuid'))
                datoj["Заказчик_Key"] = kwargs.get('kliento_uuid')
            else:
                print('=== нет у нас заказчика === ')
            if kwargs.get('valuto_kliento', False):
                datoj['ВалютаЗаказчика_Key'] = str(kwargs.get('valuto_kliento').uuid)
            if kwargs.get('ekspedinto', False):
                datoj['Грузоотправитель_Key'] = str(kwargs.get('ekspedinto').uuid)
            if kwargs.get('ricevanto', False):
                datoj['Грузополучатель_Key'] = str(kwargs.get('ricevanto').uuid)
            if kwargs.get('kontrakto_kliento', False):
                datoj['ДоговорЗаказчика_Key'] = kwargs.get('kontrakto_kliento_uuid')
            if kwargs.get('kombatanto_kliento', False):
                datoj['КонтактноеЛицоЗаказчика'] = kwargs.get('kombatanto_kliento_uuid')
            if kwargs.get('organizo', False):
                datoj['Организация_Key'] = kwargs.get('organizo_uuid')
            if kwargs.get('urboj_recivado_uuid', False):
                datoj['ГородПриемки'] = kwargs.get('urboj_recivado_uuid')
                datoj["ГородПриемки_Type"] = "StandardODATA.Catalog_СКЛ_Города"
            else: 
                datoj['ГородПриемки'] = kwargs.get('urboj_recivado_nomo')
                datoj["ГородПриемки_Type"] = "Edm.String"
            if kwargs.get('urboj_transdono_uuid', False):
                datoj['ГородПередачи'] = kwargs.get('urboj_transdono_uuid')
                datoj["ГородПередачи_Type"] = "StandardODATA.Catalog_СКЛ_Города"
            else: 
                datoj['ГородПередачи'] = kwargs.get('urboj_transdono_nomo')
                datoj["ГородПередачи_Type"] = "Edm.String"
            if kwargs.get('prj_statuso', False):
                datoj['Организация_Key'] = kwargs.get('organizo_uuid')
            if kwargs.get('temperatura_reghimo', False):
                datoj['ТемпературныйРежим_Key'] = kwargs.get('temperatura_reghimo_uuid')
            if kwargs.get('liveranto', False):
                datoj['рл_Поставщик_Key'] = kwargs.get('liveranto_uuid')
            if kwargs.get('liveranto_kontrakto', False):
                datoj['рл_ДоговорПоставщика_Key'] = kwargs.get('liveranto_kontrakto_uuid')
            if kwargs.get('kombatanto_kontrakto', False):
                datoj['рл_КонтактноеЛицоПоставщика_Key'] = kwargs.get('kombatanto_kontrakto_uuid')
            # из проекта
            if kwargs.get('projekto_statuso', False):
                datoj['удалитьРезультатВыполнения_Key'] = str(kwargs.get('projekto_statuso').uuid)

            # добавление маршрута
            if kwargs.get('tasko_statuso', False):
                i = 0
                for tasko_numero in kwargs.get('tasko_numero'):
                    tasko_kom_dato = kwargs.get('tasko_kom_dato')[i].strftime("%Y-%m-%dT%H:%M:%S")
                    tasko_fin_dato = kwargs.get('tasko_fin_dato')[i].strftime("%Y-%m-%dT%H:%M:%S")
                    datoj['Маршрут'].append({
                        "LineNumber": tasko_numero, # обязательное поле для маршрута
                        "ДатаНачала": 	tasko_kom_dato,
                        "ДатаОкончания": tasko_fin_dato,
                        "АдресНачала": kwargs.get('tasko_kom_adreso')[i],
                        "АдресНачала_Type": "Edm.String",
                        "АдресКонца": kwargs.get('tasko_fin_adreso')[i],
                        "АдресКонца_Type": "Edm.String",
                        "Состояние_Key": str(kwargs.get('tasko_statuso')[i].uuid),
                        "Комментарий": kwargs.get('tasko_priskribo')[i],
                        "ИдентификаторМаршрута": kwargs.get('tasko_nomo')[i]
                    })
                    i += 1
                datoj["ИдМаршрута"] = 1
            if kwargs.get('kargo_numero', False):
                # добавление груза ММП
                i = 0
                for kargo_numero in kwargs.get('kargo_numero'):
                    kargo = {"LineNumber": kargo_numero}
                    if kwargs.get('kargo_nomo', False):
                        kargo["Наименование"] = kwargs.get('kargo_nomo')[i]
                    if kwargs.get('kargo_kvanto_pecoj', False):
                        kargo["КоличествоМест"] = kwargs.get('kargo_kvanto_pecoj')[i] 
                    if kwargs.get('kargo_longo', False):
                        kargo["Длина"] = kwargs.get('kargo_longo')[i] 
                    if kwargs.get('kargo_largho', False):
                        kargo["Ширина"] = kwargs.get('kargo_largho')[i] 
                    if kwargs.get('kargo_alto', False):
                        kargo["Высота"] = kwargs.get('kargo_alto')[i] 
                    if kwargs.get('kargo_pezo_fakta', False):
                        kargo["ВесФактический"] = kwargs.get('kargo_pezo_fakta')[i] 
                    if kwargs.get('kargo_volumeno', False):
                        kargo["Объем"] = kwargs.get('kargo_volumeno')[i] 
                    if kwargs.get('kargo_pezo_volumena', False):
                        kargo["ВесОбъемный"] = kwargs.get('kargo_pezo_volumena')[i] 
                    if kwargs.get('kargo_tipo_pakumoj', False):
                        kargo["ТипУпаковки_Key"] = str(kwargs.get('kargo_tipo_pakumoj')[i].uuid) 
                    if kwargs.get('kargo_indikatoro', False):
                        kargo["НуженДатчик"] = kwargs.get('kargo_indikatoro')[i] 
                    if kwargs.get('kargo_numero_indikatoro', False):
                        kargo["СерийныйНомерДатчика"] = kwargs.get('kargo_numero_indikatoro')[i] 
                    if kwargs.get('kargo_temperatura_reghimo', False):
                        kargo["Терморежим_Key"] = str(kwargs.get('kargo_temperatura_reghimo')[i].uuid) 
                    if kwargs.get('kargo_grave', False):
                        kargo["ОпасныйНеопасный"] = kwargs.get('kargo_grave')[i] 
                    if kwargs.get('kargo_retropasxo_pakumo', False):
                        kargo["ВозвратУпаковки"] = kwargs.get('kargo_retropasxo_pakumo')[i] 
                    if kwargs.get('kargo_retropasxo_indikatoro', False):
                        kargo["ВозвратДатчика"] = kwargs.get('kargo_retropasxo_indikatoro')[i] 
                    
                    datoj['ГрузыММП'].append(kargo)
                    i += 1
            # print('=== datoj[ГрузыММП] === ', datoj['ГрузыММП'])
            print('=== datoj === ', datoj)
            json_string = json.dumps(datoj)

            # r = requests.delete(url, auth=(login, password), verify=False)
            r = requests.post(url, data=json_string, auth=(login, password), verify=False)
            if ((r.status_code < 200) or (r.status_code > 299)):
                # print('Ошибка связи с 1С сервером = ', r.status_code)
                message = 'Ошибка связи с 1С сервером = ' + str(r.status_code)
                status = False
                errors.append(ErrorNode(
                    field = str(r.status_code),
                    message = _('Ошибка связи с 1С сервером')
                ))
                aldoni = None
                return status, message, errors, aldoni 
            jsj = r.json()
            errors = list()
            message = jsj
            if jsj.get('Ref_Key',False):
                status = True
                aldoni = jsj
            else:
                status = False
                errors.append(ErrorNode(
                    field='',
                    message=_('Не получен ответ от 1С')
                ))
        return status, message, errors, aldoni

    @staticmethod
    def create(uzanto, **kwargs):
        status = False
        message = None
        errors = list()
        aldoni = None
        membro = None

        # Находим организацию-клиента
        if not uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_ekspedo'):
            if kwargs.get('kliento', False):
                try:
                    membro = OrganizoMembro.objects.get(organizo=kwargs.get('kliento'), uzanto=uzanto, forigo=False,
                                                        arkivo=False, publikigo=True)
                except OrganizoMembro.DoesNotExist: # не является членом организации
                    pass
        # Создавать может член организации-владельца документа
        if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_ekspedo') or membro:
            status, message, errors, aldoni = AldoniDokumentoEkspedo1c.krei(**kwargs)
        else:
            message = _('Недостаточно прав AldoniDokumentoEkspedo1c')
            errors.append(ErrorNode(
                field='AldoniDokumentoEkspedo1c.create',
                message=_('Недостаточно прав')
            ))

        return status, message, errors, aldoni

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        aldoni = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            # Добавляем данные
            status, message, errors, aldoni = AldoniDokumentoEkspedo1c.create(root, info, **kwargs)
        else:
            message = _('Требуется авторизация')

        return AldoniDokumentoEkspedo1c(status=status, message=message, errors=errors, aldoni=aldoni)


# сохраняем документ экспедирования
def dokumento_ekspedo_save(dokumento_ekspedo):

    def eventoj():
        DokumentoEkspedoEventoj.dokumento_ekspedo(dokumento_ekspedo.kliento, dokumento_ekspedo)

    dokumento_ekspedo.save()
    transaction.on_commit(eventoj)


# Импорт характеристик экседирования из 1с
class ImportoDokumentoEkspedo1c(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    ekspedo = graphene.Field(DokumentoEkspedoNode)

    class Arguments:
        url = graphene.String(description=_('URL (ссылка), откуда производить импорт данных'))
        tipo_dokumento_id = graphene.Int(description=_('ID типа документов'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий документов'))

    @staticmethod
    def kontrolanta_argumentoj(root, info, **kwargs):
        """Проверка аргументов
        Что бы не проверять аргументы в create и в edit, проверим отдельно

        Args:
            дублируем как и в create и в edit
        """
        # uzanto = info.context.user
        # возвращаемые данные:
        errors = list()
        # проверяем наличие записей с таким кодом
        if 'posedanto_dokumento_kliento_tipo_id' in kwargs: # тип владельца-клиента документа по умолчанию
            try:
                posedanto_dokumento_kliento_tipo = DokumentoPosedantoTipo.objects.get(
                    id=kwargs.get('posedanto_dokumento_kliento_tipo_id'), forigo=False,
                    arkivo=False, publikigo=True)
                kwargs['posedanto_dokumento_kliento_tipo'] = posedanto_dokumento_kliento_tipo
            except DokumentoPosedantoTipo.DoesNotExist:
                errors.append(ErrorNode(
                    field='posedanto_dokumento_kliento_tipo_id',
                    message=_('Неверный тип владельца-клиента документа')
                ))
        if 'posedanto_dokumento_organizo_tipo_id' in kwargs: # тип владельца-поставщика документа по умолчанию
            try:
                posedanto_dokumento_organizo_tipo = DokumentoPosedantoTipo.objects.get(
                    id=kwargs.get('posedanto_dokumento_organizo_tipo_id'), forigo=False,
                    arkivo=False, publikigo=True)
                kwargs['posedanto_dokumento_organizo_tipo'] = posedanto_dokumento_organizo_tipo
            except DokumentoPosedantoTipo.DoesNotExist:
                errors.append(ErrorNode(
                    field='posedanto_dokumento_organizo_tipo_id',
                    message=_('Неверный тип владельца-поставщика документа')
                ))
        if 'posedanto_dokumento_statuso_id' in kwargs: # статус владельца документа по умолчанию
            try:
                posedanto_dokumento_statuso = DokumentoPosedantoStatuso.objects.get(
                    id=kwargs.get('posedanto_dokumento_statuso_id'), forigo=False,
                    arkivo=False, publikigo=True)
                kwargs['posedanto_dokumento_statuso'] = posedanto_dokumento_statuso
            except DokumentoPosedantoStatuso.DoesNotExist:
                errors.append(ErrorNode(
                    field='posedanto_dokumento_statuso',
                    message=_('Неверный статус документов')
                ))
        return errors, kwargs

    @staticmethod
    def argumentoj_kreanta(root, info, **kwargs):
        """argumentoj kiam kreanta - аргументы при создании (по умолчанию)

        Args:
            root (_type_): _description_
            info (_type_): _description_
        """
        errors = list()
        # проверяем обязательные параметры
        # установка параметров "по умочанию"
        if not 'posedanto_dokumento_kliento_tipo_id' in kwargs: # тип владельца-клиента документа по умолчанию
            kwargs['posedanto_dokumento_kliento_tipo_id'] = 2
        if not 'posedanto_dokumento_organizo_tipo_id' in kwargs: # тип владельца-поставщика документа по умолчанию
            kwargs['posedanto_dokumento_organizo_tipo_id'] = 1
        if not 'posedanto_dokumento_statuso_id' in kwargs: # статус владельца документа по умолчанию
            kwargs['posedanto_dokumento_statuso_id'] = 1
        return errors, kwargs

    @staticmethod
    def aldoni(root, info, kvanto, informlibro, js):
        """
        Добавляем данные из параметра js в базу
        
        спра́вочн||ик informlibro
        он нужен для разделения полученных данных из 1с и добавляемых своих (справочники)
        """
        errors = list()
        status = False
        message = ''
        ekspedo = None
        
        uuid = js['Ref_Key']
        publikiga_dato = js['Date']

        if not len(errors):
            dokumento = Dokumento.objects.create(
                forigo=False,
                arkivo=False,
                autoro = informlibro['autoro'],
                tipo = informlibro['tipo_dokumento'],
                publikigo=True,
                publikiga_dato=publikiga_dato
            )

            set_enhavo(dokumento.nomo, 'Importo',  'ru_RU')
            if informlibro['realeco']:
                dokumento.realeco.set(informlibro['realeco'])
            if informlibro['kategorio']:
                dokumento.kategorio.set(informlibro['kategorio'])

            dokumento.save()
            kvanto += 1

        kodo = js['Number']
        adreso_elsxargxado = js['АдресВыгрузки']
        adreso_kargado = js['АдресЗагрузки']
        pezo = js['Вес']
        tempo_elsxargxado = js['ВремяВыгрузкиС']
        ekspedinto = None
        ricevanto = None
        kliento = None
        kombatanto_kliento = None
        organizo = None
        kialo_nuligo_key = None
        if js['ПричинаОтмены_Key'] != '00000000-0000-0000-0000-000000000000':
            kialo_nuligo_key = js['ПричинаОтмены_Key']
        # autoro_logist_key = None
        # if js['АвторЛогист_Key'] != '00000000-0000-0000-0000-000000000000':
        #     autoro_logist_key = js['АвторЛогист_Key']
        temperatura_reghimo = None
        faro_hc_key = None
        if js['СозданиеХЦ_Key'] != '00000000-0000-0000-0000-000000000000':
            faro_hc_key = js['СозданиеХЦ_Key']
        urboj_recivado = None
        urboj_transdono = None
        fordono_kargo_dato=datetime.strptime(js['ФПередачаГрузаДата'], "%Y-%m-%dT%H:%M:%S")
        fordono_kargo_tempo=datetime.strptime(js['ФПередачаГрузаВремя'], "%Y-%m-%dT%H:%M:%S")
        trajno_key = None
        if js['Транспорт_Key'] != '00000000-0000-0000-0000-000000000000':
            trajno_key = js['Транспорт_Key']
        kondukisto_key = None
        if js['Водитель_Key'] != '00000000-0000-0000-0000-000000000000':
            kondukisto_key = js['Водитель_Key']
        liveranto = None
        kombatanto_kontrakto = None
        trajno_konservejo_gk_key = None
        if js['ТранспортСоСкладаГК_Key'] != '00000000-0000-0000-0000-000000000000':
            trajno_konservejo_gk_key = js['ТранспортСоСкладаГК_Key']
        kondukisto_konservejo_gk_key = None
        if js['ВодительСоСкладаГК_Key'] != '00000000-0000-0000-0000-000000000000':
            kondukisto_konservejo_gk_key = js['ВодительСоСкладаГК_Key']
        sekcio_kliento_key = None
        if js['рл_ПодразделениеЗаказчика_Key'] != '00000000-0000-0000-0000-000000000000':
            sekcio_kliento_key = js['рл_ПодразделениеЗаказчика_Key']

        valuto_kliento = None
        if js['ВалютаЗаказчика_Key'] != '00000000-0000-0000-0000-000000000000':
            try:
                valuto_kliento = MonoValuto.objects.get(uuid=js['ВалютаЗаказчика_Key'])
            except MonoValuto.DoesNotExist:
                errors.append(ErrorNode(
                    field='ВалютаЗаказчика_Key',
                    message=_('Нет такой валюты в базе')
                ))
        if js['Грузоотправитель_Key'] != '00000000-0000-0000-0000-000000000000':
            try:
                ekspedinto = Organizo.objects.get(uuid=js['Грузоотправитель_Key'])
            except Organizo.DoesNotExist:
                errors.append(ErrorNode(
                    field='Грузоотправитель_Key',
                    message=_('Нет такого грузоотправителя в базе')
                ))
        if js['Грузополучатель_Key'] != '00000000-0000-0000-0000-000000000000':
            try:
                ricevanto = Organizo.objects.get(uuid=js['Грузополучатель_Key'])
            except Organizo.DoesNotExist:
                errors.append(ErrorNode(
                    field='Грузополучатель_Key',
                    message=_('Нет такого грузополучателя в базе')
                ))
        if js['Заказчик_Key'] != '00000000-0000-0000-0000-000000000000':
            try:
                kliento = Organizo.objects.get(uuid=js['Заказчик_Key'])
                # если есть клиент, то создаём
                posedanto_kliento = DokumentoPosedanto.objects.create(
                    forigo=False,
                    arkivo=False,
                    publikigo=True,
                    publikiga_dato=publikiga_dato,
                    tipo=informlibro['posedanto_dokumento_kliento_tipo'],
                    statuso=informlibro['posedanto_dokumento_statuso'],
                    posedanto_organizo=kliento,
                    dokumento=dokumento
                )
            except Organizo.DoesNotExist:
                errors.append(ErrorNode(
                    field='Заказчик_Key',
                    message=_('Нет такого грузополучателя в базе')
                ))
        if js['Организация_Key'] != '00000000-0000-0000-0000-000000000000':
            try:
                organizo = Organizo.objects.get(uuid=js['Организация_Key'])
                # если есть владелец, то создаём
                posedanto_organizo = DokumentoPosedanto.objects.create(
                    forigo=False,
                    arkivo=False,
                    publikigo=True,
                    publikiga_dato=publikiga_dato,
                    tipo=informlibro['posedanto_dokumento_organizo_tipo'],
                    statuso=informlibro['posedanto_dokumento_statuso'],
                    posedanto_organizo=organizo,
                    dokumento=dokumento
                )
            except Organizo.DoesNotExist:
                errors.append(ErrorNode(
                    field='Организация_Key',
                    message=_('Нет такой организации в базе')
                ))
        kontrakto_kliento = None
        if js['ДоговорЗаказчика_Key'] != '00000000-0000-0000-0000-000000000000':
            try:
                kontrakto_kliento_contract = DokumentoContract.objects.get(uuid=js['ДоговорЗаказчика_Key'])
                kontrakto_kliento = kontrakto_kliento_contract.dokumento
            except DokumentoContract.DoesNotExist:
                errors.append(ErrorNode(
                    field='ДоговорЗаказчика_Key',
                    message=_('Нет такого договора в базе')
                ))
        liveranto_kontrakto = None
        if js['рл_ДоговорПоставщика_Key'] != '00000000-0000-0000-0000-000000000000':
            try:
                liveranto_kontrakto_contract = DokumentoContract.objects.get(uuid=js['рл_ДоговорПоставщика_Key'])
                liveranto_kontrakto = liveranto_kontrakto_contract.dokumento
            except DokumentoContract.DoesNotExist:
                errors.append(ErrorNode(
                    field='ДоговорЗаказчика_Key',
                    message=_('Нет такого договора в базе')
                ))
        if js['КонтактноеЛицоЗаказчика'] and js['КонтактноеЛицоЗаказчика'] != '00000000-0000-0000-0000-000000000000':
            if js['КонтактноеЛицоЗаказчика_Type'] == "StandardODATA.Catalog_КонтактныеЛица":
                try:
                    kombatanto_kliento = Kombatanto.objects.get(uuid=js['КонтактноеЛицоЗаказчика'])
                except Kombatanto.DoesNotExist:
                    errors.append(ErrorNode(
                        field='КонтактноеЛицоЗаказчика',
                        message=_('Нет такого контактного лица в базе')
                    ))
            elif  js['КонтактноеЛицоЗаказчика_Type'] == 'Edm.String':
                # создаёи контрагента
                errors, kombatanto_tipo, kombatanto_speco = ImportoKontakto1c.PrepariKombatanto(errors)

                if not len(errors):
                    kombatanto_kliento = Kombatanto.objects.create(
                        autoro=informlibro['uzanto'],
                        forigo=False,
                        arkivo=False,
                        publikigo=True,
                        publikiga_dato=timezone.now(),
                        tipo=kombatanto_tipo,
                        speco=kombatanto_speco,
                        organizo=kliento,
                    )
                    set_enhavo(kombatanto_kliento.familinomo, js['КонтактноеЛицоЗаказчика'], 'ru_RU')
                    set_enhavo(kombatanto_kliento.informoj, js['КонтактноеЛицоЗаказчика'], 'ru_RU')

                    kombatanto_kliento.save()
        if js['ТемпературныйРежим_Key'] != '00000000-0000-0000-0000-000000000000':
            try:
                temperatura_reghimo = ObjektoTemperaturaReghimo.objects.get(uuid=js['ТемпературныйРежим_Key'])
            except ObjektoTemperaturaReghimo.DoesNotExist:
                errors.append(ErrorNode(
                    field="{}{}".format('ТемпературныйРежим_Key = ',js['ТемпературныйРежим_Key']),
                    message=_('Нет такой записи в базе')
                ))
        urboj_recivado_nomo = None
        if js['ГородПриемки'] and js['ГородПриемки'] != '00000000-0000-0000-0000-000000000000':
            if js['ГородПриемки_Type'] == "StandardODATA.Catalog_СКЛ_Города":
                try:
                    urboj_recivado = InformilojUrboj.objects.get(uuid=js['ГородПриемки'])
                except InformilojUrboj.DoesNotExist:
                    errors.append(ErrorNode(
                        field="{}{}".format('ГородПриемки = ',js['ГородПриемки']),
                        message=_('Нет такой записи в базе')
                    ))
            elif js['ГородПриемки_Type'] == 'Edm.String':
                urboj_recivado_nomo = js['ГородПриемки']
        urboj_transdono_nomo = None
        if js['ГородПередачи'] and js['ГородПередачи'] != '00000000-0000-0000-0000-000000000000':
            if js['ГородПередачи_Type'] == "StandardODATA.Catalog_СКЛ_Города":
                try:
                    urboj_transdono = InformilojUrboj.objects.get(uuid=js['ГородПередачи'])
                except InformilojUrboj.DoesNotExist:
                    errors.append(ErrorNode(
                        field="{}{}".format('ГородПередачи = ',js['ГородПередачи']),
                        message=_('Нет такой записи в базе')
                    ))
            elif js['ГородПередачи_Type'] == 'Edm.String':
                urboj_transdono_nomo = js['ГородПередачи']
        if js['рл_Поставщик_Key'] != '00000000-0000-0000-0000-000000000000':
            try:
                liveranto = Organizo.objects.get(uuid=js['рл_Поставщик_Key'])
            except Organizo.DoesNotExist:
                errors.append(ErrorNode(
                    field="{}{}".format('рл_Поставщик_Key = ',js['рл_Поставщик_Key']),
                    message=_('Нет такой записи в базе')
                ))
        if js['рл_КонтактноеЛицоПоставщика_Key'] != '00000000-0000-0000-0000-000000000000':
            try:
                kombatanto_kontrakto = Kombatanto.objects.get(uuid=js['рл_КонтактноеЛицоПоставщика_Key'])
            except Kombatanto.DoesNotExist:
                errors.append(ErrorNode(
                    field="{}{}".format('рл_КонтактноеЛицоПоставщика_Key = ',js['рл_КонтактноеЛицоПоставщика_Key']),
                    message=_('Нет такой записи в базе')
                ))

        if not len(errors):
            ekspedo = DokumentoEkspedo.objects.create(
                uuid=uuid,
                dokumento=dokumento,
                forigo=False,
                arkivo=False,
                publikigo=True,
                publikiga_dato=publikiga_dato,
                kodo=kodo,
                valuto_kliento=valuto_kliento,
                pezo=pezo,
                tempo_elsxargxado=tempo_elsxargxado,
                tempo_s=js['ВремяС'],
                ekspedinto=ekspedinto,
                ricevanto=ricevanto,
                kontrakto_kliento=kontrakto_kliento,
                kliento=kliento,
                kliento_sciigi=js['ЗаказчикИзвещен'],
                kombatanto_kliento=kombatanto_kliento,
                volumeno=js['Объем'],
                organizo=organizo,
                kialo_nuligo_key=kialo_nuligo_key,
                kosto_kargo=js['СтоимостьГруза'],
                monsumo_kliento=js['СуммаЗаказчика'],
                monsumo_kliento_fiksa_mane=js['СуммаЗаказчикаУстановленаВручную'],
                monsumo_transportisto=js['СуммаПеревозчика'],
                monsumo_transportisto_fiksa_mane=js['СуммаПеревозчикаУстановленаВручную'],
                fakto=js['Факт'],
                kosto_kliento=js['ЦенаЗаказчика'],
                kosto_transportisto=js['ЦенаПеревозчика'],
                partopago_kliento=js['АвансЗаказчика'],
                partopago_transportisto=js['АвансПеревозчику'],
                # autoro_logist_key=autoro_logist_key,
                pezo_fakto=js['ВесФакт'],
                pezo_kalkula=js['ВесРасч'],
                dangxera_kargo=js['ОпасныйГруз'],
                retropasxo_tsd=js['ВозвратТСД'],
                akcepto_liveri_nelabora_tempo=js['ПриемДоставкаВНерабочееВремя'],
                aldone_pakumo=js['ДополнительнаяУпаковка'],
                temperatura_reghimo=temperatura_reghimo,
                faro_hc_key=faro_hc_key,
                indikatoro_temperatura=js['ДатчикТемпературы'],
                livero_tp=js['ПредоставлениеТП'],
                urboj_recivado=urboj_recivado,
                urboj_transdono=urboj_transdono,
                fordono_kargo_dato=fordono_kargo_dato,
                fordono_kargo_tempo=fordono_kargo_tempo,
                konservejo_ricevado_dato=datetime.strptime(js['СКЛ_ДатаПриемки'], "%Y-%m-%dT%H:%M:%S"),
                konservejo_ricevado_tempo_de=datetime.strptime(js['СКЛ_ВремяПриемкиС'], "%Y-%m-%dT%H:%M:%S"),
                konservejo_ricevado_tempo_en=datetime.strptime(js['СКЛ_ВремяПриемкиПо'], "%Y-%m-%dT%H:%M:%S"),
                konservejo_fordono_dato=datetime.strptime(js['СКЛ_ДатаПередачи'], "%Y-%m-%dT%H:%M:%S"),
                konservejo_fordono_tempo_de=datetime.strptime(js['СКЛ_ВремяПередачиС'], "%Y-%m-%dT%H:%M:%S"),
                konservejo_fordono_tempo_en=datetime.strptime(js['СКЛ_ВремяПередачиПо'], "%Y-%m-%dT%H:%M:%S"),
                asekurado=js['ТребуетсяСтрахование'],
                horoj_sxargxistoj=js['КоличествоЧасовГрузчики'],
                kvanto_sxargxistoj=js['КоличествоГрузчиков'],
                kvanto_pecoj=js['КоличествоМест'],
                sxargxisto_rigilaro=js['ТребуютсяГрузчикиТакелажники'],
                en_peza_kategorio=js['Тяжеловес'],
                trajno_key=trajno_key,
                kondukisto_key=kondukisto_key,
                konservejo_temperatura_reghimo_1=js['скл_ТемпературныйРежим1'],
                konservejo_temperatura_reghimo_2=js['скл_ТемпературныйРежим2'],
                konservejo_temperatura_reghimo_3=js['скл_ТемпературныйРежим3'],
                konservejo_temperatura_reghimo_4=js['скл_ТемпературныйРежим4'],
                konservejo_temperatura_reghimo_alia=js['скл_ТемпературныйРежимИной'],
                konservejo_temperatura_reghimo_5=js['скл_ТемпературныйРежим5'],
                konservejo_temperatura_reghimo_6=js['скл_ТемпературныйРежим6'],
                liveranto=liveranto,
                liveranto_kontrakto=liveranto_kontrakto,
                kombatanto_kontrakto=kombatanto_kontrakto,
                trajno_venigo_dato=datetime.strptime(js['рл_ДатаПодачиТранспорт'], "%Y-%m-%dT%H:%M:%S"),
                trajno_venigo_tempo=datetime.strptime(js['рл_ВремяПодачиТранспорт'], "%Y-%m-%dT%H:%M:%S"),
                trajno_finigho_dato=datetime.strptime(js['рл_ДатаОкончанияТранспорт'], "%Y-%m-%dT%H:%M:%S"),
                trajno_finigho_tempo=datetime.strptime(js['рл_ВремяОкончанияТранспорт'], "%Y-%m-%dT%H:%M:%S"),
                trajno_konservejo_gk_key=trajno_konservejo_gk_key,
                kondukisto_konservejo_gk_key=kondukisto_konservejo_gk_key,
                sensoro_temperatura_2=js['ДатчикТемпературы2'],
                sensoro_temperatura_3=js['ДатчикТемпературы3'],
                sensoro_temperatura_4=js['ДатчикТемпературы4'],
                sensoro_temperatura_5=js['ДатчикТемпературы5'],
                sensoro_temperatura_alia=js['ДатчикТемпературыИной'],
                Livero_tp_2=js['ПредоставлениеТП2'],
                Livero_tp_3=js['ПредоставлениеТП3'],
                Livero_tp_4=js['ПредоставлениеТП4'],
                Livero_tp_5=js['ПредоставлениеТП5'],
                Livero_tp_alia=js['ПредоставлениеТПИной'],
                plenumado_dato=js['ДатаНаИсполнении'],
                fiksa_termoskribi=js['рл_СтационарныйТермописец'],
                fiksa_termoskribi_2=js['рл_СтационарныйТермописец2'],
                fiksa_termoskribi_3=js['рл_СтационарныйТермописец3'],
                fiksa_termoskribi_4=js['рл_СтационарныйТермописец4'],
                fiksa_termoskribi_5=js['рл_СтационарныйТермописец5'],
                fiksa_termoskribi_alia=js['рл_СтационарныйТермописецИной'],
                sekcio_kliento_key=sekcio_kliento_key,
                sxargxistoj_sxargxado=js['рл_ГрузчикиПогрузка'],
                sxargxistoj_elsxargxado=js['рл_ГрузчикиВыгрузка']
            )

            if adreso_elsxargxado:
                set_enhavo(ekspedo.adreso_elsxargxado, adreso_elsxargxado, 'ru_RU')
            if adreso_kargado:
                set_enhavo(ekspedo.adreso_kargado, adreso_kargado, 'ru_RU')
            if js['Комментарий']:
                set_enhavo(ekspedo.komento, js['Комментарий'], 'ru_RU')
            if js['КомментарийАдресаВыгрузки']:
                set_enhavo(ekspedo.komento_adreso_elsxargxado, js['КомментарийАдресаВыгрузки'], 'ru_RU')
            if js['КомментарийАдресаЗагрузки']:
                set_enhavo(ekspedo.komento_adreso_kargado, js['КомментарийАдресаЗагрузки'], 'ru_RU')
            if js['КомментарийОтмены']:
                set_enhavo(ekspedo.komento_nuligo, js['КомментарийОтмены'], 'ru_RU')
            if js['КонтактноеЛицоПоАдресу']:
                set_enhavo(ekspedo.kombatanto_adresoj, js['КонтактноеЛицоПоАдресу'], 'ru_RU')
            if js['КонтактноеЛицоПоАдресуРазгрузки']:
                set_enhavo(ekspedo.kombatanto_adresoj_elsxargxado, js['КонтактноеЛицоПоАдресуРазгрузки'], 'ru_RU')
            if js['ОписаниеГруза']:
                set_enhavo(ekspedo.priskribo_kargo, js['ОписаниеГруза'], 'ru_RU')
            if js['ОрганизацияПоАдресу']:
                set_enhavo(ekspedo.organizo_adreso, js['ОрганизацияПоАдресу'], 'ru_RU')
            if js['ОрганизацияПоАдресуРазгрузки']:
                set_enhavo(ekspedo.organizo_adreso_elsxargxado, js['ОрганизацияПоАдресуРазгрузки'], 'ru_RU')
            if js['ТелефонЗаказчика']:
                set_enhavo(ekspedo.telefono_kliento, js['ТелефонЗаказчика'], 'ru_RU')
            if js['ТелефонПоАдресу']:
                set_enhavo(ekspedo.telefono_adreso, js['ТелефонПоАдресу'], 'ru_RU')
            if js['ТелефонПоАдресуРазгрузки']:
                set_enhavo(ekspedo.telefono_adreso_elsxargxado, js['ТелефонПоАдресуРазгрузки'], 'ru_RU')
            if js['ДопУсловия']:
                set_enhavo(ekspedo.aldona_kondicxaro, js['ДопУсловия'], 'ru_RU')
            if js['СтавкаНДСЗаказчик']:
                set_enhavo(ekspedo.impostokvoto_kliento, js['СтавкаНДСЗаказчик'], 'ru_RU')
            if js['СтавкаНДСПеревозчик']:
                set_enhavo(ekspedo.impostokvoto_transportisto, js['СтавкаНДСПеревозчик'], 'ru_RU')
            if js['КомментарийРасчета']:
                set_enhavo(ekspedo.priskribo_kalkulado, js['КомментарийРасчета'], 'ru_RU')
            if js['НомерНакладнойСК']:
                set_enhavo(ekspedo.numero_frajtoletero, js['НомерНакладнойСК'], 'ru_RU')
            if js['ОписаниеОпасногоГруза']:
                set_enhavo(ekspedo.priskribo_dangxera_kargo, js['ОписаниеОпасногоГруза'], 'ru_RU')
            if js['КлассОпасности']:
                set_enhavo(ekspedo.klaso_dangxera, js['КлассОпасности'], 'ru_RU')
            if js['ДопТребованияХЦ']:
                set_enhavo(ekspedo.aldone_postulo_hc, js['ДопТребованияХЦ'], 'ru_RU')
            if js['ИныеУслуги']:
                set_enhavo(ekspedo.aliaj_servo, js['ИныеУслуги'], 'ru_RU')
            if js['ПередачаГрузаФИО']:
                set_enhavo(ekspedo.fordono_kargo_fio, js['ПередачаГрузаФИО'], 'ru_RU')
            if js['НаименованиеГруза']:
                set_enhavo(ekspedo.nomo_kargo, js['НаименованиеГруза'], 'ru_RU')
            if js['скл_Задание']:
                set_enhavo(ekspedo.konservejo_tasko, js['скл_Задание'], 'ru_RU')
            if js['УИД']:
                ekspedo.uid = js['УИД']
            if js['скл_Комментарий']:
                set_enhavo(ekspedo.konservejo_komento, js['скл_Комментарий'], 'ru_RU')
            if js['скл_КомментарийКлиента']:
                set_enhavo(ekspedo.priskribo_kliento, js['скл_КомментарийКлиента'], 'ru_RU')
            if js['НомерНакладнойКлиента']:
                set_enhavo(ekspedo.numero_frajtoletero_kliento, js['НомерНакладнойКлиента'], 'ru_RU')
            if js['EmailОтправителя']:
                set_enhavo(ekspedo.email_ekspedinto, js['EmailОтправителя'], 'ru_RU')
            if js['EmailПолучателя']:
                set_enhavo(ekspedo.email_ricevanto, js['EmailПолучателя'], 'ru_RU')
            if js['рл_ТипЗаявки']:
                set_enhavo(ekspedo.tipo_mendo, js['рл_ТипЗаявки'], 'ru_RU')
            if urboj_recivado_nomo:
                set_enhavo(ekspedo.urboj_recivado_nomo, urboj_recivado_nomo, 'ru_RU')
            if urboj_transdono_nomo:
                set_enhavo(ekspedo.urboj_transdono_nomo, urboj_transdono_nomo, 'ru_RU')
                
            dokumento_ekspedo_save(ekspedo)
            
            errors, ekspedo_kargo = ImportoDokumentoEkspedo1c.aldoni_kargo(ekspedo, publikiga_dato, errors, js)

            # for js_kargo in js['ГрузыММП']:
            #     tipo_pakumoj = None
            #     if js_kargo['ТипУпаковки_Key'] != '00000000-0000-0000-0000-000000000000':
            #         try:
            #             tipo_pakumoj = ObjektoTipoPakumo.objects.get(uuid=js_kargo['ТипУпаковки_Key'])
            #         except ObjektoTipoPakumo.DoesNotExist:
            #             errors.append(ErrorNode(
            #                 field="{}{}".format('ТипУпаковки_Key = ',js_kargo['ТипУпаковки_Key']),
            #                 message=_('Нет такой записи в базе ObjektoTipoPakumo')
            #             ))
            #     indikatoro_key = None
            #     if js_kargo['Датчик_Key'] != '00000000-0000-0000-0000-000000000000':
            #         indikatoro_key = js_kargo['Датчик_Key']

            #     temperatura_reghimo = None
            #     if js_kargo['Терморежим_Key'] != '00000000-0000-0000-0000-000000000000':
            #         try:
            #             temperatura_reghimo = ObjektoTemperaturaReghimo.objects.get(uuid=js_kargo['Терморежим_Key'])
            #         except ObjektoTemperaturaReghimo.DoesNotExist:
            #             errors.append(ErrorNode(
            #                 field="{}{}".format('Терморежим_Key = ',js_kargo['Терморежим_Key']),
            #                 message=_('Нет такой записи в базе от ГрузыММП')
            #             ))
            #     if not len(errors):
            #         ekspedo_kargo = DokumentoEkspedoKargo.objects.create(
            #             forigo=False,
            #             arkivo=False,
            #             publikigo=True,
            #             publikiga_dato=publikiga_dato,
            #             numero=js_kargo['LineNumber'],
            #             kvanto_pecoj=js_kargo['КоличествоМест'],
            #             longo=js_kargo['Длина'],
            #             largho=js_kargo['Ширина'],
            #             alto=js_kargo['Высота'],
            #             pezo_fakta=js_kargo['ВесФактический'],
            #             volumeno=js_kargo['Объем'],
            #             pezo_volumena=js_kargo['ВесОбъемный'],
            #             tipo_pakumoj=tipo_pakumoj,
            #             indikatoro=js_kargo['НуженДатчик'],
            #             temperatura_reghimo=temperatura_reghimo,
            #             grave=js_kargo['ОпасныйНеопасный'],
            #             retropasxo_pakumo=js_kargo['ВозвратУпаковки'],
            #             retropasxo_indikatoro=js_kargo['ВозвратДатчика'],
            #             indikatoro_key=indikatoro_key,
            #             ekspedo=ekspedo,
            #         )
            #         if js_kargo['Наименование']:
            #             set_enhavo(ekspedo_kargo.nomo, js_kargo['Наименование'], 'ru_RU')
            #         if js_kargo['СерийныйНомерДатчика']:
            #             set_enhavo(ekspedo_kargo.numero_indikatoro, js_kargo['СерийныйНомерДатчика'], 'ru_RU')
                        
            #         ekspedo_kargo.save()

            errors, ekspedo_konservejo_kargo_tabelo = ImportoDokumentoEkspedo1c.aldoni_konservejo_kargo(ekspedo, publikiga_dato, errors, js)

            errors, ekspedo_konservejo_kargo_kondukisto_tabelo = ImportoDokumentoEkspedo1c.aldoni_konservejo_kargo_kondukisto(ekspedo, publikiga_dato, errors, js)
            
            # Маршрут импортируется в задачи
            # ИдМаршрута на 18.11.2021 в 1с всегда передавать = 1

            #  
            errors, projekto, tasko = ImportoDokumentoEkspedo1c.aldoni_projekto_taskoj(
                root, info, ekspedo, publikiga_dato, errors, js, dokumento, organizo, kliento)
            
            # projekto = None
            # if len(js['Маршрут'])>0: # если есть маршрут, то создаём проект
            #     # находим текущий статус маршрута согласно поля удалитьРезультатВыполнения_Key 
            #     statuso_id = 1 # Новый
            #     statuso = None
            #     if js['удалитьРезультатВыполнения_Key'] and js['удалитьРезультатВыполнения_Key'] != '00000000-0000-0000-0000-000000000000':
            #         try:
            #             statuso = TaskojProjektoStatuso.objects.get(uuid=js['удалитьРезультатВыполнения_Key'])
            #             statuso_id = statuso.id
            #         except TaskojProjektoStatuso.DoesNotExist:
            #             errors.append(ErrorNode(
            #                 field="{}{}".format('удалитьРезультатВыполнения_Key = ',js['удалитьРезультатВыполнения_Key']),
            #                 message=_('Нет такого статуса проекта в базе')
            #             ))
                
            #     # подготавливаем под задачи+проекты+владельцы
            #     kwargoj = {
            #         'prj_nomo': 'Экспедирование',
            #         'prj_kategorio': [7,],
            #         'prj_tipo_id': 3,
            #         'prj_statuso_id': statuso_id,
            #         'prj_posedanto_tipo_id': [1],
            #         'prj_posedanto_statuso_id': [1],
            #         'posedanto_dokumento_uuid': [dokumento.uuid],
            #         'prj_posedanto_organizo_uuid': [],
                    
            #         'tipo_id': 3,
                    
            #         # эти в цикле собираем:
            #         'kategorio':[],
            #         'pozicio': [],
            #         'nomo': [],
            #         'priskribo': [],
            #         'statuso_id': [],
            #         'posedanto_tipo_id':[],
            #         'posedanto_statuso_id':[],
            #         'kom_dato':[],
            #         'fin_dato':[],
            #         'kom_adreso':[],
            #         'fin_adreso':[]
            #     }
            #     if kliento:
            #         kwargoj['prj_posedanto_organizo_uuid'] = [kliento.uuid]
            #     # поставщика услуг также во владельцы проекта надо ставить
            #     if organizo:
            #         kwargoj['prj_posedanto_tipo_id'].append(1)
            #         kwargoj['prj_posedanto_statuso_id'].append(1)
            #         kwargoj['posedanto_dokumento_uuid'].append(dokumento.uuid)
            #         kwargoj['prj_posedanto_organizo_uuid'].append(organizo.uuid)

            #     i_tasko = 1
            #     for js_tasko in js['Маршрут']:
            #         kwargoj['pozicio'].append(i_tasko)
            #         kwargoj['kategorio'].append(13)
            #         kwargoj['nomo'].append(js_tasko['ИдентификаторМаршрута'])
            #         kwargoj['priskribo'].append(js_tasko['Комментарий'])
            #         if js_tasko['Состояние_Key'] and js_tasko['Состояние_Key'] != '00000000-0000-0000-0000-000000000000':
            #             statuso_id = 1 # Новый
            #             statuso = None
            #             try:
            #                 statuso = TaskojTaskoStatuso.objects.get(uuid=js_tasko['Состояние_Key'])
            #                 statuso_id = statuso.id
            #             except TaskojTaskoStatuso.DoesNotExist:
            #                 errors.append(ErrorNode(
            #                     field="{}{}".format('Состояние_Key в Маршруте = ',js_tasko['Состояние_Key']),
            #                     message=_('Нет такого статуса задачи в базе')
            #                 ))
            #         kwargoj['statuso_id'].append(statuso_id)
            #         kwargoj['posedanto_tipo_id'].append(1)
            #         kwargoj['posedanto_statuso_id'].append(1)
            #         kwargoj['kom_dato'].append(js_tasko['ДатаНачала'])
            #         kwargoj['fin_dato'].append(js_tasko['ДатаОкончания'])
            #         kwargoj['kom_adreso'].append(js_tasko['АдресНачала'])
            #         kwargoj['fin_adreso'].append(js_tasko['АдресКонца'])
                    
            #         i_tasko += 1
            #     print('=== добавление задач 1 === ')
            #     status, message2, errors2, projekto, taskoj = RedaktuKreiTaskojProjektoTaskojPosedanto.create(root, info, **kwargoj)

            #     if errors2 and len(errors2)>0:
            #         errors.extend(errors2)
        else:
            message = _('Nevalida argumentvaloroj')
            for error in errors:
                message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                    error.message, _('в поле'), error.field)
        return status, message, errors, ekspedo, kvanto

    @staticmethod
    def aldoni_kargo(ekspedo, publikiga_dato, errors, js):
        ekspedo_kargo_tabelo = []
        for js_kargo in js['ГрузыММП']:
            tipo_pakumoj = None
            if js_kargo['ТипУпаковки_Key'] != '00000000-0000-0000-0000-000000000000':
                try:
                    tipo_pakumoj = ObjektoTipoPakumo.objects.get(uuid=js_kargo['ТипУпаковки_Key'])
                except ObjektoTipoPakumo.DoesNotExist:
                    errors.append(ErrorNode(
                        field="{}{}".format('ТипУпаковки_Key = ',js_kargo['ТипУпаковки_Key']),
                        message=_('Нет такой записи в базе ObjektoTipoPakumo')
                    ))
            indikatoro_key = None
            if js_kargo['Датчик_Key'] != '00000000-0000-0000-0000-000000000000':
                indikatoro_key = js_kargo['Датчик_Key']

            temperatura_reghimo = None
            if js_kargo['Терморежим_Key'] != '00000000-0000-0000-0000-000000000000':
                try:
                    temperatura_reghimo = ObjektoTemperaturaReghimo.objects.get(uuid=js_kargo['Терморежим_Key'])
                except ObjektoTemperaturaReghimo.DoesNotExist:
                    errors.append(ErrorNode(
                        field="{}{}".format('Терморежим_Key = ',js_kargo['Терморежим_Key']),
                        message=_('Нет такой записи в базе от ГрузыММП')
                    ))
            if not len(errors):
                ekspedo_kargo = DokumentoEkspedoKargo.objects.create(
                    forigo=False,
                    arkivo=False,
                    publikigo=True,
                    publikiga_dato=publikiga_dato,
                    numero=js_kargo['LineNumber'],
                    kvanto_pecoj=js_kargo['КоличествоМест'],
                    longo=js_kargo['Длина'],
                    largho=js_kargo['Ширина'],
                    alto=js_kargo['Высота'],
                    pezo_fakta=js_kargo['ВесФактический'],
                    volumeno=js_kargo['Объем'],
                    pezo_volumena=js_kargo['ВесОбъемный'],
                    tipo_pakumoj=tipo_pakumoj,
                    indikatoro=js_kargo['НуженДатчик'],
                    temperatura_reghimo=temperatura_reghimo,
                    grave=js_kargo['ОпасныйНеопасный'],
                    retropasxo_pakumo=js_kargo['ВозвратУпаковки'],
                    retropasxo_indikatoro=js_kargo['ВозвратДатчика'],
                    indikatoro_key=indikatoro_key,
                    ekspedo=ekspedo,
                )
                if js_kargo['Наименование']:
                    set_enhavo(ekspedo_kargo.nomo, js_kargo['Наименование'], 'ru_RU')
                if js_kargo['СерийныйНомерДатчика']:
                    set_enhavo(ekspedo_kargo.numero_indikatoro, js_kargo['СерийныйНомерДатчика'], 'ru_RU')
                    
                ekspedo_kargo.save()
                ekspedo_kargo_tabelo.append(ekspedo_kargo)
        return errors, ekspedo_kargo_tabelo

    @staticmethod
    def aldoni_konservejo_kargo(ekspedo, publikiga_dato, errors, js):
        ekspedo_konservejo_kargo_tabelo = []
        for js_konservejo in js['скл_Грузы']:

            tipo_kontenero = None # это тип упаковки - у нас нет данных
            if not len(errors):
                ekspedo_konservejo_kargo = DokumentoEkspedoKonservejoKargo.objects.create(
                    forigo=False,
                    arkivo=False,
                    publikigo=True,
                    publikiga_dato=publikiga_dato,
                    numero=js_konservejo['LineNumber'],
                    tipo_kontenero=tipo_kontenero,
                    longo=js_konservejo['Длина'],
                    largho=js_konservejo['Ширина'],
                    alto=js_konservejo['Высота'],
                    kvanto_pecoj=js_konservejo['КоличествоМест'],
                    pezo=js_konservejo['Вес'],
                    ekspedo=ekspedo,
                )

                if js_konservejo['Объем']:
                    set_enhavo(ekspedo_konservejo_kargo.temperatura_reghimo, js_konservejo['Объем'], 'ru_RU')

                ekspedo_konservejo_kargo.save()
                ekspedo_konservejo_kargo_tabelo.append(ekspedo_konservejo_kargo)
        return errors, ekspedo_konservejo_kargo_tabelo

    @staticmethod
    def aldoni_konservejo_kargo_kondukisto(ekspedo, publikiga_dato, errors, js):
        ekspedo_konservejo_kargo_kondukisto_tabelo = []
        for js_kondukisto in js['скл_ГрузыВодитель']:

            if not len(errors):
                ekspedo_konservejo_kargo_kondukisto = DokumentoEkspedoKonservejoKargoKondukisto.objects.create(
                    forigo=False,
                    arkivo=False,
                    publikigo=True,
                    publikiga_dato=publikiga_dato,
                    numero=js_kondukisto['LineNumber'],
                    longo=js_kondukisto['Длина'],
                    largho=js_kondukisto['Ширина'],
                    alto=js_kondukisto['Высота'],
                    kvanto_pecoj=js_kondukisto['КоличествоМест'],
                    pezo=js_kondukisto['Вес'],
                    ekspedo=ekspedo,
                )

                if js_kondukisto['Объем']:
                    set_enhavo(ekspedo_konservejo_kargo_kondukisto.temperatura_reghimo, js_kondukisto['Объем'], 'ru_RU')

                ekspedo_konservejo_kargo_kondukisto.save()
                ekspedo_konservejo_kargo_kondukisto_tabelo.append(ekspedo_konservejo_kargo_kondukisto)
        return errors, ekspedo_konservejo_kargo_kondukisto_tabelo


    @staticmethod
    def aldoni_projekto_taskoj(root, info, ekspedo, publikiga_dato, errors, js, dokumento, organizo, kliento):
        taskoj = None
        projekto = None
        if len(js['Маршрут'])>0: # если есть маршрут, то создаём проект
            # находим текущий статус маршрута согласно поля удалитьРезультатВыполнения_Key 
            statuso_id = 1 # Новый
            statuso = None
            if js['удалитьРезультатВыполнения_Key'] and js['удалитьРезультатВыполнения_Key'] != '00000000-0000-0000-0000-000000000000':
                try:
                    statuso = TaskojProjektoStatuso.objects.get(uuid=js['удалитьРезультатВыполнения_Key'])
                    statuso_id = statuso.id
                except TaskojProjektoStatuso.DoesNotExist:
                    errors.append(ErrorNode(
                        field="{}{}".format('удалитьРезультатВыполнения_Key = ',js['удалитьРезультатВыполнения_Key']),
                        message=_('Нет такого статуса проекта в базе')
                    ))
            
            # подготавливаем под задачи+проекты+владельцы
            kwargoj = {
                'realeco_id': 1,

                'prj_nomo': 'Экспедирование',
                'prj_kategorio': [7,],
                'prj_tipo_id': 3,
                'prj_statuso_id': statuso_id,
                'prj_posedanto_tipo_id': [1],
                'prj_posedanto_statuso_id': [1],
                'posedanto_dokumento_uuid': [dokumento.uuid],
                'prj_posedanto_organizo_uuid': [None, ],
                
                'tipo_id': 3,
                
                # эти в цикле собираем:
                'kategorio':[],
                'pozicio': [],
                'nomo': [],
                'priskribo': [],
                'statuso_id': [],
                'posedanto_tipo_id':[],
                'posedanto_statuso_id':[],
                'kom_dato':[],
                'fin_dato':[],
                'kom_adreso':[],
                'fin_adreso':[]
            }
            if kliento:
                kwargoj['prj_posedanto_organizo_uuid'] = [kliento.uuid]
            # поставщика услуг также во владельцы проекта надо ставить
            if organizo:
                kwargoj['prj_posedanto_tipo_id'].append(1)
                kwargoj['prj_posedanto_statuso_id'].append(1)
                kwargoj['posedanto_dokumento_uuid'].append(dokumento.uuid)
                kwargoj['prj_posedanto_organizo_uuid'].append(organizo.uuid)

            i_tasko = 1
            for js_tasko in js['Маршрут']:
                kwargoj['pozicio'].append(i_tasko)
                kwargoj['kategorio'].append(13)
                kwargoj['nomo'].append(js_tasko['ИдентификаторМаршрута'])
                kwargoj['priskribo'].append(js_tasko['Комментарий'])
                if js_tasko['Состояние_Key'] and js_tasko['Состояние_Key'] != '00000000-0000-0000-0000-000000000000':
                    statuso_id = 1 # Новый
                    statuso = None
                    try:
                        statuso = TaskojTaskoStatuso.objects.get(uuid=js_tasko['Состояние_Key'])
                        statuso_id = statuso.id
                    except TaskojTaskoStatuso.DoesNotExist:
                        errors.append(ErrorNode(
                            field="{}{}".format('Состояние_Key в Маршруте = ',js_tasko['Состояние_Key']),
                            message=_('Нет такого статуса задачи в базе')
                        ))
                kwargoj['statuso_id'].append(statuso_id)
                kwargoj['posedanto_tipo_id'].append(1)
                kwargoj['posedanto_statuso_id'].append(1)
                kwargoj['kom_dato'].append(js_tasko['ДатаНачала'])
                kwargoj['fin_dato'].append(js_tasko['ДатаОкончания'])
                kwargoj['kom_adreso'].append(js_tasko['АдресНачала'])
                kwargoj['fin_adreso'].append(js_tasko['АдресКонца'])
                
                i_tasko += 1
            if not info:
                kwargoj['uzanto'] = js.get('uzanto', None)
            print('=== добавление задач 2 === ')
            # status, message2, errors2, projekto, taskoj = RedaktuKreiTaskojProjektoTaskojPosedanto.create(root, info, **kwargoj)
            status, message2, errors2, projekto, taskoj = RedaktuKreiTaskojProjektoTaskojPosedanto.krei(info, **kwargoj)
            
            if errors2 and len(errors2)>0:
                errors.extend(errors2)
        return errors, projekto, taskoj
    
    @staticmethod
    def redakti_uid(root, info, kvanto, uuid, ekspedo_, informlibro, **kwargs):
        """
        Редактируем в базе данные из параметра kwargs синхронизирую по uid (УИД)
        
        """
        errors = list()
        status = False
        message = ''
        ekspedo = ekspedo_
        uid = kwargs.get('УИД',False)
        uzanto = None
        if info:
            uzanto = info.context.user
        else:
            print('= kwargs == uzanto = ', kwargs.get('uzanto', None))
            uzanto = kwargs.get('uzanto', None)
            # и ищем пользователя в базе
        print('=== uzanto = ', uzanto)
        if not ekspedo and uuid: # если передан uuid в нашей базе, то сразу берём его
            try: # закоментировано для тестирования поиска по uid
                ekspedo = DokumentoEkspedo.objects.get(uuid=uuid, forigo=False,
                                                    arkivo=False, publikigo=True)
            except DokumentoEkspedo.DoesNotExist:
                ekspedo = None
        print('==== uuid === ', uuid)
        if uid and not ekspedo:
            # если ещё не открыт, то ищём в базе по УИД
            try:
                print('==== uid === ', uid)
                ekspedo = DokumentoEkspedo.objects.get(uid=uid, forigo=False,
                                                arkivo=False, publikigo=True)
                print('==== ekspedo ==== ', ekspedo)
            except DokumentoEkspedo.DoesNotExist:
                ekspedo = None
                print('====== не найден документ экспедирования ======== ')
        print('======= пошли в исправление ==== ', ekspedo)
        if not informlibro:
            # проверяем обязательные параметры
            errors, kwargs = ImportoDokumentoEkspedo1c.argumentoj_kreanta(root, info, **kwargs)
            if not len(errors):
                # проверяем наличие записей с таким кодом
                errors, informlibro = ImportoDokumentoEkspedo1c.kontrolanta_argumentoj(root, info, **kwargs)
        if ekspedo and not len(errors):
            print('=== kwargs == ', kwargs)
            publikiga_dato = timezone.now()

            if not len(errors):
                dokumento = ekspedo.dokumento
                kvanto += 1

            kodo = kwargs.get('Number','555')
            print('=== kodo == ', kodo)
            adreso_elsxargxado = kwargs['АдресВыгрузки']
            adreso_kargado = kwargs['АдресЗагрузки']
            pezo = kwargs['Вес']
            tempo_elsxargxado = kwargs['ВремяВыгрузкиС']
            ekspedinto = ekspedo.ekspedinto
            ricevanto = ekspedo.ricevanto
            kliento = ekspedo.kliento
            kombatanto_kliento = ekspedo.kombatanto_kliento
            organizo = ekspedo.organizo
            kialo_nuligo_key = ekspedo.kialo_nuligo_key
            kontrakto_kliento = ekspedo.kontrakto_kliento
            if kwargs['ПричинаОтмены_Key'] != '00000000-0000-0000-0000-000000000000':
                kialo_nuligo_key = kwargs['ПричинаОтмены_Key']
            # autoro_logist_key = None
            # if kwargs['АвторЛогист_Key'] != '00000000-0000-0000-0000-000000000000':
            #     autoro_logist_key = kwargs['АвторЛогист_Key']
            temperatura_reghimo = None
            faro_hc_key = None
            if kwargs['СозданиеХЦ_Key'] != '00000000-0000-0000-0000-000000000000':
                faro_hc_key = kwargs['СозданиеХЦ_Key']
            urboj_recivado = None
            urboj_transdono = None
            fordono_kargo_dato=datetime.strptime(kwargs['ФПередачаГрузаДата'], "%Y-%m-%dT%H:%M:%S")
            fordono_kargo_tempo=datetime.strptime(kwargs['ФПередачаГрузаВремя'], "%Y-%m-%dT%H:%M:%S")
            trajno_key = None
            if kwargs['Транспорт_Key'] != '00000000-0000-0000-0000-000000000000':
                trajno_key = kwargs['Транспорт_Key']
            kondukisto_key = None
            if kwargs['Водитель_Key'] != '00000000-0000-0000-0000-000000000000':
                kondukisto_key = kwargs['Водитель_Key']
            liveranto = None
            kombatanto_kontrakto = None
            trajno_konservejo_gk_key = None
            if kwargs['ТранспортСоСкладаГК_Key'] != '00000000-0000-0000-0000-000000000000':
                trajno_konservejo_gk_key = kwargs['ТранспортСоСкладаГК_Key']
            kondukisto_konservejo_gk_key = None
            if kwargs['ВодительСоСкладаГК_Key'] != '00000000-0000-0000-0000-000000000000':
                kondukisto_konservejo_gk_key = kwargs['ВодительСоСкладаГК_Key']
            sekcio_kliento_key = None
            if kwargs['рл_ПодразделениеЗаказчика_Key'] != '00000000-0000-0000-0000-000000000000':
                sekcio_kliento_key = kwargs['рл_ПодразделениеЗаказчика_Key']

            valuto_kliento = None
            if kwargs['ВалютаЗаказчика_Key'] != '00000000-0000-0000-0000-000000000000':
                try:
                    valuto_kliento = MonoValuto.objects.get(uuid=kwargs['ВалютаЗаказчика_Key'])
                except MonoValuto.DoesNotExist:
                    errors.append(ErrorNode(
                        field='ВалютаЗаказчика_Key',
                        message=_('Нет такой валюты в базе')
                    ))
            if kwargs['Грузоотправитель_Key'] != '00000000-0000-0000-0000-000000000000':
                try:
                    ekspedinto = Organizo.objects.get(uuid=kwargs['Грузоотправитель_Key'])
                except Organizo.DoesNotExist:
                    errors.append(ErrorNode(
                        field='Грузоотправитель_Key',
                        message=_('Нет такого грузоотправителя в базе')
                    ))
            if kwargs['Грузополучатель_Key'] != '00000000-0000-0000-0000-000000000000':
                try:
                    ricevanto = Organizo.objects.get(uuid=kwargs['Грузополучатель_Key'])
                except Organizo.DoesNotExist:
                    errors.append(ErrorNode(
                        field='Грузополучатель_Key',
                        message=_('Нет такого грузополучателя в базе')
                    ))
            print('========= dokumento  ============ ', dokumento)
            posedantoj_dokumento = dokumento.dokumentoj_dokumentoposedanto_dokumento.all()
            print('========= dokumento.dokumentoj_dokumentoposedanto_dokumento count ============ ', 
                posedantoj_dokumento.count())
            if kwargs['Заказчик_Key'] != '00000000-0000-0000-0000-000000000000':
                pos_kliento = False # клиент не найден
                for posedanto_dokumento in posedantoj_dokumento:
                    if str(posedanto_dokumento.posedanto_organizo.uuid) == str(kwargs['Заказчик_Key']):
                        pos_kliento = True
                        break
                # проверяем и добавляем клиента в модель DokumentoEkspedo, а не только владельцем
                if not pos_kliento:
                    # если не было клиента, то добавляем
                    if not kliento or (str(kliento.uuid) != kwargs['Заказчик_Key']):
                        try:
                            kliento = Organizo.objects.get(uuid=kwargs['Заказчик_Key'])
                        except Organizo.DoesNotExist:
                            errors.append(ErrorNode(
                                field='Заказчик_Key', 
                                message=_('Нет такого заказчика в базе')
                            ))
                            kliento = None
                    if kliento:
                        posedanto_kliento = DokumentoPosedanto.objects.create(
                            forigo=False,
                            arkivo=False,
                            publikigo=True,
                            publikiga_dato=publikiga_dato,
                            tipo=informlibro['posedanto_dokumento_kliento_tipo'],
                            statuso=informlibro['posedanto_dokumento_statuso'],
                            posedanto_organizo=kliento,
                            dokumento=dokumento
                        )
            if kwargs['Организация_Key'] != '00000000-0000-0000-0000-000000000000':
                pos_organizo = False # организация не найдена
                for posedanto_dokumento in posedantoj_dokumento:
                    if str(posedanto_dokumento.posedanto_organizo.uuid) == str(kwargs['Организация_Key']):
                        pos_organizo = True
                        break
                if not pos_organizo:
                    # если есть владелец, то создаём
                    if not organizo or (str(organizo.uuid) != kwargs['Организация_Key']):
                        try:
                            organizo = Organizo.objects.get(uuid=kwargs['Организация_Key'])
                        except Organizo.DoesNotExist:
                            errors.append(ErrorNode(
                                field='Организация_Key',
                                message=_('Нет такой организации в базе')
                            ))
                            organizo = None
                    if organizo:
                        posedanto_organizo = DokumentoPosedanto.objects.create(
                            forigo=False,
                            arkivo=False,
                            publikigo=True,
                            publikiga_dato=publikiga_dato,
                            tipo=informlibro['posedanto_dokumento_organizo_tipo'],
                            statuso=informlibro['posedanto_dokumento_statuso'],
                            posedanto_organizo=organizo,
                            dokumento=dokumento
                        )
            if kwargs['ДоговорЗаказчика_Key'] != '00000000-0000-0000-0000-000000000000':
                try:
                    kontrakto_kliento_contract = DokumentoContract.objects.get(uuid=kwargs['ДоговорЗаказчика_Key'])
                    kontrakto_kliento = kontrakto_kliento_contract.dokumento
                except DokumentoContract.DoesNotExist:
                    errors.append(ErrorNode(
                        field='ДоговорЗаказчика_Key',
                        message=_('Нет такого договора в базе')
                    ))
            liveranto_kontrakto = None
            if kwargs['рл_ДоговорПоставщика_Key'] != '00000000-0000-0000-0000-000000000000':
                try:
                    liveranto_kontrakto_contract = DokumentoContract.objects.get(uuid=kwargs['рл_ДоговорПоставщика_Key'])
                    liveranto_kontrakto = liveranto_kontrakto_contract.dokumento
                except DokumentoContract.DoesNotExist:
                    errors.append(ErrorNode(
                        field='ДоговорЗаказчика_Key',
                        message=_('Нет такого договора в базе')
                    ))
            if kwargs['КонтактноеЛицоЗаказчика'] and kwargs['КонтактноеЛицоЗаказчика'] != '00000000-0000-0000-0000-000000000000':
                if kwargs['КонтактноеЛицоЗаказчика_Type'] == "StandardODATA.Catalog_КонтактныеЛица":
                    try:
                        kombatanto_kliento = Kombatanto.objects.get(uuid=kwargs['КонтактноеЛицоЗаказчика'])
                    except Kombatanto.DoesNotExist:
                        errors.append(ErrorNode(
                            field='КонтактноеЛицоЗаказчика',
                            message=_('Нет такого контактного лица в базе')
                        ))
                elif  kwargs['КонтактноеЛицоЗаказчика_Type'] == 'Edm.String':
                    # создаёи контрагента
                    errors, kombatanto_tipo, kombatanto_speco = ImportoKontakto1c.PrepariKombatanto(errors)

                    if not len(errors):
                        kombatanto_kliento = Kombatanto.objects.create(
                            forigo=False,
                            arkivo=False,
                            publikigo=True,
                            publikiga_dato=publikiga_dato,
                            tipo=kombatanto_tipo,
                            speco=kombatanto_speco,
                            organizo=kliento,
                        )
                        if uzanto:
                            kombatanto_kliento.autoro=uzanto
                        set_enhavo(kombatanto_kliento.familinomo, kwargs['КонтактноеЛицоЗаказчика'], 'ru_RU')
                        set_enhavo(kombatanto_kliento.informoj, kwargs['КонтактноеЛицоЗаказчика'], 'ru_RU')

                        kombatanto_kliento.save()
            if kwargs['ТемпературныйРежим_Key'] != '00000000-0000-0000-0000-000000000000':
                try:
                    temperatura_reghimo = ObjektoTemperaturaReghimo.objects.get(uuid=kwargs['ТемпературныйРежим_Key'])
                except ObjektoTemperaturaReghimo.DoesNotExist:
                    errors.append(ErrorNode(
                        field="{}{}".format('ТемпературныйРежим_Key = ',kwargs['ТемпературныйРежим_Key']),
                        message=_('Нет такой записи в базе')
                    ))
            urboj_recivado_nomo = None
            if kwargs['ГородПриемки'] and kwargs['ГородПриемки'] != '00000000-0000-0000-0000-000000000000':
                if kwargs['ГородПриемки_Type'] == "StandardODATA.Catalog_СКЛ_Города":
                    try:
                        urboj_recivado = InformilojUrboj.objects.get(uuid=kwargs['ГородПриемки'])
                    except InformilojUrboj.DoesNotExist:
                        errors.append(ErrorNode(
                            field="{}{}".format('ГородПриемки = ',kwargs['ГородПриемки']),
                            message=_('Нет такой записи в базе')
                        ))
                elif kwargs['ГородПриемки_Type'] == 'Edm.String':
                    urboj_recivado_nomo = kwargs['ГородПриемки']
            urboj_transdono_nomo = None
            if kwargs['ГородПередачи'] and kwargs['ГородПередачи'] != '00000000-0000-0000-0000-000000000000':
                if kwargs['ГородПередачи_Type'] == "StandardODATA.Catalog_СКЛ_Города":
                    try:
                        urboj_transdono = InformilojUrboj.objects.get(uuid=kwargs['ГородПередачи'])
                    except InformilojUrboj.DoesNotExist:
                        errors.append(ErrorNode(
                            field="{}{}".format('ГородПередачи = ',kwargs['ГородПередачи']),
                            message=_('Нет такой записи в базе')
                        ))
                elif kwargs['ГородПередачи_Type'] == 'Edm.String':
                    urboj_transdono_nomo = kwargs['ГородПередачи']
            if kwargs['рл_Поставщик_Key'] != '00000000-0000-0000-0000-000000000000':
                try:
                    liveranto = Organizo.objects.get(uuid=kwargs['рл_Поставщик_Key'])
                except Organizo.DoesNotExist:
                    errors.append(ErrorNode(
                        field="{}{}".format('рл_Поставщик_Key = ',kwargs['рл_Поставщик_Key']),
                        message=_('Нет такой записи в базе')
                    ))
            if kwargs['рл_КонтактноеЛицоПоставщика_Key'] != '00000000-0000-0000-0000-000000000000':
                try:
                    kombatanto_kontrakto = Kombatanto.objects.get(uuid=kwargs['рл_КонтактноеЛицоПоставщика_Key'])
                except Kombatanto.DoesNotExist:
                    errors.append(ErrorNode(
                        field="{}{}".format('рл_КонтактноеЛицоПоставщика_Key = ',kwargs['рл_КонтактноеЛицоПоставщика_Key']),
                        message=_('Нет такой записи в базе')
                    ))

            if not len(errors):
                ekspedo.kodo=kodo
                ekspedo.valuto_kliento=valuto_kliento
                ekspedo.pezo=pezo
                ekspedo.tempo_elsxargxado=tempo_elsxargxado
                ekspedo.tempo_s=kwargs['ВремяС']
                ekspedo.ekspedinto=ekspedinto
                ekspedo.ricevanto=ricevanto
                ekspedo.kontrakto_kliento=kontrakto_kliento
                ekspedo.kliento=kliento
                ekspedo.kliento_sciigi=kwargs['ЗаказчикИзвещен']
                ekspedo.kombatanto_kliento=kombatanto_kliento
                ekspedo.volumeno=kwargs['Объем']
                ekspedo.organizo=organizo
                ekspedo.kialo_nuligo_key=kialo_nuligo_key
                ekspedo.kosto_kargo=kwargs['СтоимостьГруза']
                ekspedo.monsumo_kliento=kwargs['СуммаЗаказчика']
                ekspedo.monsumo_kliento_fiksa_mane=kwargs['СуммаЗаказчикаУстановленаВручную']
                ekspedo.monsumo_transportisto=kwargs['СуммаПеревозчика']
                ekspedo.monsumo_transportisto_fiksa_mane=kwargs['СуммаПеревозчикаУстановленаВручную']
                ekspedo.fakto=kwargs['Факт']
                ekspedo.kosto_kliento=kwargs['ЦенаЗаказчика']
                ekspedo.kosto_transportisto=kwargs['ЦенаПеревозчика']
                ekspedo.partopago_kliento=kwargs['АвансЗаказчика']
                ekspedo.partopago_transportisto=kwargs['АвансПеревозчику']
                    # autoro_logist_key=autoro_logist_key,
                ekspedo.pezo_fakto=kwargs['ВесФакт']
                ekspedo.pezo_kalkula=kwargs['ВесРасч']
                ekspedo.dangxera_kargo=kwargs['ОпасныйГруз']
                ekspedo.retropasxo_tsd=kwargs['ВозвратТСД']
                ekspedo.akcepto_liveri_nelabora_tempo=kwargs['ПриемДоставкаВНерабочееВремя']
                ekspedo.aldone_pakumo=kwargs['ДополнительнаяУпаковка']
                ekspedo.temperatura_reghimo=temperatura_reghimo
                ekspedo.faro_hc_key=faro_hc_key
                ekspedo.indikatoro_temperatura=kwargs['ДатчикТемпературы']
                ekspedo.livero_tp=kwargs['ПредоставлениеТП']
                ekspedo.urboj_recivado=urboj_recivado
                ekspedo.urboj_transdono=urboj_transdono
                ekspedo.fordono_kargo_dato=fordono_kargo_dato
                ekspedo.fordono_kargo_tempo=fordono_kargo_tempo
                ekspedo.konservejo_ricevado_dato=datetime.strptime(kwargs['СКЛ_ДатаПриемки'], "%Y-%m-%dT%H:%M:%S")
                ekspedo.konservejo_ricevado_tempo_de=datetime.strptime(kwargs['СКЛ_ВремяПриемкиС'], "%Y-%m-%dT%H:%M:%S")
                ekspedo.konservejo_ricevado_tempo_en=datetime.strptime(kwargs['СКЛ_ВремяПриемкиПо'], "%Y-%m-%dT%H:%M:%S")
                ekspedo.konservejo_fordono_dato=datetime.strptime(kwargs['СКЛ_ДатаПередачи'], "%Y-%m-%dT%H:%M:%S")
                ekspedo.konservejo_fordono_tempo_de=datetime.strptime(kwargs['СКЛ_ВремяПередачиС'], "%Y-%m-%dT%H:%M:%S")
                ekspedo.konservejo_fordono_tempo_en=datetime.strptime(kwargs['СКЛ_ВремяПередачиПо'], "%Y-%m-%dT%H:%M:%S")
                ekspedo.asekurado=kwargs['ТребуетсяСтрахование']
                ekspedo.horoj_sxargxistoj=kwargs['КоличествоЧасовГрузчики']
                ekspedo.kvanto_sxargxistoj=kwargs['КоличествоГрузчиков']
                ekspedo.kvanto_pecoj=kwargs['КоличествоМест']
                ekspedo.sxargxisto_rigilaro=kwargs['ТребуютсяГрузчикиТакелажники']
                ekspedo.en_peza_kategorio=kwargs['Тяжеловес']
                ekspedo.trajno_key=trajno_key
                ekspedo.kondukisto_key=kondukisto_key
                ekspedo.konservejo_temperatura_reghimo_1=kwargs['скл_ТемпературныйРежим1']
                ekspedo.konservejo_temperatura_reghimo_2=kwargs['скл_ТемпературныйРежим2']
                ekspedo.konservejo_temperatura_reghimo_3=kwargs['скл_ТемпературныйРежим3']
                ekspedo.konservejo_temperatura_reghimo_4=kwargs['скл_ТемпературныйРежим4']
                ekspedo.konservejo_temperatura_reghimo_alia=kwargs['скл_ТемпературныйРежимИной']
                ekspedo.konservejo_temperatura_reghimo_5=kwargs['скл_ТемпературныйРежим5']
                ekspedo.konservejo_temperatura_reghimo_6=kwargs['скл_ТемпературныйРежим6']
                ekspedo.liveranto=liveranto
                ekspedo.liveranto_kontrakto=liveranto_kontrakto
                ekspedo.kombatanto_kontrakto=kombatanto_kontrakto
                ekspedo.trajno_venigo_dato=datetime.strptime(kwargs['рл_ДатаПодачиТранспорт'], "%Y-%m-%dT%H:%M:%S")
                ekspedo.trajno_venigo_tempo=datetime.strptime(kwargs['рл_ВремяПодачиТранспорт'], "%Y-%m-%dT%H:%M:%S")
                ekspedo.trajno_finigho_dato=datetime.strptime(kwargs['рл_ДатаОкончанияТранспорт'], "%Y-%m-%dT%H:%M:%S")
                ekspedo.trajno_finigho_tempo=datetime.strptime(kwargs['рл_ВремяОкончанияТранспорт'], "%Y-%m-%dT%H:%M:%S")
                ekspedo.trajno_konservejo_gk_key=trajno_konservejo_gk_key
                ekspedo.kondukisto_konservejo_gk_key=kondukisto_konservejo_gk_key
                ekspedo.sensoro_temperatura_2=kwargs['ДатчикТемпературы2']
                ekspedo.sensoro_temperatura_3=kwargs['ДатчикТемпературы3']
                ekspedo.sensoro_temperatura_4=kwargs['ДатчикТемпературы4']
                ekspedo.sensoro_temperatura_5=kwargs['ДатчикТемпературы5']
                ekspedo.sensoro_temperatura_alia=kwargs['ДатчикТемпературыИной']
                ekspedo.Livero_tp_2=kwargs['ПредоставлениеТП2']
                ekspedo.Livero_tp_3=kwargs['ПредоставлениеТП3']
                ekspedo.Livero_tp_4=kwargs['ПредоставлениеТП4']
                ekspedo.Livero_tp_5=kwargs['ПредоставлениеТП5']
                ekspedo.Livero_tp_alia=kwargs['ПредоставлениеТПИной']
                ekspedo.plenumado_dato=kwargs['ДатаНаИсполнении']
                ekspedo.fiksa_termoskribi=kwargs['рл_СтационарныйТермописец']
                ekspedo.fiksa_termoskribi_2=kwargs['рл_СтационарныйТермописец2']
                ekspedo.fiksa_termoskribi_3=kwargs['рл_СтационарныйТермописец3']
                ekspedo.fiksa_termoskribi_4=kwargs['рл_СтационарныйТермописец4']
                ekspedo.fiksa_termoskribi_5=kwargs['рл_СтационарныйТермописец5']
                ekspedo.fiksa_termoskribi_alia=kwargs['рл_СтационарныйТермописецИной']
                ekspedo.sekcio_kliento_key=sekcio_kliento_key
                ekspedo.sxargxistoj_sxargxado=kwargs['рл_ГрузчикиПогрузка']
                ekspedo.sxargxistoj_elsxargxado=kwargs['рл_ГрузчикиВыгрузка']

                if adreso_elsxargxado:
                    set_enhavo(ekspedo.adreso_elsxargxado, adreso_elsxargxado, 'ru_RU')
                if adreso_kargado:
                    set_enhavo(ekspedo.adreso_kargado, adreso_kargado, 'ru_RU')
                if kwargs['Комментарий']:
                    set_enhavo(ekspedo.komento, kwargs['Комментарий'], 'ru_RU')
                if kwargs['КомментарийАдресаВыгрузки']:
                    set_enhavo(ekspedo.komento_adreso_elsxargxado, kwargs['КомментарийАдресаВыгрузки'], 'ru_RU')
                if kwargs['КомментарийАдресаЗагрузки']:
                    set_enhavo(ekspedo.komento_adreso_kargado, kwargs['КомментарийАдресаЗагрузки'], 'ru_RU')
                if kwargs['КомментарийОтмены']:
                    set_enhavo(ekspedo.komento_nuligo, kwargs['КомментарийОтмены'], 'ru_RU')
                if kwargs['КонтактноеЛицоПоАдресу']:
                    set_enhavo(ekspedo.kombatanto_adresoj, kwargs['КонтактноеЛицоПоАдресу'], 'ru_RU')
                if kwargs['КонтактноеЛицоПоАдресуРазгрузки']:
                    set_enhavo(ekspedo.kombatanto_adresoj_elsxargxado, kwargs['КонтактноеЛицоПоАдресуРазгрузки'], 'ru_RU')
                if kwargs['ОписаниеГруза']:
                    set_enhavo(ekspedo.priskribo_kargo, kwargs['ОписаниеГруза'], 'ru_RU')
                if kwargs['ОрганизацияПоАдресу']:
                    set_enhavo(ekspedo.organizo_adreso, kwargs['ОрганизацияПоАдресу'], 'ru_RU')
                if kwargs['ОрганизацияПоАдресуРазгрузки']:
                    set_enhavo(ekspedo.organizo_adreso_elsxargxado, kwargs['ОрганизацияПоАдресуРазгрузки'], 'ru_RU')
                if kwargs['ТелефонЗаказчика']:
                    set_enhavo(ekspedo.telefono_kliento, kwargs['ТелефонЗаказчика'], 'ru_RU')
                if kwargs['ТелефонПоАдресу']:
                    set_enhavo(ekspedo.telefono_adreso, kwargs['ТелефонПоАдресу'], 'ru_RU')
                if kwargs['ТелефонПоАдресуРазгрузки']:
                    set_enhavo(ekspedo.telefono_adreso_elsxargxado, kwargs['ТелефонПоАдресуРазгрузки'], 'ru_RU')
                if kwargs['ДопУсловия']:
                    set_enhavo(ekspedo.aldona_kondicxaro, kwargs['ДопУсловия'], 'ru_RU')
                if kwargs['СтавкаНДСЗаказчик']:
                    set_enhavo(ekspedo.impostokvoto_kliento, kwargs['СтавкаНДСЗаказчик'], 'ru_RU')
                if kwargs['СтавкаНДСПеревозчик']:
                    set_enhavo(ekspedo.impostokvoto_transportisto, kwargs['СтавкаНДСПеревозчик'], 'ru_RU')
                if kwargs['КомментарийРасчета']:
                    set_enhavo(ekspedo.priskribo_kalkulado, kwargs['КомментарийРасчета'], 'ru_RU')
                if kwargs['НомерНакладнойСК']:
                    set_enhavo(ekspedo.numero_frajtoletero, kwargs['НомерНакладнойСК'], 'ru_RU')
                if kwargs['ОписаниеОпасногоГруза']:
                    set_enhavo(ekspedo.priskribo_dangxera_kargo, kwargs['ОписаниеОпасногоГруза'], 'ru_RU')
                if kwargs['КлассОпасности']:
                    set_enhavo(ekspedo.klaso_dangxera, kwargs['КлассОпасности'], 'ru_RU')
                if kwargs['ДопТребованияХЦ']:
                    set_enhavo(ekspedo.aldone_postulo_hc, kwargs['ДопТребованияХЦ'], 'ru_RU')
                if kwargs['ИныеУслуги']:
                    set_enhavo(ekspedo.aliaj_servo, kwargs['ИныеУслуги'], 'ru_RU')
                if kwargs['ПередачаГрузаФИО']:
                    set_enhavo(ekspedo.fordono_kargo_fio, kwargs['ПередачаГрузаФИО'], 'ru_RU')
                if kwargs['НаименованиеГруза']:
                    set_enhavo(ekspedo.nomo_kargo, kwargs['НаименованиеГруза'], 'ru_RU')
                if kwargs['скл_Задание']:
                    set_enhavo(ekspedo.konservejo_tasko, kwargs['скл_Задание'], 'ru_RU')
                if kwargs['УИД']:
                    ekspedo.uid = kwargs['УИД']
                if kwargs['скл_Комментарий']:
                    set_enhavo(ekspedo.konservejo_komento, kwargs['скл_Комментарий'], 'ru_RU')
                if kwargs['скл_КомментарийКлиента']:
                    set_enhavo(ekspedo.priskribo_kliento, kwargs['скл_КомментарийКлиента'], 'ru_RU')
                if kwargs['НомерНакладнойКлиента']:
                    set_enhavo(ekspedo.numero_frajtoletero_kliento, kwargs['НомерНакладнойКлиента'], 'ru_RU')
                if kwargs['EmailОтправителя']:
                    set_enhavo(ekspedo.email_ekspedinto, kwargs['EmailОтправителя'], 'ru_RU')
                if kwargs['EmailПолучателя']:
                    set_enhavo(ekspedo.email_ricevanto, kwargs['EmailПолучателя'], 'ru_RU')
                if kwargs['рл_ТипЗаявки']:
                    set_enhavo(ekspedo.tipo_mendo, kwargs['рл_ТипЗаявки'], 'ru_RU')
                if urboj_recivado_nomo:
                    set_enhavo(ekspedo.urboj_recivado_nomo, urboj_recivado_nomo, 'ru_RU')
                if urboj_transdono_nomo:
                    set_enhavo(ekspedo.urboj_transdono_nomo, urboj_transdono_nomo, 'ru_RU')
                print('=== сохраняем данные экспедирования ====')
                dokumento_ekspedo_save(ekspedo)
                print('=== сохранили данные экспедирования ====')

                # нужно проверить совпадение списка грузов
                ekspedo_kargoj = DokumentoEkspedoKargo.objects.filter(
                    ekspedo = ekspedo,
                    forigo=False,
                    arkivo=False,
                    publikigo=True,
                ).delete()
                ekspedo_kargoj = DokumentoEkspedoKargo.objects.filter(
                    ekspedo = ekspedo,
                    forigo=False,
                    arkivo=False,
                    publikigo=True,
                )
                print('=== список грузов в базе после удаления: ', ekspedo_kargoj)
                print('=== список грузов в 1c: ', kwargs['ГрузыММП'])
                # т.к. сложно идентифицировать каждый отдельный груз, то удаляю грузы в базе и импортирую новые из 1с
                     
                errors, ekspedo_kargo = ImportoDokumentoEkspedo1c.aldoni_kargo(ekspedo, publikiga_dato, errors, kwargs)
                ekspedo_kargoj = DokumentoEkspedoKargo.objects.filter(
                    ekspedo = ekspedo,
                    forigo=False,
                    arkivo=False,
                    publikigo=True,
                )
                print('=== список грузов в базе после добавления: ', ekspedo_kargoj)

                ekspedo_kargoj = DokumentoEkspedoKonservejoKargo.objects.filter(
                    ekspedo = ekspedo,
                    forigo=False,
                    arkivo=False,
                    publikigo=True,
                ).delete()
                # print('=== список грузов на складе в базе: ', ekspedo_kargoj)
                print('=== список грузов на складе в 1c: ', kwargs['скл_Грузы'])
                errors, ekspedo_konservejo_kargo_tabelo = ImportoDokumentoEkspedo1c.aldoni_konservejo_kargo(ekspedo, publikiga_dato, errors, kwargs)
                ekspedo_kargoj = DokumentoEkspedoKonservejoKargoKondukisto.objects.filter(
                    ekspedo = ekspedo,
                    forigo=False,
                    arkivo=False,
                    publikigo=True,
                ).delete()
                errors, ekspedo_konservejo_kargo_kondukisto_tabelo = ImportoDokumentoEkspedo1c.aldoni_konservejo_kargo_kondukisto(ekspedo, publikiga_dato, errors, kwargs)

                # Маршрут импортируется в задачи
                # ИдМаршрута на 18.11.2021 в 1с всегда передавать = 1
                # удаляем старый проект и задачи c их владельцами
                taskoj_projektoj_posedanto = TaskojProjektoPosedanto.objects.filter(
                    posedanto_dokumento = ekspedo.dokumento, 
                    forigo=False,
                    arkivo=False,
                    publikigo=True,
                )
                print('=== taskoj_projektoj_posedanto = ', taskoj_projektoj_posedanto)
                taskoj_projektoj_posedanto_uuid = set(taskoj_projektoj_posedanto.values_list('projekto__uuid', flat=True))
                # print('=== taskoj_projektoj_posedanto_uuid = ', taskoj_projektoj_posedanto_uuid)
                # set_taskoj_projektoj_posedanto_uuid = set(taskoj_projektoj_posedanto_uuid)
                print('=== taskoj_projektoj_posedanto_uuid set = ', taskoj_projektoj_posedanto_uuid)
                taskoj_projektoj = TaskojProjekto.objects.filter(
                    uuid__in = taskoj_projektoj_posedanto_uuid,
                    forigo=False,
                    arkivo=False,
                    publikigo=True,
                )
                print('=== taskoj_projektoj = ', taskoj_projektoj)
                taskoj_tasko = TaskojTasko.objects.filter(
                    projekto__in = taskoj_projektoj, 
                    forigo=False,
                    arkivo=False,
                    publikigo=True,
                )
                print('=== taskoj_tasko = ', taskoj_tasko)
                taskoj_tasko_posedanto = TaskojTaskoPosedanto.objects.filter(
                    tasko__in = taskoj_tasko, 
                    forigo=False,
                    arkivo=False,
                    publikigo=True,
                )
                print('=== taskoj_tasko_posedanto = ', taskoj_tasko_posedanto)
                taskoj_tasko_posedanto.delete()
                taskoj_tasko.delete()
                taskoj_projektoj_posedanto.delete()
                taskoj_projektoj.delete()

                    #  
                if uzanto:
                    print('=== uzanto = 2 = ', uzanto)
                print('= kwargs == uzanto = 2 = ', kwargs.get('uzanto', None))
                errors, projekto, tasko = ImportoDokumentoEkspedo1c.aldoni_projekto_taskoj(
                    root, info, ekspedo, publikiga_dato, errors, kwargs, dokumento, organizo, kliento)
                print('=== projekto = ', projekto)
                print('=== tasko = ', tasko)
                speco_dokumento = None
                if not len(errors):
                    print('=== ошибок нет 1 ===')
                    # если нет ошибок, то меняем "Вид документа" с "На синхронизации с 1с" на "Основной"
                    try:
                        speco_dokumento = DokumentoSpeco.objects.get(id=1,
                                                    forigo=False, arkivo=False, publikigo=True)
                        
                    except DokumentoSpeco.DoesNotExist:
                        errors.append(ErrorNode(
                            field='4 - Заявка на грузоперевозки',
                            message=_('Неверный вид документа')
                        ))
                if not len(errors):
                    print('=== ошибок нет 2 ===')
                    dokumento.speco = speco_dokumento
                    dokumento.save()
                    print('=== сохранили новый документ с видом === ', speco_dokumento)
                
            else:
                message = _('Nevalida argumentvaloroj')
                for error in errors:
                    message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                        error.message, _('в поле'), error.field)
        else: # здесь нужно добавить документ экспедирования
            print('====== не найден документ экспедирования ======== ')
        return status, message, errors, ekspedo, kvanto

    @staticmethod
    def konekti_url(url):
        """konekti - соединиться

        Args:
            url (_string_): url для соединения
        """
        print('=== konekti_url ===')
        # url = kwargs.get('url', False)
        if not url:
            # url = settings.ODATA_URL+"Document_бит_ЗаявкаЭкспедированияММП?$top=100&$skip=10&$format=application/json&$filter=Date gt datetime'2021-09-01T00:00:00'&$inlinecount=allpages"
            # с 1 сентября 10 единиц
            # url = settings.ODATA_URL+"Document_бит_ЗаявкаЭкспедированияММП?$format=application/json&$filter=Date gt datetime'2021-09-01T00:00:00'&$top=10"
            # за период
            # url = settings.ODATA_URL+"Document_бит_ЗаявкаЭкспедированияММП?$format=application/json&$filter=Date gt datetime'2021-01-15T00:00:00'&$filter=Date lt datetime'2021-01-16T00:00:00'&$top=10"
            # с 1 сентября
            # url = settings.ODATA_URL+"Document_бит_ЗаявкаЭкспедированияММП?$format=application/json&$filter=Date gt datetime'2021-09-01T00:00:00'&$inlinecount=allpages"
            # c начала года 
            url = settings.ODATA_URL+"Document_бит_ЗаявкаЭкспедированияММП?$format=application/json&$filter=Date gt datetime'2021-01-01T00:00:00'&$inlinecount=allpages"
            
        login = settings.ODATA_USER
        password = settings.ODATA_PASSWORD
        print('=== pre requests  ===')
        r = requests.get(url,auth=(login, password), verify=False)
        jsj = r.json()
        print('=== konekti_url jsj[value] === ', jsj['value'])
        
        return jsj['value']

    @staticmethod
    def redakti(root, info, uzanto, jsj, **kwargs):
        """редактирование или добавление данных в БД при помощи импортирования из 1с
        Данная функция вызывается после проверки прав пользователя.
        Создана для периодического вызова для синхронизации переданных, но не принятых в 1с данных.

        Args:
            uzanto (_uzanto_): пользователь из справочника пользователей
        """
        status = False
        message = None
        errors = list()
        ekspedo = None
        
        informlibro = {}
        if uzanto:
            informlibro = {'uzanto':uzanto,}
            informlibro['autoro'] = uzanto
            kwargs['uzanto'] = uzanto
            kwargs['autoro'] = uzanto

        with transaction.atomic():
            kvanto = 0
            if not len(errors):
                if 'realeco' not in kwargs:
                    realeco_id = [1,]
                elif not len(kwargs.get('realeco')):
                    errors.append(ErrorNode(
                        field='realeco',
                        message=_('Необходимо указать хотя бы одно значение')
                    ))
                else:
                    realeco_id = set(kwargs.get('realeco'))
            if not len(errors):
                if len(realeco_id):
                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))
                    informlibro['realeco'] = realeco
                    if len(dif):
                        errors.append(ErrorNode(
                            field=str(dif),
                            message=_('Указаны несуществующие реальности')
                        ))

            if not len(errors):
                if 'kategorio' not in kwargs:
                    errors.append(ErrorNode(
                        field='kategorio',
                        message=_('Поле обязательно для заполнения')
                    ))
                elif not len(kwargs.get('kategorio')):
                    errors.append(ErrorNode(
                        field='kategorio',
                        message=_('Необходимо указать хотя бы одно значение')
                    ))
                else:
                    kategorio_id = set(kwargs.get('kategorio'))
                    if len(kategorio_id):
                        kategorio = DokumentoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))
                        informlibro['kategorio'] = kategorio

                        if len(dif):
                            errors.append(ErrorNode(
                                field=str(dif),
                                message=_('Указаны несуществующие категория документов')
                            ))

            if not len(errors):
                if 'tipo_dokumento_id' in kwargs:
                    try:
                        tipo = DokumentoTipo.objects.get(id=kwargs.get('tipo_dokumento_id'), forigo=False,
                                                            arkivo=False, publikigo=True)
                        informlibro['tipo_dokumento'] = tipo
                    except DokumentoTipo.DoesNotExist:
                        errors.append(ErrorNode(
                            field='tipo_dokumento_id',
                            message=_('Неверный тип документов')
                        ))
                else:
                    errors.append(ErrorNode(
                        field='tipo_dokumento_id',
                        message=_('Не указан тип документа для импорта данных')
                    ))

            # проверяем обязательные параметры
            errors, kwargs = ImportoDokumentoEkspedo1c.argumentoj_kreanta(root, info, **kwargs)
            if not len(errors):
                # проверяем наличие записей с таким кодом
                errors, informlibro = ImportoDokumentoEkspedo1c.kontrolanta_argumentoj(root, info, **kwargs)

            for js in jsj:
                if len(errors):
                    print('=== есть ошибки, выходим из цикла ===')
                    break
                uuid = js['Ref_Key']
                uid = js['УИД']
                try:
                    ekspedo = DokumentoEkspedo.objects.get(uuid=uuid, forigo=False,
                                                        arkivo=False, publikigo=True)
                except DokumentoEkspedo.DoesNotExist:
                    ekspedo = None
                    if uid:
                        try:
                            ekspedo = DokumentoEkspedo.objects.get(uid=uid, forigo=False,
                                                            arkivo=False, publikigo=True)
                        except DokumentoEkspedo.DoesNotExist:
                            ekspedo = None

                if ekspedo:
                    print('=== редактируем ===')
                    #
                    status, messagej, errors, ekspedo, kvanto = ImportoDokumentoEkspedo1c.redakti_uid(root, info, kvanto, ekspedo.uuid, ekspedo, informlibro, **js)
                else:
                    print('=== добавляем ===')
                    status, messagej, errors, ekspedo, kvanto = ImportoDokumentoEkspedo1c.aldoni(root, info, kvanto, informlibro, js)

            if len(errors):
                for error in errors:
                    message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                        error.message, _('в поле'), error.field)
            elif not message:
                status = True
                errors = list()
                if kvanto > 0:
                    message =  '{} {} {}'.format(_('Добавлено/изменено'), kvanto, _('записей'))
                else:
                    message =  _('Не найдено новых объектов для добавления/изменения')
        return status, message, errors, ekspedo

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        ekspedo = None
        uzanto = info.context.user

        if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_ekspedo'):
            jsj = ImportoDokumentoEkspedo1c.konekti_url(False)
            status, messagej, errors, ekspedo = ImportoDokumentoEkspedo1c.redakti(root, info, uzanto, jsj, **kwargs)
        else:
            message = _('Недостаточно прав')

        return status, message, errors, ekspedo

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        ekspedo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            # Импортируем данные
            status, message, errors, ekspedo = ImportoDokumentoEkspedo1c.create(root, info, **kwargs)
        else:
            message = _('Требуется авторизация')

        return ImportoDokumentoEkspedo1c(status=status, message=message, errors=errors, ekspedo=ekspedo)


class DokumentoMutation1c(graphene.ObjectType):
    importo_dokumento_ekspedo1c = ImportoDokumentoEkspedo1c.Field(
        description=_('''Импорт документов экспедирования из 1с''')
    )
