"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class DokumentojConfig(AppConfig):
    # Название модуля
    name = 'dokumentoj'
    # Визуальное название, будет отображаться в админке, потому делаем переводимым
    verbose_name = _('Dokumentoj')

    # Метод срабатывает при регистрации приложения в системе django
    def ready(self):
        # Импортом сообщаем django, что надо проанализировать код в модуле tasks
        # При этом происходит регистрация функций заданий celery
        import dokumentoj.tasks

