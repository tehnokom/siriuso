"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from typing_extensions import Self
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from celery import shared_task, Celery
from celery.schedules import crontab
from django_celery_beat.models import PeriodicTask, IntervalSchedule
import datetime
from django.utils import timezone
from django.core.cache import cache
from django.db import models

from .models import DokumentoEkspedo, DokumentoEkspedoKargo, dokumento_Speco_1c
from siriuso.utils import get_enhavo
from .api.mutation1c import AldoniDokumentoEkspedo1c, ImportoDokumentoEkspedo1c
from main.models import Uzanto
from taskoj.models import TaskojProjekto, TaskojTasko

app = Celery()

def krei_tasko(nomo, nomo_tasko):
    try:
        task = PeriodicTask.objects.get(name=nomo)
        if not task.enabled:
            task.enabled = True
            task.save()
    except PeriodicTask.DoesNotExist:
        interval, krei_interval = IntervalSchedule.objects.get_or_create(every=5, period='minutes')
        PeriodicTask.objects.create(
            name=nomo,
            task=nomo_tasko,
            # interval=IntervalSchedule.objects.get(every=10, period='seconds'),
            interval = interval,
            #   args=json.dumps([options['order_id'][0]]),
            start_time=timezone.now(),
            )

def dokumento_ekspedo_al_1c(uzanto, dokumento_ekspedo): # al (эсперанто) - to (английский) - к (русский)
    """отправка документа экспедирования в 1с

    Args:
        uzanto (_type_): ссылка на пользователя. Если нет - передаётся None
        dokumento_ekspedo (_type_): передаётся сам документ экспедирования
    """
    print('=== dokumento_ekspedo_al_1c === ')
    ekspedo = {}
    # for eks in dokumento_ekspedo_values[0]:
    #     if (isinstance(dokumento_ekspedo_values[0][eks], dict)):
    #         # print('== nomo = ', eks, ' ==== eks === ',dokumento_ekspedo[0][eks]['enhavo'],' === type eks === ', type(dokumento_ekspedo[0][eks]))
    #         ekspedo[eks] = get_enhavo(dokumento_ekspedo_values[0][eks], empty_values=True)[0]
    #     else:
    #         ekspedo[eks] = dokumento_ekspedo_values[0][eks]
    print('=== dokumento_ekspedo === ', dokumento_ekspedo._meta.get_fields(False))
    for eks in dokumento_ekspedo._meta.get_fields(False):
        # if not eks.is_relation:
            # print('=== eks === ', eks)
            # print('=== eks name === ', eks.name)
            # print('=== eks.db_type === ', eks.db_type)
        # print('======= поле  = ', eks.name)
        # print('======= содержания поля = ', getattr(dokumento_ekspedo, eks.name))
        if isinstance(eks, models.JSONField):
            ekspedo[eks.name] = get_enhavo(getattr(dokumento_ekspedo, eks.name), empty_values=True)[0]
        else:
            ekspedo[eks.name] = getattr(dokumento_ekspedo, eks.name)
            
        # if (isinstance(eks, dict)):
        #     print('=== eks dict ===')
        #     # print('== nomo = ', eks, ' ==== eks === ',dokumento_ekspedo[0][eks]['enhavo'],' === type eks === ', type(dokumento_ekspedo[0][eks]))
        #     ekspedo[eks] = get_enhavo(eks, empty_values=True)[0]
        # else:
        #     ekspedo[eks] = eks
    # добавляем клиента и организацию
    dokumento = dokumento_ekspedo.dokumento
    posedantoj_dokumento = dokumento.dokumentoj_dokumentoposedanto_dokumento.all()
    ekspedo['kliento_uuid'] = str(dokumento_ekspedo.kliento.uuid)
    # print('=== kliento_uuid = ', ekspedo['kliento_uuid'])
    ekspedo['kliento'] = dokumento_ekspedo.kliento
    ekspedo['valuto_kliento'] = dokumento_ekspedo.valuto_kliento
    ekspedo['kombatanto_kliento_uuid'] = str(dokumento_ekspedo.kombatanto_kliento.uuid)
    ekspedo['kombatanto_kliento'] = dokumento_ekspedo.kombatanto_kliento
    ekspedo['organizo_uuid'] = str(dokumento_ekspedo.organizo.uuid)
    ekspedo['organizo'] = dokumento_ekspedo.organizo
    # различение клиента и исполнителя?
    
    print('=== ekspedo === ', ekspedo)
    # формируем параметры маршрута
    # открываем проект
    print('=== dokumento_ekspedo === ', dokumento_ekspedo)
    print('=== dokumento_ekspedo.dokumento === ', dokumento_ekspedo.dokumento)
    projekto = TaskojProjekto.objects.filter(taskojprojektoposedanto__posedanto_dokumento__uuid=dokumento_ekspedo.dokumento.uuid,
    # projekto = TaskojProjekto.objects.filter(universo_taskoj_projekto_posedanto__posedanto_dokumento__uuid=dokumento_ekspedo.dokumento.uuid,
                                                    forigo=False, arkivo=False, 
                                                    publikigo=True)
    if len(projekto)>0:# если проекта есть, то берём первый
        ekspedo['projekto_statuso'] = projekto[0].statuso
    # добавляем задачи (маршрут)
    print('===== открываем задачу =======')
    taskoj = TaskojTasko.objects.filter(taskojtaskoposedanto__posedanto_dokumento__uuid=dokumento_ekspedo.dokumento.uuid,
                                                    forigo=False, arkivo=False, 
                                                    publikigo=True)
    if len(taskoj)>0:
        ekspedo['tasko_numero'] = []
        ekspedo['tasko_kom_dato'] = []
        ekspedo['tasko_fin_dato'] = []
        ekspedo['tasko_kom_adreso'] = []
        ekspedo['tasko_fin_adreso'] = []
        ekspedo['tasko_statuso'] = []
        ekspedo['tasko_nomo'] = []
        ekspedo['tasko_priskribo'] = []
        for tasko in taskoj:
            ekspedo['tasko_numero'].append(tasko.pozicio)
            ekspedo['tasko_kom_dato'].append(tasko.kom_dato)
            ekspedo['tasko_fin_dato'].append(tasko.fin_dato)
            ekspedo['tasko_kom_adreso'].append(get_enhavo(tasko.kom_adreso, empty_values=True)[0])
            ekspedo['tasko_fin_adreso'].append(get_enhavo(tasko.fin_adreso, empty_values=True)[0])
            ekspedo['tasko_statuso'].append(tasko.statuso)
            ekspedo['tasko_nomo'].append(get_enhavo(tasko.nomo, empty_values=True)[0])
            ekspedo['tasko_priskribo'].append(get_enhavo(tasko.priskribo, empty_values=True)[0])

    # параметры перевозимого груза
    kargoj = DokumentoEkspedoKargo.objects.filter(ekspedo__uuid=dokumento_ekspedo.uuid,
                                                    forigo=False, arkivo=False, 
                                                    publikigo=True)
    print('====== kargoj ==== ', kargoj)
    if len(kargoj)>0:
        ekspedo['kargo_numero'] = []
        ekspedo['kargo_kvanto_pecoj'] = []
        ekspedo['kargo_longo'] = []
        ekspedo['kargo_largho'] = []
        ekspedo['kargo_alto'] = []
        ekspedo['kargo_pezo_fakta'] = []
        ekspedo['kargo_volumeno'] = []
        ekspedo['kargo_pezo_volumena'] = []
        ekspedo['kargo_tipo_pakumoj'] = []
        ekspedo['kargo_indikatoro'] = []
        ekspedo['kargo_temperatura_reghimo'] = []
        ekspedo['kargo_grave'] = []
        ekspedo['kargo_retropasxo_pakumo'] = []
        ekspedo['kargo_retropasxo_indikatoro'] = []
        for kargo in kargoj:
            ekspedo['kargo_numero'].append(kargo.numero)
            ekspedo['kargo_kvanto_pecoj'].append(kargo.kvanto_pecoj)
            ekspedo['kargo_longo'].append(kargo.longo)
            ekspedo['kargo_largho'].append(kargo.largho)
            ekspedo['kargo_alto'].append(kargo.alto)
            ekspedo['kargo_pezo_fakta'].append(kargo.pezo_fakta)
            ekspedo['kargo_volumeno'].append(kargo.volumeno)
            ekspedo['kargo_pezo_volumena'].append(kargo.pezo_volumena)
            ekspedo['kargo_tipo_pakumoj'].append(kargo.tipo_pakumoj)
            ekspedo['kargo_indikatoro'].append(kargo.indikatoro)
            ekspedo['kargo_temperatura_reghimo'].append(kargo.temperatura_reghimo)
            ekspedo['kargo_grave'].append(kargo.grave)
            ekspedo['kargo_retropasxo_pakumo'].append(kargo.retropasxo_pakumo)
            ekspedo['kargo_retropasxo_indikatoro'].append(kargo.retropasxo_indikatoro)

    status, message, errors, aldonij = AldoniDokumentoEkspedo1c.krei(**ekspedo)
    if status:
        # изменяем данные в базе на новые
        print('========= изменяем данные в базе на новые ===========')
        kvanto = 0
        root = None
        aldonij['uzanto'] = uzanto
        print('=== aldonij[uzanto] = ', aldonij['uzanto'])
        info = None
        status, message, errors, ekspedo, kvanto = ImportoDokumentoEkspedo1c.redakti_uid(root, info, kvanto, None, dokumento_ekspedo, None, **aldonij)
    else: # если ответ отрицательный - обрыв связи, то запускаем периодическую проверку
        krei_tasko('dokumento_ekspedo_al_1c_periode', 'dokumentoj.tasks.dokumento_ekspedo_al_1c_periode')
    return status

@shared_task
def task_dokumento_ekspedo_al_1c(uzanto_id, dokumento_ekspedo_uuid): # al (эсперанто) - to (английский) - к (русский)
    """Отправка документа экспедирования в 1с

    Args:
        uzanto_id : id пользователя, создавшего задачу
        dokumento_ekspedo_uuid (_UUID_): uuid Документа экспедирования DokumentoEkspedo

    Returns:
        _type_: _description_
    """
    # формируем параметры документа экспедирования
    dokumento_ekspedo = DokumentoEkspedo.objects.filter(uuid=dokumento_ekspedo_uuid, forigo=False,
                                            arkivo=False, publikigo=True)
    dokumento_ekspedo_values = dokumento_ekspedo.values()
    print(' == dokumento_ekspedo_values == ', dokumento_ekspedo_values)
    # dokumento_ekspedo_values = DokumentoEkspedo.objects.filter(uuid=dokumento_ekspedo_uuid, forigo=False,
    #                                         arkivo=False, publikigo=True).values()
    # for eks in dokumento_ekspedo_values[0]:
    #     if (isinstance(dokumento_ekspedo_values[0][eks], dict)):
    #         # print('== nomo = ', eks, ' ==== eks === ',dokumento_ekspedo[0][eks]['enhavo'],' === type eks === ', type(dokumento_ekspedo[0][eks]))
    #         ekspedo[eks] = get_enhavo(dokumento_ekspedo_values[0][eks], empty_values=True)[0]
    #     else:
    #         ekspedo[eks] = dokumento_ekspedo_values[0][eks]
    uzanto = None
    print('========= проверка на число =========== ')
    if isinstance(uzanto_id, int):
        uzanto = Uzanto.objects.get(id=uzanto_id)
    else:
        print('===== не число ========')
        return
   
    if not len(dokumento_ekspedo_values):
        # print('=== документа экспедирования нет === ', dokumento_ekspedo_uuid)
        # запускаем проверку, т.к. отсутствие документа экспедирования возможно ошибочно
        krei_tasko('dokumento_ekspedo_al_1c_periode', 'dokumentoj.tasks.dokumento_ekspedo_al_1c_periode')
        return
    dokumento_ekspedo_al_1c(uzanto,dokumento_ekspedo[0])

    return 0


# @periodic_task(run_every=timedelta(minutes=1))
# def update_keep_alive(self):
#     print('тестирование')
    # print("running keep alive task")
    # statsd.incr(statsd_tags.CELERY_BEAT_ALIVE)


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # Calls test('hello') every 10 seconds.
    sender.add_periodic_task(10.0, test.s('hello'), name='add every 10')

#     # Calls test('world') every 30 seconds
#     sender.add_periodic_task(30.0, test.s('world'), expires=10)

#     # Executes every Monday morning at 7:30 a.m.
#     sender.add_periodic_task(
#         crontab(hour=8, minute=30, day_of_week=2),
#         test.s('Happy Mondays!'),
#     )

@app.task
def test(arg):
    print(arg)

# @app.task
# def add(x, y):
#     z = x + y
#     print(z)


@shared_task(name="repeat_order_make")
def repeat_order_make():
#   order = Order.objects.get(pk=order_id)
#   if order.status != '0':
#     print('Статус получен!')
    # task = PeriodicTask.objects.get(name='Repeat order {}'.format(order_id))
#     task.enabled = False
#     task.save()
#   else:
    # Необходимая логика при повторной отправке заказа
    print('Я должна повторно оформлять заказ каждые 10 секунд')
    

# LOCK_EXPIRE = 60 * 30
LOCK_EXPIRE = 60 * 1

@shared_task
def dokumento_ekspedo_al_1c_periode(): # periode - периоди́чески
    status = False
    # Проверяем есть ли кэш
    is_running = cache.get("dokumento_ekspedo_al_1c_periode")

    # Если кэш есть, значит предыдущая функция не закончена, выйдем из функции
    if is_running:
        print('=== НАС НЕ ПУСТИЛО - фунцикля ещё работает = ', is_running)
        return

    # Ставим кэш.
    cache.set('dokumento_ekspedo_al_1c_periode', '1', timeout=LOCK_EXPIRE)
    # i = 0
    # while i<1000000000:
    #     i += 1

    print('Проверяем данные на 1с и отправляем не отправленные = ')

    # выбираем документы экспедирования, на которые ещё не получен ответ из 1с
    dokumentoj_ekspedo = DokumentoEkspedo.objects.filter(dokumento__speco__id=dokumento_Speco_1c, forigo=False, arkivo=False, publikigo=True)
    # dokumentoj_ekspedo = DokumentoEkspedo.objects.filter(dokumento__speco__id__in=dokumento_Speco_1c, forigo=False, arkivo=False, publikigo=True)
    # dokumento_Speco_Id:4
    print('Запросили документы = ', dokumentoj_ekspedo)
    # if len(dokumentoj_ekspedo) > 0:# для теста проверяем только первую запись - потом заменить на for
    #     dokumento_ekspedo = dokumentoj_ekspedo[0]
    statuso = True # если в цикле хоть одна с ошибкой - периодическую задачу не закрываем
    for dokumento_ekspedo in dokumentoj_ekspedo:
        print('=== Документ для сверки: === ', dokumento_ekspedo)
        kvanto = 0
        root = None
        # info.context.user
        # aldonij['uzanto'] = uzanto
        # print('=== aldonij[uzanto] = ', aldonij['uzanto'])
        info = None
        uzanto = None
        if dokumento_ekspedo.uid:
            print('=== УИД === ', dokumento_ekspedo.uid)
            url = settings.ODATA_URL +\
                    "Document_бит_ЗаявкаЭкспедированияММП?$format=application/json&$filter=УИД eq '"+str(dokumento_ekspedo.uid)+"'&$inlinecount=allpages"
            jsj = ImportoDokumentoEkspedo1c.konekti_url(url)
            kwargs = {}
            if not len(jsj):
                # отправляем в 1с
                print('== отправляем в 1с ==')
                # pass
                status = dokumento_ekspedo_al_1c(None, dokumento_ekspedo)
                if not status:
                    statuso = False
                
# не отправляются данные на сервер
            else:
                # получаем из 1с
                print('== получаем из 1с ==')
                status, message, errors, ekspedo = ImportoDokumentoEkspedo1c.redakti(root, info, uzanto, jsj, **kwargs)
                if not status:
                    statuso = False
        else:
            print('=== УИД не найден === ', dokumento_ekspedo)


    # Удалим кэш в конце функции
    cache.delete("dokumento_ekspedo_al_1c_periode")
    
    if statuso:
        # отключаем периодическую задачу
        task = PeriodicTask.objects.get(name='dokumento_ekspedo_al_1c_periode')
        task.enabled = False
        task.save()


