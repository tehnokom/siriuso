"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from django import forms
from django.utils.translation import gettext_lazy as _
import json

from .models import *
from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget
from universo_bazo.models import Realeco
from siriuso.utils.admin_mixins import TekstoMixin


# Форма справочника валют
class MonoValutoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=Realeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = MonoValuto
        fields = [field.name for field in MonoValuto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Справочник валют
@admin.register(MonoValuto)
class MonoValutoAdmin(admin.ModelAdmin, TekstoMixin):
    form = MonoValutoFormo
    list_display = ('nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = MonoValuto


# Форма типов счетов
class MonoKontoTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=Realeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = MonoKontoTipo
        fields = [field.name for field in MonoKontoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы счетов
@admin.register(MonoKontoTipo)
class MonoKontoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = MonoKontoTipoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = MonoKontoTipo


# Форма шаблонов счетов (кошельков)
class MonoKontoSxablonoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = MonoKontoSxablono
        fields = [field.name for field in MonoKontoSxablono._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Шаблоны счетов (кошельков)
@admin.register(MonoKontoSxablono)
class MonoKontoSxablonoAdmin(admin.ModelAdmin, TekstoMixin):
    form = MonoKontoSxablonoFormo
    list_display = ('id','nomo_teksto','tipo','bilanco')
    exclude = ('uuid',)
    class Meta:
        model = MonoKontoSxablono


# Форма счетов (кошельков)
class MonoKontoFormo(forms.ModelForm):
    
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = MonoKonto
        fields = [field.name for field in MonoKonto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Счета (кошельки)
@admin.register(MonoKonto)
class MonoKontoAdmin(admin.ModelAdmin, TekstoMixin):
    form = MonoKontoFormo
    list_display = ('uuid','bilanco','posedanto_uzanto','posedanto_organizo','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema',)
    class Meta:
        model = MonoKonto


# Форма типов инвойсов (счетов на оплату)
class MonoFakturoTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=Realeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = MonoFakturoTipo
        fields = [field.name for field in MonoFakturoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы инвойсов (счетов на оплату)
@admin.register(MonoFakturoTipo)
class MonoFakturoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = MonoFakturoTipoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = MonoFakturoTipo


# Форма инвойсов (счетов на оплату)
class MonoFakturoFormo(forms.ModelForm):
    
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = MonoFakturo
        fields = [field.name for field in MonoFakturo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Инвойсы (счета на оплату)
@admin.register(MonoFakturo)
class MonoFakturoAdmin(admin.ModelAdmin, TekstoMixin):
    form = MonoFakturoFormo
    list_display = ('uuid','ekspedanto','ricevanto','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema',)
    class Meta:
        model = MonoFakturo


# Форма объектов указанных в инвойсах (содержимое табличной части)
class MonoFakturoObjektoFormo(forms.ModelForm):
    
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = MonoFakturoObjekto
        fields = [field.name for field in MonoFakturoObjekto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Объекты указанные в инвойсах (содержимое табличной части)
@admin.register(MonoFakturoObjekto)
class MonoFakturoObjektoAdmin(admin.ModelAdmin, TekstoMixin):
    form = MonoFakturoObjektoFormo
    list_display = ('uuid','fakturo','objekto','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema',)
    class Meta:
        model = MonoFakturoObjekto


# Форма типов транзакций между счетами (кошельками)
class MonoTransakcioTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=Realeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = MonoTransakcioTipo
        fields = [field.name for field in MonoTransakcioTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы транзакций между счетами (кошельками)
@admin.register(MonoTransakcioTipo)
class MonoTransakcioTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = MonoTransakcioTipoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = MonoTransakcioTipo


# Форма транзакций между счетами (кошельками)
class MonoTransakcioFormo(forms.ModelForm):
    
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = MonoTransakcio
        fields = [field.name for field in MonoTransakcio._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Транзакции между счетами (кошельками)
@admin.register(MonoTransakcio)
class MonoTransakcioAdmin(admin.ModelAdmin, TekstoMixin):
    form = MonoTransakcioFormo
    list_display = ('uuid','fakturo','ekspedanto','ricevanto','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema',)
    class Meta:
        model = MonoTransakcio
