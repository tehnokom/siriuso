"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import Permission

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from universo_bazo.models import UniversoBazaMaks, UniversoBazaRealeco, Realeco
from organizoj.models import Organizo
from objektoj.models import Objekto
from main.models import Uzanto


# Справочник валют, использует абстрактный класс UniversoBazaMaks
class MonoValuto(UniversoBazaMaks):

    # уникальный личный ID валют
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='mono_valutoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'mono_valutoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Valuto')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Valutoj')
        # права
        permissions = (
            ('povas_vidi_mono_valutoj', _('Povas vidi valutoj')),
            ('povas_krei_mono_valutoj', _('Povas krei valutoj')),
            ('povas_forigi_mono_valutoj', _('Povas forigi valutoj')),
            ('povas_shangxi_mono_valutoj', _('Povas ŝanĝi valutoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(models.Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(MonoValuto, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('mono',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'mono.povas_vidi_mono_valutoj', 
                'mono.povas_krei_mono_valutoj',
                'mono.povas_forigi_mono_valutoj',
                'mono.povas_shangxi_mono_valutoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='mono', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('mono.povas_vidi_mono_valutoj')
                    or user_obj.has_perm('mono.povas_vidi_mono_valutoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('mono.povas_vidi_mono_valutoj'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond


# Типы счетов, использует абстрактный класс UniversoBazaMaks
class MonoKontoTipo(UniversoBazaMaks):

    # уникальный личный ID типа счетов
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # валюта типа счёта
    valuto = models.ForeignKey(MonoValuto, verbose_name=_('Valuto'), blank=True, null=True,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='mono_kontoj_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'mono_kontoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de monaj kontoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de monaj kontoj')
        # права
        permissions = (
            ('povas_vidi_mono_kontoj_tipoj', _('Povas vidi tipoj de monaj kontoj')),
            ('povas_krei_mono_kontoj_tipoj', _('Povas krei tipoj de monaj kontoj')),
            ('povas_forigi_mono_kontoj_tipoj', _('Povas forigi tipoj de monaj kontoj')),
            ('povas_shangxi_mono_kontoj_tipoj', _('Povas ŝanĝi tipoj de monaj kontoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(models.Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(MonoKontoTipo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('mono',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'mono.povas_vidi_mono_kontoj_tipoj', 
                'mono.povas_krei_mono_kontoj_tipoj',
                'mono.povas_forigi_mono_kontoj_tipoj',
                'mono.povas_shangxi_mono_kontoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='mono', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('mono.povas_vidi_mono_kontoj_tipoj')
                    or user_obj.has_perm('mono.povas_vidi_mono_kontoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('mono.povas_vidi_mono_kontoj_tipoj'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond


# Шаблоны счетов (кошельков), использует абстрактный класс UniversoBazaRealeco
class MonoKontoSxablono(UniversoBazaRealeco):
    
    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)
    
    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # тип счёта
    tipo = models.ForeignKey(MonoKontoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # баланс счёта
    bilanco = models.FloatField(_('Bilanco'), blank=True, null=True, default=0)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'mono_kontoj_sxablonoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ŝablono de kontoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ŝablonoj de kontoj')
        # права
        permissions = (
            ('povas_vidi_mono_kontoj_sxablonoj', _('Povas vidi ŝablonoj de kontoj')),
            ('povas_krei_mono_kontoj_sxablonoj', _('Povas krei ŝablonoj de kontoj')),
            ('povas_forigi_mono_kontoj_sxablonoj', _('Povas forigi ŝablonoj de kontoj')),
            ('povas_shangxi_mono_kontoj_sxablonoj', _('Povas ŝanĝi ŝablonoj de kontoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}) {} - {}'.format(self.id, self.tipo, self.bilanco)

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(models.Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(MonoKontoSxablono, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                                  update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('mono',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'mono.povas_vidi_mono_kontoj_sxablonoj',
                'mono.povas_krei_mono_kontoj_sxablonoj',
                'mono.povas_forigi_mono_kontoj_sxablonoj',
                'mono.povas_shangxi_mono_kontoj_sxablonoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='mono', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('mono.povas_vidi_mono_kontoj_sxablonoj')
                    or user_obj.has_perm('mono.povas_vidi_mono_kontoj_sxablonoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('mono.povas_vidi_mono_kontoj_sxablonoj'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond


# Счета (кошельки), использует абстрактный класс UniversoBazaRealeco
class MonoKonto(UniversoBazaRealeco):

    # тип счёта
    tipo = models.ForeignKey(MonoKontoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # пользователь владелец счёта
    posedanto_uzanto = models.ForeignKey(Uzanto, verbose_name=_('Uzanto posedanto'), blank=True, null=True,
                                         default=None, on_delete=models.CASCADE)

    # организация владелец счёта
    posedanto_organizo = models.ForeignKey(Organizo, verbose_name=_('Organizo posedanto'), blank=True,
                                           null=True, default=None, on_delete=models.CASCADE)

    # баланс счёта
    bilanco = models.FloatField(_('Bilanco'), blank=True, null=True, default=0)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'mono_kontoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Konto')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kontoj')
        # права
        permissions = (
            ('povas_vidi_mono_kontoj', _('Povas vidi kontoj')),
            ('povas_krei_mono_kontoj', _('Povas krei kontoj')),
            ('povas_forigi_mono_kontoj', _('Povas forigi kontoj')),
            ('povas_shangxi_mono_kontoj', _('Povas ŝanĝi kontoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('mono',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'mono.povas_vidi_mono_kontoj', 
                'mono.povas_krei_mono_kontoj',
                'mono.povas_forigi_mono_kontoj',
                'mono.povas_shangxi_mono_kontoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='mono', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('mono.povas_vidi_mono_kontoj')
                    or user_obj.has_perm('mono.povas_vidi_mono_kontoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('mono.povas_vidi_mono_kontoj'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond


# Типы инвойсов (счетов на оплату), использует абстрактный класс UniversoBazaMaks
class MonoFakturoTipo(UniversoBazaMaks):

    # уникальный личный ID типа инвойса
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='mono_fakturoj_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'mono_fakturoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de fakturoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de fakturoj')
        # права
        permissions = (
            ('povas_vidi_mono_fakturoj_tipoj', _('Povas vidi tipoj de fakturoj')),
            ('povas_krei_mono_fakturoj_tipoj', _('Povas krei tipoj de fakturoj')),
            ('povas_forigi_mono_fakturoj_tipoj', _('Povas forigi tipoj de fakturoj')),
            ('povas_shangxi_mono_fakturoj_tipoj', _('Povas ŝanĝi tipoj de fakturoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(models.Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(MonoFakturoTipo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                                  update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('mono',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'mono.povas_vidi_mono_fakturoj_tipoj', 
                'mono.povas_krei_mono_fakturoj_tipoj',
                'mono.povas_forigi_mono_fakturoj_tipoj',
                'mono.povas_shangxi_mono_fakturoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='mono', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('mono.povas_vidi_mono_fakturoj_tipoj')
                    or user_obj.has_perm('mono.povas_vidi_mono_fakturoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('mono.povas_vidi_mono_fakturoj_tipoj'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond


# Инвойсы (счета на оплату), использует абстрактный класс UniversoBazaRealeco
class MonoFakturo(UniversoBazaRealeco):

    # тип инвойса
    tipo = models.ForeignKey(MonoFakturoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # выставитель инвойса
    ekspedanto = models.ForeignKey(MonoKonto, verbose_name=_('Ekspedanto'), blank=False, null=False,
                                   default=None, on_delete=models.CASCADE)

    # получатель инвойса
    ricevanto = models.ForeignKey(MonoKonto, verbose_name=_('Ricevanto'), blank=False, null=False, default=None,
                                  related_name='%(app_label)s_%(class)s_ricevanto', on_delete=models.CASCADE)

    # сумма инвойса по всем позициям
    sumo = models.FloatField(_('Sumo'), blank=False, null=True, default=0)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'mono_fakturoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Fakturo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Fakturoj')
        # права
        permissions = (
            ('povas_vidi_mono_fakturoj', _('Povas vidi fakturoj')),
            ('povas_krei_mono_fakturoj', _('Povas krei fakturoj')),
            ('povas_forigi_mono_fakturoj', _('Povas forigi fakturoj')),
            ('povas_shangxi_mono_fakturoj', _('Povas ŝanĝi fakturoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('mono',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'mono.povas_vidi_mono_fakturoj', 
                'mono.povas_krei_mono_fakturoj',
                'mono.povas_forigi_mono_fakturoj',
                'mono.povas_shangxi_mono_fakturoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='mono', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('mono.povas_vidi_mono_fakturoj')
                    or user_obj.has_perm('mono.povas_vidi_mono_fakturoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('mono.povas_vidi_mono_fakturoj'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond


# Объекты указанные в инвойсах (содержимое табличной части), использует абстрактный класс UniversoBazaRealeco
class MonoFakturoObjekto(UniversoBazaRealeco):

    # инвойс к которому относится запись
    fakturo = models.ForeignKey(MonoFakturo, verbose_name=_('Fakturo'), blank=False, null=False, default=None,
                                on_delete=models.CASCADE)

    # объект инвойса (на каждый объект отдельная запись в этой моделе)
    objekto = models.ForeignKey(Objekto, verbose_name=_('Objekto'), blank=False, null=False, default=None,
                                on_delete=models.CASCADE)

    # цена этой позиции
    prezo = models.FloatField(_('Prezo'), blank=False, null=True, default=0)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'mono_fakturoj_objektoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Objekto de fakturo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Objektoj de fakturoj')
        # права
        permissions = (
            ('povas_vidi_mono_fakturoj_objektoj', _('Povas vidi objektoj de fakturoj')),
            ('povas_krei_mono_fakturoj_objektoj', _('Povas krei objektoj de fakturoj')),
            ('povas_forigi_mono_fakturoj_objektoj', _('Povas forigi objektoj de fakturoj')),
            ('povas_shangxi_mono_fakturoj_objektoj', _('Povas ŝanĝi objektoj de fakturoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('mono',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'mono.povas_vidi_mono_fakturoj_objektoj', 
                'mono.povas_krei_mono_fakturoj_objektoj',
                'mono.povas_forigi_mono_fakturoj_objektoj',
                'mono.povas_shangxi_mono_fakturoj_objektoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='mono', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('mono.povas_vidi_mono_fakturoj_objektoj')
                    or user_obj.has_perm('mono.povas_vidi_mono_fakturoj_objektoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('mono.povas_vidi_mono_fakturoj_objektoj'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond


# Типы транзакций между счетами (кошельками), использует абстрактный класс UniversoBazaMaks
class MonoTransakcioTipo(UniversoBazaMaks):

    # уникальный личный ID типа транзакции
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='mono_transakcioj_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'mono_transakcioj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de transakcioj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de transakcioj')
        # права
        permissions = (
            ('povas_vidi_mono_transakcioj_tipoj', _('Povas vidi tipoj de transakcioj')),
            ('povas_krei_mono_transakcioj_tipoj', _('Povas krei tipoj de transakcioj')),
            ('povas_forigi_mono_transakcioj_tipoj', _('Povas forigi tipoj de transakcioj')),
            ('povas_shangxi_mono_transakcioj_tipoj', _('Povas ŝanĝi tipoj de transakcioj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(models.Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(MonoTransakcioTipo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                                     update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('mono',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'mono.povas_vidi_mono_transakcioj_tipoj', 
                'mono.povas_krei_mono_transakcioj_tipoj',
                'mono.povas_forigi_mono_transakcioj_tipoj',
                'mono.povas_shangxi_mono_transakcioj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='mono', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('mono.povas_vidi_mono_transakcioj_tipoj')
                    or user_obj.has_perm('mono.povas_vidi_mono_transakcioj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('mono.povas_vidi_mono_transakcioj_tipoj'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond


# Транзакции между счетами (кошельками), использует абстрактный класс UniversoBazaRealeco
class MonoTransakcio(UniversoBazaRealeco):

    # тип транзакции
    tipo = models.ForeignKey(MonoTransakcioTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # инвойс на основе которого делается транзакция
    fakturo = models.ForeignKey(MonoFakturo, verbose_name=_('Fakturo'), blank=False, null=False,
                                default=None, on_delete=models.CASCADE)

    # отправитель платежа
    ekspedanto = models.ForeignKey(MonoKonto, verbose_name=_('Ekspedanto'), blank=False, null=False,
                                   default=None, on_delete=models.CASCADE)

    # получатель платежа
    ricevanto = models.ForeignKey(MonoKonto, verbose_name=_('Ricevanto'), blank=False, null=False, default=None,
                                  related_name='%(app_label)s_%(class)s_ricevanto', on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'mono_transakcioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Mona transakcio')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Monaj transakcioj')
        # права
        permissions = (
            ('povas_vidi_mono_transakcioj', _('Povas vidi monaj transakcioj')),
            ('povas_krei_mono_transakcioj', _('Povas krei monaj transakcioj')),
            ('povas_forigi_mono_transakcioj', _('Povas forigi monaj transakcioj')),
            ('povas_shangxi_mono_transakcioj', _('Povas ŝanĝi monaj transakcioj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('mono',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'mono.povas_vidi_mono_transakcioj', 
                'mono.povas_krei_mono_transakcioj',
                'mono.povas_forigi_mono_transakcioj',
                'mono.povas_shangxi_mono_transakcioj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='mono', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('mono.povas_vidi_mono_transakcioj')
                    or user_obj.has_perm('mono.povas_vidi_mono_transakcioj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('mono.povas_vidi_mono_transakcioj'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond
