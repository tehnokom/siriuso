"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene  # сам Graphene
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from ..models import *  # модели приложения


# Модель справочника валют
class MonoValutoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = MonoValuto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов счетов
class MonoKontoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = MonoKontoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact'],
            'valuto__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель шаблонов счетов (кошельков)
class MonoKontoSxablonoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = MonoKontoSxablono
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact'],
            'tipo__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель счетов (кошельков)
class MonoKontoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = MonoKonto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'posedanto_uzanto__id': ['exact'],
            'posedanto_uzanto__uuid': ['exact'],
            'tipo__id': ['exact'],
            'posedanto_organizo__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов инвойсов (счетов на оплату)
class MonoFakturoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = MonoFakturoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель инвойсов (счетов на оплату)
class MonoFakturoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = MonoFakturo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'tipo__id': ['exact'],
            'ekspedanto__uuid': ['exact'],
            'ricevanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель объектов указанные в инвойсах (содержимое табличной части)
class MonoFakturoObjektoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = MonoFakturoObjekto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'fakturo__uuid': ['exact'],
            'objekto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов транзакций между счетами (кошельками)
class MonoTransakcioTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = MonoTransakcioTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель транзакций между счетами (кошельками)
class MonoTransakcioNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = MonoTransakcio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'tipo_id': ['exact'],
            'fakturo__uuid': ['exact'],
            'ekspedanto__uuid': ['exact'],
            'ricevanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


class MonoQuery(graphene.ObjectType):
    mono_valutoj = SiriusoFilterConnectionField(
        MonoValutoNode,
        description=_('Выводит все доступные модели справочника валют')
    )
    mono_kontoj_tipoj = SiriusoFilterConnectionField(
        MonoKontoTipoNode,
        description=_('Выводит все доступные модели типов счетов')
    )
    mono_kontoj = SiriusoFilterConnectionField(
        MonoKontoNode,
        description=_('Выводит все доступные модели счетов (кошельков)')
    )
    mono_fakturoj_tipoj = SiriusoFilterConnectionField(
        MonoFakturoTipoNode,
        description=_('Выводит все доступные модели типов инвойсов (счетов на оплату)')
    )
    mono_fakturoj= SiriusoFilterConnectionField(
        MonoFakturoNode,
        description=_('Выводит все доступные модели инвойсов (счетов на оплату)')
    )
    mono_fakturoj_objektoj= SiriusoFilterConnectionField(
        MonoFakturoObjektoNode,
        description=_('Выводит все доступные модели объектов указанные в инвойсах (содержимое табличной части)')
    )
    mono_transakcioj_tipoj = SiriusoFilterConnectionField(
        MonoTransakcioTipoNode,
        description=_('Выводит все доступные модели типов транзакций между счетами (кошельками)')
    )
    mono_transakcioj= SiriusoFilterConnectionField(
        MonoTransakcioNode,
        description=_('Выводит все доступные модели транзакций между счетами (кошельками)')
    )
