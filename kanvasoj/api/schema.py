"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene  # сам Graphene
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from ..models import *  # модели приложения


# Модель категорий холстов
class KanvasoKategorioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = KanvasoKategorio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов холстов
class KanvasoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = KanvasoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов холстов
class KanvasoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = KanvasoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель владельцев холстов
class KanvasoPosedantoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = KanvasoPosedanto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'kanvaso__uuid': ['exact'],
            'posedanto_uzanto__id': ['exact'],
            'posedanto_organizo__uuid': ['exact'],
            'posedanto_komunumo__id': ['exact'],
            'posedanto_komunumo__uuid': ['exact'],
            'tipo__id': ['exact'],
            'statuso__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель владельцев объектов холстов
class KanvasoObjektoPosedantoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = KanvasoObjektoPosedanto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'kanvaso_objekto__uuid': ['exact'],
            'posedanto_uzanto__id': ['exact'],
            'posedanto_organizo__uuid': ['exact'],
            'statuso__id': ['exact'],
            'tipo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель объектов холстов
class KanvasoObjektoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    # владельцы
    posedanto =  SiriusoFilterConnectionField(KanvasoObjektoPosedantoNode,
        description=_('Выводит владельцев объектов холстов'))

    class Meta:
        model = KanvasoObjekto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'statuso__id': ['exact','in'],
            'tipo__id': ['exact'],
            'kategorio__id': ['exact'],
            'kanvaso__id': ['exact'],
            'kanvaso__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_posedanto(self, info, **kwargs):
        return KanvasoObjektoPosedanto.objects.filter(kanvaso_objekto=self, forigo=False, arkivo=False, 
            publikigo=True)


class KanvasoKlasteroLigiloNode(SiriusoAuthNode, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = KanvasoKlasteroLigilo
        filter_fields = {
        }
        interfaces = (graphene.relay.Node,)


# Модель холстов
class KanvasoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    # объект холста
    kanvaso_objekto =  SiriusoFilterConnectionField(KanvasoObjektoNode,
        description=_('Выводит объекта холстов, привязанные к холсту'))

    # владельцы
    posedanto =  SiriusoFilterConnectionField(KanvasoPosedantoNode,
        description=_('Выводит владельцев холстов'))

    class Meta:
        model = Kanvaso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'tipo__id': ['exact'],
            'statuso__id': ['exact','in'],
            'kanvasoj_kanvasoposedanto_kanvaso__posedanto_komunumo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_kanvaso_objekto(self, info, **kwargs):
        return KanvasoObjekto.objects.filter(kanvaso=self, forigo=False, arkivo=False, 
            publikigo=True)

    def resolve_posedanto(self, info, **kwargs):
        return KanvasoPosedanto.objects.filter(kanvaso=self, forigo=False, arkivo=False, 
            publikigo=True)


# Модель типов владельцев холстов
class KanvasoPosedantoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = KanvasoPosedantoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов владельца холста
class KanvasoPosedantoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = KanvasoPosedantoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов связей холстов между собой
class KanvasoLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = KanvasoLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель связей холстов между собой
class KanvasoLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = KanvasoLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'ligilo__uuid': ['exact'],
            'tipo__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель категорий объектов холстов
class KanvasoObjektoKategorioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = KanvasoObjektoKategorio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов объектов холстов
class KanvasoObjektoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = KanvasoObjektoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов объектов холстов
class KanvasoObjektoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = KanvasoObjektoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов владельцев объектов холстов
class KanvasoObjektoPosedantoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = KanvasoObjektoPosedantoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов владельца объектов холстов
class KanvasoObjektoPosedantoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = KanvasoObjektoPosedantoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)
        

# Модель типов связей объектов холстов между собой
class KanvasoObjektoLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = KanvasoObjektoLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель связей объектов холстов между собой
class KanvasoObjektoLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = KanvasoObjektoLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'ligilo__uuid': ['exact'],
            'tipo__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


class KanvasojQuery(graphene.ObjectType):
    kanvasoj_kanvaso_kategorio = SiriusoFilterConnectionField(
        KanvasoKategorioNode,
        description=_('Выводит все доступные модели категорий холстов')
    )
    kanvasoj_kanvaso_tipo = SiriusoFilterConnectionField(
        KanvasoTipoNode,
        description=_('Выводит все доступные модели типов холстов')
    )
    kanvasoj_kanvaso_statuso = SiriusoFilterConnectionField(
        KanvasoStatusoNode,
        description=_('Выводит все доступные модели статусов холстов')
    )
    kanvasoj_kanvaso = SiriusoFilterConnectionField(
        KanvasoNode,
        description=_('Выводит все доступные модели холстов')
    )
    kanvasoj_kanvaso_posedantoj_tipo = SiriusoFilterConnectionField(
        KanvasoPosedantoTipoNode,
        description=_('Выводит все доступные модели типов владельцев холстов')
    )
    kanvasoj_kanvaso_posedantoj_statuso = SiriusoFilterConnectionField(
        KanvasoPosedantoStatusoNode,
        description=_('Выводит все доступные модели статусов владельца холстов')
    )
    kanvasoj_kanvaso_posedantoj = SiriusoFilterConnectionField(
        KanvasoPosedantoNode,
        description=_('Выводит все доступные модели владельцев холстов')
    )
    kanvasoj_kanvaso_ligilo_tipo = SiriusoFilterConnectionField(
        KanvasoLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей холстов между собой')
    )
    kanvasoj_kanvaso_ligilo = SiriusoFilterConnectionField(
        KanvasoLigiloNode,
        description=_('Выводит все доступные модели связей холстов между собой')
    )
    kanvasoj_kanvaso_objekto_kategorio = SiriusoFilterConnectionField(
        KanvasoObjektoKategorioNode,
        description=_('Выводит все доступные модели категорий объектов холстов')
    )
    kanvasoj_kanvaso_objekto_tipo = SiriusoFilterConnectionField(
        KanvasoObjektoTipoNode,
        description=_('Выводит все доступные модели типов объектов холстов')
    )
    kanvasoj_kanvaso_objekto_statuso = SiriusoFilterConnectionField(
        KanvasoObjektoStatusoNode,
        description=_('Выводит все доступные модели статусов объектов холстов')
    )
    kanvasoj_kanvaso_objekto = SiriusoFilterConnectionField(
        KanvasoObjektoNode,
        description=_('Выводит все доступные модели объектов холстов')
    )
    kanvasoj_kanvaso_objekto_posedantoj_tipo = SiriusoFilterConnectionField(
        KanvasoObjektoPosedantoTipoNode,
        description=_('Выводит все доступные модели типов владельцев объектов холстов')
    )
    kanvasoj_kanvaso_objekto_posedantoj_statuso = SiriusoFilterConnectionField(
        KanvasoObjektoPosedantoStatusoNode,
        description=_('Выводит все доступные модели статусов владельца объектов холстов')
    )
    kanvasoj_kanvaso_objekto_posedanto = SiriusoFilterConnectionField(
        KanvasoObjektoPosedantoNode,
        description=_('Выводит все доступные модели владельцев объектов холстов')
    )
    kanvasoj_kanvaso_objekto_ligilo_tipo = SiriusoFilterConnectionField(
        KanvasoObjektoLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей объектов холстов между собой')
    )
    kanvasoj_kanvaso_objekto_ligilo = SiriusoFilterConnectionField(
        KanvasoObjektoLigiloNode,
        description=_('Выводит все доступные модели связей объектов холстов между собой')
    )
