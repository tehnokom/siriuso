"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import transaction  # создание и изменения будем делать в блокирующей транзакции
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from .schema import *
from ..models import *
from .subscription import KanvasoEventoj


# Модель категорий досок
class RedaktuKanvasoKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso_kategorioj = graphene.Field(KanvasoKategorioNode,
        description=_('Созданная/изменённая категория досок'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kanvaso_kategorioj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_kategorioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True, arkivo=False)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            kanvaso_kategorioj = KanvasoKategorio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(kanvaso_kategorioj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(kanvaso_kategorioj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    kanvaso_kategorioj.realeco.set(realeco)
                                else:
                                    kanvaso_kategorioj.realeco.clear()

                            kanvaso_kategorioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kanvaso_kategorioj = KanvasoKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kanvasoj.povas_forigi_kanvasoj_kanvaso_kategorioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kanvasoj.povas_shangxi_kanvasoj_kanvaso_kategorioj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True, arkivo=False)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:

                                kanvaso_kategorioj.forigo = kwargs.get('forigo', kanvaso_kategorioj.forigo)
                                kanvaso_kategorioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kanvaso_kategorioj.arkivo = kwargs.get('arkivo', kanvaso_kategorioj.arkivo)
                                kanvaso_kategorioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kanvaso_kategorioj.publikigo = kwargs.get('publikigo', kanvaso_kategorioj.publikigo)
                                kanvaso_kategorioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kanvaso_kategorioj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(kanvaso_kategorioj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(kanvaso_kategorioj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        kanvaso_kategorioj.realeco.set(realeco)
                                    else:
                                        kanvaso_kategorioj.realeco.clear()

                                kanvaso_kategorioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KanvasoKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKanvasoKategorio(status=status, message=message, kanvaso_kategorioj=kanvaso_kategorioj)


# Модель типов досок
class RedaktuKanvasoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso_tipoj = graphene.Field(KanvasoTipoNode, 
        description=_('Созданный/изменённый тип досок'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kanvaso_tipoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True, arkivo=False)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            kanvaso_tipoj = KanvasoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(kanvaso_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(kanvaso_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    kanvaso_tipoj.realeco.set(realeco)
                                else:
                                    kanvaso_tipoj.realeco.clear()

                            kanvaso_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kanvaso_tipoj = KanvasoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kanvasoj.povas_forigi_kanvasoj_kanvaso_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kanvasoj.povas_shangxi_kanvasoj_kanvaso_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True, arkivo=False)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:

                                kanvaso_tipoj.forigo = kwargs.get('forigo', kanvaso_tipoj.forigo)
                                kanvaso_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kanvaso_tipoj.arkivo = kwargs.get('arkivo', kanvaso_tipoj.arkivo)
                                kanvaso_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kanvaso_tipoj.publikigo = kwargs.get('publikigo', kanvaso_tipoj.publikigo)
                                kanvaso_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kanvaso_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(kanvaso_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(kanvaso_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        kanvaso_tipoj.realeco.set(realeco)
                                    else:
                                        kanvaso_tipoj.realeco.clear()

                                kanvaso_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KanvasoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKanvasoTipo(status=status, message=message, kanvaso_tipoj=kanvaso_tipoj)


# Модель статусов досок
class RedaktuKanvasoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso_statusoj = graphene.Field(KanvasoStatusoNode, 
        description=_('Созданный/изменённый статус досок'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kanvaso_statusoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        # проверяем наличие записей с таким кодом
                        if not message:
                            kanvaso_statusoj = KanvasoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(kanvaso_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(kanvaso_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            kanvaso_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kanvaso_statusoj = KanvasoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kanvasoj.povas_forigi_kanvasoj_kanvaso_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kanvasoj.povas_shangxi_kanvasoj_kanvaso_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:

                                kanvaso_statusoj.forigo = kwargs.get('forigo', kanvaso_statusoj.forigo)
                                kanvaso_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kanvaso_statusoj.arkivo = kwargs.get('arkivo', kanvaso_statusoj.arkivo)
                                kanvaso_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kanvaso_statusoj.publikigo = kwargs.get('publikigo', kanvaso_statusoj.publikigo)
                                kanvaso_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kanvaso_statusoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(kanvaso_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(kanvaso_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                kanvaso_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KanvasoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKanvasoStatuso(status=status, message=message, kanvaso_statusoj=kanvaso_statusoj)


# сохраняем объекты досок
def kanvaso_save(kanvaso):

    def eventoj():
        KanvasoEventoj.kanvasoj(kanvaso)

    kanvaso.save()
    # вызов функции через коммит транзакции обязателен, иначе данные в подписке ещё не изменёнными будут
    transaction.on_commit(eventoj)


class KlasterojUUID(graphene.InputObjectType):
    klastero = graphene.String(description=_('UUID кластера'))
    pozicio = graphene.Int(description=_('Позиция в списке'))
    nomo = graphene.String(description=_('Название'))
    priskribo = graphene.String(description=_('Описание'))


# Модель досок
class RedaktuKanvaso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso = graphene.Field(KanvasoNode, 
        description=_('Созданная/изменённая доска'))
    kanvaso_posedantoj = graphene.Field(KanvasoPosedantoNode, 
        description=_('Созданный/изменённый владелец досок'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий досок'))
        tipo_id = graphene.Int(description=_('Тип досок'))
        statuso_id = graphene.Int(description=_('Статус досок'))
        pozicio = graphene.Int(description=_('Позиция в списке'))
        klasteroj_uuid = graphene.List(KlasterojUUID, description=_('Список UUID кластеров с дополнительными настройками'))
        
        # параметры для создания владельца
        posedanto_uzanto_id = graphene.Int(description=_('Владелец досок'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец досок'))
        posedanto_komunumo_id = graphene.Int(description=_('Сообщество владелец досок'))
        posedanto_tipo_id = graphene.Int(description=_('Тип владельца досок'))
        posedanto_statuso_id = graphene.Int(description=_('Статус владельца досок'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kanvaso = None
        realeco = None
        kategorio = KanvasoKategorio.objects.none()
        tipo = None
        statuso = None
        posedanto_uzanto = None
        posedanto_organizo = None
        posedanto_tipo = None
        posedanto_statuso = None
        posedanto_komunumo = None
        klasteroj = []
        uzanto = info.context.user
        kanvaso_posedantoj = None

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not (message or kwargs.get('uuid', False)):
                    uzanto_perm = uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso')
                    # получаем пользователя, для которого создаётся пространство
                    if not message:
                        if 'posedanto_uzanto_id' in kwargs:
                            try:
                                posedanto_uzanto = Uzanto.objects.get(
                                    id=kwargs.get('posedanto_uzanto_id'), 
                                    is_active=True,
                                    konfirmita=True
                                )
                            except Uzanto.DoesNotExist:
                                if not (uzanto_perm):
                                    message = _('Недостаточно прав')
                                else:
                                    message = _('Неверный владелец/пользователь доски')
                    # если пользователь владелец пространства, по которому создаётся доска, то можно создавать
                    # проверяем по полю posedanto
                    if not (uzanto_perm) and not message:
                        if posedanto_uzanto == uzanto:
                            uzanto_perm = True
                    # проверяем по полю posedanto
                    if uzanto_perm:
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'kategorio' in kwargs:
                                if not len(kwargs.get('kategorio')):
                                    message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'kategorio')
                                else:
                                    kategorio_id = set(kwargs.get('kategorio'))
                                    if len(kategorio_id):
                                        kategorio = KanvasoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True, arkivo=False)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))
                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие категории досок'),
                                                        str(dif)
                                            )

                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = _('Неверный параллельный мир')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = KanvasoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except KanvasoTipo.DoesNotExist:
                                    message = _('Неверный тип досок')

                        if not message:
                            if 'statuso_id' in kwargs:
                                try:
                                    statuso = KanvasoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except KanvasoStatuso.DoesNotExist:
                                    message = _('Неверный статус досок')

                        if not message:
                            if kwargs.get('klasteroj_uuid', False):
                                for klastero in kwargs.get('klasteroj_uuid'):
                                    try:
                                        klasteroj.append({
                                            "klastero":Klastero.objects.get(uuid=klastero.klastero, forigo=False,
                                                                            arkivo=False, publikigo=True),
                                            "pozicio":klastero.pozicio,
                                            "nomo":klastero.nomo,
                                            "priskribo":klastero.priskribo,
                                            })
                                    except Klastero.DoesNotExist:
                                        message = _('Неверный кластер: ' + klastero)

                        if not message:
                            # теперь проводим проверку параметров по владельцу
                            message, posedanto_tipo = RedaktuKanvasoPosedanto.provado(tipo_id=kwargs.get('posedanto_tipo_id', False))

                        if not message:
                            kanvaso = Kanvaso.objects.create(
                                forigo=False,
                                arkivo=False,
                                realeco = realeco,
                                tipo = tipo,
                                pozicio = kwargs.get('pozicio', None),
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato = timezone.now(),
                            )

                            if statuso:
                                kanvaso.statuso = statuso

                            if (kwargs.get('nomo', False)):
                                set_enhavo(kanvaso.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(kanvaso.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'kategorio' in kwargs:
                                if kategorio:
                                    kanvaso.kategorio.set(kategorio)
                                else:
                                    kanvaso.kategorio.clear()
                            kanvaso_save(kanvaso)

                            if len(klasteroj)>0:
                                for klastero in klasteroj:
                                    sci = KanvasoKlasteroLigilo.objects.create(
                                        kanvaso=kanvaso,
                                        klastero=klastero["klastero"],
                                        pozicio=klastero["pozicio"]if klastero["pozicio"] else None,
                                        nomo=klastero["nomo"] if klastero["nomo"] else None,
                                        priskribo=klastero["priskribo"] if klastero["priskribo"] else None
                                    )
                                kanvaso.klasteroj.all()

                            status = True
                            # далее блок добавления владельца
                            if (kwargs.get('posedanto_uzanto_id', False)):
                                if not message:
                                    if 'posedanto_statuso_id' in kwargs:
                                        try:
                                            posedanto_statuso = KanvasoPosedantoStatuso.objects.get(id=kwargs.get('posedanto_statuso_id'), forigo=False,
                                                                            arkivo=False, publikigo=True)
                                        except KanvasoPosedantoStatuso.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверный статус владельца досок'),'posedanto_statuso_id')

                                if not message:
                                    if 'posedanto_organizo_uuid' in kwargs:
                                        try:
                                            posedanto_organizo = Organizo.objects.get(
                                                uuid=kwargs.get('posedanto_organizo_uuid'),
                                                forigo=False,
                                                arkivo=False,  
                                                publikigo=True
                                            )
                                        except Organizo.DoesNotExist:
                                            message = _('Неверная организация владелец досок')

                                if not message:
                                    if 'posedanto_komunumo_id' in kwargs:
                                        try:
                                            posedanto_komunumo = Komunumo.objects.get(
                                                id=kwargs.get('posedanto_komunumo_id'),
                                                forigo=False,
                                                arkivo=False
                                            )
                                        except Komunumo.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверное сообщество-владелец досок'),'posedanto_komunumo_id')

                                if not message:
                                    kanvaso_posedantoj = KanvasoPosedanto.objects.create(
                                        forigo=False,
                                        arkivo=False,
                                        realeco=realeco,
                                        kanvaso=kanvaso,
                                        posedanto_uzanto = posedanto_uzanto,
                                        posedanto_organizo = posedanto_organizo,
                                        tipo = posedanto_tipo,
                                        statuso = posedanto_statuso,
                                        posedanto_komunumo = posedanto_komunumo,
                                        publikigo = kwargs.get('publikigo', False),
                                        publikiga_dato = timezone.now()
                                    )

                                    kanvaso_posedantoj.save()

                                    status = True
                                    message = _('Обе записи созданы')
    
                            if not message:
                                message = _('Запись создана')

                    else:
                        if not message:
                            message = _('Недостаточно прав')
                elif not message:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('kategorio', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('statuso_id', False)
                            or kwargs.get('pozicio', False) or ('klasteroj_uuid' in kwargs)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kanvaso = Kanvaso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if ((not uzanto.has_perm('kanvasoj.povas_forigi_kanvasoj_kanvaso', kanvaso))
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not (uzanto.has_perm('kanvasoj.povas_shangxi_kanvasoj_kanvaso', kanvaso)):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))
                                    if len(kategorio_id):
                                        kategorio = KanvasoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True, arkivo=False)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))
                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие категории досок'),
                                                        str(dif)
                                            )

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = _('Неверный параллельный мир')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = KanvasoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KanvasoTipo.DoesNotExist:
                                        message = _('Неверный тип досок')

                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = KanvasoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KanvasoStatuso.DoesNotExist:
                                        message = _('Неверный статус досок')

                            if not message:
                                if kwargs.get('klasteroj_uuid', False):
                                    for klastero in kwargs.get('klasteroj_uuid'):
                                        try:
                                            klasteroj.append({
                                                "klastero":Klastero.objects.get(uuid=klastero.klastero, forigo=False,
                                                                                arkivo=False, publikigo=True),
                                                "pozicio":klastero.pozicio,
                                                "nomo":klastero.nomo,
                                                "priskribo":klastero.priskribo,
                                                })
                                        except Klastero.DoesNotExist:
                                            message = _('Неверный кластер: ' + klastero)

                            if not message:
                                kanvaso.forigo = kwargs.get('forigo', kanvaso.forigo)
                                kanvaso.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kanvaso.arkivo = kwargs.get('arkivo', kanvaso.arkivo)
                                kanvaso.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kanvaso.publikigo = kwargs.get('publikigo', kanvaso.publikigo)
                                kanvaso.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kanvaso.realeco = realeco if kwargs.get('realeco_id', False) else kanvaso.realeco
                                kanvaso.statuso = statuso if kwargs.get('statuso_id', False) else kanvaso.statuso
                                kanvaso.tipo = tipo if kwargs.get('tipo_id', False) else kanvaso.tipo
                                kanvaso.pozicio = kwargs.get('pozicio', kanvaso.pozicio)

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(kanvaso.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(kanvaso.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        kanvaso.kategorio.set(kategorio)
                                    else:
                                        kanvaso.kategorio.clear()

                                if kwargs.get('forigo', False):
                                    # выбераем все нижестоящие связи и помечаем на удаление
                                    kanvaso_posedanto = KanvasoPosedanto.objects.filter(kanvaso__id=kanvaso.id, forigo=False).update(forigo=True)
                                    kanvaso_ligilo = KanvasoLigilo.objects.filter(posedanto__id=kanvaso.id, forigo=False).update(forigo=True)
                                    kanvaso_objekto_posedanto = KanvasoObjektoPosedanto.objects.filter(kanvaso_objekto__kanvaso__id=kanvaso.id, forigo=False).update(forigo=True)
                                    kanvaso_objekto_ligilo = KanvasoObjektoLigilo.objects.filter(posedanto__kanvaso__id=kanvaso.id, forigo=False).update(forigo=True)
                                    kanvaso_objektoj = KanvasoObjekto.objects.filter(kanvaso__id=kanvaso.id, forigo=False).update(forigo=True)

                                kanvaso_save(kanvaso)
                                if 'klasteroj_uuid' in kwargs:
                                    kanvaso.klasteroj.clear()
                                    if len(klasteroj)>0:
                                        for klastero in klasteroj:
                                            sci = KanvasoKlasteroLigilo.objects.create(
                                                kanvaso=kanvaso,
                                                klastero=klastero["klastero"],
                                                pozicio=klastero["pozicio"]if klastero["pozicio"] else None,
                                                nomo=klastero["nomo"] if klastero["nomo"] else None,
                                                priskribo=klastero["priskribo"] if klastero["priskribo"] else None
                                            )
                                        kanvaso.klasteroj.all()

                                status = True
                                message = _('Запись успешно изменена')
                        except Kanvaso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKanvaso(status=status, message=message, kanvaso=kanvaso, kanvaso_posedantoj=kanvaso_posedantoj)


# Модель типов владельцев досок
class RedaktuKanvasoPosedantoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso_posedantoj_tipoj = graphene.Field(KanvasoPosedantoTipoNode, 
        description=_('Созданный/изменённый тип владельцев досок'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kanvaso_posedantoj_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_posedantoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            kanvaso_posedantoj_tipoj = KanvasoPosedantoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(kanvaso_posedantoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(kanvaso_posedantoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            kanvaso_posedantoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kanvaso_posedantoj_tipoj = KanvasoPosedantoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kanvasoj.povas_forigi_kanvasoj_kanvaso_posedantoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kanvasoj.povas_shangxi_kanvasoj_kanvaso_posedantoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                kanvaso_posedantoj_tipoj.forigo = kwargs.get('forigo', kanvaso_posedantoj_tipoj.forigo)
                                kanvaso_posedantoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kanvaso_posedantoj_tipoj.arkivo = kwargs.get('arkivo', kanvaso_posedantoj_tipoj.arkivo)
                                kanvaso_posedantoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kanvaso_posedantoj_tipoj.publikigo = kwargs.get('publikigo', kanvaso_posedantoj_tipoj.publikigo)
                                kanvaso_posedantoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kanvaso_posedantoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(kanvaso_posedantoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(kanvaso_posedantoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                kanvaso_posedantoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KanvasoPosedantoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKanvasoPosedantoTipo(status=status, message=message, kanvaso_posedantoj_tipoj=kanvaso_posedantoj_tipoj)


# Модель статусов владельцев досок
class RedaktuKanvasoPosedantoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso_posedantoj_statusoj = graphene.Field(KanvasoPosedantoStatusoNode, 
        description=_('Созданный/изменённый статус владельца досок'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kanvaso_posedantoj_statusoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_posedantoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            kanvaso_posedantoj_statusoj = KanvasoPosedantoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(kanvaso_posedantoj_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(kanvaso_posedantoj_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            kanvaso_posedantoj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kanvaso_posedantoj_statusoj = KanvasoPosedantoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kanvasoj.povas_forigi_kanvasoj_kanvaso_posedantoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kanvasoj.povas_shangxi_kanvasoj_kanvaso_posedantoj_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                kanvaso_posedantoj_statusoj.forigo = kwargs.get('forigo', kanvaso_posedantoj_statusoj.forigo)
                                kanvaso_posedantoj_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kanvaso_posedantoj_statusoj.arkivo = kwargs.get('arkivo', kanvaso_posedantoj_statusoj.arkivo)
                                kanvaso_posedantoj_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kanvaso_posedantoj_statusoj.publikigo = kwargs.get('publikigo', kanvaso_posedantoj_statusoj.publikigo)
                                kanvaso_posedantoj_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kanvaso_posedantoj_statusoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(kanvaso_posedantoj_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(kanvaso_posedantoj_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                kanvaso_posedantoj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KanvasoPosedantoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKanvasoPosedantoStatuso(status=status, message=message, kanvaso_posedantoj_statusoj=kanvaso_posedantoj_statusoj)


# Модель владельцев досок
class RedaktuKanvasoPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso_posedantoj = graphene.Field(KanvasoPosedantoNode, 
        description=_('Созданный/изменённый владелец досок'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        kanvaso_uuid = graphene.String(description=_('UUID доски (передаётся либо ID, либо UUID)'))
        kanvaso_id = graphene.Int(description=_('ID доски (передаётся либо ID, либо UUID)'))
        posedanto_uzanto_id = graphene.Int(description=_('Владелец досок'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец досок'))
        posedanto_komunumo_id = graphene.Int(description=_('Сообщество владелец досок'))
        tipo_id = graphene.Int(description=_('Тип владельца досок'))
        statuso_id = graphene.Int(description=_('Статус владельца досок'))

    @staticmethod
    def provado(**kwargs):
        """
        проверяем часть параметров на корректность (вызов из другой функции нужен)
        """
        message = None
        tipo = None
        if not message:
            if kwargs.get('tipo_id', False):
                try:
                    tipo = KanvasoPosedantoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                        arkivo=False, publikigo=True)
                except KanvasoPosedantoTipo.DoesNotExist:
                    message = _('Неверный тип владельца досок')
        return message, tipo

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kanvaso_posedantoj = None
        realeco = None
        kanvaso = None
        posedanto_uzanto = None
        posedanto_organizo = None
        posedanto_komunumo = None
        tipo = None
        statuso = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if not message:
                        if 'kanvaso_id' in kwargs and 'kanvaso_uuid' in kwargs:
                            message = '{} "{}" {} "{}"'.format(_('Недопустимо передавать сразу оба параметра'),'kanvaso_id',_('и'),'kanvaso_uuid')
                    if not message:
                        try:
                            if 'kanvaso_uuid' in kwargs:
                                kanvaso = Kanvaso.objects.get(uuid=kwargs.get('kanvaso_uuid'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            elif 'kanvaso_id' in kwargs:
                                kanvaso = Kanvaso.objects.get(id=kwargs.get('kanvaso_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            else:
                                message = '{} "{}", "{}"'.format(_('Одно из полей обязательно для заполнения:'),'kanvaso_uuid', 'kanvaso_id')
                        except Kanvaso.DoesNotExist:
                            message = '{} "{}"'.format(_('Неверная доска'),'kanvaso_uuid')

                    if uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_posedantoj', kanvaso):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_organizo_uuid' in kwargs:
                                try:
                                    posedanto_organizo = Organizo.objects.get(
                                        uuid=kwargs.get('posedanto_organizo_uuid'),
                                        forigo=False,
                                        arkivo=False,  
                                        publikigo=True
                                    )
                                except Organizo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная организация владелец досок'),'posedanto_organizo_uuid')

                        if not message:
                            if 'posedanto_komunumo_id' in kwargs:
                                try:
                                    posedanto_komunumo = Komunumo.objects.get(
                                        id=kwargs.get('posedanto_komunumo_id'),
                                        forigo=False,
                                        arkivo=False
                                    )
                                except Komunumo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверное сообщество-владелец досок'),'posedanto_komunumo_id')

                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный параллельный мир'),'realeco_id')

                        message, tipo = RedaktuKanvasoPosedanto.provado(**kwargs)

                        if not message:
                            if 'statuso_id' in kwargs:
                                try:
                                    statuso = KanvasoPosedantoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except KanvasoPosedantoStatuso.DoesNotExist:
                                    message = _('Неверный статус владельца досок')

                        if 'posedanto_uzanto_id' in kwargs:
                            try:
                                posedanto_uzanto = Uzanto.objects.get(
                                    id=kwargs.get('posedanto_uzanto_id'), 
                                    is_active=True,
                                    konfirmita=True
                                )
                            except Uzanto.DoesNotExist:
                                message = _('Неверный владелец/пользователь досок')

                        if not message:
                            kanvaso_posedantoj = KanvasoPosedanto.objects.create(
                                forigo = False,
                                arkivo = False,
                                realeco = realeco,
                                kanvaso = kanvaso,
                                posedanto_uzanto = posedanto_uzanto,
                                posedanto_organizo = posedanto_organizo,
                                posedanto_komunumo = posedanto_komunumo,
                                tipo = tipo,
                                statuso = statuso,
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato = timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(kanvaso_posedantoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(kanvaso_posedantoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            kanvaso_posedantoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('posedanto_uzanto_id', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('posedanto_organizo_uuid', False)
                            or kwargs.get('posedanto_komunumo_id', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('kanvaso_uuid', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kanvaso_posedantoj = KanvasoPosedanto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not (uzanto.has_perm('kanvasoj.povas_forigi_kanvasoj_kanvaso_posedantoj', kanvaso_posedantoj))
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not (uzanto.has_perm('kanvasoj.povas_shangxi_kanvasoj_kanvaso_posedantoj', kanvaso_posedantoj)):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_uzanto_id' in kwargs:
                                    try:
                                        posedanto_uzanto = Uzanto.objects.get(
                                            id=kwargs.get('posedanto_uzanto_id'), 
                                            is_active=True,
                                            konfirmita=True
                                        )
                                    except Uzanto.DoesNotExist:
                                        message = _('Неверный владелец/пользователь досок')

                            if not message:
                                if 'posedanto_organizo_uuid' in kwargs:
                                    try:
                                        posedanto_organizo = Organizo.objects.get(
                                            uuid=kwargs.get('posedanto_organizo_uuid'),
                                            forigo=False,
                                            arkivo=False,  
                                            publikigo=True
                                        )
                                    except Organizo.DoesNotExist:
                                        message = _('Неверная организация владелец досок')

                            if not message:
                                if 'posedanto_komunumo_id' in kwargs:
                                    try:
                                        posedanto_komunumo = Komunumo.objects.get(
                                            id=kwargs.get('posedanto_komunumo_id'),
                                            forigo=False,
                                            arkivo=False
                                        )
                                    except Komunumo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверное сообщество-владелец досок'),'posedanto_komunumo_id')

                            if not message:
                                if 'kanvaso_id' in kwargs and 'kanvaso_uuid' in kwargs:
                                    message = '{} "{}" {} "{}"'.format(_('Недопустимо передавать сразу оба параметра'),'kanvaso_id',_('и'),'kanvaso_uuid')
                            if not message:
                                try:
                                    if 'kanvaso_uuid' in kwargs:
                                        kanvaso = Kanvaso.objects.get(uuid=kwargs.get('kanvaso_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    elif 'kanvaso_id' in kwargs:
                                        kanvaso = Kanvaso.objects.get(id=kwargs.get('kanvaso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                except Kanvaso.DoesNotExist:
                                    message = _('Неверная доска')

                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = _('Неверный параллельный мир')

                            message, tipo = RedaktuKanvasoPosedanto.provado(**kwargs)

                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = KanvasoPosedantoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KanvasoPosedantoStatuso.DoesNotExist:
                                        message = _('Неверный статус владельца досок')

                            if not message:

                                kanvaso_posedantoj.forigo = kwargs.get('forigo', kanvaso_posedantoj.forigo)
                                kanvaso_posedantoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kanvaso_posedantoj.arkivo = kwargs.get('arkivo', kanvaso_posedantoj.arkivo)
                                kanvaso_posedantoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kanvaso_posedantoj.publikigo = kwargs.get('publikigo', kanvaso_posedantoj.publikigo)
                                kanvaso_posedantoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kanvaso_posedantoj.realeco = realeco if kwargs.get('realeco_id', False) else kanvaso_posedantoj.realeco
                                kanvaso_posedantoj.kanvaso = realeco if kwargs.get('kanvaso_uuid', False) else kanvaso_posedantoj.kanvaso
                                kanvaso_posedantoj.posedanto_uzanto = posedanto_uzanto if kwargs.get('posedanto_uzanto_id', False) else kanvaso_posedantoj.posedanto_uzanto
                                kanvaso_posedantoj.posedanto_organizo = posedanto_organizo if kwargs.get('posedanto_organizo_uuid', False) else kanvaso_posedantoj.posedanto_organizo
                                kanvaso_posedantoj.posedanto_komunumo = posedanto_komunumo if kwargs.get('posedanto_komunumo_id', False) else kanvaso_posedantoj.posedanto_komunumo
                                kanvaso_posedantoj.statuso = statuso if kwargs.get('statuso_id', False) else kanvaso_posedantoj.statuso
                                kanvaso_posedantoj.tipo = tipo if kwargs.get('tipo_id', False) else kanvaso_posedantoj.tipo

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(kanvaso_posedantoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(kanvaso_posedantoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                kanvaso_posedantoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KanvasoPosedanto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKanvasoPosedanto(status=status, message=message, kanvaso_posedantoj=kanvaso_posedantoj)


# Модель типов связей досок между собой
class RedaktuKanvasoLigiloTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso_ligilo_tipoj = graphene.Field(KanvasoLigiloTipoNode, 
        description=_('Созданный/изменённый тип связей досок между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kanvaso_ligilo_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_ligilo_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            kanvaso_ligilo_tipoj = KanvasoLigiloTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(kanvaso_ligilo_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(kanvaso_ligilo_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            kanvaso_ligilo_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kanvaso_ligilo_tipoj = KanvasoLigiloTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kanvasoj.povas_forigi_kanvasoj_kanvaso_ligilo_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kanvasoj.povas_shangxi_kanvasoj_kanvaso_ligilo_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                kanvaso_ligilo_tipoj.forigo = kwargs.get('forigo', kanvaso_ligilo_tipoj.forigo)
                                kanvaso_ligilo_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kanvaso_ligilo_tipoj.arkivo = kwargs.get('arkivo', kanvaso_ligilo_tipoj.arkivo)
                                kanvaso_ligilo_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kanvaso_ligilo_tipoj.publikigo = kwargs.get('publikigo', kanvaso_ligilo_tipoj.publikigo)
                                kanvaso_ligilo_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kanvaso_ligilo_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(kanvaso_ligilo_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(kanvaso_ligilo_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                kanvaso_ligilo_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KanvasoLigiloTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKanvasoLigiloTipo(status=status, message=message, kanvaso_ligilo_tipoj=kanvaso_ligilo_tipoj)


# Модель связей досок между собой
class RedaktuKanvasoLigilo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso_ligilo = graphene.Field(KanvasoLigiloNode, 
        description=_('Создание/изменение связи досок между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        posedanto_id = graphene.Int(description=_('ID доски владельца связи'))
        ligilo_id = graphene.Int(description=_('ID связываемой доски'))
        tipo_id = graphene.Int(description=_('ID типа связи досок'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kanvaso_ligilo = None
        posedanto = None
        ligilo = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_ligilo'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_id' in kwargs:
                                try:
                                    posedanto = Kanvaso.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Kanvaso.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная доска'),'posedanto_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_id')

                        if not message:
                            if 'ligilo_id' in kwargs:
                                try:
                                    ligilo = Kanvaso.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Kanvaso.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная доска'),'ligilo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'ligilo_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = KanvasoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except KanvasoLigiloTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип связи досок между собой'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            kanvaso_ligilo = KanvasoLigilo.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto=posedanto,
                                ligilo=ligilo,
                                tipo=tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            kanvaso_ligilo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto_id', False) or kwargs.get('ligilo_id', False)
                            or kwargs.get('tipo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kanvaso_ligilo = KanvasoLigilo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kanvasoj.povas_forigi_kanvasoj_kanvaso_ligilo')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kanvasoj.povas_shangxi_kanvasoj_kanvaso_ligilo'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_id' in kwargs:
                                    try:
                                        posedanto = Kanvaso.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Kanvaso.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная доска'),'posedanto_id')

                            if not message:
                                if 'ligilo_id' in kwargs:
                                    try:
                                        ligilo = Kanvaso.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Kanvaso.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная доска'),'ligilo_id')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = KanvasoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KanvasoLigiloTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип связи досок между собой'),'tipo_id')

                            if not message:

                                kanvaso_ligilo.forigo = kwargs.get('forigo', kanvaso_ligilo.forigo)
                                kanvaso_ligilo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kanvaso_ligilo.arkivo = kwargs.get('arkivo', kanvaso_ligilo.arkivo)
                                kanvaso_ligilo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kanvaso_ligilo.publikigo = kwargs.get('publikigo', kanvaso_ligilo.publikigo)
                                kanvaso_ligilo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kanvaso_ligilo.posedanto = posedanto if kwargs.get('posedanto_id', False) else kanvaso_ligilo.posedanto
                                kanvaso_ligilo.ligilo = ligilo if kwargs.get('ligilo_id', False) else kanvaso_ligilo.ligilo
                                kanvaso_ligilo.tipo = tipo if kwargs.get('tipo_id', False) else kanvaso_ligilo.tipo

                                kanvaso_ligilo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KanvasoLigilo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKanvasoLigilo(status=status, message=message, kanvaso_ligilo=kanvaso_ligilo)


# Модель категорий объектов холстов
class RedaktuKanvasoObjektoKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso_objekto_kategorioj = graphene.Field(KanvasoObjektoKategorioNode,
        description=_('Созданная/изменённая категория объектов холстов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kanvaso_objekto_kategorioj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_objekto_kategorioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True, arkivo=False)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            kanvaso_objekto_kategorioj = KanvasoObjektoKategorio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(kanvaso_objekto_kategorioj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(kanvaso_objekto_kategorioj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    kanvaso_objekto_kategorioj.realeco.set(realeco)
                                else:
                                    kanvaso_objekto_kategorioj.realeco.clear()

                            kanvaso_objekto_kategorioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kanvaso_objekto_kategorioj = KanvasoObjektoKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kanvasoj.povas_forigi_kanvasoj_kanvaso_objekto_kategorioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kanvasoj.povas_shangxi_kanvasoj_kanvaso_objekto_kategorioj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True, arkivo=False)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                kanvaso_objekto_kategorioj.forigo = kwargs.get('forigo', kanvaso_objekto_kategorioj.forigo)
                                kanvaso_objekto_kategorioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kanvaso_objekto_kategorioj.arkivo = kwargs.get('arkivo', kanvaso_objekto_kategorioj.arkivo)
                                kanvaso_objekto_kategorioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kanvaso_objekto_kategorioj.publikigo = kwargs.get('publikigo', kanvaso_objekto_kategorioj.publikigo)
                                kanvaso_objekto_kategorioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kanvaso_objekto_kategorioj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(kanvaso_objekto_kategorioj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(kanvaso_objekto_kategorioj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        kanvaso_objekto_kategorioj.realeco.set(realeco)
                                    else:
                                        kanvaso_objekto_kategorioj.realeco.clear()

                                kanvaso_objekto_kategorioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KanvasoObjektoKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKanvasoObjektoKategorio(status=status, message=message, kanvaso_objekto_kategorioj=kanvaso_objekto_kategorioj)


# Модель типов объектов холстов
class RedaktuKanvasoObjektoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso_objekto_tipoj = graphene.Field(KanvasoObjektoTipoNode, 
        description=_('Созданный/изменённый тип объектов холстов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kanvaso_objekto_tipoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_objekto_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' in kwargs:
                                if not len(kwargs.get('realeco')):
                                    message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                                else:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True, arkivo=False)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )

                        if not message:
                            kanvaso_objekto_tipoj = KanvasoObjektoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(kanvaso_objekto_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(kanvaso_objekto_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    kanvaso_objekto_tipoj.realeco.set(realeco)
                                else:
                                    kanvaso_objekto_tipoj.realeco.clear()

                            kanvaso_objekto_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kanvaso_objekto_tipoj = KanvasoObjektoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kanvasoj.povas_forigi_kanvasoj_kanvaso_objekto_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kanvasoj.povas_shangxi_kanvasoj_kanvaso_objekto_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True, arkivo=False)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                kanvaso_objekto_tipoj.forigo = kwargs.get('forigo', kanvaso_objekto_tipoj.forigo)
                                kanvaso_objekto_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kanvaso_objekto_tipoj.arkivo = kwargs.get('arkivo', kanvaso_objekto_tipoj.arkivo)
                                kanvaso_objekto_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kanvaso_objekto_tipoj.publikigo = kwargs.get('publikigo', kanvaso_objekto_tipoj.publikigo)
                                kanvaso_objekto_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kanvaso_objekto_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(kanvaso_objekto_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(kanvaso_objekto_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        kanvaso_objekto_tipoj.realeco.set(realeco)
                                    else:
                                        kanvaso_objekto_tipoj.realeco.clear()

                                kanvaso_objekto_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KanvasoObjektoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKanvasoObjektoTipo(status=status, message=message, kanvaso_objekto_tipoj=kanvaso_objekto_tipoj)


# Модель статусов объектов холстов
class RedaktuKanvasoObjektoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso_objekto_statusoj = graphene.Field(KanvasoObjektoStatusoNode, 
        description=_('Созданный/изменённый статус объектов холстов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kanvaso_objekto_statusoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_objekto_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            kanvaso_objekto_statusoj = KanvasoObjektoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(kanvaso_objekto_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(kanvaso_objekto_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            kanvaso_objekto_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kanvaso_objekto_statusoj = KanvasoObjektoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kanvasoj.povas_forigi_kanvasoj_kanvaso_objekto_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kanvasoj.povas_shangxi_kanvasoj_kanvaso_objekto_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                kanvaso_objekto_statusoj.forigo = kwargs.get('forigo', kanvaso_objekto_statusoj.forigo)
                                kanvaso_objekto_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kanvaso_objekto_statusoj.arkivo = kwargs.get('arkivo', kanvaso_objekto_statusoj.arkivo)
                                kanvaso_objekto_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kanvaso_objekto_statusoj.publikigo = kwargs.get('publikigo', kanvaso_objekto_statusoj.publikigo)
                                kanvaso_objekto_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kanvaso_objekto_statusoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(kanvaso_objekto_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(kanvaso_objekto_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                kanvaso_objekto_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KanvasoObjektoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKanvasoObjektoStatuso(status=status, message=message, kanvaso_objekto_statusoj=kanvaso_objekto_statusoj)


# сохраняем объекты холстов
def kanvaso_objekto_save(kanvaso_objekto):

    def eventoj():
        KanvasoEventoj.kanvaso_objekto(kanvaso_objekto)

    kanvaso_objekto.save()
    # вызов функции через коммит транзакции обязателен, иначе данные в подписке ещё не изменёнными будут
    transaction.on_commit(eventoj)


# сохраняем объекты холстов
def kanvaso_objekto_posedanto_save(kanvaso_objekto_posedanto):

    def eventoj():
        KanvasoEventoj.kanvaso_objekto_posedanto(kanvaso_objekto_posedanto)

    kanvaso_objekto_posedanto.save()
    # вызов функции через коммит транзакции обязателен, иначе данные в подписке ещё не изменёнными будут
    transaction.on_commit(eventoj)


# помечаем на удаление список объектов и связей холстов и отправляем в подписку
def kanvaso_objektoj_ligiloj_save(kanvaso_id, objektoj_forigo_uuid, ligiloj_objekto_forigo_uuid, posedantoj_objekto_forigo_uuid):

    def eventoj():
        for objekto_uuid in objektoj_forigo_uuid:
            KanvasoEventoj.kanvaso_objekto_uuid(objekto_uuid, kanvaso_id)
        # for ligilo_uuid in ligiloj_objekto_forigo_uuid:
        #     KanvasoEventoj.kanvaso_objekto_ligilo_uuid(ligilo_uuid, kanvaso_id)
        # for posedanto_uuid in posedantoj_objekto_forigo_uuid:
        #     KanvasoEventoj.kanvaso_objekto_posedanto_uuid(posedanto_uuid, kanvaso_id)

    if objektoj_forigo_uuid:
        for_obj = KanvasoObjekto.objects.filter(uuid__in=objektoj_forigo_uuid).update(forigo=True)
    if ligiloj_objekto_forigo_uuid:
        for_obj = KanvasoObjektoLigilo.objects.filter(uuid__in=ligiloj_objekto_forigo_uuid).update(forigo=True)
    if posedantoj_objekto_forigo_uuid:
        for_obj = KanvasoObjektoPosedanto.objects.filter(uuid__in=posedantoj_objekto_forigo_uuid).update(forigo=True)
    # вызов функции через коммит транзакции обязателен, иначе данные в подписке ещё не изменёнными будут
    transaction.on_commit(eventoj)


# выбираем uuid в массив из дерева для пометки на удаление
def array_uuid(tree, objektoj_forigo_uuid, ligiloj_objekto_forigo_uuid, posedantoj_objektoj_forigo_uuid):
    if not len(tree['ch']):
        return objektoj_forigo_uuid, ligiloj_objekto_forigo_uuid, posedantoj_objektoj_forigo_uuid
    for lis in tree['ch']:
        objektoj_forigo_uuid, ligiloj_objekto_forigo_uuid, posedantoj_objektoj_forigo_uuid = array_uuid(
            lis, objektoj_forigo_uuid, ligiloj_objekto_forigo_uuid, posedantoj_objektoj_forigo_uuid)
        objektoj_forigo_uuid.append(lis['uuid'])
        ligiloj_objekto_forigo_uuid.append(lis['kanvasoj_kanvasoobjektoligilo_ligilo__uuid'])
        if lis['kanvasoj_kanvasoobjektoposedanto_kanvaso_objekto__uuid']:
            posedantoj_objektoj_forigo_uuid.append(lis['kanvasoj_kanvasoobjektoposedanto_kanvaso_objekto__uuid'])
    return objektoj_forigo_uuid, ligiloj_objekto_forigo_uuid, posedantoj_objektoj_forigo_uuid


# Модель объектов холстов
class RedaktuKanvasoObjekto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso_objekto = graphene.Field(KanvasoObjektoNode, 
        description=_('Созданный/изменённый объект холста'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        kanvaso_id = graphene.Int(description=_('ID доски, в которую входит объект холста (передаётся либо ID, либо UUID)'))
        kanvaso_uuid = graphene.String(description=_('UUID доски, в которую входит объект холста (передаётся либо ID, либо UUID)'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий объектов холстов'))
        tipo_id = graphene.Int(description=_('Тип объектов холстов'))
        statuso_id = graphene.Int(description=_('Статус объектов холстов'))
        pozicio = graphene.Int(description=_('Позиция в списке'))

        # параметры для создания владельца
        posedanto_uzanto_id = graphene.Int(description=_('Владелец объектов холстов'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец объектов холстов'))
        posedanto_tipo_id = graphene.Int(description=_('Тип владельца объектов холстов'))
        posedanto_statuso_id = graphene.Int(description=_('Статус владельца объектов холстов'))

        # параметры для создания связи с другим объектом
        ligilo_posedanto_uuid = graphene.String(description=_('UUID вышестоящего объекта холста'))
        ligilo_tipo_id = graphene.Int(description=_('Тип связи объектов'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kanvaso_objekto = None
        realeco = None
        kanvaso = None
        kategorio = KanvasoObjektoKategorio.objects.none()
        tipo = None
        statuso = None
        posedanto_uzanto = None
        posedanto_organizo = None
        posedanto_tipo = None
        posedanto_statuso = None
        ligilo_posedanto = None
        ligilo_tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if not message:
                        if 'kanvaso_id' in kwargs and 'kanvaso_uuid' in kwargs:
                            message = '{} "{}" {} "{}"'.format(_('Недопустимо передавать сразу оба параметра'),'kanvaso_id',_('и'),'kanvaso_uuid')
                    if not message:
                        if 'kanvaso_id' not in kwargs and 'kanvaso_uuid' not in kwargs :
                            message = '{} "{}", "{}"'.format(_('Одно из полей обязательно для заполнения:'),'kanvaso_id','kanvaso_uuid')
                        elif 'kanvaso_uuid' in kwargs:
                            try:
                                kanvaso = Kanvaso.objects.get(uuid=kwargs.get('kanvaso_uuid'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except Kanvaso.DoesNotExist:
                                message = _('Неверная доска в которую входит объект холста')
                        else:
                            try:
                                kanvaso = Kanvaso.objects.get(id=kwargs.get('kanvaso_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except Kanvaso.DoesNotExist:
                                message = _('Неверная доска в которую входит объект холста')

                    if uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_objekto', kanvaso):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            if 'kategorio' in kwargs:
                                if not len(kwargs.get('kategorio')):
                                    message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'kategorio')
                                else:
                                    kategorio_id = set(kwargs.get('kategorio'))

                                    if len(kategorio_id):
                                        kategorio = KanvasoObjektoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True, arkivo=False)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие категории объектов холстов'),
                                                        str(dif)
                                            )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = _('Неверный параллельный мир')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = KanvasoObjektoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except KanvasoObjektoTipo.DoesNotExist:
                                    message = _('Неверный тип объектов холстов')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'statuso_id' in kwargs:
                                try:
                                    statuso = KanvasoObjektoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except KanvasoObjektoStatuso.DoesNotExist:
                                    message = _('Неверный статус объектов холстов')

                        # блок проверки параметров для создания сопутствующих моделей
                        # проверка по владельцам
                        if (kwargs.get('posedanto_tipo_id', False)):
                            if not message:
                                try:
                                    posedanto_tipo = KanvasoObjektoPosedantoTipo.objects.get(id=kwargs.get('posedanto_tipo_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                                except KanvasoObjektoPosedantoTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип владельца объектов холстов'),'posedanto_tipo_id')

                            if not message:
                                if 'posedanto_statuso_id' in kwargs:
                                    try:
                                        posedanto_statuso = KanvasoObjektoPosedantoStatuso.objects.get(id=kwargs.get('posedanto_statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KanvasoObjektoPosedantoStatuso.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный статус владельца объектов холстов'),'posedanto_statuso_id')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_statuso_id')

                            if not message:
                                if 'posedanto_uzanto_id' in kwargs:
                                    try:
                                        posedanto_uzanto = Uzanto.objects.get(
                                            id=kwargs.get('posedanto_uzanto_id'), 
                                            is_active=True,
                                            konfirmita=True
                                        )
                                    except Uzanto.DoesNotExist:
                                        message = _('Неверный владелец/пользователь объектов холстов')

                            if not message:
                                if 'posedanto_organizo_uuid' in kwargs:
                                    try:
                                        posedanto_organizo = Organizo.objects.get(
                                            uuid=kwargs.get('posedanto_organizo_uuid'),
                                            forigo=False,
                                            arkivo=False,  
                                            publikigo=True
                                        )
                                    except Organizo.DoesNotExist:
                                        message = _('Неверная организация владелец объектов холстов')
                        # проверка по связи
                        if (kwargs.get('ligilo_posedanto_uuid', False)):
                            if not message:
                                try:
                                    ligilo_posedanto = KanvasoObjekto.objects.get(uuid=kwargs.get('ligilo_posedanto_uuid'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                                except KanvasoObjektoPosedantoStatuso.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект холста'),'ligilo_posedanto_uuid')
                            # проверяем, что оба объекта относятся к одному и тому же канвасу
                            if not message:
                                if ligilo_posedanto.kanvaso.id != kanvaso.id:
                                    message = '{} "{}" != "{}"'.format(_('Соединять карточки можно только из одного канваса'),
                                                                    ligilo_posedanto.kanvaso.id,
                                                                    kanvaso.id
                                                                    )

                            if not message:
                                if 'ligilo_tipo_id' in kwargs:
                                    try:
                                        ligilo_tipo = KanvasoObjektoLigiloTipo.objects.get(id=kwargs.get('ligilo_tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KanvasoObjektoPosedantoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип связи объектов холстов'),'ligilo_tipo_id')
                                else:
                                    # установка параметра типа связи по умолчанию
                                    ligilo_tipo = KanvasoObjektoLigiloTipo.objects.get(id=1, forigo=False,
                                                                    arkivo=False, publikigo=True)
                        elif tipo:
                            #если нет связи на вышестоящий объект, то проверяем что tipo.id=1
                            if tipo.id!=1:
                                message = _('При отсутствии связи на вышестоящий объек, тип объекта холста должен быть равен 1')
                        else: #если нет связи на вышестоящий объект и нет типа то ставим =1
                            try:
                                tipo = KanvasoObjektoTipo.objects.get(id=1, forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except KanvasoObjektoTipo.DoesNotExist:
                                message = _('Не создан первый (id=1) тип объектов холстов')

                        if not message:
                            kanvaso_objekto = KanvasoObjekto.objects.create(
                                forigo=False,
                                arkivo=False,
                                realeco = realeco,
                                kanvaso = kanvaso,
                                tipo = tipo,
                                statuso = statuso,
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato = timezone.now(),
                                pozicio = kwargs.get('pozicio', None),
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(kanvaso_objekto.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(kanvaso_objekto.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'kategorio' in kwargs:
                                if kategorio:
                                    kanvaso_objekto.kategorio.set(kategorio)
                                else:
                                    kanvaso_objekto.kategorio.clear()
                            
                            kanvaso_objekto_save(kanvaso_objekto)

                            status = True

                            # далее блок добавления владельца
                            if (kwargs.get('posedanto_tipo_id', False)):
                                if not message:
                                    kanvaso_objekto_posedantoj = KanvasoObjektoPosedanto.objects.create(
                                        forigo = False,
                                        arkivo = False,
                                        realeco = realeco,
                                        kanvaso_objekto = kanvaso_objekto,
                                        posedanto_uzanto = posedanto_uzanto,
                                        posedanto_organizo = posedanto_organizo,
                                        tipo = posedanto_tipo,
                                        statuso = posedanto_statuso,
                                        publikigo = kwargs.get('publikigo', False),
                                        publikiga_dato = timezone.now()
                                    )

                                    kanvaso_objekto_posedanto_save(kanvaso_objekto_posedantoj)

                                    status = True
                                    message = _('Обе записи созданы')

                            # далее блок создания связи
                            if (kwargs.get('ligilo_posedanto_uuid', False)):
                                if not message:
                                    kanvaso_objekto_ligilo = KanvasoObjektoLigilo.objects.create(
                                        forigo = False,
                                        arkivo = False,
                                        posedanto = ligilo_posedanto,
                                        ligilo = kanvaso_objekto,
                                        tipo = ligilo_tipo,
                                        publikigo = kwargs.get('publikigo', False),
                                        publikiga_dato = timezone.now()
                                    )

                                    kanvaso_objekto_ligilo.save()

                                    status = True
                                    message = _('Записи созданы')

                            if not message:
                                message = _('Запись создана')

                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('kategorio', False)
                            or kwargs.get('tipo_id', False) 
                            or kwargs.get('statuso_id', False) or kwargs.get('pozicio', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kanvaso_objekto = KanvasoObjekto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not (uzanto.has_perm('kanvasoj.povas_forigi_kanvasoj_kanvaso_objekto', kanvaso_objekto))
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not (uzanto.has_perm('kanvasoj.povas_shangxi_kanvasoj_kanvaso_objekto', kanvaso_objekto)):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))
                                    if len(kategorio_id):
                                        kategorio = KanvasoObjektoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True, arkivo=False)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))
                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие категории объектов холстов'),
                                                        str(dif)
                                            )

                            # проверяем наличие записей с таким кодом
                            # kanvaso не меняются. Нужны проверки по правам канвасов, наличие связей, чтобы их не было между разными канвасами.
                            # if not message:
                            #     if 'kanvaso_id' in kwargs and 'kanvaso_uuid' in kwargs:
                            #         message = '{} "{}" {} "{}"'.format(_('Недопустимо передавать сразу оба параметра'),'kanvaso_id',_('и'),'kanvaso_uuid')
                            # if not message:
                            #     if 'kanvaso_uuid' in kwargs:
                            #         try:
                            #             kanvaso = Kanvaso.objects.get(uuid=kwargs.get('kanvaso_uuid'), forigo=False,
                            #                                             arkivo=False, publikigo=True)
                            #         except Kanvaso.DoesNotExist:
                            #             message = _('Неверная доска в которую входит объект холста')
                            #     elif 'kanvaso_id' in kwargs:
                            #         try:
                            #             kanvaso = Kanvaso.objects.get(id=kwargs.get('kanvaso_id'), forigo=False,
                            #                                             arkivo=False, publikigo=True)
                            #         except Kanvaso.DoesNotExist:
                            #             message = _('Неверная доска в которую входит объект холста')

                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = _('Неверный параллельный мир')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = KanvasoObjektoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KanvasoObjektoTipo.DoesNotExist:
                                        message = _('Неверный тип объектов холстов')

                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = KanvasoObjektoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KanvasoObjektoStatuso.DoesNotExist:
                                        message = _('Неверный статус объектов холстов')

                            # при удалении должны быть помечены все нижестоящие объекты и связи к ним так же на удаление
                            if not message and kwargs.get('forigo', False):
                                # выбераем всё нижестоящее дерево и помечаем на удаление
                                # шаг 1 - выбираем uuid все объекты данного канваса, uuid связей при наличии и uuid вышестоящего объекта
                                # строим дерево
                                kanvaso_id = kanvaso.id if kanvaso else kanvaso_objekto.kanvaso.id
                                kanvaso_objektoj = KanvasoObjekto.objects.filter(kanvaso__id=kanvaso_id, forigo=False, publikigo=True, 
                                                                                 arkivo=False).values('uuid', 
                                                                                                      'kanvasoj_kanvasoobjektoligilo_ligilo__uuid', 
                                                                                                      'kanvasoj_kanvasoobjektoligilo_ligilo__posedanto__uuid',
                                                                                                      'kanvasoj_kanvasoobjektoposedanto_kanvaso_objekto__uuid')
                                # добавляем пустой массив детей
                                [ob.update({'ch':[]}) for ob in kanvaso_objektoj]
                                # делаем список с uuid объектов в виде ключей, а полями заполняем поля запроса
                                n1 = {}
                                [n1.update({tr['uuid']:tr}) for tr in kanvaso_objektoj]
                                # в детей складываем всех, у кого есть ссылки на родителей согласно их ссылок, получаем список всех существующих веток
                                [n1[tr['kanvasoj_kanvasoobjektoligilo_ligilo__posedanto__uuid']].get('ch',[]).append(tr)
                                 for tr in kanvaso_objektoj if tr['kanvasoj_kanvasoobjektoligilo_ligilo__posedanto__uuid']]
                                # в n1 теперь хранится дерево с каждой веткой и подветкой отдельно
                                # удалить все подветки, оставив только главные:
                                #    [n1.pop(tr['uuid']) for tr in k if tr['kanvasoj_kanvasoobjektoligilo_ligilo__posedanto__uuid']]
                                # объявляем массивы для списка удаляемых записей
                                objektoj_forigo_uuid = []
                                posedantoj_objektoj_forigo_uuid = []
                                if n1[kanvaso_objekto.uuid]['kanvasoj_kanvasoobjektoligilo_ligilo__uuid']:
                                    ligiloj_objekto_forigo_uuid = [n1[kanvaso_objekto.uuid]['kanvasoj_kanvasoobjektoligilo_ligilo__uuid']]
                                else:
                                    ligiloj_objekto_forigo_uuid = []
                                # теперь находим удаляемую ветку и выбираем из неё uuid объектов и связей для пометки на удаление
                                objektoj_forigo_uuid, ligiloj_objekto_forigo_uuid, posedantoj_objektoj_forigo_uuid = array_uuid(n1[kanvaso_objekto.uuid], objektoj_forigo_uuid, ligiloj_objekto_forigo_uuid, posedantoj_objektoj_forigo_uuid)
                                # изменяем базу и отправляем в подписку изменённые записи
                                kanvaso_objektoj_ligiloj_save(kanvaso_id, objektoj_forigo_uuid, ligiloj_objekto_forigo_uuid, posedantoj_objektoj_forigo_uuid)

                            if not message:

                                kanvaso_objekto.forigo = kwargs.get('forigo', kanvaso_objekto.forigo)
                                kanvaso_objekto.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kanvaso_objekto.arkivo = kwargs.get('arkivo', kanvaso_objekto.arkivo)
                                kanvaso_objekto.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kanvaso_objekto.publikigo = kwargs.get('publikigo', kanvaso_objekto.publikigo)
                                kanvaso_objekto.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kanvaso_objekto.realeco = realeco if kwargs.get('realeco_id', False) else kanvaso_objekto.realeco
                                kanvaso_objekto.kanvaso = kanvaso if kanvaso else kanvaso_objekto.kanvaso
                                kanvaso_objekto.statuso = statuso if kwargs.get('statuso_id', False) else kanvaso_objekto.statuso
                                kanvaso_objekto.tipo = tipo if kwargs.get('tipo_id', False) else kanvaso_objekto.tipo
                                kanvaso_objekto.pozicio = kwargs.get('pozicio', kanvaso_objekto.pozicio)

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(kanvaso_objekto.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(kanvaso_objekto.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        kanvaso_objekto.kategorio.set(kategorio)
                                    else:
                                        kanvaso_objekto.kategorio.clear()

                                kanvaso_objekto_save(kanvaso_objekto)
                                status = True

                                message = _('Запись успешно изменена')
                        except KanvasoObjekto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKanvasoObjekto(status=status, message=message, kanvaso_objekto=kanvaso_objekto)


# Модель типов владельцев объектов холстов
class RedaktuKanvasoObjektoPosedantoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso_objekto_posedantoj_tipoj = graphene.Field(KanvasoObjektoPosedantoTipoNode, 
        description=_('Созданный/изменённый тип владельцев объектов холстов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kanvaso_objekto_posedantoj_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_objekto_posedantoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            kanvaso_objekto_posedantoj_tipoj = KanvasoObjektoPosedantoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(kanvaso_objekto_posedantoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(kanvaso_objekto_posedantoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            kanvaso_objekto_posedantoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kanvaso_objekto_posedantoj_tipoj = KanvasoObjektoPosedantoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kanvasoj.povas_forigi_kanvasoj_kanvaso_objekto_posedantoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kanvasoj.povas_shangxi_kanvasoj_kanvaso_objekto_posedantoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                kanvaso_objekto_posedantoj_tipoj.forigo = kwargs.get('forigo', kanvaso_objekto_posedantoj_tipoj.forigo)
                                kanvaso_objekto_posedantoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kanvaso_objekto_posedantoj_tipoj.arkivo = kwargs.get('arkivo', kanvaso_objekto_posedantoj_tipoj.arkivo)
                                kanvaso_objekto_posedantoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kanvaso_objekto_posedantoj_tipoj.publikigo = kwargs.get('publikigo', kanvaso_objekto_posedantoj_tipoj.publikigo)
                                kanvaso_objekto_posedantoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kanvaso_objekto_posedantoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(kanvaso_objekto_posedantoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(kanvaso_objekto_posedantoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                kanvaso_objekto_posedantoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KanvasoObjektoPosedantoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKanvasoObjektoPosedantoTipo(status=status, message=message, kanvaso_objekto_posedantoj_tipoj=kanvaso_objekto_posedantoj_tipoj)


# Модель статусов владельцев объектов холстов
class RedaktuKanvasoObjektoPosedantoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso_objekto_posedantoj_statusoj = graphene.Field(KanvasoObjektoPosedantoStatusoNode, 
        description=_('Созданный/изменённый статус владельца объектов холстов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kanvaso_objekto_posedantoj_statusoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_objekto_posedantoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            kanvaso_objekto_posedantoj_statusoj = KanvasoObjektoPosedantoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(kanvaso_objekto_posedantoj_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(kanvaso_objekto_posedantoj_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            kanvaso_objekto_posedantoj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kanvaso_objekto_posedantoj_statusoj = KanvasoObjektoPosedantoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kanvasoj.povas_forigi_kanvasoj_kanvaso_objekto_posedantoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kanvasoj.povas_shangxi_kanvasoj_kanvaso_objekto_posedantoj_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                kanvaso_objekto_posedantoj_statusoj.forigo = kwargs.get('forigo', kanvaso_objekto_posedantoj_statusoj.forigo)
                                kanvaso_objekto_posedantoj_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kanvaso_objekto_posedantoj_statusoj.arkivo = kwargs.get('arkivo', kanvaso_objekto_posedantoj_statusoj.arkivo)
                                kanvaso_objekto_posedantoj_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kanvaso_objekto_posedantoj_statusoj.publikigo = kwargs.get('publikigo', kanvaso_objekto_posedantoj_statusoj.publikigo)
                                kanvaso_objekto_posedantoj_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kanvaso_objekto_posedantoj_statusoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(kanvaso_objekto_posedantoj_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(kanvaso_objekto_posedantoj_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                kanvaso_objekto_posedantoj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KanvasoObjektoPosedantoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKanvasoObjektoPosedantoStatuso(status=status, message=message, kanvaso_objekto_posedantoj_statusoj=kanvaso_objekto_posedantoj_statusoj)


# Модель владельцев объектов холстов
class RedaktuKanvasoObjektoPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso_objekto_posedantoj = graphene.Field(KanvasoObjektoPosedantoNode, 
        description=_('Созданный/изменённый владелец объектов холстов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        kanvaso_objekto_uuid = graphene.String(description=_('Объект холста'))
        posedanto_uzanto_id = graphene.Int(description=_('Владелец объектов холстов'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец объектов холстов'))
        tipo_id = graphene.Int(description=_('Тип владельца объектов холстов'))
        statuso_id = graphene.Int(description=_('Статус владельца объектов холстов'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kanvaso_objekto_posedantoj = None
        realeco = None
        kanvaso_objekto = None
        posedanto_uzanto = None
        posedanto_organizo = None
        tipo = None
        statuso = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if not message:
                        if 'kanvaso_objekto_uuid' in kwargs:
                            try:
                                kanvaso_objekto = KanvasoObjekto.objects.get(uuid=kwargs.get('kanvaso_objekto_uuid'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except KanvasoObjekto.DoesNotExist:
                                message = _('Неверный объект холста')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kanvaso_objekto_uuid')

                    if uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_objekto_posedantoj', kanvaso_objekto):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_uzanto_id' in kwargs:
                                try:
                                    posedanto_uzanto = Uzanto.objects.get(
                                        id=kwargs.get('posedanto_uzanto_id'), 
                                        is_active=True,
                                        konfirmita=True
                                    )
                                except Uzanto.DoesNotExist:
                                    message = _('Неверный владелец/пользователь объектов холстов')

                        if not message:
                            if 'posedanto_organizo_uuid' in kwargs:
                                try:
                                    posedanto_organizo = Organizo.objects.get(
                                        uuid=kwargs.get('posedanto_organizo_uuid'),
                                        forigo=False,
                                        arkivo=False,  
                                        publikigo=True
                                    )
                                except Organizo.DoesNotExist:
                                    message = _('Неверная организация владелец объектов холстов')

                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = _('Неверный параллельный мир')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = KanvasoObjektoPosedantoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except KanvasoObjektoPosedantoTipo.DoesNotExist:
                                    message = _('Неверный тип владельца объектов холстов')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'statuso_id' in kwargs:
                                try:
                                    statuso = KanvasoObjektoPosedantoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except KanvasoObjektoPosedantoStatuso.DoesNotExist:
                                    message = _('Неверный статус владельца объектов холстов')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')

                        if not message:
                            kanvaso_objekto_posedantoj = KanvasoObjektoPosedanto.objects.create(
                                forigo = False,
                                arkivo = False,
                                realeco = realeco,
                                kanvaso_objekto = kanvaso_objekto,
                                posedanto_uzanto = posedanto_uzanto,
                                posedanto_organizo = posedanto_organizo,
                                tipo = tipo,
                                statuso = statuso,
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato = timezone.now()
                            )

                            kanvaso_objekto_posedanto_save(kanvaso_objekto_posedantoj)

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('posedanto_uzanto_id', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('posedanto_organizo_uuid', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('kanvaso_objekto_uuid', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kanvaso_objekto_posedantoj = KanvasoObjektoPosedanto.objects.get(uuid=kwargs.get('uuid'), forigo=False)

                            if (not (uzanto.has_perm('kanvasoj.povas_forigi_kanvasoj_kanvaso_objekto_posedantoj', kanvaso_objekto_posedantoj))
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not (uzanto.has_perm('kanvasoj.povas_shangxi_kanvasoj_kanvaso_objekto_posedantoj', kanvaso_objekto_posedantoj)):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_uzanto_id' in kwargs:
                                    try:
                                        posedanto_uzanto = Uzanto.objects.get(
                                            id=kwargs.get('posedanto_uzanto_id'), 
                                            is_active=True,
                                            konfirmita=True
                                        )
                                    except Uzanto.DoesNotExist:
                                        message = _('Неверный владелец/пользователь объектов холстов')

                            if not message:
                                if 'posedanto_organizo_uuid' in kwargs:
                                    try:
                                        posedanto_organizo = Organizo.objects.get(
                                            uuid=kwargs.get('posedanto_organizo_uuid'),
                                            forigo=False,
                                            arkivo=False,  
                                            publikigo=True
                                        )
                                    except Organizo.DoesNotExist:
                                        message = _('Неверная организация владелец объектов холстов')

                            if not message:
                                if 'kanvaso_objekto_uuid' in kwargs:
                                    try:
                                        kanvaso_objekto = KanvasoObjekto.objects.get(uuid=kwargs.get('kanvaso_objekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KanvasoObjekto.DoesNotExist:
                                        message = _('Неверный объект холста')

                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = _('Неверный параллельный мир')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = KanvasoObjektoPosedantoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KanvasoObjektoPosedantoTipo.DoesNotExist:
                                        message = _('Неверный тип владельца объектов холстов')

                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = KanvasoObjektoPosedantoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KanvasoObjektoPosedantoStatuso.DoesNotExist:
                                        message = _('Неверный статус владельца объектов холстов')

                            if not message:

                                kanvaso_objekto_posedantoj.forigo = kwargs.get('forigo', kanvaso_objekto_posedantoj.forigo)
                                kanvaso_objekto_posedantoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kanvaso_objekto_posedantoj.arkivo = kwargs.get('arkivo', kanvaso_objekto_posedantoj.arkivo)
                                kanvaso_objekto_posedantoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kanvaso_objekto_posedantoj.publikigo = kwargs.get('publikigo', kanvaso_objekto_posedantoj.publikigo)
                                kanvaso_objekto_posedantoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kanvaso_objekto_posedantoj.realeco = realeco if kwargs.get('realeco_id', False) else kanvaso_objekto_posedantoj.realeco
                                kanvaso_objekto_posedantoj.kanvaso_objekto = realeco if kwargs.get('kanvaso_objekto_uuid', False) else kanvaso_objekto_posedantoj.kanvaso_objekto
                                kanvaso_objekto_posedantoj.posedanto_uzanto = posedanto_uzanto if kwargs.get('posedanto_uzanto_id', False) else kanvaso_objekto_posedantoj.posedanto_uzanto
                                kanvaso_objekto_posedantoj.posedanto_organizo = posedanto_organizo if kwargs.get('posedanto_organizo_uuid', False) else kanvaso_objekto_posedantoj.posedanto_organizo
                                kanvaso_objekto_posedantoj.statuso = statuso if kwargs.get('statuso_id', False) else kanvaso_objekto_posedantoj.statuso
                                kanvaso_objekto_posedantoj.tipo = tipo if kwargs.get('tipo_id', False) else kanvaso_objekto_posedantoj.tipo

                                kanvaso_objekto_posedanto_save(kanvaso_objekto_posedantoj)
                                status = True
                                message = _('Запись успешно изменена')
                        except KanvasoObjektoPosedanto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKanvasoObjektoPosedanto(status=status, message=message, kanvaso_objekto_posedantoj=kanvaso_objekto_posedantoj)


# Модель добавления множества объектов холстов с их владельцами
class RedaktuKreiKanvasoObjektoPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso_objektoj = graphene.Field(graphene.List(KanvasoObjektoNode), 
        description=_('Создание списка объектов холстов с владельцами'))

    class Arguments:
        # параметры, одинаковые для всех объектов холстов
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        kanvaso_uuid = graphene.String(description=_('Доска в которую входит объект холста'))
        tipo_id = graphene.Int(description=_('Тип объектов холстов'))
        # параметры объектов холстов в перечне (могут быть разные для разных объектов холстов)
        nomo = graphene.List(graphene.String, description=_('Название'))
        priskribo = graphene.List(graphene.String, description=_('Описание'))
        kategorio = graphene.List(graphene.List(graphene.Int, description=_('Список категорий объектов холстов')))
        statuso_id = graphene.List(graphene.Int, description=_('Статус объектов холстов'))
        pozicio = graphene.List(graphene.Int, description=_('Позиция в списке'))

        # Владелец объектов холстов
        posedanto_tipo_id = graphene.Int(description=_('Тип владельца объектов холстов'))
        posedanto_statuso_id = graphene.Int(description=_('Статус владельца объектов холстов'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kanvaso_objektoj = []
        kanvaso_objekto = None
        realeco = None
        kanvaso = None
        kategorio = KanvasoObjektoKategorio.objects.none()
        tipo = None
        statuso = None
        posedanto_tipo = None
        posedanto_statuso = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                if uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_objekto'):
                    # Проверяем наличие всех полей
                    if not message:
                        if not kwargs.get('nomo'):
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                    if not message:
                        if not kwargs.get('priskribo'):
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                    if not message:
                        if 'kategorio' not in kwargs:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                        elif not len(kwargs.get('kategorio')):
                            message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'kategorio')
                        else:
                            mas_kategorio = []
                            for kat in kwargs.get('kategorio'):
                                mas_kategorio.extend(kat)
                            kategorio_id = set(mas_kategorio)

                            if len(kategorio_id):
                                kategorio = KanvasoObjektoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True, arkivo=False)
                                dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                if len(dif):
                                    message='{}: {}'.format(
                                                _('Указаны несуществующие категории объектов холстов'),
                                                str(dif)
                                    )

                    # проверяем наличие записей с таким кодом
                    if not message:
                        if 'realeco_id' in kwargs:
                            try:
                                realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except Realeco.DoesNotExist:
                                message = _('Неверный параллельный мир')

                    if not message:
                        if 'kanvaso_uuid' in kwargs:
                            try:
                                kanvaso = Kanvaso.objects.get(uuid=kwargs.get('kanvaso_uuid'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except Kanvaso.DoesNotExist:
                                message = _('Неверная доска в которую входят объекты холстов')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kanvaso_uuid')

                    if not message:
                        if 'tipo_id' in kwargs:
                            try:
                                tipo = KanvasoObjektoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except KanvasoObjektoTipo.DoesNotExist:
                                message = _('Неверный тип объектов холстов')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')


                    if not message:
                        if 'posedanto_tipo_id' in kwargs:
                            try:
                                posedanto_tipo = KanvasoObjektoPosedantoTipo.objects.get(id=kwargs.get('posedanto_tipo_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                            except KanvasoObjektoPosedantoTipo.DoesNotExist:
                                message = _('Неверный тип владельца объектов холстов')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_tipo_id')

                    if not message:
                        if 'posedanto_statuso_id' in kwargs:
                            try:
                                posedanto_statuso = KanvasoObjektoPosedantoStatuso.objects.get(id=kwargs.get('posedanto_statuso_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                            except KanvasoObjektoPosedantoStatuso.DoesNotExist:
                                message = _('Неверный статус владельца объектов холстов')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_statuso_id')


                    if not kwargs.get('pozicio',False):
                        message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'pozicio')

                    if (not message) and kwargs.get('pozicio',False):
                        pozicioj = kwargs.get('pozicio')
                        statusoj = kwargs.get('statuso_id')
                        i = 0
                        for pozic in pozicioj:
                            kanvaso_objekto = None
                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = KanvasoObjektoStatuso.objects.get(id=statusoj[i], forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KanvasoObjektoStatuso.DoesNotExist:
                                        message = _('Неверный статус объектов холстов')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')
                            else:
                                break

                            if not message:
                                kanvaso_objekto = KanvasoObjekto.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    realeco = realeco,
                                    kanvaso = kanvaso,
                                    tipo = tipo,
                                    statuso = statuso,
                                    publikigo = True,
                                    publikiga_dato = timezone.now(),
                                    pozicio = pozic,
                                )

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(kanvaso_objekto.nomo, kwargs.get('nomo')[i], info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(kanvaso_objekto.priskribo, 
                                            kwargs.get('priskribo')[i], 
                                            info.context.LANGUAGE_CODE)
                                # категории брать из параметров
                                if ('kategorio' in kwargs) and (kwargs.get('kategorio')[i]):
                                    kategorio = KanvasoObjektoKategorio.objects.none()

                                    kategorio_id = set(kwargs.get('kategorio')[i])
                                    if len(kategorio_id):
                                        kategorio = KanvasoObjektoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True, arkivo=False)

                                    if kategorio:
                                        kanvaso_objekto.kategorio.set(kategorio)
                                    else:
                                        kanvaso_objekto.kategorio.clear()

                                kanvaso_objekto_save(kanvaso_objekto)
                                kanvaso_objektoj.append(kanvaso_objekto)

                                if not message:
                                    kanvaso_objekto_posedantoj = KanvasoObjektoPosedanto.objects.create(
                                        forigo = False,
                                        arkivo = False,
                                        realeco = realeco,
                                        kanvaso_objekto = kanvaso_objekto,
                                        tipo = posedanto_tipo,
                                        statuso = posedanto_statuso,
                                        publikigo = True,
                                        publikiga_dato = timezone.now()
                                    )

                                    kanvaso_objekto_posedanto_save(kanvaso_objekto_posedantoj)

                                status = True
                            
                            i += 1

                        if status:
                            message = _('Запись создана')
                else:
                    message = _('Недостаточно прав')
        else:
            message = _('Требуется авторизация')

        return RedaktuKreiKanvasoObjektoPosedanto(status=status, message=message, kanvaso_objektoj=kanvaso_objektoj)


# Модель добавления холста с владельцем и множества объектов холстов с их владельцами
class RedaktuKreiKanvasoKanvasoObjektoPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso = graphene.Field(KanvasoNode, 
        description=_('Создание доски с владельцем и списка объектов холстов с владельцами'))
    kanvaso_objektoj = graphene.Field(graphene.List(KanvasoObjektoNode), 
        description=_('Создание списка объектов холстов с владельцами'))

    class Arguments:
        # параметры, одинаковые для доски и всех объектов холстов
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        # параметры для доски
        prj_nomo = graphene.String(description=_('Название'))
        prj_priskribo = graphene.String(description=_('Описание'))
        prj_kategorio = graphene.List(graphene.Int,description=_('Список категорий досок'))
        prj_tipo_id = graphene.Int(description=_('Тип досок'))
        prj_statuso_id = graphene.Int(description=_('Статус досок'))
        # параметры для создания владельца доски
        prj_posedanto_uzanto_id = graphene.Int(description=_('ID владельца-лица доски'))
        prj_posedanto_organizo_uuid = graphene.String(description=_('Организация-владелец доски'))
        prj_posedanto_tipo_id = graphene.Int(description=_('Тип владельца доскт'))
        prj_posedanto_statuso_id = graphene.Int(description=_('Статус владельца доски'))
        # параметры, одинаковые для всех объектов холстов
        tipo_id = graphene.Int(description=_('Тип объектов холстов'))
        # параметры объектов холстов в перечне (могут быть разные для разных объектов холстов)
        nomo = graphene.List(graphene.String, description=_('Название'))
        priskribo = graphene.List(graphene.String, description=_('Описание'))
        kategorio = graphene.List(graphene.List(graphene.Int, description=_('Список категорий объектов холстов')))
        statuso_id = graphene.List(graphene.Int, description=_('Статус объектов холстов'))
        pozicio = graphene.List(graphene.Int, description=_('Позиция в списке'))

        # владельцев может быть много
        posedanto_tipo_id = graphene.List(graphene.Int, description=_('Тип владельца объектов холстов'))
        posedanto_statuso_id = graphene.List(graphene.Int, description=_('Статус владельца объектов холстов'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        message_krei = None
        kanvaso = None
        posedanto_organizo = None
        posedanto_uzanto = None
        kanvaso_objektoj = []
        kanvaso_objekto = None
        realeco = None
        kategorio = KanvasoObjektoKategorio.objects.none()
        prj_kategorio = KanvasoKategorio.objects.none()
        tipo = None
        statuso = None
        posedanto_tipo = None
        posedanto_statuso = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                if not kwargs.get('pozicio',False):
                    message = '{} "{}"'.format(_('Поле обязательно для заполнения (должен быть хотя бы один объект холста)'),'pozicio')
                if uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso') :
                    # Проверяем наличие всех полей
                    if not message:
                        if not kwargs.get('prj_nomo'):
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'prj_nomo')

                    if not message:
                        if 'prj_kategorio' not in kwargs:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'prj_kategorio')
                        elif not len(kwargs.get('prj_kategorio')):
                            message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'prj_kategorio')
                        else:
                            kategorio_id = set(kwargs.get('prj_kategorio'))

                            if len(kategorio_id):
                                prj_kategorio = KanvasoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True, arkivo=False)
                                dif = set(kategorio_id) - set(prj_kategorio.values_list('id', flat=True))

                                if len(dif):
                                    message='{}: {}'.format(
                                                _('Указаны несуществующие категории досок'),
                                                str(dif)
                                    )

                    # проверяем наличие записей с таким кодом
                    if not message:
                        if 'realeco_id' in kwargs:
                            try:
                                realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except Realeco.DoesNotExist:
                                message = _('Неверный параллельный мир')

                    if not message:
                        if 'prj_tipo_id' in kwargs:
                            try:
                                tipo = KanvasoTipo.objects.get(id=kwargs.get('prj_tipo_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except KanvasoTipo.DoesNotExist:
                                message = _('Неверный тип досок')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'prj_tipo_id')

                    if not message:
                        if 'prj_statuso_id' in kwargs:
                            try:
                                statuso = KanvasoStatuso.objects.get(id=kwargs.get('prj_statuso_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except KanvasoStatuso.DoesNotExist:
                                message = _('Неверный статус досок')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'prj_statuso_id')

                    if not message:
                        kanvaso = Kanvaso.objects.create(
                            forigo=False,
                            arkivo=False,
                            realeco = realeco,
                            tipo = tipo,
                            statuso = statuso,
                            publikigo = True,
                            publikiga_dato = timezone.now()
                        )

                        if (kwargs.get('prj_nomo', False)):
                            set_enhavo(kanvaso.nomo, kwargs.get('prj_nomo'), info.context.LANGUAGE_CODE)
                        if (kwargs.get('prj_priskribo', False)):
                            set_enhavo(kanvaso.priskribo, 
                                        kwargs.get('prj_priskribo'), 
                                        info.context.LANGUAGE_CODE)
                        if 'prj_kategorio' in kwargs:
                            if prj_kategorio:
                                kanvaso.kategorio.set(prj_kategorio)
                            else:
                                kanvaso.kategorio.clear()

                        kanvaso_save(kanvaso)

                        status = True
                        # далее блок добавления владельца
                        if (kwargs.get('prj_posedanto_uzanto_id', False) or kwargs.get('prj_posedanto_organizo_uuid', False)):
                            if not message:
                                if 'prj_posedanto_uzanto_id' in kwargs:
                                    try:
                                        posedanto_uzanto = Uzanto.objects.get(
                                            id=kwargs.get('prj_posedanto_uzanto_id'), 
                                            is_active=True,
                                            konfirmita=True
                                        )

                                    except Uzanto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Несуществующий пользователь доски'),'prj_posedanto_uzanto_id')

                            if not message:
                                if 'prj_posedanto_tipo_id' in kwargs:
                                    try:
                                        posedanto_tipo = KanvasoPosedantoTipo.objects.get(id=kwargs.get('prj_posedanto_tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KanvasoPosedantoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип владельца доски'),'prj_posedanto_tipo_id')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'prj_posedanto_tipo_id')

                            if not message:
                                if 'prj_posedanto_statuso_id' in kwargs:
                                    try:
                                        posedanto_statuso = KanvasoPosedantoStatuso.objects.get(id=kwargs.get('prj_posedanto_statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KanvasoPosedantoStatuso.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный статус владельца доски'),'prj_posedanto_statuso_id')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'prj_posedanto_statuso_id')

                            if not message:
                                if 'prj_posedanto_organizo_uuid' in kwargs:
                                    try:
                                        posedanto_organizo = Organizo.objects.get(
                                            uuid=kwargs.get('prj_posedanto_organizo_uuid'),
                                            forigo=False,
                                            arkivo=False,  
                                            publikigo=True
                                        )
                                    except Organizo.DoesNotExist:
                                        message = _('Неверная организация владелец доски')

                            if not message:
                                kanvaso_posedantoj = KanvasoPosedanto.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    realeco=realeco,
                                    kanvaso=kanvaso,
                                    posedanto_organizo = posedanto_organizo,
                                    posedanto_uzanto = posedanto_uzanto,
                                    tipo = posedanto_tipo,
                                    statuso = posedanto_statuso,
                                    publikigo = True,
                                    publikiga_dato = timezone.now()
                                )

                                kanvaso_posedantoj.save()

                                status = True
                                message_krei = _('Записи доски и владельца созданы')

                        if not message_krei:
                            message_krei = _('Запись доски создана')

                pozicioj = kwargs.get('pozicio')
                if pozicioj and kanvaso and (not message) and (uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_objekto')):
                    # Проверяем наличие всех полей
                    if not message:
                        if not kwargs.get('nomo'):
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')
                        else:
                            if len(kwargs.get('nomo'))<len(pozicioj):
                                message = '{} "{} < {}"'.format(_('количество наименований меньше количества позиций'),len(kwargs.get('nomo')),len(pozicioj))


                    if not message:
                        if not kwargs.get('priskribo'):
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')
                        else:
                            if len(kwargs.get('priskribo'))<len(pozicioj):
                                message = '{} "{} < {}"'.format(_('количество описаний меньше количества позиций'),len(kwargs.get('priskribo')),len(pozicioj))

                    if not message:
                        if 'kategorio' not in kwargs:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                        elif len(kwargs.get('kategorio'))<len(pozicioj):
                            message = '{} "{} < {}"'.format(_('количество категорий меньше количества позиций'),len(kwargs.get('kategorio')),len(pozicioj))
                        else:
                            mas_kategorio = []
                            for kat in kwargs.get('kategorio'):
                                mas_kategorio.extend(kat)
                            kategorio_id = set(mas_kategorio)

                            if len(kategorio_id):
                                kategorio = KanvasoObjektoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True, arkivo=False)
                                dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                if len(dif):
                                    message='{}: {}'.format(
                                                _('Указаны несуществующие категории объектов холстов'),
                                                str(dif)
                                    )

                    # проверяем наличие записей с таким кодом
                    tipo = None
                    if not message:
                        if 'tipo_id' in kwargs:
                            try:
                                tipo = KanvasoObjektoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except KanvasoObjektoTipo.DoesNotExist:
                                message = _('Неверный тип объектов холстов')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                    if not message:
                        kanvaso_objekto_posedanto_tipo_id = set(kwargs.get('posedanto_tipo_id',[]))
                        if len(kanvaso_objekto_posedanto_tipo_id):
                            kanvaso_objekto_posedanto_tipo = KanvasoObjektoPosedantoTipo.objects.filter(id__in=kanvaso_objekto_posedanto_tipo_id, 
                                                                                                        forigo=False, arkivo=False, publikigo=True)
                            set_value = set(kanvaso_objekto_posedanto_tipo.values_list('id', flat=True))
                            arr_value = []
                            for s in set_value:
                                arr_value.append(s)
                            dif = set(kanvaso_objekto_posedanto_tipo_id) - set(arr_value)
                            if len(dif):
                                message='{}: {}'.format(
                                                _('Указаны несуществующие типы владельцев'),
                                                str(dif)
                                )
                        else:
                            message='{}: {}'.format(
                                            _('Не указаны типы владельцев'),
                                            'posedanto_tipo_id'
                            )

                    if not message:
                        posedanto_statuso_id = set(kwargs.get('posedanto_statuso_id',[]))
                        if len(posedanto_statuso_id):
                            posedanto_statuso = KanvasoObjektoPosedantoStatuso.objects.filter(id__in=posedanto_statuso_id, forigo=False, arkivo=False, publikigo=True)
                            set_value = set(posedanto_statuso.values_list('id', flat=True))
                            arr_value = []
                            for s in set_value:
                                arr_value.append(s)
                            dif = set(posedanto_statuso_id) - set(arr_value)
                            if len(dif):
                                message='{}: {}'.format(
                                                _('Указаны несуществующие статусы владельцев'),
                                                str(dif)
                                )
                            elif len(posedanto_statuso_id)<len(kanvaso_objekto_posedanto_tipo):
                                message='{}: {}<{}'.format(
                                                _('количество статусов владельцев меньше количества типов владельцев объектов холстов'),
                                                len(posedanto_statuso_id),
                                                len(kanvaso_objekto_posedanto_tipo)
                                )
                        else:
                            message='{}: {}'.format(
                                            _('Не указаны статусы владельцев'),
                                            'posedanto_statuso_id'
                            )

                    if (not message) and (not kwargs.get('pozicio',False)):
                        message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'pozicio')

                    statusoj = kwargs.get('statuso_id')
                    if len(statusoj)<len(pozicioj):
                        message = '{} "{} < {}"'.format(_('количество статусов меньше количества позиций'),len(statusoj),len(pozicioj))
                    if (not message) and kwargs.get('pozicio',False):
                        i = 0
                        statuso = None
                        status = False
                        for pozic in pozicioj:
                            kanvaso_objekto = None
                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = KanvasoObjektoStatuso.objects.get(id=statusoj[i], forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KanvasoObjektoStatuso.DoesNotExist:
                                        message = _('Неверный статус объектов холстов')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')
                                    break
                            else:
                                break

                            if not message:
                                kanvaso_objekto = KanvasoObjekto.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    realeco = realeco,
                                    kanvaso = kanvaso,
                                    tipo = tipo,
                                    statuso = statuso,
                                    publikigo = True,
                                    publikiga_dato = timezone.now(),
                                    pozicio = pozic,
                                )

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(kanvaso_objekto.nomo, kwargs.get('nomo')[i], info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(kanvaso_objekto.priskribo, 
                                            kwargs.get('priskribo')[i], 
                                            info.context.LANGUAGE_CODE)
                                # категории брать из параметров
                                if ('kategorio' in kwargs) and (kwargs.get('kategorio')[i]):
                                    kategorio = KanvasoObjektoKategorio.objects.none()

                                    kategorio_id = set(kwargs.get('kategorio')[i])
                                    if len(kategorio_id):
                                        kategorio = KanvasoObjektoKategorio.objects.filter(id__in=kategorio_id, 
                                                                                           forigo=False, publikigo=True, arkivo=False)

                                    if kategorio:
                                        kanvaso_objekto.kategorio.set(kategorio)
                                    else:
                                        kanvaso_objekto.kategorio.clear()

                                kanvaso_objekto_save(kanvaso_objekto)
                                kanvaso_objektoj.append(kanvaso_objekto)

                                for posedanto_tipo in kanvaso_objekto_posedanto_tipo:
                                    kanvaso_objekto_posedantoj = KanvasoObjektoPosedanto.objects.create(
                                        forigo = False,
                                        arkivo = False,
                                        realeco = realeco,
                                        kanvaso_objekto = kanvaso_objekto,
                                        tipo = posedanto_tipo,
                                        statuso = posedanto_statuso[i],
                                        publikigo = True,
                                        publikiga_dato = timezone.now()
                                    )
                                    kanvaso_objekto_posedanto_save(kanvaso_objekto_posedantoj)
                                status = True
                            i += 1
                            if message:
                                break

                        if status and not message:
                            message_krei = '{}, {}'.format(message_krei,_('Запись объектов холстов создана'))
                elif (not kanvaso) and (not message):
                    message = _('Доска не создано')
                elif not (uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_objekto')) and (not message):
                    message = _('Недостаточно прав')
                if not message:
                    message = message_krei
                elif message_krei:
                    message = "{}, и ошибка - {}".format(message_krei, message)
        else:
            message = _('Требуется авторизация')

        return RedaktuKreiKanvasoKanvasoObjektoPosedanto(
            status=status, 
            message=message, 
            kanvaso=kanvaso, 
            kanvaso_objektoj=kanvaso_objektoj
        )


# Модель типов связей объектов холстов между собой
class RedaktuKanvasoObjektoLigiloTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso_objekto_ligilo_tipoj = graphene.Field(KanvasoObjektoLigiloTipoNode, 
        description=_('Созданный/изменённый тип связей объектов холстов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kanvaso_objekto_ligilo_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_objekto_ligilo_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            kanvaso_objekto_ligilo_tipoj = KanvasoObjektoLigiloTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(kanvaso_objekto_ligilo_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(kanvaso_objekto_ligilo_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            kanvaso_objekto_ligilo_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kanvaso_objekto_ligilo_tipoj = KanvasoObjektoLigiloTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kanvasoj.povas_forigi_kanvasoj_kanvaso_objekto_ligilo_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kanvasoj.povas_shangxi_kanvasoj_kanvaso_objekto_ligilo_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                kanvaso_objekto_ligilo_tipoj.forigo = kwargs.get('forigo', kanvaso_objekto_ligilo_tipoj.forigo)
                                kanvaso_objekto_ligilo_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kanvaso_objekto_ligilo_tipoj.arkivo = kwargs.get('arkivo', kanvaso_objekto_ligilo_tipoj.arkivo)
                                kanvaso_objekto_ligilo_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kanvaso_objekto_ligilo_tipoj.publikigo = kwargs.get('publikigo', kanvaso_objekto_ligilo_tipoj.publikigo)
                                kanvaso_objekto_ligilo_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kanvaso_objekto_ligilo_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(kanvaso_objekto_ligilo_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(kanvaso_objekto_ligilo_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                kanvaso_objekto_ligilo_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KanvasoObjektoLigiloTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKanvasoObjektoLigiloTipo(status=status, message=message, kanvaso_objekto_ligilo_tipoj=kanvaso_objekto_ligilo_tipoj)


# сохраняем объекты холстов
def kanvaso_objekto_ligilo_save(kanvaso_objekto_ligilo):

    def eventoj():
        KanvasoEventoj.kanvaso_objekto_ligilo(kanvaso_objekto_ligilo)

    kanvaso_objekto_ligilo.save()
    # вызов функции через коммит транзакции обязателен, иначе данные в подписке ещё не изменёнными будут
    transaction.on_commit(eventoj)


# Модель связей объектов холстов между собой
# У объекта связь с вышестоящим объектом может быть только одна (28.09.2023)
class RedaktuKanvasoObjektoLigilo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kanvaso_objekto_ligilo = graphene.Field(KanvasoObjektoLigiloNode, 
        description=_('Создание/изменение связи объектов холстов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        posedanto_uuid = graphene.String(description=_('UUID объекта холста владельца связи'))
        ligilo_uuid = graphene.String(description=_('UUID связываемого объекта холста'))
        tipo_id = graphene.Int(description=_('ID типа связи объектов холстов'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kanvaso_objekto_ligilo = None
        posedanto = None
        ligilo = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kanvasoj.povas_krei_kanvasoj_kanvaso_objekto_ligilo'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_uuid' in kwargs:
                                try:
                                    posedanto = KanvasoObjekto.objects.get(uuid=kwargs.get('posedanto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except KanvasoObjekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект холста'),'posedanto_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_uuid')

                        if not message:
                            if 'ligilo_uuid' in kwargs:
                                try:
                                    ligilo = KanvasoObjekto.objects.get(uuid=kwargs.get('ligilo_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                    # проверяем, есть ли указатель с данного ligilo, усли уже есть, то создание запрещено
                                    try:
                                        ligilo_prev = KanvasoObjektoLigilo.objects.get(ligilo__uuid=kwargs.get('ligilo_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                        message = '{} "{}"'.format(_('Данный объект ligilo_uuid уже привязан к другому объекту:'),ligilo_prev.posedanto.uuid)
                                    except KanvasoObjektoLigilo.DoesNotExist:
                                        pass
                                except KanvasoObjekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект холста'),'ligilo_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'ligilo_uuid')
                        # проверяем, что оба объекта относятся к одному и тому же канвасу
                        if not message:
                            if ligilo.kanvaso.id != posedanto.kanvaso.id:
                                message = '{} "{}" != "{}"'.format(_('Соединять карточки можно только из одного канваса'),
                                                                   posedanto.kanvaso.id,
                                                                   ligilo.kanvaso.id
                                                                   )

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = KanvasoObjektoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except KanvasoObjektoLigiloTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип связи объектов холстов между собой'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            kanvaso_objekto_ligilo = KanvasoObjektoLigilo.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto=posedanto,
                                ligilo=ligilo,
                                tipo=tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            kanvaso_objekto_ligilo_save(kanvaso_objekto_ligilo)

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto', False) or kwargs.get('ligilo', False)
                            or kwargs.get('tipo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kanvaso_objekto_ligilo = KanvasoObjektoLigilo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kanvasoj.povas_forigi_kanvasoj_kanvaso_objekto_ligilo')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kanvasoj.povas_shangxi_kanvasoj_kanvaso_objekto_ligilo'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_id' in kwargs:
                                    try:
                                        posedanto = KanvasoObjekto.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KanvasoObjekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект холста'),'posedanto_id')

                            if not message:
                                if 'ligilo_id' in kwargs:
                                    try:
                                        ligilo = KanvasoObjekto.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KanvasoObjekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект холста'),'ligilo_id')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = KanvasoObjektoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KanvasoObjektoLigiloTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип связи объектов холстов между собой'),'tipo_id')

                            if not message:

                                kanvaso_objekto_ligilo.forigo = kwargs.get('forigo', kanvaso_objekto_ligilo.forigo)
                                kanvaso_objekto_ligilo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kanvaso_objekto_ligilo.arkivo = kwargs.get('arkivo', kanvaso_objekto_ligilo.arkivo)
                                kanvaso_objekto_ligilo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kanvaso_objekto_ligilo.publikigo = kwargs.get('publikigo', kanvaso_objekto_ligilo.publikigo)
                                kanvaso_objekto_ligilo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kanvaso_objekto_ligilo.posedanto = posedanto if kwargs.get('posedanto_id', False) else kanvaso_objekto_ligilo.posedanto
                                kanvaso_objekto_ligilo.ligilo = ligilo if kwargs.get('ligilo_id', False) else kanvaso_objekto_ligilo.ligilo
                                kanvaso_objekto_ligilo.tipo = tipo if kwargs.get('tipo_id', False) else kanvaso_objekto_ligilo.tipo

                                kanvaso_objekto_ligilo_save(kanvaso_objekto_ligilo)
                                status = True
                                message = _('Запись успешно изменена')
                        except KanvasoObjektoLigilo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKanvasoObjektoLigilo(status=status, message=message, kanvaso_objekto_ligilo=kanvaso_objekto_ligilo)


class KanvasojMutations(graphene.ObjectType):
    redaktu_kanvasoj_kanvaso_kategorio = RedaktuKanvasoKategorio.Field(
        description=_('''Создаёт или редактирует категории досок''')
    )
    redaktu_kanvasoj_kanvaso_tipo = RedaktuKanvasoTipo.Field(
        description=_('''Создаёт или редактирует типы досок''')
    )
    redaktu_kanvasoj_kanvaso_statuso = RedaktuKanvasoStatuso.Field(
        description=_('''Создаёт или редактирует статусы досок''')
    )
    redaktu_kanvasoj_kanvaso = RedaktuKanvaso.Field(
        description=_('''Создаёт или редактирует доски''')
    )
    redaktu_kanvasoj_kanvaso_posedantoj_tipo = RedaktuKanvasoPosedantoTipo.Field(
        description=_('''Создаёт или редактирует типы владельцев досок''')
    )
    redaktu_kanvasoj_kanvaso_posedantoj_statuso = RedaktuKanvasoPosedantoStatuso.Field(
        description=_('''Создаёт или редактирует статусы владельцев досок''')
    )
    redaktu_kanvasoj_kanvaso_posedantoj = RedaktuKanvasoPosedanto.Field(
        description=_('''Создаёт или редактирует владельцев досок''')
    )
    redaktu_kanvasoj_kanvaso_ligilo_tipo = RedaktuKanvasoLigiloTipo.Field(
        description=_('''Создаёт или редактирует типы связей досок между собой''')
    )
    redaktu_kanvasoj_kanvaso_ligilo = RedaktuKanvasoLigilo.Field(
        description=_('''Создаёт или редактирует связи досок между собой''')
    )
    redaktu_kanvasoj_kanvaso_objekto_kategorio = RedaktuKanvasoObjektoKategorio.Field(
        description=_('''Создаёт или редактирует категории объектов холстов''')
    )
    redaktu_kanvasoj_kanvaso_objekto_tipo = RedaktuKanvasoObjektoTipo.Field(
        description=_('''Создаёт или редактирует типы объектов холстов''')
    )
    redaktu_kanvasoj_kanvaso_objekto_statuso = RedaktuKanvasoObjektoStatuso.Field(
        description=_('''Создаёт или редактирует статусы объектов холстов''')
    )
    redaktu_kanvasoj_kanvaso_objekto = RedaktuKanvasoObjekto.Field(
        description=_('''Создаёт или редактирует объекты холстов''')
    )
    redaktu_kanvasoj_kanvaso_objekto_posedantoj_tipo = RedaktuKanvasoObjektoPosedantoTipo.Field(
        description=_('''Создаёт или редактирует типы владельцев объектов холстов''')
    )
    redaktu_kanvasoj_kanvaso_objekto_posedantoj_statuso = RedaktuKanvasoObjektoPosedantoStatuso.Field(
        description=_('''Создаёт или редактирует статусы владельцев объектов холстов''')
    )
    redaktu_kanvasoj_kanvaso_objekto_posedantoj = RedaktuKanvasoObjektoPosedanto.Field(
        description=_('''Создаёт или редактирует владельцев объектов холстов''')
    )
    redaktu_krei_kanvasoj_kanvaso_objekto_posedanto = RedaktuKreiKanvasoObjektoPosedanto.Field(
        description=_('''Создаёт список объектов холстов с владельцами''')
    )
    redaktu_krei_kanvaso_kanvasoj_kanvaso_objekto_posedanto = RedaktuKreiKanvasoKanvasoObjektoPosedanto.Field(
        description=_('''Создание холста с владельцем и списка объектов холстов с владельцами''')
    )
    redaktu_kanvasoj_kanvaso_objekto_ligilo_tipo = RedaktuKanvasoObjektoLigiloTipo.Field(
        description=_('''Создаёт или редактирует типы связей объектов холстов между собой''')
    )
    redaktu_kanvasoj_kanvaso_objekto_ligilo = RedaktuKanvasoObjektoLigilo.Field(
        description=_('''Создаёт или редактирует связи объектов холстов между собой''')
    )

