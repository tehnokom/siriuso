import channels_graphql_ws
import graphene

from graphql import GraphQLError
from .schema import KanvasoNode, KanvasoObjektoPosedantoNode, KanvasoObjektoNode, KanvasoObjektoLigiloNode
from ..models import Kanvaso, KanvasoObjekto, KanvasoObjektoLigilo, KanvasoObjektoPosedanto
from universo_uzantoj_websocket.models import UniversoUzantojWebsocket


# Реализация подписки для изменения объектов и их связей в канвасе:
# - объект создан/удалён/изменён
# - изменилась связь у объекта


# Подписки для WebSocket на действия в кубе
class KanvasoEventoj(channels_graphql_ws.Subscription):
    """Подписка на изменения в объектах канваса"""
    evento = graphene.String(required=True)
    kanvaso = graphene.relay.node.Field(KanvasoNode)
    objekto = graphene.relay.node.Field(KanvasoObjektoNode)
    objekto_ligilo = graphene.relay.node.Field(KanvasoObjektoLigiloNode)
    objekto_posedanto = graphene.relay.node.Field(KanvasoObjektoPosedantoNode)

    class Arguments:
        """Тут аргументы, передающиеся для подписки на события"""
        # ID канваса
        kanvasoj = graphene.List(graphene.Int, required=True)

    def subscribe(self, info, kanvasoj):
        """В этом методе определяется список канвасов для подписки на изминения в них"""
        uzanto = info.context.user
        if uzanto.is_authenticated:
            # просмотр разрешен всем зарегестрированным пользователям
            # проверяем, есть ли такие канвасы в системе
            kanvaso_res = Kanvaso.objects.filter(
                id__in=kanvasoj, 
                forigo=False,
                arkivo=False, publikigo=True)

            kanvaso_set = set(
                kanvaso_res.values_list('id', flat=True)
            )

            if not kanvaso_res:
                raise GraphQLError(
                    'Невозможно отслеживать канвасы c ID {}, они не сущствуют или не доступны'.format(
                        set(kanvasoj) - kanvaso_res
                    )
                )

            # записываем информацию об установки подписки
            try:
                uzanto_websocket = UniversoUzantojWebsocket.objects.get(
                    posedanto = uzanto
                )
            except UniversoUzantojWebsocket.DoesNotExist:
                uzanto_websocket = UniversoUzantojWebsocket.objects.create(
                    posedanto = uzanto,
                )
            uzanto_websocket.save()

            return list(map(lambda v: 'Kanvaso_{}'.format(v), kanvaso_set)) or None

        raise GraphQLError('Необходима авторизация')

    @staticmethod
    def publish(payload, info, kanvasoj):
        """ публикация изминений в канвасе """
        evento = payload.get('evento')

        kanvaso = None
        objekto = None
        objekto_ligilo = None
        objekto_posedanto = None
        try:
            if evento == 'kanvaso_objekto_ligilo':
                objekto_ligilo = KanvasoObjektoLigilo.objects.get(
                    uuid=payload.get('objekto_ligilo'),
                )
                objekto = objekto_ligilo.posedanto

            elif evento == 'kanvaso_objekto':
                objekto = KanvasoObjekto.objects.get(
                    uuid=payload.get('objekto'),
                )
            elif evento == 'kanvaso_objekto_posedanto':
                objekto_posedanto = KanvasoObjektoPosedanto.objects.get(
                    uuid=payload.get('objekto_posedanto'),
                )
            elif evento == 'kanvaso':
                kanvaso = Kanvaso.objects.get(
                    uuid=payload.get('kanvaso'),
                )

            # рассылаем всем
            return KanvasoEventoj(evento=evento, kanvaso=kanvaso, objekto=objekto, objekto_ligilo=objekto_ligilo, objekto_posedanto=objekto_posedanto)
        except KanvasoObjekto.DoesNotExist or KanvasoObjektoLigilo.DoesNotExist:
            pass

        return KanvasoEventoj.SKIP

    @classmethod
    def kanvaso_objekto_ligilo(cls, kanvaso_objekto_ligilo):
        """Этот метод должен вызываться при изминении связей объектов в канвасе"""
        cls.broadcast(
            group='Kanvaso_{}'.format(kanvaso_objekto_ligilo.posedanto.kanvaso.id),
            payload={
                'evento': 'kanvaso_objekto_ligilo',
                'objekto_ligilo': str(kanvaso_objekto_ligilo.uuid)
            }
        )

    @classmethod
    def kanvaso_objekto_ligilo_uuid(cls, kanvaso_objekto_ligilo_uuid, kanvaso_id):
        """Этот метод должен вызываться при изминении связей объектов в канвасе
        Передаются только uuid связи и id канваса
        """
        cls.broadcast(
            group='Kanvaso_{}'.format(kanvaso_id),
            payload={
                'evento': 'kanvaso_objekto_ligilo',
                'objekto_ligilo': str(kanvaso_objekto_ligilo_uuid)
            }
        )

    @classmethod
    def kanvaso_objekto(cls, kanvaso_objekto):
        """Этот метод должен вызываться при изминении объекта в канвасе."""
        group='Kanvaso_{}'.format(kanvaso_objekto.kanvaso.id)
        payload={
            'evento': 'kanvaso_objekto',
            'objekto': str(kanvaso_objekto.uuid),
        }
        cls.broadcast(
            group=group,
            payload=payload
        )

    @classmethod
    def kanvaso_objekto_uuid(cls, kanvaso_objekto_uuid, kanvaso_id):
        """Этот метод должен вызываться при изминении объекта в канвасе.
        Передаются только uuid объекта и id канваса
        """
        group='Kanvaso_{}'.format(kanvaso_id)
        payload={
            'evento': 'kanvaso_objekto',
            'objekto': str(kanvaso_objekto_uuid),
        }
        cls.broadcast(
            group=group,
            payload=payload
        )

    @classmethod
    def kanvaso_objekto_posedanto(cls, kanvaso_objekto_posedanto):
        """Этот метод должен вызываться при изминении связей объектов в канвасе"""
        cls.broadcast(
            group='Kanvaso_{}'.format(kanvaso_objekto_posedanto.posedanto.kanvaso.id),
            payload={
                'evento': 'kanvaso_objekto_posedanto',
                'objekto_posedanto': str(kanvaso_objekto_posedanto.uuid)
            }
        )

    @classmethod
    def kanvaso_objekto_posedanto_uuid(cls, kanvaso_objekto_posedanto_uuid, kanvaso_id):
        """Этот метод должен вызываться при изминении связей объектов в канвасе
        Передаются только uuid связи и id канваса
        """
        cls.broadcast(
            group='Kanvaso_{}'.format(kanvaso_id),
            payload={
                'evento': 'kanvaso_objekto_posedanto',
                'objekto_posedanto': str(kanvaso_objekto_posedanto_uuid)
            }
        )

    @classmethod
    def kanvasoj(cls, kanvaso):
        """Этот метод должен вызываться при изминении в канвасе."""
        group='Kanvaso_{}'.format(kanvaso.id)
        payload={
            'evento': 'kanvaso',
            'kanvaso': str(kanvaso.uuid),
        }
        cls.broadcast(
            group=group,
            payload=payload
        )


class KanvasoSubscription(graphene.ObjectType):
    Kanvaso_eventoj = KanvasoEventoj.Field()
