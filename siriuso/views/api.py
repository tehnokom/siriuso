"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from rest_framework import viewsets
from uuid import UUID


class SiriusoUuidIdModelViewSet(viewsets.ReadOnlyModelViewSet):

    def get_object(self):
        if self.lookup_field and self.lookup_field in self.kwargs:
            ido = self.kwargs[self.lookup_field]

            if ido.isnumeric():
                self.kwargs = {'id': ido}
                self.lookup_field = 'id'
            else:
                try:
                    uuid = UUID(self.kwargs.get(self.lookup_field), version=4)
                except ValueError:
                    pass

        return super().get_object()
