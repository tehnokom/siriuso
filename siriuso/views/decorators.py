"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.shortcuts import redirect


def auth_user_redirect(function=None, redirect_page=None):
    """
    Декоратор для представлений страниц, который переадресует
    зарегистрированны пользователей на указанную страницу.
    Если имя страницы не указано, то переадресует на стартовую страницу
    """

    def wrapped(request, *args, **kwargs):
        if request.user.is_authenticated:
            # response = redirect('start_page') if redirect_page is None else redirect(redirect_page)
            response = redirect('https://universo.pro/') if redirect_page is None else redirect(redirect_page)
            return response

        return function(request, *args, **kwargs)

    if function is not None:
        return wrapped

    def decorator(view_function):
        return wrapped

    return decorator
