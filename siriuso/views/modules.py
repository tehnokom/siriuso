"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.views import View
from django.views.generic import TemplateView
import re

# class SiriusoViewMixin:
#    re_browser = re.compile(r"(android|bb\\d+|meego).+mobile|avantgo|bada\\/|blackberry|blazer|compal|elaine|fennec|" +
#                        r"hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|" +
#                        r"opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\/|plucker|pocket|psp|series(4|6)0|symbian|" +
#                        r"treo|up\\.(browser|link)|vodafone|wap|windows ce|xda|xiino", re.I|re.M)
#
#    re_version = re.compile(r"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al" +
#                        r"(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be" +
#                        r"(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\/|capi|ccwa|cdm\\-|cell" +
#                        r"|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds" +
#                        r"(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 " +
#                        r"u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|" +
#                        r"hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|" +
#                        r"iac( |\\-|\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|" +
#                        r"keji|kgt( |\\/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\/(k|l|u)|50|54|\\-[a-w])|" +
#                        r"libw|lynx|m1\\-w|m3ga|m50\\/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(rc|ri)|mi(o8|oa|ts)|" +
#                        r"mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|" +
#                        r"n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|" +
#                        r"op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|" +
#                        r"pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380" +
#                        r"|r600|raks|rim9|ro(ve|zo)|s55\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\/|" +
#                        r"se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|" +
#                        r"so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|" +
#                        r"tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|" +
#                        r"utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|" +
#                        r"80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\\-|your|zeto|" +
#                        r"zte\\-", re.I|re.M)
#
#    def is_mobile(self, request):
#        if 'HTTP_USER_AGENT' in request.META:
#            user_agent = request.META['HTTP_USER_AGENT']
#
#            b = self.re_browser.search(user_agent)
#            v = self.re_version.search(user_agent[0:4])
#
#            if b or v:
#                return True
#
#        return False


class SiriusoView(View):
    """
    Класс на базе django.views.View с доработкой диспетчера
    для опредения клиентской платформы браузера (мобильная/PC)
    и реализацией  отдельного метода обработки для мобильных платформ
    """
    def dispatch(self, request, *args, **kwargs):
        # Изменяем работу диспетчера, вводим новый метод
        # для обработки GET запросов от мобильных устройств
        if request.method.lower() in self.http_method_names:
            method = request.method.lower()

            if method == 'get' and getattr(request, 'client_type', False):
                if request.client_type != 'pc':
                    method += '_{}'.format(request.client_type)

            handler = getattr(self, method, self.http_method_not_allowed)
        else:
            handler = self.http_method_not_allowed
        return handler(request, *args, **kwargs)

    def get_mobile(self, request, *args, **kwargs):
        # Метод-заглушка. Обеспечивает отдачу контента для мобильного устройства,
        # если у экземпляра-потомка не онаружен метод для отдачи мобильного контента
        handler = getattr(self, 'get', self.http_method_not_allowed)
        return handler(request, *args, **kwargs)


class SiriusoTemplateView(TemplateView):
    mobile_template_name = None
    tablet_template_name = None

    _is_mobile = False
    _is_tablet = False

    def dispatch(self, request, *args, **kwargs):
        if getattr(request, 'client_type', False):
            self._is_mobile = (request.client_type == 'mobile')
            self._is_tablet = (not self._is_mobile and request.client_type == 'tablet')

        mixin_method = getattr(self, 'pagination_dispatch', None)

        if mixin_method is not None:
            handler = mixin_method(request, *args, **kwargs)
            if handler is not None:
                return handler(request, *args, **kwargs)

        return super().dispatch(request, *args, **kwargs)

    def get_template_names(self):
        if self.mobile_template_name is not None\
                and self._is_mobile:
            return [self.mobile_template_name] if not self._is_tablet else [self.tablet_template_name]

        return super().get_template_names()