"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""


def ckeditor_config():
    return {
        'default': {
            'height': 150,
            'width': 730,
            'removePlugins': 'elementspath',
        },
        'forumo_komento': {
            'toolbar': 'forumo',
            'toolbar_forumo': [
                ['Smiley'],
                ['Bold', 'Italic', 'Strike'],
                ['NumberedList', 'BulletedList', 'Blockquote'],
            ],
            'height': 150,
            'width': 730,
            'enterMode': 2,
            'resize_enabled': False,
            'removePlugins': ','.join(['elementspath',]),
            'extraPlugins': ','.join(['autogrow']),
        },
    }
