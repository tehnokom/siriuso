"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene # сам Graphene
from graphene_django import DjangoObjectType # Класс описания модели Django для Graphene
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId # Миксины для описания типов  Graphene
from graphene_permissions.permissions import AllowAny # Класс доступа к типу Graphene
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from siriuso.api.filters import SiriusoFilterConnectionField # Коннектор для получения данных
from siriuso.api.types import SiriusoLingvo # Объект Graphene для представления мультиязычного поля
from ..models import * # модели приложения

class MemorkartojBildoMixin:
    """
    Миксин разрешает файлы изображений как абсолютный URL
    """

    @staticmethod
    def __resolve_bildo(request, bildo, default=None):
        if re.search(r'^/static/', str(bildo)):
            image = bildo.name
        else:
            image = getattr(bildo, 'url') if bildo else default
        return request.build_absolute_uri(image) if image else None

    def resolve_bildo(self, info):
        return MemorkartojBildoMixin.__resolve_bildo(info.context, getattr(self, 'bildo'))

    def resolve_sono(self, info):
        return MemorkartojBildoMixin.__resolve_bildo(info.context, getattr(self, 'sono'))


# Модель карточек
class MemorkartojModuloNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'id': ['icontains',],
        'nomo__enhavo': ['contains', 'icontains'],
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование карточки'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание карточки'))

    class Meta:
        model = MemorkartojModulo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact']
        }
        interfaces = (graphene.relay.Node,)

# Модель карточек
class MemorkartojKartoNode(SiriusoAuthNode, MemorkartojBildoMixin, SiriusoObjectId, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'id': ['icontains',],
        'nomo__enhavo': ['contains', 'icontains'],
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование карточки'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание карточки'))

    class Meta:
        model = MemorkartojKarto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__id': ['exact'],
            'posedanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

# Модель типа теста/игры
class MemorkartojTestoNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'id': ['icontains',],
        'nomo__enhavo': ['contains', 'icontains'],
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование карточки'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание карточки'))

    class Meta:
        model = MemorkartojTesto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact']
        }
        interfaces = (graphene.relay.Node,)

# Статистика тестов
class MemorkartojStatistikoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)

    class Meta:
        model = MemorkartojStatistiko
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'testo__uuid': ['exact'],
            'testo__id': ['exact'],
            'karto__uuid': ['exact'],
            'karto__id': ['exact'],
            'autoro__uuid': ['exact'],
            'autoro__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)

# подписка на чужие модули
class MemorkartojAbonoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)

    class Meta:
        model = MemorkartojAbono
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'modulo__uuid': ['exact'],
            'modulo__id': ['exact'],
            'posedanto__uuid': ['exact'],
            'posedanto__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)

class MemorkartojQuery(graphene.ObjectType):
    memorkartoj_modulo = SiriusoFilterConnectionField(MemorkartojModuloNode,
                                                          description=_('Выводит все доступные модули'))
    memorkartoj_karto = SiriusoFilterConnectionField(MemorkartojKartoNode,
                                                          description=_('Выводит все доступные карточки'))
    memorkartoj_testo = SiriusoFilterConnectionField(MemorkartojTestoNode,
                                                          description=_('Выводит все доступные тесты/игры'))
    memorkartoj_statistiko = SiriusoFilterConnectionField(MemorkartojStatistikoNode,
                                                          description=_('Выводит всю доступную статистику'))
    memorkartoj_abono = SiriusoFilterConnectionField(MemorkartojAbonoNode,
                                                          description=_('Выводит все доступные подписки на чужие модули'))
