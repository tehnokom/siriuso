"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.utils import timezone
from django.db import transaction # создание и изменения будем делать в блокирующейтранзакции
from django.utils.translation import gettext_lazy as _
import graphene

from siriuso.utils.thumbnailer import get_image_properties, get_web_image
from siriuso.utils import set_enhavo
# from mesagxilo.tasks import sciigi_mesagxilo
from .schema import *
from ..models import *

# Максимальный размер загружаемого файла
MAX_FILE_SIZE = 1024 * 1024 * 10  # 10 МБ

# Модель Модуль карточек
class RedaktuMemorkartojModulo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    modulo = graphene.Field(MemorkartojModuloNode, description=_('Созданная/изменённая запись модели карточек'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название модуля'))
        priskribo = graphene.String(description=_('Описание модуля'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        modulo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
                with transaction.atomic():
                    # Создаём новую запись
                    if not kwargs.get('uuid', False):
                        if uzanto.has_perm('memorkartoj.povas_krei_modulo'):
                            # Проверяем наличие всех полей
                            if not (kwargs.get('nomo', False) or message):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                            elif not (kwargs.get('priskribo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'priskribo')

                            elif kwargs.get('forigo', False):
                                message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                            elif kwargs.get('arkivo', False):
                                message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                            if not message:
                                modulo = MemorkartojModulo.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    publikigo=kwargs.get('publikigo', False),
                                    publikiga_dato=timezone.now(),
                                    posedanto=uzanto
                                )

                                set_enhavo(modulo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                                set_enhavo(modulo.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                                modulo.save()

                                status = True
                                message = _('Запись создана')
                        else:
                            message = _('Недостаточно прав')
                    else:
                        # Изменяем запись
                        if not (kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                                or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                                or kwargs.get('publikigo', False)):
                            message = _('Не задано ни одно поле для изменения')

                        if not message:
                            # Ищем запись для изменения
                            try:
                                modulo = MemorkartojModulo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                                if (not uzanto.has_perm('memorkartoj.povas_forigi_modulo')
                                        and kwargs.get('forigo', False)):
                                    message = _('Недостаточно прав для удаления')
                                elif not uzanto.has_perm('memorkartoj.povas_shanghi_modulo'):
                                    message = _('Недостаточно прав для изменения')
                                else:
                                    modulo.forigo = kwargs.get('forigo', modulo.forigo)
                                    modulo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                    modulo.arkivo = kwargs.get('arkivo', modulo.arkivo)
                                    modulo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                    modulo.publikigo = kwargs.get('publikigo', modulo.publikigo)
                                    modulo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                    if kwargs.get('nomo', False):
                                        set_enhavo(modulo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                    if kwargs.get('priskribo', False):
                                        set_enhavo(modulo.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)

                                    modulo.save()
                                    status = True
                                    message = _('Запись успешно изменена')
                            except MemorkartojModulo.DoesNotExist:
                                message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMemorkartojModulo(status=status, message=message, modulo=modulo)

# Модель Карточка
class RedaktuMemorkartojKarto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    karto = graphene.Field(MemorkartojKartoNode, description=_('Созданная/изменённая запись карточки'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название модуля'))
        priskribo = graphene.String(description=_('Описание модуля'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        posedanto_id = graphene.Int(description=_('Модуль'))
        bildo = graphene.String(description=_('присоединенное изображение'))
        sono = graphene.String(description=_('присоединенный звук'))
        video = graphene.String(description=_('ссылка на видео'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        karto = None
        posedanto = None
        bildo = None
        sono = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
                with transaction.atomic():
                    # Создаём новую запись
                    if not kwargs.get('uuid', False):
                        if uzanto.has_perm('memorkartoj.povas_krei_karto'):
                            # Проверяем наличие всех полей
                            if not (kwargs.get('nomo', False) or message):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                            elif not (kwargs.get('priskribo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'priskribo')

                            elif not (kwargs.get('posedanto_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'posedanto_id')

                            elif kwargs.get('forigo', False):
                                message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                            elif kwargs.get('arkivo', False):
                                message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                            # Проверяем наличие записи с таким кодом
                            if not message:
                                if (kwargs.get('posedanto_id', False)):
                                    try:
                                        posedanto = MemorkartojModulo.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                    except MemorkartojModulo.DoesNotExist:
                                        message = _('Неверный модуль карточек')

                            # Проверяем присланное изображения
                            if not (message):
                                if 'bildo' in info.context.FILES:
                                    file = get_web_image(info.context.FILES['bildo'])
                                    file_prop = get_image_properties(file)

                                    if file_prop:
                                        if file.size > MAX_FILE_SIZE:
                                            errors.append(
                                                ErrorNode(field='bildo',
                                                      message=_('Размер файла изображения больше 10 МБайт'))
                                            )
                                        elif file_prop.get('format') not in ['JPEG', 'PNG', 'GIF']:
                                            errors.append(
                                                ErrorNode(field='bildo',
                                                          message=_('Неверный формат изображения, разрешены: JPEG, GIF, PNG'))
                                            )
                                        else:
                                            bildo = info.context.FILES['bildo']

                                    else:
                                        errors.append(ErrorNode(field='bildo',
                                                                message=_('Файл не является изображением')))

                            # Проверяем присланный файл
                            # нужна еще проверка файла на тип звука
                            if not (message):
                                if 'sono' in info.context.FILES:
                                    file = info.context.FILES['sono']
                                    if file:
                                        if file.size > MAX_FILE_SIZE:
                                            errors.append(
                                                ErrorNode(field='sono',
                                                      message=_('Размер аудиофайла больше 10 МБайт'))
                                            )
                                        elif not file.content-type in ["audio/mpeg", "audio/wav"]:
                                            errors.append(
                                                ErrorNode(field='sono',
                                                          message=_('Неверный аудиоформат, разрешены: MP3, WAV'))
                                            )
                                        elif not os.path.splitext(file.name)[1] in (".mp3", ".wav"):
                                            errors.append(
                                                ErrorNode(field='sono',
                                                          message=_('Неверное расширение файла, разрешены: MP3, WAV'))
                                            )
                                        else:
                                             sono = info.context.FILES['sono']


    # file = self.cleaned_data.get('sound') 
 
    # if file:
    #     if file._size > 4*1024*1024:
    #         raise ValidationError("Audio file too large ( > 4mb )")
    #     if not file.content-type in ["audio/mpeg","audio/wav"]:
    #         raise ValidationError("Content-Type is not mpeg")
    #     if not os.path.splitext(file.name)[1] in [".mp3",".wav"]:
    #         raise ValidationError("Doesn't have proper extension")
 
    #         return file
    #     else:
    #         raise ValidationError("Couldn't read uploaded file")

                                    
                                    # sono = info.context.FILES['sono']

                            if not message:
                                karto = MemorkartojKarto.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    posedanto=posedanto,
                                    bildo=bildo,
                                    sono=sono,
                                    video=kwargs.get('video', False),
                                    publikigo=kwargs.get('publikigo', False),
                                    publikiga_dato=timezone.now()
                                )

                                set_enhavo(karto.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                                set_enhavo(karto.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                                karto.save()

                                status = True
                                message = _('Запись создана')
                        else:
                            message = _('Недостаточно прав')
                    else:
                        # Изменяем запись
                        if not (kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                                or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                                or kwargs.get('publikigo', False)):
                            message = _('Не задано ни одно поле для изменения')

                        # Проверяем наличие записи с таким кодом
                        if not message:
                            if (kwargs.get('posedanto_id', False)):
                                try:
                                    posedanto = MemorkartojModulo.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except MemorkartojModulo.DoesNotExist:
                                    message = _('Неверный модуль карточек')

                        # Проверяем присланное изображения
                        if not (message):
                            if 'bildo' in info.context.FILES:
                                file = get_web_image(info.context.FILES['bildo'])
                                file_prop = get_image_properties(file)

                                if file_prop:
                                    if file.size > MAX_FILE_SIZE:
                                        errors.append(
                                            ErrorNode(field='bildo',
                                                  message=_('Размер файла изображения больше 10 МБайт'))
                                        )
                                    elif file_prop.get('format') not in ['JPEG', 'PNG', 'GIF']:
                                        errors.append(
                                            ErrorNode(field='bildo',
                                                      message=_('Неверный формат изображения, разрешены: JPEG, GIF, PNG'))
                                        )
                                    else:
                                        bildo = info.context.FILES['bildo']

                                else:
                                    errors.append(ErrorNode(field='bildo',
                                                            message=_('Файл не является изображением')))

                        # Проверяем присланный файл
                        # нужна еще проверка файла на тип звука
                        if not (message):
                            if 'sono' in info.context.FILES:
                                sono = info.context.FILES['sono']

                        if not message:
                            # Ищем запись для изменения
                            try:
                                karto = MemorkartojKarto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                                if (not uzanto.has_perm('memorkartoj.povas_forigi_karto')
                                        and kwargs.get('forigo', False)):
                                    message = _('Недостаточно прав для удаления')
                                elif not uzanto.has_perm('memorkartoj.povas_shanghi_karto'):
                                    message = _('Недостаточно прав для изменения')
                                else:
                                    karto.forigo = kwargs.get('forigo', karto.forigo)
                                    karto.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                    karto.arkivo = kwargs.get('arkivo', karto.arkivo)
                                    karto.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                    karto.posedanto = posedanto or karto.posedanto
                                    karto.bildo = bildo or karto.bildo
                                    karto.sono = sono or karto.sono
                                    karto.video = kwargs.get('video', karto.video)
                                    karto.publikigo = kwargs.get('publikigo', karto.publikigo)
                                    karto.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                    if kwargs.get('nomo', False):
                                        set_enhavo(karto.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                    if kwargs.get('priskribo', False):
                                        set_enhavo(karto.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)

                                    karto.save()
                                    status = True
                                    message = _('Запись успешно изменена')
                            except MemorkartojKarto.DoesNotExist:
                                message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMemorkartojKarto(status=status, message=message, karto=karto)

# Модель Тип теста/игры
class RedaktuMemorkartojTesto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    testo = graphene.Field(MemorkartojTestoNode, description=_('Созданная/изменённая запись теста/игры'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название теста/игры'))
        priskribo = graphene.String(description=_('Описание теста/игры'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        testo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
                with transaction.atomic():
                    # Создаём новую запись
                    if not kwargs.get('uuid', False):
                        if uzanto.has_perm('memorkartoj.povas_krei_testo'):
                            # Проверяем наличие всех полей
                            if not (kwargs.get('nomo', False) or message):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                            elif not (kwargs.get('priskribo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'priskribo')

                            elif kwargs.get('forigo', False):
                                message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                            elif kwargs.get('arkivo', False):
                                message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                            if not message:
                                testo = MemorkartojTesto.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    publikigo=kwargs.get('publikigo', False),
                                    publikiga_dato=timezone.now()
                                )

                                set_enhavo(testo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                                set_enhavo(testo.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                                testo.save()

                                status = True
                                message = _('Запись создана')
                        else:
                            message = _('Недостаточно прав')
                    else:
                        # Изменяем запись
                        if not (kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                                or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                                or kwargs.get('publikigo', False)):
                            message = _('Не задано ни одно поле для изменения')

                        if not message:
                            # Ищем запись для изменения
                            try:
                                testo = MemorkartojTesto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                                if (not uzanto.has_perm('memorkartoj.povas_forigi_testo')
                                        and kwargs.get('forigo', False)):
                                    message = _('Недостаточно прав для удаления')
                                elif not uzanto.has_perm('memorkartoj.povas_shanghi_testo'):
                                    message = _('Недостаточно прав для изменения')
                                else:
                                    testo.forigo = kwargs.get('forigo', testo.forigo)
                                    testo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                    testo.arkivo = kwargs.get('arkivo', testo.arkivo)
                                    testo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                    testo.publikigo = kwargs.get('publikigo', testo.publikigo)
                                    testo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                    if kwargs.get('nomo', False):
                                        set_enhavo(testo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                    if kwargs.get('priskribo', False):
                                        set_enhavo(testo.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)

                                    testo.save()
                                    status = True
                                    message = _('Запись успешно изменена')
                            except MemorkartojTesto.DoesNotExist:
                                message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMemorkartojTesto(status=status, message=message, testo=testo)

# Модель Статистика тестов
class RedaktuMemorkartojStatistiko(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    statistiko = graphene.Field(MemorkartojStatistikoNode, description=_('Созданная/изменённая запись модели карточек'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        testo_id = graphene.Int(description=_('Id типа теста/игры'))
        karto_uuid = graphene.String(description=_('UUID карточки'))
        autoro_id = graphene.Int(description=_('ID пользователя'))
        nombro = graphene.Int(description=_('количество проходов данного теста всего'))
        sukcesa = graphene.Int(description=_('количество успешных проходов'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        statistiko = None
        testo = None
        karto = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
                with transaction.atomic():
                    # Создаём новую запись
                    if not kwargs.get('uuid', False):
                        if uzanto.has_perm('memorkartoj.povas_krei_statistiko'):
                            # Проверяем наличие всех полей
                            if kwargs.get('forigo', False) and not message:
                                message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                            elif not (kwargs.get('nombro', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nombro')

                            elif not (kwargs.get('sukcesa', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'sukcesa')

                            # Проверяем наличие записи с таким кодом
                            if not message:
                                if (kwargs.get('testo_id', False)):
                                    try:
                                        testo = MemorkartojTesto.objects.get(id=kwargs.get('testo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                    except MemorkartojTesto.DoesNotExist:
                                        message = _('Неверный тип теста/игры')

                            if not message:
                                if (kwargs.get('karto_uuid', False)):
                                    try:
                                        karto = MemorkartojKarto.objects.get(uuid=kwargs.get('karto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                    except MemorkartojKarto.DoesNotExist:
                                        message = _('Неверная карточка')

                            if not message:
                                if (kwargs.get('autoro_id', False)):
                                    try:
                                        autoro = Uzanto.objects.get(id=kwargs.get('autoro_id'), is_active=True,
                                                konfirmita=True)
                                    except Uzanto.DoesNotExist:
                                        message = _('Неверный пользователь')
                                else:
                                    autoro = uzanto

                            if not message:
                                statistiko = MemorkartojStatistiko.objects.create(
                                    forigo=False,
                                    testo=testo,
                                    karto=karto,
                                    autoro=autoro,
                                    nombro=kwargs.get('nombro'),
                                    sukcesa=kwargs.get('sukcesa')
                                )

                                statistiko.save()

                                status = True
                                message = _('Запись создана')
                        else:
                            message = _('Недостаточно прав')
                    else:
                        # Изменяем запись
                        if not (kwargs.get('testo_id', False) or kwargs.get('karto_uuid', False)
                                or kwargs.get('forigo', False) or kwargs.get('autoro_id', False)
                                or kwargs.get('nombro', False) or kwargs.get('sukcesa', False)):
                            message = _('Не задано ни одно поле для изменения')

                        # Проверяем наличие записи с таким кодом
                        if not message:
                            if (kwargs.get('testo_id', False)):
                                try:
                                    testo = MemorkartojTesto.objects.get(id=kwargs.get('testo_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                                except MemorkartojTesto.DoesNotExist:
                                    message = _('Неверный тип теста/игры')

                        if not message:
                            if (kwargs.get('karto_uuid', False)):
                                try:
                                    karto = MemorkartojKarto.objects.get(uuid=kwargs.get('karto_uuid'), forigo=False,
                                                                arkivo=False, publikigo=True)
                                except MemorkartojKarto.DoesNotExist:
                                    message = _('Неверная карточка')

                        if not message:
                            if (kwargs.get('autoro_id', False)):
                                try:
                                    autoro = Uzanto.objects.get(id=kwargs.get('autoro_id'), is_active=True,
                                            konfirmita=True)
                                except Uzanto.DoesNotExist:
                                    message = _('Неверный пользователь')

                        if not message:
                            # Ищем запись для изменения
                            try:
                                statistiko = MemorkartojStatistiko.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                                if (not uzanto.has_perm('memorkartoj.povas_forigi_statistiko')
                                        and kwargs.get('forigo', False)):
                                    message = _('Недостаточно прав для удаления')
                                elif not uzanto.has_perm('memorkartoj.povas_shanghi_statistiko'):
                                    message = _('Недостаточно прав для изменения')
                                else:
                                    statistiko.forigo = kwargs.get('forigo', statistiko.forigo)
                                    statistiko.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                    statistiko.nombro = kwargs.get('nombro', statistiko.nombro)
                                    statistiko.sukcesa = kwargs.get('sukcesa', statistiko.sukcesa)
                                    karto.testo = testo or karto.testo
                                    karto.karto = karto or karto.karto
                                    karto.autoro = autoro or karto.autoro

                                    statistiko.save()
                                    status = True
                                    message = _('Запись успешно изменена')
                            except MemorkartojStatistiko.DoesNotExist:
                                message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMemorkartojStatistiko(status=status, message=message, statistiko=statistiko)

# Модель добавление результата пользователя в статистику тестов/игр
class RedaktuMemorkartojStatistikoAdd(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    statistiko = graphene.Field(MemorkartojStatistikoNode, description=_('Созданная/изменённая запись модели карточек'))

    class Arguments:
        testo_id = graphene.Int(description=_('Id типа теста/игры'))
        karto_uuid = graphene.String(description=_('UUID карточки'))
        sukcesa = graphene.Boolean(description=_('Успешность прохода (добавляется количество и при истине - добавляется успех)'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        statistiko = None
        testo = None
        karto = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Проверяем наличие записи с таким кодом
                if not message:
                    if (kwargs.get('testo_id', False)):
                        try:
                            testo = MemorkartojTesto.objects.get(id=kwargs.get('testo_id'), forigo=False,
                                                        arkivo=False, publikigo=True)
                        except MemorkartojTesto.DoesNotExist:
                            message = _('Неверный тип теста/игры')

                if not message:
                    if (kwargs.get('karto_uuid', False)):
                        try:
                            karto = MemorkartojKarto.objects.get(uuid=kwargs.get('karto_uuid'), forigo=False,
                                                        arkivo=False, publikigo=True)
                        except MemorkartojKarto.DoesNotExist:
                            message = _('Неверная карточка')


                # проверяем, было ли прохождение данного теста
                if not message:
                    try:
                        statistiko = MemorkartojStatistiko.objects.get(testo=testo, karto=karto, autoro=uzanto, forigo=False)
                    except MemorkartojStatistiko.DoesNotExist:
                        pass

                if not message:
                    # Создаём новую запись
                    if not statistiko:
                        if uzanto.has_perm('memorkartoj.povas_krei_statistiko'):

                            statistiko = MemorkartojStatistiko.objects.create(
                                forigo=False,
                                testo=testo,
                                karto=karto,
                                autoro=uzanto,
                                nombro=1,
                                sukcesa=1 if kwargs.get('sukcesa') else 0
                            )

                            statistiko.save()

                            status = True
                            message = _('Запись создана')
                        else:
                            message = _('Недостаточно прав')
                    else:
                        # Изменяем запись
                        if not uzanto.has_perm('memorkartoj.povas_shanghi_statistiko'):
                            message = _('Недостаточно прав для изменения')
                        else:

                            statistiko.nombro = statistiko.nombro + 1
                            statistiko.sukcesa = statistiko.sukcesa+1 if kwargs.get('sukcesa') else statistiko.sukcesa

                            statistiko.save()
                            status = True
                            message = _('Запись успешно изменена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMemorkartojStatistiko(status=status, message=message, statistiko=statistiko)

# Модель подписки на чужие модули
class RedaktuMemorkartojAbono(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    abono = graphene.Field(MemorkartojAbonoNode, description=_('Созданная/изменённая запись подписки на чужие модули'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        posedanto_id = graphene.Int(description=_('ID пользователя'))
        modulo_id = graphene.Int(description=_('Модуль'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        abono = None
        posedanto = None
        modulo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
                with transaction.atomic():
                    # Создаём новую запись
                    if not kwargs.get('uuid', False):
                        if uzanto.has_perm('memorkartoj.povas_krei_abono'):
                            # Проверяем наличие всех полей
                            if not (kwargs.get('modulo_id', False) or message):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'modulo_id')

                            elif not (kwargs.get('posedanto_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'posedanto_id')

                            elif kwargs.get('forigo', False):
                                message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                            elif kwargs.get('arkivo', False):
                                message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                            # Проверяем наличие записи с таким кодом
                            if not message:
                                if (kwargs.get('modulo_id', False)):
                                    try:
                                        modulo = MemorkartojModulo.objects.get(id=kwargs.get('modulo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                    except MemorkartojModulo.DoesNotExist:
                                        message = _('Неверный модуль карточек')

                            if not message:
                                if (kwargs.get('posedanto_id', False)):
                                    try:
                                        posedanto = Uzanto.objects.get(id=kwargs.get('posedanto_id'), is_active=True,
                                                konfirmita=True)
                                    except Uzanto.DoesNotExist:
                                        message = _('Неверный пользователь')
                                else:
                                    autoro = uzanto

                            if not message:
                                abono = MemorkartojAbono.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    posedanto=posedanto,
                                    modulo=modulo,
                                    publikigo=kwargs.get('publikigo', False),
                                    publikiga_dato=timezone.now()
                                )

                                abono.save()

                                status = True
                                message = _('Запись создана')
                        else:
                            message = _('Недостаточно прав')
                    else:
                        # Изменяем запись
                        if not (kwargs.get('posedanto_id', False) or kwargs.get('modulo_id', False)
                                or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                                or kwargs.get('publikigo', False)):
                            message = _('Не задано ни одно поле для изменения')

                        # Проверяем наличие записи с таким кодом
                        if not message:
                            if (kwargs.get('modulo_id', False)):
                                try:
                                    modulo = MemorkartojModulo.objects.get(id=kwargs.get('modulo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except MemorkartojModulo.DoesNotExist:
                                    message = _('Неверный модуль карточек')

                        if not message:
                            if (kwargs.get('posedanto_id', False)):
                                try:
                                    posedanto = Uzanto.objects.get(id=kwargs.get('posedanto_id'), is_active=True,
                                            konfirmita=True)
                                except Uzanto.DoesNotExist:
                                    message = _('Неверный пользователь')

                        if not message:
                            # Ищем запись для изменения
                            try:
                                abono = MemorkartojAbono.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                                if (not uzanto.has_perm('memorkartoj.povas_forigi_abono')
                                        and kwargs.get('forigo', False)):
                                    message = _('Недостаточно прав для удаления')
                                elif not uzanto.has_perm('memorkartoj.povas_shanghi_abono'):
                                    message = _('Недостаточно прав для изменения')
                                else:
                                    abono.forigo = kwargs.get('forigo', abono.forigo)
                                    abono.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                    abono.arkivo = kwargs.get('arkivo', abono.arkivo)
                                    abono.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                    abono.modulo = modulo or abono.modulo
                                    abono.posedanto = posedanto or abono.posedanto
                                    abono.publikigo = kwargs.get('publikigo', abono.publikigo)
                                    abono.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                    abono.save()
                                    status = True
                                    message = _('Запись успешно изменена')
                            except MemorkartojAbono.DoesNotExist:
                                message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMemorkartojAbono(status=status, message=message, abono=abono)

class MemorkartojMutations(graphene.ObjectType):
    redaktu_memorkartoj_modulo = RedaktuMemorkartojModulo.Field(
        description=_('''Создаёт или редактирует модуль''')
    )
    redaktu_memorkartoj_karto = RedaktuMemorkartojKarto.Field(
        description=_('''Создаёт или редактирует карточку''')
    )
    redaktu_memorkartoj_testo = RedaktuMemorkartojTesto.Field(
        description=_('''Создаёт или редактирует тест/игру''')
    )
    redaktu_memorkartoj_statistiko = RedaktuMemorkartojStatistiko.Field(
        description=_('''Создаёт или редактирует статистика тестов''')
    )
    redaktu_memorkartoj_statistiko_add = RedaktuMemorkartojStatistikoAdd.Field(
        description=_('''Увеличивает результат статистики тестов''')
    )
    redaktu_memorkartoj_abono = RedaktuMemorkartojAbono.Field(
        description=_('''Создаёт или редактирует карточку''')
    )

