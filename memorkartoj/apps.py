"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class MemorkartojConfig(AppConfig):
    # Название модуля
    name = 'memorkartoj'
    # Визуальное название, будет отображаться в админке, потому делаем переводимым
    verbose_name = _('Memorkartoj')
    
    # Метод срабатывает при регистрации приложения в системе django
    # def ready(self):
        # Импортом сообщаем django, что надо проанализировать код в модуле tasks
        # При этом происходит регистрация функций заданий celery
        # import memorkartoj.tasks

