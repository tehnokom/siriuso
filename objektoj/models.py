"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import Permission
from django.db.models import Max, Q

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from main.models import SiriusoBazaAbstrakta3, Uzanto
from universo_bazo.models import UniversoBazaMaks, UniversoBazaRealeco
from organizoj.models import Organizo
from resursoj.models import Resurso, ResursoModifo, ResursoModifoStato, \
    ResursoStokejoTipo
from kosmo.models import KosmoStelsistemoKubo


# Шаблоны объектов Универсо, использует абстрактный класс UniversoBazaMaks
class ObjektoSxablono(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # шаблон объектов владелец, то есть родительский объект
    posedanto_objekto = models.ForeignKey('self', verbose_name=_('Posedanto'), null=True, blank=True,
                                          default=None, on_delete=models.CASCADE)

    # разъём (слот), который занимает этот объект у родительского объекта
    konektilo = models.IntegerField(_('Konektilo'), blank=True, null=True, default=None)

    # ресурс Универсо на основе которого был создан этот объект
    resurso = models.ForeignKey(Resurso, verbose_name=_('Resurso'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # ячейка (куб,чанк) звёздной системы в которой находится объект
    kubo = models.ForeignKey(KosmoStelsistemoKubo, verbose_name=_('Kubo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'objektoj_sxablonoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ŝablono de objektoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ŝablono de objektoj')
        # права
        permissions = (
            ('povas_vidi_objektoj_sxablonoj', _('Povas vidi ŝablonoj de objektoj')),
            ('povas_krei_objektoj_sxablonoj', _('Povas krei ŝablonoj de objektoj')),
            ('povas_forigi_objektoj_sxablonoj', _('Povas forigi ŝablonoj de objektoj')),
            ('povas_shangxi_objektoj_sxablonoj', _('Povas ŝanĝi ŝablonoj de objektoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(ObjektoSxablono, self).save(force_insert=force_insert, force_update=force_update,
                                                  using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'objektoj.povas_vidi_objektoj_sxablonoj',
                'objektoj.povas_krei_objektoj_sxablonoj',
                'objektoj.povas_forigi_objektoj_sxablonoj',
                'objektoj.povas_shangxi_objektoj_sxablonoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('objektoj.povas_vidi_objektoj_sxablonoj')
                    or user_obj.has_perm('objektoj.povas_vidi_objektoj_sxablonoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('objektoj.povas_vidi_objektoj_sxablonoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Объекты в Универсо, использует абстрактный класс UniversoBazaRealeco
class Objekto(UniversoBazaRealeco):

    # объект владелец, то есть родительский объект
    posedanto_objekto = models.ForeignKey('self', verbose_name=_('Objekto posedanto'), null=True, blank=True,
                                          default=None, on_delete=models.CASCADE)

    # разъём (слот), который занимает этот объект у родительского объекта
    konektilo = models.IntegerField(_('Konektilo'), blank=True, null=True, default=None)

    # ресурс Универсо на основе которого был создан этот объект
    resurso = models.ForeignKey(Resurso, verbose_name=_('Resurso'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # модификация ресурса Универсо на основе которого был создан этот объект
    modifo = models.ForeignKey(ResursoModifo, verbose_name=_('Modifo'), blank=True, null=True, default=None,
                               on_delete=models.CASCADE)

    # состояние модификации ресурса Универсо в котором сейчас находится объект
    stato = models.ForeignKey(ResursoModifoStato, verbose_name=_('Stato'), blank=True, null=True, default=None,
                              on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=True, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=True, default=default_lingvo,
                                   encoder=CallableEncoder)

    # целостность объекта
    integreco = models.IntegerField(_('Integreco'), blank=True, null=True, default=None)

    # внутренний объём
    volumeno_interna = models.FloatField(_('Interna volumeno'), blank=True, null=True, default=None)

    # внешний объём
    volumeno_ekstera = models.FloatField(_('Ekstera volumeno'), blank=True, null=True, default=None)

    # объём хронения (внешний)
    volumeno_stokado = models.FloatField(_('Volumeno de stokado'), blank=True, null=True, default=None)

    # ячейка (куб,чанк) звёздной системы в которой находится объект
    kubo = models.ForeignKey(KosmoStelsistemoKubo, verbose_name=_('Kubo'), blank=True, null=True, default=None,
                             on_delete=models.CASCADE)

    # координаты (месторасположение) объекта по оси X в кубе
    koordinato_x = models.FloatField(_('Koordinato X'), blank=True, null=True, default=None)

    # координаты (месторасположение) объекта по оси Y в кубе
    koordinato_y = models.FloatField(_('Koordinato Y'), blank=True, null=True, default=None)

    # координаты (месторасположение) объекта по оси Z в кубе
    koordinato_z = models.FloatField(_('Koordinato Z'), blank=True, null=True, default=None)

    # вращение объекта по оси X в кубе
    rotacia_x = models.FloatField(_('Rotacia X'), blank=True, null=True, default=None)

    # вращение объекта по оси Y в кубе
    rotacia_y = models.FloatField(_('Rotacia Y'), blank=True, null=True, default=None)

    # вращение объекта по оси Z в кубе
    rotacia_z = models.FloatField(_('Rotacia Z'), blank=True, null=True, default=None)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'objektoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Objekto')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Objektoj')
        # права
        permissions = (
            ('povas_vidi_objektoj', _('Povas vidi objektoj')),
            ('povas_krei_objektoj', _('Povas krei objektoj')),
            ('povas_forigi_objektoj', _('Povas forigi objektoj')),
            ('povas_sxangxi_objektoj', _('Povas ŝanĝi objektoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid, nomo и владельцы этой модели
        posedantoj = ObjektoPosedanto.objects.filter(objekto=self, forigo=False, arkivo=False, publikigo=True)
        idj = "{}: {}".format(self.uuid, get_enhavo(self.nomo, empty_values=True)[0])
        if len(posedantoj)>0:
            i = True
            for user in posedantoj:
                if user.posedanto_uzanto: 
                    if i:
                        idj = "{}: {}".format(idj, user.posedanto_uzanto.id)
                        i = False
                    else:
                        idj = "{}; {}".format(idj, user.posedanto_uzanto.id)
                if user.posedanto_organizo: 
                    if i:
                        idj = "{}: {}".format(idj, get_enhavo(user.posedanto_organizo.nomo, empty_values=True)[0])
                        i = False
                    else:
                        idj = "{}; {}".format(idj, get_enhavo(user.posedanto_organizo.nomo, empty_values=True)[0])
        return idj

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(Objekto, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'objektoj.povas_vidi_objektoj', 
                'objektoj.povas_krei_objektoj',
                'objektoj.povas_forigi_objektoj',
                'objektoj.povas_sxangxi_objektoj'
            ))

            # Добавляем прав всем на просмотр, т.к. нужно видеть других игроков
            all_perms.update(['povas_vidi_objektoj',])
            # all_perms.update(['povas_vidi_objektoj','povas_krei_objektoj',
            #     'povas_forigi_objektoj', 'povas_shanghi_objektoj'])

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        print('============== ПРОВЕРИЛИ ПРАВА ==================')
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            # на текущий момент показываем все объекты, потом будем сокращать
            cond = Q()
            # if (perms.has_registrita_perm('objektoj.povas_vidi_objektoj')
            #         or user_obj.has_perm('objektoj.povas_vidi_objektoj')):
            #     # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
            #     cond = Q()
            # else:
            #     # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
            #     cond = Q(uuid__isnull=True)
            print('права авторизованного')

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('objektoj.povas_vidi_objektoj'):
                # Если есть права на просмотр
                print('чужому есть права')
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                print('прав нет')
                cond = Q(uuid__isnull=True)

        return cond

    # расстояние между объектами
    def distanceTo(self, objekto):
        return ((self.koordinato_x - objekto.koordinato_x) ** 2 +\
            (self.koordinato_y - objekto.koordinato_y) ** 2 +\
            (self.koordinato_z - objekto.koordinato_z) ** 2) ** 0.5


# Типы владельцев объектов, использует абстрактный класс UniversoBazaMaks
class ObjektoPosedantoTipo(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'objektoj_posedantoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de posedantoj de objektoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de posedantoj de objektoj')
        # права
        permissions = (
            ('povas_vidi_objektoj_posedantoj_tipoj',
             _('Povas vidi tipoj de posedantoj de objektoj')),
            ('povas_krei_objektoj_posedantoj_tipoj',
             _('Povas krei tipoj de posedantoj de objektoj')),
            ('povas_forigi_objektoj_posedantoj_tipoj',
             _('Povas forigi tipoj de posedantoj de objektoj')),
            ('povas_sxangxi_objektoj_posedantoj_tipoj',
             _('Povas ŝanĝi tipoj de posedantoj de objektoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(ObjektoPosedantoTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'objektoj.povas_vidi_objektoj_posedantoj_tipoj',
                'objektoj.povas_krei_objektoj_posedantoj_tipoj',
                'objektoj.povas_forigi_objektoj_posedantoj_tipoj',
                'objektoj.povas_sxangxi_objektoj_posedantoj_tipoj'
            ))

            # Добавляем прав всем на просмотр, т.к. нужно видеть других игроков
            # all_perms.update(['povas_vidi_objektoj_posedantoj_tipoj',])

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('objektoj.povas_vidi_objektoj_posedantoj_tipoj')
                    or user_obj.has_perm('objektoj.povas_vidi_objektoj_posedantoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('objektoj.povas_vidi_objektoj_posedantoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Статус владельца в рамках владения объектом, использует абстрактный класс UniversoBazaMaks
class ObjektoPosedantoStatuso(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'objektoj_posedantoj_statusoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Statuso de posedantoj de objektoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statusoj de posedantoj de objektoj')
        # права
        permissions = (
            ('povas_vidi_objektoj_posedantoj_statusoj',
             _('Povas vidi statusoj de posedantoj de objektoj')),
            ('povas_krei_objektoj_posedantoj_statusoj',
             _('Povas krei statusoj de posedantoj de objektoj')),
            ('povas_forigi_objektoj_posedantoj_statusoj',
             _('Povas forigi statusoj de posedantoj de objektoj')),
            ('povas_sxangxi_objektoj_posedantoj_statusoj',
             _('Povas ŝanĝi statusoj de posedantoj de objektoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(ObjektoPosedantoStatuso, self).save(force_insert=force_insert, force_update=force_update,
                                                          using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'objektoj.povas_vidi_objektoj_posedantoj_statusoj',
                'objektoj.povas_krei_objektoj_posedantoj_statusoj',
                'objektoj.povas_forigi_objektoj_posedantoj_statusoj',
                'objektoj.povas_sxangxi_objektoj_posedantoj_statusoj'
            ))

            # Добавляем прав всем на просмотр, т.к. нужно видеть других игроков
            # all_perms.update(['povas_vidi_objektoj_posedantoj_statusoj',])

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('objektoj.povas_vidi_objektoj_posedantoj_statusoj')
                    or user_obj.has_perm('objektoj.povas_vidi_objektoj_posedantoj_statusoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('objektoj.povas_vidi_objektoj_posedantoj_statusoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Владельцы объектов в Универсо, использует абстрактный класс UniversoBazaRealeco
class ObjektoPosedanto(UniversoBazaRealeco):

    # объект владения
    objekto = models.ForeignKey(Objekto, verbose_name=_('Objekto'), blank=False, null=False,
                                default=None, on_delete=models.CASCADE)

    # пользователь владелец объекта Универсо
    posedanto_uzanto = models.ForeignKey(Uzanto, verbose_name=_('Posedanta uzanto'), blank=True, null=True, default=None,
                               on_delete=models.CASCADE)

    # организация владелец объекта Универсо
    posedanto_organizo = models.ForeignKey(Organizo, verbose_name=_('Posedanta organizo'), blank=True,
                                           null=True, default=None, on_delete=models.CASCADE)

    # размер доли владения
    parto = models.IntegerField(_('Parto'), blank=False, null=False, default=100)

    # тип владельца объекта
    tipo = models.ForeignKey(ObjektoPosedantoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # статус владельца объекта
    statuso = models.ForeignKey(ObjektoPosedantoStatuso, verbose_name=_('Statuso'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'objektoj_posedantoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Posedanto de objekto')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Posedantoj de objektoj')
        # права
        permissions = (
            ('povas_vidi_objektoj_posedantoj', _('Povas vidi posedantoj de objektoj')),
            ('povas_krei_objektoj_posedantoj', _('Povas krei posedantoj de objektoj')),
            ('povas_forigi_objektoj_posedantoj', _('Povas forigi posedantoj de objektoj')),
            ('povas_sxangxi_objektoj_posedantoj', _('Povas ŝanĝi posedantoj de objektoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле id владельца Siriuso и/или nomo организации владельца этой модели
        posedanto = "{}".format(self.objekto.uuid)
        start = True
        if self.posedanto_uzanto: 
            posedanto = "{}: {}".format(posedanto, self.posedanto_uzanto.id)
            start = False
        if self.posedanto_organizo:
            if start: 
                posedanto = "{}: {}".format(posedanto, get_enhavo(self.posedanto_organizo.nomo, empty_values=True)[0])
            else:
                posedanto = "{}; {}".format(posedanto, get_enhavo(self.posedanto_organizo.nomo, empty_values=True)[0])
        return posedanto

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(ObjektoPosedanto, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'objektoj.povas_vidi_objektoj_posedantoj',
                'objektoj.povas_krei_objektoj_posedantoj',
                'objektoj.povas_forigi_objektoj_posedantoj',
                'objektoj.povas_sxangxi_objektoj_posedantoj'
            ))

            # Добавляем прав всем на просмотр, т.к. нужно видеть других игроков
            # all_perms.update(['povas_vidi_objektoj_posedantoj',])

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('objektoj.povas_vidi_objektoj_posedantoj')
                    or user_obj.has_perm('objektoj.povas_vidi_objektoj_posedantoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('objektoj.povas_vidi_objektoj_posedantoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Места хранения в объектах, использует абстрактный класс UniversoBazaMaks
class ObjektoStokejo(UniversoBazaMaks):

    # объект владелец места хранения
    posedanto_objekto = models.ForeignKey(Objekto, verbose_name=_('Objekto posedanto'), blank=False, null=False,
                                          default=None, on_delete=models.CASCADE)

    # уникальный личный ID места хранения в рамках объекта
    id = models.IntegerField(_('ID'), blank=True, null=True, default=None)

    # тип места хранения ресурсов на основе которого создано это место хранения в объекте
    tipo = models.ForeignKey(ResursoStokejoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'objektoj_stokejoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Stokejo de objektoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Stokejoj de objektoj')
        # установка уникальности личного ID места хранения в рамках объекта
        unique_together = ("id", "posedanto_objekto")
        # права
        permissions = (
            ('povas_vidi_objektoj_stokejoj', _('Povas vidi stokejoj de objektoj')),
            ('povas_krei_objektoj_stokejoj', _('Povas krei stokejoj de objektoj')),
            ('povas_forigi_objektoj_stokejoj', _('Povas forigi stokejoj de objektoj')),
            ('povas_sxangxi_objektoj_stokejoj', _('Povas ŝanĝi stokejoj de objektoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{} ({})'.format(self.uuid, get_enhavo(self.priskribo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.filter(posedanto_objekto=self.posedanto_objekto).aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        # реализация автоинкремента шаблона при сохранении
        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(ObjektoStokejo, self).save(force_insert=force_insert, force_update=force_update,
                                                 using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'objektoj.povas_vidi_objektoj_stokejoj', 
                'objektoj.povas_krei_objektoj_stokejoj',
                'objektoj.povas_forigi_objektoj_stokejoj',
                'objektoj.povas_sxangxi_objektoj_stokejoj'
            ))

            # Добавляем прав всем на просмотр, т.к. нужно видеть других игроков
            # all_perms.update(['povas_vidi_objektoj_stokejoj',])

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('objektoj.povas_vidi_objektoj_stokejoj')
                    or user_obj.has_perm('objektoj.povas_vidi_objektoj_stokejoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('objektoj.povas_vidi_objektoj_stokejoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы связей объектов между собой, использует абстрактный класс UniversoBazaMaks
class ObjektoLigiloTipo(UniversoBazaMaks):

    # уникальный личный ID типа связей объектов между собой
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'objektoj_ligiloj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de ligiloj de objektoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de ligiloj de objektoj')
        # права
        permissions = (
            ('povas_vidi_objektoj_ligiloj_tipoj',
             _('Povas vidi tipoj de ligiloj de objektoj')),
            ('povas_krei_objektoj_ligiloj_tipoj',
             _('Povas krei tipoj de ligiloj de objektoj')),
            ('povas_forigi_objektoj_ligiloj_tipoj',
             _('Povas forigi tipoj de ligiloj de objektoj')),
            ('povas_sxangxi_objektoj_ligiloj_tipoj',
             _('Povas ŝanĝi tipoj de ligiloj de objektoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(ObjektoLigiloTipo, self).save(force_insert=force_insert, force_update=force_update,
                                          using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'objektoj.povas_vidi_objektoj_ligiloj_tipoj', 
                'objektoj.povas_krei_objektoj_ligiloj_tipoj',
                'objektoj.povas_forigi_objektoj_ligiloj_tipoj',
                'objektoj.povas_sxangxi_objektoj_ligiloj_tipoj'
            ))

            # Добавляем прав всем на просмотр, т.к. нужно видеть других игроков
            # all_perms.update(['povas_vidi_objektoj_ligiloj_tipoj',])

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('objektoj.povas_vidi_objektoj_ligiloj_tipoj')
                    or user_obj.has_perm('objektoj.povas_vidi_objektoj_ligiloj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('objektoj.povas_vidi_objektoj_ligiloj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Связь объектов между собой, использует абстрактный класс UniversoBazaMaks
class ObjektoLigilo(UniversoBazaMaks):

    # объект владелец связи
    posedanto = models.ForeignKey(Objekto, verbose_name=_('Objekto - posedanto'), blank=False, null=False,
                                          default=None, on_delete=models.CASCADE)

    # место хранения владельца связи
    posedanto_stokejo = models.ForeignKey(ObjektoStokejo, verbose_name=_('Stokejo posedanto'), blank=True,
                                          null=True, default=None, on_delete=models.CASCADE)

    # разъём (слот), который используется у родительского объекта
    konektilo_posedanto = models.IntegerField(_('Konektilo - posedanto'), blank=True, null=True, default=None)

    # связываемый объект
    ligilo = models.ForeignKey(Objekto, verbose_name=_('Objekto - ligilo'), blank=False, null=False,
                                       default=None, related_name='%(app_label)s_%(class)s_ligilo_objekto',
                                       on_delete=models.CASCADE)

    # разъём (слот), который используется у связываемого объекта
    konektilo_ligilo = models.IntegerField(_('Konektilo - ligilo'), blank=True, null=True, default=None)

    # тип связи объектов
    tipo = models.ForeignKey(ObjektoLigiloTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'objektoj_ligiloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ligilo de objektoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ligiloj de objektoj')
        # права
        permissions = (
            ('povas_vidi_objektoj_ligiloj', _('Povas vidi ligiloj de objektoj')),
            ('povas_krei_objektoj_ligiloj', _('Povas krei ligiloj de objektoj')),
            ('povas_forigi_objektoj_ligiloj', _('Povas forigi ligiloj de objektoj')),
            ('povas_sxangxi_objektoj_ligiloj', _('Povas ŝanĝi ligiloj de objektoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(ObjektoLigilo, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'objektoj.povas_vidi_objektoj_ligiloj', 
                'objektoj.povas_krei_objektoj_ligiloj',
                'objektoj.povas_forigi_objektoj_ligiloj',
                'objektoj.povas_sxangxi_objektoj_ligiloj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('objektoj.povas_vidi_objektoj_ligiloj')
                    or user_obj.has_perm('objektoj.povas_vidi_objektoj_ligiloj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('objektoj.povas_vidi_objektoj_ligiloj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Связь пользователя с управляемым объектом в соответствующем мире, использует абстрактный класс UniversoBazaRealeco
class ObjektoUzanto(UniversoBazaRealeco):

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # управляемый объект (корабль, андроид)
    objekto = models.ForeignKey(Objekto, verbose_name=_('Direktebla objekto'), blank=False, null=False,
                                default=None, on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'objektoj_uzantoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Uzanto de objektoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Uzantoj de objektoj')
        # уникальность связи обязательна
        unique_together = ('autoro', 'realeco')
        # права
        permissions = (
            ('povas_vidi_objektoj_uzantoj', _('Povas vidi uzantoj de objektoj')),
            ('povas_krei_objektoj_uzantoj', _('Povas krei uzantoj de objektoj')),
            ('povas_forigi_objektoj_uzantoj', _('Povas forigi uzantoj de objektoj')),
            ('povas_sxangxi_objektoj_uzantoj', _('Povas ŝanĝi uzantoj de objektoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(ObjektoUzanto, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'objektoj.povas_vidi_objektoj_uzantoj', 
                'objektoj.povas_krei_objektoj_uzantoj',
                'objektoj.povas_forigi_objektoj_uzantoj',
                'objektoj.povas_sxangxi_objektoj_uzantoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('objektoj.povas_vidi_objektoj_uzantoj')
                    or user_obj.has_perm('objektoj.povas_vidi_objektoj_uzantoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('objektoj.povas_vidi_objektoj_uzantoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# volumeno - объем
# loza - свобо́дный 
def loza_volumeno(objekto):
    """
    расчёт свободного объёма в объекте
    """
    # находим тип связи - находится внутри (id = 3)
    try:
        tipo_ligilo = ObjektoLigiloTipo.objects.get(id=3, forigo=False,
                                            arkivo=False, publikigo=True)
    except ObjektoLigiloTipo.DoesNotExist:
        return 'нет типа связи по нахождению внутри'
    ligiloj = ObjektoLigilo.objects.filter(
        tipo=tipo_ligilo, #связь "находится внутри"
        posedanto=objekto,
        forigo=False,
        arkivo=False, 
        publikigo=True)
    summa = 0.0
    for ligilo in ligiloj:
        if ligilo.ligilo.volumeno_stokado:
            summa += ligilo.ligilo.volumeno_stokado
    if objekto.volumeno_interna:
        volumeno = objekto.volumeno_interna - summa
    else:
        volumeno = 999999999
    return volumeno


# Температурный Режим temperatura reĝimo
class ObjektoTemperaturaReghimo(SiriusoBazaAbstrakta3):

    # Code
    kodo = models.CharField(_('Kodo'), max_length=16, default=None, blank=True, null=True)

    # Description, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # НижняяГраница  (Нижняя - malsupra) (граница - limo)
    malsupra_limo = models.IntegerField(_("Malsupra limo"), default=None, blank=True, null=True)

    # ВерхняяГраница  (Верхняя - supra)
    supra_limo = models.IntegerField(_("Supra limo"), default=None, blank=True, null=True)

    # НижнейГраницыНет 
    malsupra_limo_ne = models.BooleanField(_("Malsupra limo ne"), blank=True, null=True, default=None)

    # ВерхнейГраницыНет 
    supra_limo_ne = models.BooleanField(_("Supra limo ne"), blank=True, null=True, default=None)

    # Predefined  Предопределенный
    predefined = models.BooleanField(_("Predefined"), blank=True, null=True, default=None)

    # PredefinedDataName 
    predefined_nomo = models.JSONField(verbose_name=_('Predefined nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'objektoj_temperatura_reghimo'
        # читабельное название модели, в единственном числе
        verbose_name = _('Objekto de temperatura de reghimo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Objektoj de temperatura de reghimo')
        # права
        permissions = (
            ('povas_vidi_objektoj_temperatura_reghimo', _('Povas vidi objektoj de temperatura de reghimo')),
            ('povas_krei_objektoj_temperatura_reghimo', _('Povas krei objektoj de temperatura de reghimo')),
            ('povas_forigi_objektoj_temperatura_reghimo', _('Povas forigi objektoj de temperatura de reghimo')),
            ('povas_shangxi_objektoj_temperatura_reghimo', _('Povas ŝanĝi objektoj de temperatura de reghimo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.kodo, get_enhavo(self.priskribo, empty_values=True)[0])

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'objektoj.povas_vidi_objektoj_temperatura_reghimo', 
                'objektoj.povas_krei_objektoj_temperatura_reghimo',
                'objektoj.povas_forigi_objektoj_temperatura_reghimo',
                'objektoj.povas_shangxi_objektoj_temperatura_reghimo'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('objektoj.povas_vidi_objektoj_temperatura_reghimo')
                    or user_obj.has_perm('objektoj.povas_vidi_objektoj_temperatura_reghimo')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('objektoj.povas_vidi_objektoj_temperatura_reghimo'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# ТипКонтейнера tipo (конте́йнер kontenero;)
class ObjektoTipoKontenero(SiriusoBazaAbstrakta3):

    # Code
    kodo = models.CharField(_('Kodo'), max_length=16, default=None, blank=True, null=True)

    # Description, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # Объем
    volumeno = models.FloatField(_("Volumeno"), default=None, blank=True, null=True)

    # Вес
    pezo = models.FloatField(_("Pezo"), default=None, blank=True, null=True)

    # Комментарий
    komento = models.JSONField(verbose_name=_('Komento'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # ВесТары
    pezo_pakumo = models.FloatField(_("Pezo pakumo"), default=None, blank=True, null=True)

    # Predefined  Предопределенный
    predefined = models.BooleanField(_("Predefined"), blank=True, null=True, default=None)

    # PredefinedDataName 
    predefined_nomo = models.JSONField(verbose_name=_('Predefined nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'objektoj_tipo_kontenero'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de kontenero de objekto')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de konteneroj de objektoj')
        # права
        permissions = (
            ('povas_vidi_objektoj_tipo_kontenero', _('Povas vidi objektoj de kontenero de tipo')),
            ('povas_krei_objektoj_tipo_kontenero', _('Povas krei objektoj de kontenero de tipo')),
            ('povas_forigi_objektoj_tipo_kontenero', _('Povas forigi objektoj de kontenero de tipo')),
            ('povas_shangxi_objektoj_tipo_kontenero', _('Povas ŝanĝi objektoj de kontenero de tipo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.kodo, get_enhavo(self.priskribo, empty_values=True)[0])

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'objektoj.povas_vidi_objektoj_tipo_kontenero', 
                'objektoj.povas_krei_objektoj_tipo_kontenero',
                'objektoj.povas_forigi_objektoj_tipo_kontenero',
                'objektoj.povas_shangxi_objektoj_tipo_kontenero'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('objektoj.povas_vidi_objektoj_tipo_kontenero')
                    or user_obj.has_perm('objektoj.povas_vidi_objektoj_tipo_kontenero')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('objektoj.povas_vidi_objektoj_tipo_kontenero'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Catalog_ТипУпаковкиСКЛ tipo (упаковка (материал) - pakumo)
class ObjektoTipoPakumo(SiriusoBazaAbstrakta3):

    # Code
    kodo = models.CharField(_('Kodo'), max_length=16, default=None, blank=True, null=True)

    # Description, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # Длина
    longo = models.FloatField(_("Longo"), default=None, blank=True, null=True)

    # Ширина (larĝo)
    largho = models.FloatField(_("Largho"), default=None, blank=True, null=True)

    # Высота
    alto = models.FloatField(_("Alto"), default=None, blank=True, null=True)

    # ВесФактический (факти́чески fakte; ~й fakta)
    pezo_fakta = models.FloatField(_("Pezo fakta"), default=None, blank=True, null=True)

    # ВозвратУпаковки
    retropasxo_pakumo = models.BooleanField(_("Retropasxo pakumo"), blank=True, null=True, default=False)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'objektoj_tipo_pakumo'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de pakumo de objekto')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de pakumoj de objektoj')
        # права
        permissions = (
            ('povas_vidi_objektoj_tipo_pakumo', _('Povas vidi objektoj de pakumo de tipo')),
            ('povas_krei_objektoj_tipo_pakumo', _('Povas krei objektoj de pakumo de tipo')),
            ('povas_forigi_objektoj_tipo_pakumo', _('Povas forigi objektoj de pakumo de tipo')),
            ('povas_shangxi_objektoj_tipo_pakumo', _('Povas ŝanĝi objektoj de pakumo de tipo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.kodo, get_enhavo(self.priskribo, empty_values=True)[0])

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('objektoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'objektoj.povas_vidi_objektoj_tipo_pakumo', 
                'objektoj.povas_krei_objektoj_tipo_pakumo',
                'objektoj.povas_forigi_objektoj_tipo_pakumo',
                'objektoj.povas_shangxi_objektoj_tipo_pakumo'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='objektoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('objektoj.povas_vidi_objektoj_tipo_pakumo')
                    or user_obj.has_perm('objektoj.povas_vidi_objektoj_tipo_pakumo')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('objektoj.povas_vidi_objektoj_tipo_pakumo'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


