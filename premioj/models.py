"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import models
from django.db.models import Max, Q
from django.contrib.auth.models import Permission
import random, string
from django.utils.translation import gettext_lazy as _
from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, perms
from main.models import SiriusoBazaAbstrakta3
from main.models import SiriusoTipoAbstrakta
from main.models import Uzanto
import sys


# Типы наград, использует абстрактный класс SiriusoTipoAbstrakta
class PremiojTipo(SiriusoTipoAbstrakta):

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'premioj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de premioj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de premioj')
        # права
        permissions = (
            ('povas_vidi_tipon', _('Povas vidi tipon')),
            ('povas_krei_tipon', _('Povas krei tipon')),
            ('povas_forigi_tipon', _('Povas forigu tipon')),
            ('povas_shanghi_tipon', _('Povas ŝanĝi tipon')),
        )

    @staticmethod
    def _get_perm_cond(user_obj):
        return Q()


# Степени наград, использует абстрактный класс SiriusoBazaAbstrakta3
class PremiojGrado(SiriusoBazaAbstrakta3):

    # степень
    grado = models.IntegerField(_('Grado'), blank=False, default=1)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'premioj_gradoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Grado de premioj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Gradoj de premioj')
        # права
        permissions = (
            ('povas_vidi_gradon', _('Povas vidi gradon')),
            ('povas_krei_gradon', _('Povas krei gradon')),
            ('povas_forigi_gradon', _('Povas forigu gradon')),
            ('povas_shanghi_gradon', _('Povas ŝanĝi gradon')),
        )

    @staticmethod
    def _get_perm_cond(user_obj):
        return Q()

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле grado этой модели
        return '{}'.format(self.grado)


# Виды наград, использует абстрактный класс SiriusoTipoAbstrakta
class PremiojSpeco(SiriusoTipoAbstrakta):

    # название в таблице названий видов наград, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание в таблице описаний видов наград, от туда будет браться описание с нужным языковым тегом
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'premioj_speco'
        # читабельное название модели, в единственном числе
        verbose_name = _('Speco de premioj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Specoj de premioj')
        # права
        permissions = (
            ('povas_vidi_specon', _('Povas vidi specon')),
            ('povas_krei_specon', _('Povas krei specon')),
            ('povas_forigi_specon', _('Povas forigu specon')),
            ('povas_shanghi_specon', _('Povas ŝanĝi specon')),
        )

    @staticmethod
    def _get_perm_cond(user_obj):
        return Q()


# Генерация случайного названия и переименование загружаемых картинок аватар советов
def premioj_bildo_nomo(instance, filename):
    letters = string.ascii_letters + string.digits
    rnd_string = ''.join(random.choice(letters) for i in range(10))
    # указание папки для хранения файлов и собирание конечного названия с сохранением изначального расширения
    return 'premioj/bildoj/{0}.{1}'.format(rnd_string, filename.split('.')[-1])


# Перечень наград (ими будет награждение), использует абстрактный класс SiriusoBazaAbstrakta3
class PremiojPremio(SiriusoBazaAbstrakta3):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # вид награды
    speco = models.ForeignKey(PremiojSpeco, verbose_name=_('Speco'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # тип награды
    tipo = models.ForeignKey(PremiojTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # степень награды
    grado = models.ForeignKey(PremiojGrado, verbose_name=_('Grado'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # миниатюра изображения награды (размер 50х50)
    bildo_min = models.ImageField(_('Miniaturo'), upload_to=premioj_bildo_nomo, blank=True)

    # информационное изображение
    bildo_info = models.ImageField(_('Infa bildo'), upload_to=premioj_bildo_nomo, blank=True)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'premioj_premioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Premio')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Premioj')
        # права
        permissions = (
            ('povas_vidi_premiojn', _('Povas vidi premiojn')),
            ('povas_krei_premiojn', _('Povas krei premiojn')),
            ('povas_forigi_premiojn', _('Povas forigu premiojn')),
            ('povas_shanghi_premiojn', _('Povas ŝanĝi premiojn')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле speco этой модели
        return '{} {} {}'.format(self.speco, self.tipo, self.grado)

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(PremiojPremio, self).save(force_insert=force_insert, force_update=force_update,
                                           using=using, update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            all_perms = set(perms.user_registrita_perms(apps=('premioj',)))

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'premioj.povas_vidi_premiojn', 'premioj.povas_krei_premiojn',
                'premioj.povas_forigi_premiojn', 'premioj.povas_shanghi_premiojn'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='premioj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            if (perms.has_registrita_perm('premioj.povas_vidi_premiojn')
                    or user_obj.has_perm('premioj.povas_vidi_premiojn')):
                cond = Q()
            else:
                cond = Q(uuid__isnull=True)

        else:
            if perms.has_neregistrita_perm('premioj.povas_vidi_premiojn'):
                cond = Q()
            else:
                cond = Q(uuid__isnull=True)

        return cond

# Условия награждения (справочник), использует абстрактный класс SiriusoTipoAbstrakta
class PremiojKondicho(SiriusoTipoAbstrakta):

    # название в таблице названий условий награждения, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание в таблице описаний условий награждения, от туда будет браться описание с нужным языковым тегом
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'premioj_kondichoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kondiĉo de premiado')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kondiĉoj de premiadoj')
        # права
        permissions = (
            ('povas_vidi_kondichojn', _('Povas vidi kondichojn')),
            ('povas_krei_kondichojn', _('Povas krei kondichojn')),
            ('povas_forigi_kondichojn', _('Povas forigu kondichojn')),
            ('povas_shanghi_kondichojn', _('Povas ŝanĝi kondichojn')),
        )
    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            all_perms = set(perms.user_registrita_perms(apps=('premioj',)))

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'premioj.povas_vidi_kondichojn', 'premioj.povas_krei_kondichojn',
                'premioj.povas_forigi_kondichojn', 'premioj.povas_shanghi_kondichojn'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='premioj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            if (perms.has_registrita_perm('premioj.povas_vidi_kondichojn')
                    or user_obj.has_perm('premioj.povas_vidi_kondichojn')):
                cond = Q()
            else:
                cond = Q(uuid__isnull=True)

        else:
            if perms.has_neregistrita_perm('premioj.povas_vidi_kondichojn'):
                cond = Q()
            else:
                cond = Q(uuid__isnull=True)

        return cond


# Награждение (перечень всех присвоений наград), использует абстрактный класс SiriusoBazaAbstrakta3
class PremiojPremiado(SiriusoBazaAbstrakta3):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # награждённый (человек, которому вручили награду)
    premiita = models.ForeignKey(Uzanto, verbose_name=_('Premiita'), blank=False, default=None,
                                 related_name='%(app_label)s_%(class)s_premiita', on_delete=models.CASCADE)

    # награда (из перечня наград)
    premio = models.ForeignKey(PremiojPremio, verbose_name=_('Premio'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # причина награждания (какое условие было выполнено)
    kondicho = models.ForeignKey(PremiojKondicho, verbose_name=_('Kondiĉo'), blank=False, default=None,
                                 on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'premioj_premiadoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Premiado')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Premiadoj')
        # права
        permissions = (
            ('povas_vidi_premiadojn', _('Povas vidi premiadojn')),
            ('povas_krei_premiadojn', _('Povas krei premiadojn')),
            ('povas_forigi_premiadojn', _('Povas forigu premiadojn')),
            ('povas_shanghi_premiadojn', _('Povas ŝanĝi premiadojn')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поля premiita и premio этой модели
        return '{} {}'.format(self.premiita, self.premio)

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(PremiojPremiado, self).save(force_insert=force_insert, force_update=force_update,
                                           using=using, update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            all_perms = set(perms.user_registrita_perms(apps=('premioj',)))

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'premioj.povas_vidi_premiadojn', 'premioj.povas_krei_premiadojn',
                'premioj.povas_forigi_premiadojn', 'premioj.povas_shanghi_premiadojn'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='premioj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            if (perms.has_registrita_perm('premioj.povas_vidi_premiadojn')
                    or user_obj.has_perm('premioj.povas_vidi_premiadojn')):
                cond = Q()
            else:
                cond = Q(uuid__isnull=True)

        else:
            if perms.has_neregistrita_perm('premioj.povas_vidi_premiadojn'):
                cond = Q()
            else:
                cond = Q(uuid__isnull=True)

        return cond
