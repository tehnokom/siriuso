"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class PremiojConfig(AppConfig):
    name = 'premioj'
    verbose_name = _('Premioj')

    # Метод срабатывает при регистрации приложения в системе django
    def ready(self):
        # Импортом сообщаем django, что надо проанализировать кодв в модуле tasks
        # При этом происходит регистрация функций заданий celery
        import muroj.tasks