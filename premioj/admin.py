"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from .models import *


# Типы наград
@admin.register(PremiojTipo)
class PremiojTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'publikigo', 'kodo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = PremiojTipo


# Степени наград
@admin.register(PremiojGrado)
class PremiojGradoAdmin(admin.ModelAdmin):
    list_display = ('grado', 'publikigo', 'uuid')

    class Meta:
        model = PremiojGrado


# Виды наград
@admin.register(PremiojSpeco)
class PremiojSpecoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'publikigo', 'uuid')
    exclude = ('nomo', 'priskribo')

    class Meta:
        model = PremiojSpeco


# Перечень наград (справочник наград со всеми характеристиками, ими будет награждение)
@admin.register(PremiojPremio)
class PremiojPremioAdmin(admin.ModelAdmin):
    list_display = ('speco', 'tipo', 'grado', 'publikigo', 'uuid')

    class Meta:
        model = PremiojPremio


# Условия награждения (справочник)
@admin.register(PremiojKondicho)
class PremiojKondichoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'publikigo', 'uuid')
    exclude = ('nomo', 'priskribo')

    class Meta:
        model = PremiojKondicho


# Награждение (перечень всех присвоений наград пользователям)
@admin.register(PremiojPremiado)
class PremiojPremiadoAdmin(admin.ModelAdmin):
    list_display = ('premiita', 'premio', 'publikigo', 'kondicho')

    class Meta:
        model = PremiojPremiado
