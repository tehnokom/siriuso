"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.utils import timezone
from django.db import transaction # создание и изменения будем делать в блокирующейтранзакции
from django.utils.translation import gettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from ..tasks import sciigi_premioj
from .schema import *
from ..models import *

# Типы наград
class RedaktuPremiojTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    tipo = graphene.Field(PremiojTipoNode, description=_('Созданная/изменённая запись типа награды'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название типа награды'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('premioj.povas_krei_tipon'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            PremiojTipo.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except PremiojTipo.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            tipo = PremiojTipo.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            tipo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                                or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                                or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tipo = PremiojTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('premioj.povas_forigi_tipon')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('premioj.povas_shanghi_tipon'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                tipo.kodo = kwargs.get('kodo', tipo.kodo)
                                tipo.forigo = kwargs.get('forigo', tipo.forigo)
                                tipo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tipo.arkivo = kwargs.get('arkivo', tipo.arkivo)
                                tipo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tipo.publikigo = kwargs.get('publikigo', tipo.publikigo)
                                tipo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                tipo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except PremiojTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuPremiojTipo(status=status, message=message, tipo=tipo)

# Степени наград
class RedaktuPremiojGrado(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    gradoj = graphene.Field(PremiojGradoNode, description=_('Созданная/изменённая запись степени награды'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        grado = graphene.Int(description=_('Код типа'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        gradoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('premioj.povas_krei_gradon'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('grado', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'grado')

                        # Проверяем наличие записи с таким кодом
                        try:
                            PremiojGrado.objects.get(grado=kwargs.get('grado'), forigo=False)
                            message = _('Запись с такой степенью уже существует')
                        except PremiojGrado.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            gradoj = PremiojGrado.objects.create(
                                grado=kwargs.get('grado'),
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            gradoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('grado', False)
                                or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                                or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            gradoj = PremiojGrado.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('premioj.povas_forigi_gradon')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('premioj.povas_shanghi_gradon'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                gradoj.grado = kwargs.get('grado', gradoj.grado)
                                gradoj.forigo = kwargs.get('forigo', gradoj.forigo)
                                gradoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                gradoj.arkivo = kwargs.get('arkivo', gradoj.arkivo)
                                gradoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                gradoj.publikigo = kwargs.get('publikigo', gradoj.publikigo)
                                gradoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                gradoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except PremiojGrado.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuPremiojGrado(status=status, message=message, gradoj=gradoj)

# Виды наград
class RedaktuPremiojSpeco(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    speco = graphene.Field(PremiojSpecoNode, description=_('Созданная/изменённая запись вида награды'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название вида награды'))
        priskribo = graphene.String(description=_('Описание вида награды'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        speco = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('premioj.povas_krei_specon'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not (kwargs.get('priskribo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'priskribo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            PremiojSpeco.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except PremiojSpeco.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            speco = PremiojSpeco.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(speco.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            set_enhavo(speco.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                            speco.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                                or kwargs.get('priskribo', False)
                                or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                                or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            speco = PremiojSpeco.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('premioj.povas_forigi_specon')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('premioj.povas_shanghi_specon'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                speco.kodo = kwargs.get('kodo', speco.kodo)
                                speco.forigo = kwargs.get('forigo', speco.forigo)
                                speco.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                speco.arkivo = kwargs.get('arkivo', speco.arkivo)
                                speco.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                speco.publikigo = kwargs.get('publikigo', speco.publikigo)
                                speco.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(speco.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                if kwargs.get('priskribo', False):
                                    set_enhavo(speco.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)

                                speco.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except PremiojSpeco.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuPremiojSpeco(status=status, message=message, speco=speco)

# Перечень наград (ими будет награждение)
class RedaktuPremiojPremio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    premio = graphene.Field(PremiojPremioNode, description=_('Созданная/изменённая запись награды'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        tipo_kodo = graphene.String(description=_('Код типа награды'))
        speco_kodo = graphene.String(description=_('Код вида награды'))
        grado_grado = graphene.Int(description=_('Степень награды'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        premio = None
        tipo = None
        grado = None
        speco = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('premioj.povas_krei_premiojn'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            if not (kwargs.get('tipo_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'tipo_kodo')
                            else:
                                try:
                                    tipo = PremiojTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except PremiojTipo.DoesNotExist:
                                    message = _('Неверный тип награды')

                        if not message:
                            if not (kwargs.get('grado_grado', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'grado_grado')
                            else:
                                try:
                                    grado = PremiojGrado.objects.get(grado=kwargs.get('grado_grado'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except PremiojGrado.DoesNotExist:
                                    message = _('Неверная степень награды')

                        if not message:
                            if not (kwargs.get('speco_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'speco_kodo')
                            else:
                                try:
                                    speco = PremiojSpeco.objects.get(kodo=kwargs.get('speco_kodo'), forigo=False,
                                                                     arkivo=False)
                                except PremiojSpeco.DoesNotExist:
                                    message = _('Неверный вид награды')

                        if not message:
                            premio = PremiojPremio.objects.create(
                                tipo=tipo,
                                grado=grado,
                                speco=speco,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            premio.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('grado_grado', False) or kwargs.get('tipo_kodo', False)
                                or kwargs.get('speco_kodo', False) or kwargs.get('publikigo', False)
                                or kwargs.get('forigo', False) or kwargs.get('arkivo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'tipo_kodo' in kwargs and not message:
                        try:
                            tipo = PremiojTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except PremiojTipo.DoesNotExist:
                            message = _('Неверный тип награды')

                    if 'grado_grado' in kwargs and not message:
                        try:
                            grado = PremiojGrado.objects.get(grado=kwargs.get('grado_grado'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except PremiojGrado.DoesNotExist:
                            message = _('Неверная степень награды')

                    if 'speco_kodo' in kwargs and not message:
                        try:
                            speco = PremiojSpeco.objects.get(kodo=kwargs.get('speco_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except PremiojSpeco.DoesNotExist:
                            message = _('Неверный вид награды')

                    # Ищем запись для изменения
                    if not message:
                        try:
                            premio = PremiojPremio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('premioj.povas_forigi_premiojn')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('premioj.povas_shanghi_premiojn'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                premio.tipo = tipo or premio.tipo
                                premio.grado = grado or premio.grado
                                premio.speco = speco or premio.speco
                                premio.forigo = kwargs.get('forigo', premio.forigo)
                                premio.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                premio.arkivo = kwargs.get('arkivo', premio.arkivo)
                                premio.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                premio.publikigo = kwargs.get('publikigo', premio.publikigo)
                                premio.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                premio.lasta_autoro = uzanto
                                premio.lasta_dato = timezone.now()

                                premio.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except PremiojPremio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuPremiojPremio(status=status, message=message, premio=premio)

# Условия награждения
class RedaktuPremiojKondicho(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kondicho = graphene.Field(PremiojKondichoNode, description=_('Созданная/изменённая запись условия награждения'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название условия награждения'))
        priskribo = graphene.String(description=_('Описание условия награждения'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kondicho = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('premioj.povas_krei_kondichojn'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not (kwargs.get('priskribo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'priskribo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            PremiojKondicho.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except PremiojKondicho.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            kondicho = PremiojKondicho.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(kondicho.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            set_enhavo(kondicho.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                            kondicho.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                                or kwargs.get('priskribo', False)
                                or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                                or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kondicho = PremiojKondicho.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('premioj.povas_forigi_kondichojn')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('premioj.povas_shanghi_kondichojn'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                kondicho.kodo = kwargs.get('kodo', kondicho.kodo)
                                kondicho.forigo = kwargs.get('forigo', kondicho.forigo)
                                kondicho.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kondicho.arkivo = kwargs.get('arkivo', kondicho.arkivo)
                                kondicho.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kondicho.publikigo = kwargs.get('publikigo', kondicho.publikigo)
                                kondicho.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(kondicho.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                if kwargs.get('priskribo', False):
                                    set_enhavo(kondicho.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)

                                kondicho.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except PremiojKondicho.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuPremiojKondicho(status=status, message=message, kondicho=kondicho)

# Награждение (перечень всех присвоений наград)
class RedaktuPremiojPremiado(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    premiado = graphene.Field(PremiojPremiadoNode, description=_('Созданная/изменённая запись награды'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kondicho_kodo = graphene.String(description=_('Код причины награждания'))
        premio_id = graphene.Int(description=_('Код награды'))
        premiita_id = graphene.String(description=_('Код награждённого'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        premio = None
        kondicho = None
        premiita = None
        premiado = None
        uzantos = info.context.user

        if uzantos.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzantos.has_perm('premioj.povas_krei_premiadojn'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            if not (kwargs.get('kondicho_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kondicho_kodo')
                            else:
                                try:
                                    kondicho = PremiojKondicho.objects.get(kodo=kwargs.get('kondicho_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except PremiojKondicho.DoesNotExist:
                                    message = _('Неверный код причины награждания')

                        if not message:
                            if not (kwargs.get('premiita_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'premiita_id')
                            else:
                                try:
                                    premiita = Uzanto.objects.get(id=kwargs.get('premiita_id'), is_active=True,
                                                konfirmita=True)
                                except Uzanto.DoesNotExist:
                                    message = _('Неверный награждённый')

                        if not message:
                            if not (kwargs.get('premio_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'premio_id')
                            else:
                                try:
                                    premio = PremiojPremio.objects.get(id=kwargs.get('premio_id'), forigo=False,
                                                                     arkivo=False)
                                except PremiojPremio.DoesNotExist:
                                    message = _('Неверный код награды')

                        if not message:
                            premiado = PremiojPremiado.objects.create(
                                kondicho=kondicho,
                                premiita=premiita,
                                premio=premio,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            premiado.save()

                            status = True
                            message = _('Запись создана')
                            # Отправить сообщение о новой записи в чате всем остальным участникам чата 
                            sciigi_premioj.delay(premiado.uuid)
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kondicho_kodo', False) or kwargs.get('premiita_id', False)
                                or kwargs.get('premio_id', False) or kwargs.get('publikigo', False)
                                or kwargs.get('forigo', False) or kwargs.get('arkivo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'kondicho_kodo' in kwargs and not message:
                        try:
                            kondicho = PremiojKondicho.objects.get(kodo=kwargs.get('kondicho_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except PremiojKondicho.DoesNotExist:
                            message = _('Неверный код причины награждания')

                    if 'premiita_id' in kwargs and not message:
                        try:
                            premiita = Uzanto.objects.get(id=kwargs.get('premiita_id'), is_active=True,
                                                konfirmita=True)
                        except Uzanto.DoesNotExist:
                            message = _('Неверный награждённый')

                    if 'premio_id' in kwargs and not message:
                        try:
                            premio = PremiojPremio.objects.get(id=kwargs.get('premio_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except PremiojPremio.DoesNotExist:
                            message = _('Неверный код награды')

                    # Ищем запись для изменения
                    if not message:
                        try:
                            premiado = PremiojPremiado.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzantos.has_perm('premioj.povas_forigi_premiadojn')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzantos.has_perm('premioj.povas_shanghi_premiadojn'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                premiado.kondicho = kondicho or premiado.kondicho
                                premiado.premiita = premiita or premiado.premiita
                                premiado.premio = premio or premiado.premio
                                premiado.forigo = kwargs.get('forigo', premiado.forigo)
                                premiado.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                premiado.arkivo = kwargs.get('arkivo', premiado.arkivo)
                                premiado.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                premiado.publikigo = kwargs.get('publikigo', premiado.publikigo)
                                premiado.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                premiado.lasta_autoro = uzantos
                                premiado.lasta_dato = timezone.now()

                                premiado.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except PremiojPremiado.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuPremiojPremiado(status=status, message=message, premiado=premiado)

class PremiojMutations(graphene.ObjectType):
    redaktu_premioj_tipo = RedaktuPremiojTipo.Field(
        description=_('''Создаёт или редактирует типы наград''')
    )
    redaktu_premioj_grado = RedaktuPremiojGrado.Field(
        description=_('''Создаёт или редактирует степени наград''')
    )
    redaktu_premioj_speco = RedaktuPremiojSpeco.Field(
        description=_('''Создаёт или редактирует виды наград''')
    )
    redaktu_premioj_premio = RedaktuPremiojPremio.Field(
        description=_('''Создаёт или редактирует перечень наград''')
    )
    redaktu_premioj_kondicho = RedaktuPremiojKondicho.Field(
        description=_('''Создаёт или редактирует условия награждения''')
    )
    redaktu_premioj_premiado = RedaktuPremiojPremiado.Field(
        description=_('''Создаёт или редактирует запись награды''')
    )

