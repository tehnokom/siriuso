"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene # сам Graphene
from graphene_django import DjangoObjectType # Класс описания модели Django для Graphene
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions # Миксины для описания типов  Graphene
from graphene_permissions.permissions import AllowAny # Класс доступа к типу Graphene
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from siriuso.api.filters import SiriusoFilterConnectionField # Коннектор для получения данных
from siriuso.api.types import SiriusoLingvo # Объект Graphene для представления мультиязычного поля
from ..models import * # модели приложения

# Типы наград
class PremiojTipoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование типа награды'))

    class Meta:
        model = PremiojTipo
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)

# Степени наград
class PremiojGradoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'grado': ['icontains',]
    }


    class Meta:
        model = PremiojGrado
        filter_fields = {
            'uuid': ['exact'],
            'grado': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)

# Виды наград
class PremiojSpecoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'priskribo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование вида награды'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание вида награды'))

    class Meta:
        model = PremiojSpeco
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith']
        }
        interfaces = (graphene.relay.Node,)

# Перечень наград (ими будет награждение)
class PremiojPremioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'id': ['icontains',]
    }

    class Meta:
        model = PremiojPremio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'speco__kodo': ['exact'],
            'speco__uuid': ['exact'],
            'tipo__kodo': ['exact'],
            'tipo__uuid': ['exact'],
            'grado__grado': ['exact'],
            'grado__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

# Условия награждения
class PremiojKondichoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'priskribo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование условия'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание условия'))

    class Meta:
        model = PremiojKondicho
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith']
        }
        interfaces = (graphene.relay.Node,)

# Награждение (перечень всех присвоений наград)
class PremiojPremiadoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'id': ['icontains',]
    }

    class Meta:
        model = PremiojPremiado
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'premiita__id': ['exact'],
            'premiita__uuid': ['exact'],
            'premio__id': ['exact'],
            'premio__uuid': ['exact'],
            'kondicho__kodo': ['exact'],
            'kondicho__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

class PremiojQuery(graphene.ObjectType):
    premioj_tipoj = SiriusoFilterConnectionField(PremiojTipoNode,
                                                          description=_('Выводит все доступные типы наград'))
    premioj_gradoj = SiriusoFilterConnectionField(PremiojGradoNode,
                                                          description=_('Выводит все доступные степени наград'))
    premioj_specoj = SiriusoFilterConnectionField(PremiojSpecoNode,
                                                          description=_('Выводит все доступные виды наград'))
    premioj_premioj = SiriusoFilterConnectionField(PremiojPremioNode,
                                                          description=_('Выводит весь доступный перечень наград'))
    premioj_kondichoj = SiriusoFilterConnectionField(PremiojKondichoNode,
                                                          description=_('Выводит все доступные условия награждения'))
    premioj_premiadoj = SiriusoFilterConnectionField(PremiojPremiadoNode,
                                                          description=_('Выводит перечень всех присвоений наград'))
