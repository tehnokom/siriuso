"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import Permission
from django.db.models import Max, Q

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from komunumoj.models import Komunumo
from universo_bazo.models import UniversoBazaMaks, UniversoBazaRealeco, Realeco
from objektoj.models import Objekto
from organizoj.models import Organizo
from dokumentoj.models import Dokumento
from main.models import Uzanto


# Категории проектов, использует абстрактный класс UniversoBazaMaks
class TaskojProjektoKategorio(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='taskoj_projekto_kategorioj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_projektoj_kategorioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kategorio de projektoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kategorioj de projektoj')
        # права
        permissions = (
            ('povas_vidi_projekto_kategorioj', _('Povas vidi kategorioj de projektoj')),
            ('povas_krei_projekto_kategorioj', _('Povas krei kategorioj de projektoj')),
            ('povas_forigi_projekto_kategorioj', _('Povas forigi kategorioj de projektoj')),
            ('povas_shangxi_projekto_kategorioj', _('Povas ŝanĝi kategorioj de projektoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojProjektoKategorio, self).save(force_insert=force_insert, force_update=force_update,
                                                    using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_projekto_kategorioj',
                'taskoj.povas_krei_projekto_kategorioj',
                'taskoj.povas_forigi_projekto_kategorioj',
                'taskoj.povas_shangxi_projekto_kategorioj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_projekto_kategorioj')
                    or user_obj.has_perm('taskoj.povas_vidi_projekto_kategorioj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_projekto_kategorioj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы проектов, использует абстрактный класс UniversoBazaMaks
class TaskojProjektoTipo(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='taskoj_projekto_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_projektoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de projektoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de projektoj')
        # права
        permissions = (
            ('povas_vidi_projekto_tipoj', _('Povas vidi tipoj de projektoj')),
            ('povas_krei_projekto_tipoj', _('Povas krei tipoj de projektoj')),
            ('povas_forigi_projekto_tipoj', _('Povas forigi tipoj de projektoj')),
            ('povas_shangxi_projekto_tipoj', _('Povas ŝanĝi tipoj de projektoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojProjektoTipo, self).save(force_insert=force_insert, force_update=force_update,
                                               using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_projekto_tipoj',
                'taskoj.povas_krei_projekto_tipoj',
                'taskoj.povas_forigi_projekto_tipoj',
                'taskoj.povas_shangxi_projekto_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_projekto_tipoj')
                    or user_obj.has_perm('taskoj.povas_vidi_projekto_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_projekto_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Статус проекта, использует абстрактный класс UniversoBazaMaks
class TaskojProjektoStatuso(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_projektoj_statusoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Statuso de projektoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statusoj de projektoj')
        # права
        permissions = (
            ('povas_vidi_projekto_statusoj', _('Povas vidi statusoj de projektoj')),
            ('povas_krei_projekto_statusoj', _('Povas krei statusoj de projektoj')),
            ('povas_forigi_projekto_statusoj', _('Povas forigi statusoj de projektoj')),
            ('povas_shangxi_projekto_statusoj', _('Povas ŝanĝi statusoj de projektoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojProjektoStatuso, self).save(force_insert=force_insert, force_update=force_update,
                                                  using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_projekto_statusoj',
                'taskoj.povas_krei_projekto_statusoj',
                'taskoj.povas_forigi_projekto_statusoj',
                'taskoj.povas_shangxi_projekto_statusoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_projekto_statusoj')
                    or user_obj.has_perm('taskoj.povas_vidi_projekto_statusoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_projekto_statusoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Проекты, использует абстрактный класс UniversoBazaRealeco
class TaskojProjekto(UniversoBazaRealeco):

    # категория проектов
    kategorio = models.ManyToManyField(TaskojProjektoKategorio, verbose_name=_('Kategorio'),
                                       db_table='taskoj_projektoj_kategorioj_ligiloj')

    # тип проекта
    tipo = models.ForeignKey(TaskojProjektoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # статус проекта
    statuso = models.ForeignKey(TaskojProjektoStatuso, verbose_name=_('Statuso'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # объект
    objekto = models.ForeignKey(Objekto, verbose_name=_('Objekto'), blank=True, null=True, default=None,
                                on_delete=models.CASCADE)

    # позиция в списке
    pozicio = models.IntegerField(_('Pozicio'), blank=True, null=True, default=None)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # начальные координаты выполнения проекта по оси X в кубе
    kom_koordinato_x = models.FloatField(_('Komenca koordinato X'), blank=True, null=True, default=None)

    # начальные координаты выполнения проекта по оси Y в кубе
    kom_koordinato_y = models.FloatField(_('Komenca koordinato Y'), blank=True, null=True, default=None)

    # начальные координаты выполнения проекта по оси Z в кубе
    kom_koordinato_z = models.FloatField(_('Komenca koordinato Z'), blank=True, null=True, default=None)

    # конечные координаты выполнения проекта по оси X в кубе
    fin_koordinato_x = models.FloatField(_('Fina koordinato X'), blank=True, null=True, default=None)

    # конечные координаты выполнения проекта по оси Y в кубе
    fin_koordinato_y = models.FloatField(_('Fina koordinato Y'), blank=True, null=True, default=None)

    # конечные координаты выполнения проекта по оси Z в кубе
    fin_koordinato_z = models.FloatField(_('Fina koordinato Z'), blank=True, null=True, default=None)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_projektoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Projekto')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Projektoj')
        # права
        permissions = (
            ('povas_vidi_projektoj', _('Povas vidi projektoj')),
            ('povas_krei_projektoj', _('Povas krei projektoj')),
            ('povas_forigi_projektoj', _('Povas forigi projektoj')),
            ('povas_shangxi_projektoj', _('Povas ŝanĝi projektoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}, uuid={}'.format(get_enhavo(self.nomo, empty_values=True)[0], self.uuid)

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(TaskojProjekto, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_projektoj',
                'taskoj.povas_krei_projektoj',
                'taskoj.povas_forigi_projektoj',
                'taskoj.povas_shangxi_projektoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_projektoj')
                    or user_obj.has_perm('taskoj.povas_vidi_projektoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_projektoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы владельцев проектов, использует абстрактный класс UniversoBazaMaks
class TaskojProjektoPosedantoTipo(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_projektoj_posedantoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de posedantoj de projektoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de posedantoj de projektoj')
        # права
        permissions = (
            ('povas_vidi_projektoj_posedantoj_tipoj',
             _('Povas vidi tipoj de posedantoj de projektoj')),
            ('povas_krei_projektoj_posedantoj_tipoj',
             _('Povas krei tipoj de posedantoj de projektoj')),
            ('povas_forigi_projektoj_posedantoj_tipoj',
             _('Povas forigi tipoj de posedantoj de projektoj')),
            ('povas_shangxi_projektoj_posedantoj_tipoj',
             _('Povas ŝanĝi tipoj de posedantoj de projektoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojProjektoPosedantoTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                        using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_projektoj_posedantoj_tipoj',
                'taskoj.povas_krei_projektoj_posedantoj_tipoj',
                'taskoj.povas_forigi_projektoj_posedantoj_tipoj',
                'taskoj.povas_shangxi_projektoj_posedantoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_projektoj_posedantoj_tipoj')
                    or user_obj.has_perm('taskoj.povas_vidi_projektoj_posedantoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_projektoj_posedantoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Статус владельца проекта, использует абстрактный класс UniversoBazaMaks
class TaskojProjektoPosedantoStatuso(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_projektoj_posedantoj_statusoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Statuso de posedantoj de projektoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statusoj de posedantoj de projektoj')
        # права
        permissions = (
            ('povas_vidi_projektoj_posedantoj_statusoj',
             _('Povas vidi statusoj de posedantoj de projektoj')),
            ('povas_krei_projektoj_posedantoj_statusoj',
             _('Povas krei statusoj de posedantoj de projektoj')),
            ('povas_forigi_projektoj_posedantoj_statusoj',
             _('Povas forigi statusoj de posedantoj de projektoj')),
            ('povas_shangxi_projektoj_posedantoj_statusoj',
             _('Povas ŝanĝi statusoj de posedantoj de projektoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojProjektoPosedantoStatuso, self).save(force_insert=force_insert, force_update=force_update,
                                                           using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_projektoj_posedantoj_statusoj',
                'taskoj.povas_krei_projektoj_posedantoj_statusoj',
                'taskoj.povas_forigi_projektoj_posedantoj_statusoj',
                'taskoj.povas_shangxi_projektoj_posedantoj_statusoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_projektoj_posedantoj_statusoj')
                    or user_obj.has_perm('taskoj.povas_vidi_projektoj_posedantoj_statusoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_projektoj_posedantoj_statusoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Владельцы проектов, использует абстрактный класс UniversoBazaRealeco
class TaskojProjektoPosedanto(UniversoBazaRealeco):

    # проект
    projekto = models.ForeignKey(TaskojProjekto, verbose_name=_('Projekto'), blank=False, null=False,
                                 default=None, on_delete=models.CASCADE)

    # пользователь владелец проекта
    posedanto_uzanto = models.ForeignKey(Uzanto, verbose_name=_('Posedanta uzanto'), blank=True, null=True,
                                         default=None, on_delete=models.CASCADE)

    # организация владелец проекта
    posedanto_organizo = models.ForeignKey(Organizo, verbose_name=_('Posedanta organizo'), blank=True,
                                           null=True, default=None, on_delete=models.CASCADE)

    # объект владелец проекта
    posedanto_objekto = models.ForeignKey(Objekto, verbose_name=_('Posedanta objekto'), blank=True,
                                           null=True, default=None, on_delete=models.CASCADE)

    # сообщество владелец проекта
    posedanto_komunumo = models.ForeignKey(Komunumo, verbose_name=_('Posedanta komunumo'), blank=True,
                                           default=None, null=True, on_delete=models.CASCADE)

    # документ владелец проекта 
    posedanto_dokumento = models.ForeignKey(Dokumento, verbose_name=_('Posedanta dokumento'), blank=True,
                                           null=True, default=None, 
                                           related_name='%(app_label)s_%(class)s_posedanto_dokumento',
                                           on_delete=models.CASCADE)

    # тип владельца проекта
    tipo = models.ForeignKey(TaskojProjektoPosedantoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # статус владельца проекта
    statuso = models.ForeignKey(TaskojProjektoPosedantoStatuso, verbose_name=_('Statuso'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_projektoj_posedantoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Posedanto de projekto')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Posedantoj de projektoj')
        # права
        permissions = (
            ('povas_vidi_projektoj_posedantoj', _('Povas vidi posedantoj de projektoj')),
            ('povas_krei_projektoj_posedantoj', _('Povas krei posedantoj de projektoj')),
            ('povas_forigi_projektoj_posedantoj', _('Povas forigi posedantoj de projektoj')),
            ('povas_shangxi_projektoj_posedantoj', _('Povas ŝanĝi posedantoj de projektoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле projekto этой модели
        return '{}'.format(self.projekto)

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(TaskojProjektoPosedanto, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_projektoj_posedantoj',
                'taskoj.povas_krei_projektoj_posedantoj',
                'taskoj.povas_forigi_projektoj_posedantoj',
                'taskoj.povas_shangxi_projektoj_posedantoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_projektoj_posedantoj')
                    or user_obj.has_perm('taskoj.povas_vidi_projektoj_posedantoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_projektoj_posedantoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы связей проектов между собой, использует абстрактный класс UniversoBazaMaks
class TaskojProjektoLigiloTipo(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_projektoj_ligiloj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de ligiloj de projektoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de ligiloj de projektoj')
        # права
        permissions = (
            ('povas_vidi_projektoj_ligiloj_tipoj',
             _('Povas vidi tipoj de ligiloj de projektoj')),
            ('povas_krei_projektoj_ligiloj_tipoj',
             _('Povas krei tipoj de ligiloj de projektoj')),
            ('povas_forigi_projektoj_ligiloj_tipoj',
             _('Povas forigi tipoj de ligiloj de projektoj')),
            ('povas_sxangxi_projektoj_ligiloj_tipoj',
             _('Povas ŝanĝi tipoj de ligiloj de projektoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojProjektoLigiloTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                     using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_projektoj_ligiloj_tipoj',
                'taskoj.povas_krei_projektoj_ligiloj_tipoj',
                'taskoj.povas_forigi_projektoj_ligiloj_tipoj',
                'taskoj.povas_sxangxi_projektoj_ligiloj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_projektoj_ligiloj_tipoj')
                    or user_obj.has_perm('taskoj.povas_vidi_projektoj_ligiloj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_projektoj_ligiloj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Связь проектов между собой, использует абстрактный класс UniversoBazaMaks
class TaskojProjektoLigilo(UniversoBazaMaks):

    # проект владелец связи
    posedanto = models.ForeignKey(TaskojProjekto, verbose_name=_('Projekto - posedanto'),
                                  blank=False, null=False, default=None, on_delete=models.CASCADE)

    # связываемый проект
    ligilo = models.ForeignKey(TaskojProjekto, verbose_name=_('Projekto - ligilo'), blank=False,
                               null=False, default=None, related_name='%(app_label)s_%(class)s_ligilo',
                               on_delete=models.CASCADE)

    # тип связи проектов
    tipo = models.ForeignKey(TaskojProjektoLigiloTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_projektoj_ligiloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ligilo de projektoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ligiloj de projektoj')
        # права
        permissions = (
            ('povas_vidi_projektoj_ligiloj', _('Povas vidi ligiloj de projektoj')),
            ('povas_krei_projektoj_ligiloj', _('Povas krei ligiloj de projektoj')),
            ('povas_forigi_projektoj_ligiloj', _('Povas forigi ligiloj de projektoj')),
            ('povas_sxangxi_projektoj_ligiloj', _('Povas ŝanĝi ligiloj de projektoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(TaskojProjektoLigilo, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_projektoj_ligiloj',
                'taskoj.povas_krei_projektoj_ligiloj',
                'taskoj.povas_forigi_projektoj_ligiloj',
                'taskoj.povas_sxangxi_projektoj_ligiloj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_projektoj_ligiloj')
                    or user_obj.has_perm('taskoj.povas_vidi_projektoj_ligiloj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_projektoj_ligiloj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Категории задач, использует абстрактный класс UniversoBazaMaks
class TaskojTaskoKategorio(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='taskoj_taskoj_kategorioj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_taskoj_kategorioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kategorio de taskoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kategorioj de taskoj')
        # права
        permissions = (
            ('povas_vidi_taskoj_kategorioj', _('Povas vidi kategorioj de taskoj')),
            ('povas_krei_taskoj_kategorioj', _('Povas krei kategorioj de taskoj')),
            ('povas_forigi_taskoj_kategorioj', _('Povas forigi kategorioj de taskoj')),
            ('povas_shangxi_taskoj_kategorioj', _('Povas ŝanĝi kategorioj de taskoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojTaskoKategorio, self).save(force_insert=force_insert, force_update=force_update,
                                                 using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_taskoj_kategorioj',
                'taskoj.povas_krei_taskoj_kategorioj',
                'taskoj.povas_forigi_taskoj_kategorioj',
                'taskoj.povas_shangxi_taskoj_kategorioj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_taskoj_kategorioj')
                    or user_obj.has_perm('taskoj.povas_vidi_taskoj_kategorioj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_taskoj_kategorioj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы задач, использует абстрактный класс UniversoBazaMaks
class TaskojTaskoTipo(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='taskoj_taskoj_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_taskoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de taskoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de taskoj')
        # права
        permissions = (
            ('povas_vidi_taskoj_tipoj', _('Povas vidi tipoj de taskoj')),
            ('povas_krei_taskoj_tipoj', _('Povas krei tipoj de taskoj')),
            ('povas_forigi_taskoj_tipoj', _('Povas forigi tipoj de taskoj')),
            ('povas_shangxi_taskoj_tipoj', _('Povas ŝanĝi tipoj de taskoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojTaskoTipo, self).save(force_insert=force_insert, force_update=force_update,
                                            using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_taskoj_tipoj',
                'taskoj.povas_krei_taskoj_tipoj',
                'taskoj.povas_forigi_taskoj_tipoj',
                'taskoj.povas_shangxi_taskoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_taskoj_tipoj')
                    or user_obj.has_perm('taskoj.povas_vidi_taskoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_taskoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Статус задачи, использует абстрактный класс UniversoBazaMaks
class TaskojTaskoStatuso(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_taskoj_statusoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Statuso de taskoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statusoj de taskoj')
        # права
        permissions = (
            ('povas_vidi_taskoj_statusoj', _('Povas vidi statusoj de taskoj')),
            ('povas_krei_taskoj_statusoj', _('Povas krei statusoj de taskoj')),
            ('povas_forigi_taskoj_statusoj', _('Povas forigi statusoj de taskoj')),
            ('povas_shangxi_taskoj_statusoj', _('Povas ŝanĝi statusoj de taskoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojTaskoStatuso, self).save(force_insert=force_insert, force_update=force_update,
                                               using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_taskoj_statusoj',
                'taskoj.povas_krei_taskoj_statusoj',
                'taskoj.povas_forigi_taskoj_statusoj',
                'taskoj.povas_shangxi_taskoj_statusoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_taskoj_statusoj')
                    or user_obj.has_perm('taskoj.povas_vidi_taskoj_statusoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_taskoj_statusoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Задачи, использует абстрактный класс UniversoBazaRealeco
class TaskojTasko(UniversoBazaRealeco):

    # проект, в который входит задача
    projekto = models.ForeignKey(TaskojProjekto, verbose_name=_('Projekto'), blank=False, default=None,
                                 on_delete=models.CASCADE)

    # объект (над каким объектом идёт задача, например - к какому объекту идёт движение)
    objekto = models.ForeignKey(Objekto, verbose_name=_('Objekto'), blank=True, null=True, default=None,
                                on_delete=models.CASCADE)

    # категория задач 
    kategorio = models.ManyToManyField(TaskojTaskoKategorio, verbose_name=_('Kategorio'),
                                       db_table='taskoj_taskoj_kategorioj_ligiloj')

    # тип задачи 
    tipo = models.ForeignKey(TaskojTaskoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # статус задачи
    statuso = models.ForeignKey(TaskojTaskoStatuso, verbose_name=_('Statuso'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # позиция в списке
    pozicio = models.IntegerField(_('Pozicio'), blank=True, null=True, default=None)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # начальные координаты выполнения задачи по оси X в кубе
    kom_koordinato_x = models.FloatField(_('Komenca koordinato X'), blank=True, null=True, default=None)

    # начальные координаты выполнения задачи по оси Y в кубе
    kom_koordinato_y = models.FloatField(_('Komenca koordinato Y'), blank=True, null=True, default=None)

    # начальные координаты выполнения задачи по оси Z в кубе
    kom_koordinato_z = models.FloatField(_('Komenca koordinato Z'), blank=True, null=True, default=None)

    # конечные координаты выполнения задачи по оси X в кубе
    fin_koordinato_x = models.FloatField(_('Fina koordinato X'), blank=True, null=True, default=None)

    # конечные координаты выполнения задачи по оси Y в кубе
    fin_koordinato_y = models.FloatField(_('Fina koordinato Y'), blank=True, null=True, default=None)

    # конечные координаты выполнения задачи по оси Z в кубе
    fin_koordinato_z = models.FloatField(_('Fina koordinato Z'), blank=True, null=True, default=None)
    
    # дата начала выполнения задачи (1с - ДатаНачала)
    kom_dato = models.DateTimeField(_("Komenca dato"), auto_now=False, auto_now_add=False, blank=True, null=True)

    # дата окончания выполнения задачи (1с - ДатаОкончания)
    fin_dato = models.DateTimeField(_("Fina dato"), auto_now=False, auto_now_add=False, blank=True, null=True)

    # адрес начала выполнения задачи (1с - АдресНачала)
    kom_adreso = models.JSONField(verbose_name=_('Komenca adreso'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)
    
    # адрес окончания выполнения задачи (1с - АдресКонца)
    fin_adreso = models.JSONField(verbose_name=_('Fina adreso'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_taskoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tasko')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Taskoj')
        # права
        permissions = (
            ('povas_vidi_taskoj', _('Povas vidi taskoj')),
            ('povas_krei_taskoj', _('Povas krei taskoj')),
            ('povas_forigi_taskoj', _('Povas forigi taskoj')),
            ('povas_shangxi_taskoj', _('Povas ŝanĝi taskoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(TaskojTasko, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_taskoj',
                'taskoj.povas_krei_taskoj',
                'taskoj.povas_forigi_taskoj',
                'taskoj.povas_shangxi_taskoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_taskoj')
                    or user_obj.has_perm('taskoj.povas_vidi_taskoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_taskoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы владельцев задач, использует абстрактный класс UniversoBazaMaks
class TaskojTaskoPosedantoTipo(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_taskoj_posedantoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de posedantoj de taskoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de posedantoj de taskoj')
        # права
        permissions = (
            ('povas_vidi_taskoj_posedantoj_tipoj', _('Povas vidi tipoj de posedantoj de taskoj')),
            ('povas_krei_taskoj_posedantoj_tipoj', _('Povas krei tipoj de posedantoj de taskoj')),
            ('povas_forigi_taskoj_posedantoj_tipoj', _('Povas forigi tipoj de posedantoj de taskoj')),
            ('povas_shangxi_taskoj_posedantoj_tipoj', _('Povas ŝanĝi tipoj de posedantoj de taskoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojTaskoPosedantoTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                     using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_taskoj_posedantoj_tipoj',
                'taskoj.povas_krei_taskoj_posedantoj_tipoj',
                'taskoj.povas_forigi_taskoj_posedantoj_tipoj',
                'taskoj.povas_shangxi_taskoj_posedantoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_taskoj_posedantoj_tipoj')
                    or user_obj.has_perm('taskoj.povas_vidi_taskoj_posedantoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_taskoj_posedantoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Статус владельца задачи, использует абстрактный класс UniversoBazaMaks
class TaskojTaskoPosedantoStatuso(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_taskoj_posedantoj_statusoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Statuso de posedantoj de taskoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statusoj de posedantoj de taskoj')
        # права
        permissions = (
            ('povas_vidi_taskoj_posedantoj_statusoj',
             _('Povas vidi statusoj de posedantoj de taskoj')),
            ('povas_krei_taskoj_posedantoj_statusoj',
             _('Povas krei statusoj de posedantoj de taskoj')),
            ('povas_forigi_taskoj_posedantoj_statusoj',
             _('Povas forigi statusoj de posedantoj de taskoj')),
            ('povas_shangxi_taskoj_posedantoj_statusoj',
             _('Povas ŝanĝi statusoj de posedantoj de taskoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojTaskoPosedantoStatuso, self).save(force_insert=force_insert, force_update=force_update,
                                                        using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_taskoj_posedantoj_statusoj',
                'taskoj.povas_krei_taskoj_posedantoj_statusoj',
                'taskoj.povas_forigi_taskoj_posedantoj_statusoj',
                'taskoj.povas_shangxi_taskoj_posedantoj_statusoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_taskoj_posedantoj_statusoj')
                    or user_obj.has_perm('taskoj.povas_vidi_taskoj_posedantoj_statusoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_taskoj_posedantoj_statusoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Владельцы задач, использует абстрактный класс UniversoBazaRealeco
class TaskojTaskoPosedanto(UniversoBazaRealeco):

    # задача
    tasko = models.ForeignKey(TaskojTasko, verbose_name=_('Tasko'), blank=False, null=False,
                              default=None, on_delete=models.CASCADE)

    # пользователь владелец задачи 
    posedanto_uzanto = models.ForeignKey(Uzanto, verbose_name=_('Posedanta uzanto'), blank=True, null=True,
                                         default=None, on_delete=models.CASCADE)

    # организация владелец задачи 
    posedanto_organizo = models.ForeignKey(Organizo, verbose_name=_('Posedanta organizo'), blank=True,
                                           null=True, default=None, on_delete=models.CASCADE)

    # объект владелец задачи 
    posedanto_objekto = models.ForeignKey(Objekto, verbose_name=_('Posedanta objekto'), blank=True,
                                           null=True, default=None, on_delete=models.CASCADE)

    # документ владелец задачи 
    posedanto_dokumento = models.ForeignKey(Dokumento, verbose_name=_('Posedanta dokumento'), blank=True,
                                           null=True, default=None, on_delete=models.CASCADE)

    # тип владельца задачи
    tipo = models.ForeignKey(TaskojTaskoPosedantoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)
    
    # статус владельца задачи
    statuso = models.ForeignKey(TaskojTaskoPosedantoStatuso, verbose_name=_('Statuso'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_taskoj_posedantoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Posedanto de tasko')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Posedantoj de taskoj')
        # права
        permissions = (
            ('povas_vidi_taskoj_posedantoj', _('Povas vidi posedantoj de taskoj')),
            ('povas_krei_taskoj_posedantoj', _('Povas krei posedantoj de taskoj')),
            ('povas_forigi_taskoj_posedantoj', _('Povas forigi posedantoj de taskoj')),
            ('povas_shangxi_taskoj_posedantoj', _('Povas ŝanĝi posedantoj de taskoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле tasko этой модели
        return '{}'.format(self.tasko)

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(TaskojTaskoPosedanto, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_taskoj_posedantoj',
                'taskoj.povas_krei_taskoj_posedantoj',
                'taskoj.povas_forigi_taskoj_posedantoj',
                'taskoj.povas_shangxi_taskoj_posedantoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_taskoj_posedantoj')
                    or user_obj.has_perm('taskoj.povas_vidi_taskoj_posedantoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_taskoj_posedantoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы связей задач между собой, использует абстрактный класс UniversoBazaMaks
class TaskojTaskoLigiloTipo(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_taskoj_ligiloj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de ligiloj de taskoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de ligiloj de taskoj')
        # права
        permissions = (
            ('povas_vidi_taskoj_ligiloj_tipoj',
             _('Povas vidi tipoj de ligiloj de taskoj')),
            ('povas_krei_taskoj_ligiloj_tipoj',
             _('Povas krei tipoj de ligiloj de taskoj')),
            ('povas_forigi_taskoj_ligiloj_tipoj',
             _('Povas forigi tipoj de ligiloj de taskoj')),
            ('povas_sxangxi_taskoj_ligiloj_tipoj',
             _('Povas ŝanĝi tipoj de ligiloj de taskoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(TaskojTaskoLigiloTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                          using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_taskoj_ligiloj_tipoj',
                'taskoj.povas_krei_taskoj_ligiloj_tipoj',
                'taskoj.povas_forigi_taskoj_ligiloj_tipoj',
                'taskoj.povas_sxangxi_taskoj_ligiloj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_taskoj_ligiloj_tipoj')
                    or user_obj.has_perm('taskoj.povas_vidi_taskoj_ligiloj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_taskoj_ligiloj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Связь задач между собой, использует абстрактный класс UniversoBazaMaks
class TaskojTaskoLigilo(UniversoBazaMaks):

    # задача владелец связи
    posedanto = models.ForeignKey(TaskojTasko, verbose_name=_('Tasko - posedanto'),
                                  blank=False, null=False, default=None, on_delete=models.CASCADE)

    # связываемая задача
    ligilo = models.ForeignKey(TaskojTasko, verbose_name=_('Tasko - ligilo'), blank=False,
                               null=False, default=None, related_name='%(app_label)s_%(class)s_ligilo',
                               on_delete=models.CASCADE)

    # тип связи шаблонов задачи
    tipo = models.ForeignKey(TaskojTaskoLigiloTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'taskoj_taskoj_ligiloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ligilo de taskoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ligiloj de taskoj')
        # права
        permissions = (
            ('povas_vidi_taskoj_ligiloj', _('Povas vidi ligiloj de taskoj')),
            ('povas_krei_taskoj_ligiloj', _('Povas krei ligiloj de taskoj')),
            ('povas_forigi_taskoj_ligiloj', _('Povas forigi ligiloj de taskoj')),
            ('povas_sxangxi_taskoj_ligiloj', _('Povas ŝanĝi ligiloj de taskoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(TaskojTaskoLigilo, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('taskoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'taskoj.povas_vidi_taskoj_ligiloj',
                'taskoj.povas_krei_taskoj_ligiloj',
                'taskoj.povas_forigi_taskoj_ligiloj',
                'taskoj.povas_sxangxi_taskoj_ligiloj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='taskoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('taskoj.povas_vidi_taskoj_ligiloj')
                    or user_obj.has_perm('taskoj.povas_vidi_taskoj_ligiloj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('taskoj.povas_vidi_taskoj_ligiloj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond
