"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import graphene
import requests
from django.conf import settings

from siriuso.api.types import ErrorNode
from siriuso.utils import set_enhavo
from objektoj.models import ObjektoPosedanto, ObjektoLigiloTipo, ObjektoLigilo,\
    ObjektoStokejo, loza_volumeno
from objektoj.api.subscription import ObjektoEventoj
from objektoj.api.mutations import objekto_ligilo_save, objekto_save
from organizoj.models import OrganizoMembro
from dokumentoj.models import DokumentoPosedanto
from .schema import *
from ..models import *


# Модель категорий проектов
class RedaktuTaskojProjektoKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    projekto_kategorioj = graphene.Field(TaskojProjektoKategorioNode,
        description=_('Созданная/изменённая категория проектов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        projekto_kategorioj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_projekto_kategorioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            projekto_kategorioj = TaskojProjektoKategorio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(projekto_kategorioj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(projekto_kategorioj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    projekto_kategorioj.realeco.set(realeco)
                                else:
                                    projekto_kategorioj.realeco.clear()

                            projekto_kategorioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            projekto_kategorioj = TaskojProjektoKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_projekto_kategorioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_projekto_kategorioj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                projekto_kategorioj.forigo = kwargs.get('forigo', projekto_kategorioj.forigo)
                                projekto_kategorioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                projekto_kategorioj.arkivo = kwargs.get('arkivo', projekto_kategorioj.arkivo)
                                projekto_kategorioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                projekto_kategorioj.publikigo = kwargs.get('publikigo', projekto_kategorioj.publikigo)
                                projekto_kategorioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                projekto_kategorioj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(projekto_kategorioj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(projekto_kategorioj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        projekto_kategorioj.realeco.set(realeco)
                                    else:
                                        projekto_kategorioj.realeco.clear()

                                projekto_kategorioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojProjektoKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojProjektoKategorio(status=status, message=message, projekto_kategorioj=projekto_kategorioj)


# Модель типов проектов
class RedaktuTaskojProjektoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    projekto_tipoj = graphene.Field(TaskojProjektoTipoNode, 
        description=_('Созданный/изменённый тип проектов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        projekto_tipoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_projekto_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            projekto_tipoj = TaskojProjektoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(projekto_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(projekto_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    projekto_tipoj.realeco.set(realeco)
                                else:
                                    projekto_tipoj.realeco.clear()

                            projekto_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            projekto_tipoj = TaskojProjektoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_projekto_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_projekto_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                projekto_tipoj.forigo = kwargs.get('forigo', projekto_tipoj.forigo)
                                projekto_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                projekto_tipoj.arkivo = kwargs.get('arkivo', projekto_tipoj.arkivo)
                                projekto_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                projekto_tipoj.publikigo = kwargs.get('publikigo', projekto_tipoj.publikigo)
                                projekto_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                projekto_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(projekto_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(projekto_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        projekto_tipoj.realeco.set(realeco)
                                    else:
                                        projekto_tipoj.realeco.clear()

                                projekto_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojProjektoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojProjektoTipo(status=status, message=message, projekto_tipoj=projekto_tipoj)


# Модель статусов проектов
class RedaktuTaskojProjektoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    projekto_statusoj = graphene.Field(TaskojProjektoStatusoNode, 
        description=_('Созданный/изменённый статусов проектов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        projekto_statusoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_projekto_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            projekto_statusoj = TaskojProjektoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(projekto_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(projekto_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            projekto_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            projekto_statusoj = TaskojProjektoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_projekto_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_projekto_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                projekto_statusoj.forigo = kwargs.get('forigo', projekto_statusoj.forigo)
                                projekto_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                projekto_statusoj.arkivo = kwargs.get('arkivo', projekto_statusoj.arkivo)
                                projekto_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                projekto_statusoj.publikigo = kwargs.get('publikigo', projekto_statusoj.publikigo)
                                projekto_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                projekto_statusoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(projekto_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(projekto_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                projekto_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojProjektoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojProjektoStatuso(status=status, message=message, projekto_statusoj=projekto_statusoj)


def provado_posedanto_objekto(uzanto, **kwargs):
    """
    проверяем соответствие владельца, по которому идёт добавление проекта, запрашивающему пользователю
    provado - проверка
    """
    posedanto = None
    message = ''
    # если сразу задаётся владелец, то проверяем владельца
    if kwargs.get('posedanto_uzanto_id', False):
        if uzanto.id in kwargs.get('posedanto_uzanto_id'):
            return uzanto, message

    # если задан владельцем объект, то проверяем владельцев объекта
    if kwargs.get('posedanto_objekto_uuid', False):
        set_posedanto = list(filter(None, set(kwargs.get('posedanto_objekto_uuid'))))
        posedantoj = []
        objekto = Objekto.objects.filter(uuid__in=set_posedanto, forigo=False,
                                        arkivo=False, publikigo=True)
        posedantoj = ObjektoPosedanto.objects.filter(objekto__in=objekto, 
                    posedanto_uzanto=uzanto,
                    forigo=False, arkivo=False, publikigo=True)
        if len(posedantoj)>0:
            return posedantoj[0], message

    # если задан владельцем организации, то проверяем владельцев организаций
    if kwargs.get('posedanto_organizo_uuid', False):
        set_posedanto = list(filter(None, set(kwargs.get('posedanto_organizo_uuid'))))
        posedantoj = []
        organizo = Organizo.objects.filter(uuid__in=set_posedanto, forigo=False,
                                        arkivo=False, publikigo=True)
        posedantoj = OrganizoMembro.objects.filter(organizo__in=organizo, 
                    uzanto=uzanto,
                    forigo=False, arkivo=False, publikigo=True)
        if len(posedantoj)>0:
            return posedantoj[0], message

    # если задан владельцем документ, то проверяем владельцев документов
    if kwargs.get('posedanto_dokumento_uuid', False):
        set_posedanto = list(filter(None, set(kwargs.get('posedanto_dokumento_uuid'))))
        posedantoj = []
        dokumkento = Dokumento.objects.filter(uuid__in=set_posedanto, forigo=False,
                                        arkivo=False, publikigo=True)
        posedantoj = DokumentoPosedanto.objects.filter(dokumkento__in=dokumkento, 
                    posedanto_uzanto=uzanto,
                    forigo=False, arkivo=False, publikigo=True)
        if len(posedantoj)>0:
            return posedantoj[0], message

    return posedanto, message


# Модель проектов
class RedaktuTaskojProjekto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    projekto = graphene.Field(TaskojProjektoNode, 
        description=_('Созданный/изменённый проект'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий проектов'))
        tipo_id = graphene.Int(description=_('Тип проектов'))
        statuso_id = graphene.Int(description=_('Статус проектов'))
        pozicio = graphene.Int(description=_('Позиция в списке'))
        objekto_uuid = graphene.String(description=_('Объект'))
        kom_koordinato_x = graphene.Float(description=_('Начальные координаты выполнения проекта по оси X в кубе'))
        kom_koordinato_y = graphene.Float(description=_('Начальные координаты выполнения проекта по оси Y в кубе'))
        kom_koordinato_z = graphene.Float(description=_('Начальные координаты выполнения проекта по оси Z в кубе'))
        fin_koordinato_x = graphene.Float(description=_('Конечные координаты выполнения проекта по оси X в кубе'))
        fin_koordinato_y = graphene.Float(description=_('Конечные координаты выполнения проекта по оси Y в кубе'))
        fin_koordinato_z = graphene.Float(description=_('Конечные координаты выполнения проекта по оси z в кубе'))

        # параметры для создания владельца
        posedanto_uzanto_id = graphene.List(graphene.Int, description=_('Владелец проекта'))
        posedanto_organizo_uuid = graphene.List(graphene.String, description=_('Организация владелец проекта'))
        posedanto_tipo_id = graphene.List(graphene.Int, description=_('Тип владельца проекта'))
        posedanto_statuso_id = graphene.List(graphene.Int, description=_('Статус владельца проекта'))
        posedanto_objekto_uuid = graphene.List(graphene.String, description=_('Объект владелец проекта')) # если не передаётся, а передаётся objekto_uuid, то это один и тот же объект
        posedanto_dokumento_uuid = graphene.List(graphene.String, description=_('Документ владелец проекта'))

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        uzanto = info.context.user
        projekto = None
        realeco = None
        kategorio = TaskojProjektoKategorio.objects.none()
        tipo = None
        statuso = None
        objekto = None
        posedanto_uzanto = None
        posedanto_organizo = None
        posedanto_tipo = None
        posedanto_statuso = None
        posedanto_objekto = None
        posedanto_dokumento = None
        uzanto = info.context.user
        posedanto_objekto_perm = None # если пользователь владелец объекта по проекту
        # если пользователь владелец объекта, по которому создаётся проект, то можно создавать
        # проверяем по полю posedanto
        if not uzanto.has_perm('taskoj.povas_krei_projektoj'):
            posedanto_objekto_perm, message = provado_posedanto_objekto(uzanto, **kwargs)
            if message: #временная заплатка, т.к. не везде внедрены errors
                errors.append(ErrorNode(
                    field='provado_posedanto_objekto',
                    modelo='RedaktuTaskojProjekto',
                    message=message
                ))
                message = None
                
        if uzanto.has_perm('taskoj.povas_krei_projektoj') or posedanto_objekto_perm:
            # Проверяем наличие всех полей
            if kwargs.get('forigo', False):
                errors.append(ErrorNode(
                    field='forigo',
                    message=_('При создании записи не допустимо указание поля')
                ))

            if kwargs.get('arkivo', False):
                errors.append(ErrorNode(
                    field='arkivo',
                    message=_('При создании записи не допустимо указание поля')
                ))
            
            if not kwargs.get('nomo'):
                errors.append(ErrorNode(
                    field='nomo',
                    message=_('Поле обязательно для заполнения')
                ))

            if 'kategorio' not in kwargs:
                errors.append(ErrorNode(
                    field='kategorio',
                    message=_('Поле обязательно для заполнения')
                ))
            elif not len(kwargs.get('kategorio')):
                errors.append(ErrorNode(
                    field='kategorio',
                    message=_('Необходимо указать хотя бы одну группу')
                ))
            elif not len(errors): # есть смысл загружать базу только если нет ошибок
                kategorio_id = set(kwargs.get('kategorio'))

                if len(kategorio_id):
                    kategorio = TaskojProjektoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                    if len(dif):
                        errors.append(ErrorNode(
                            field=str(dif),
                            message=_('Указаны несуществующие категории проектов')
                        ))

            # проверяем наличие записей с таким кодом
            if not len(errors):
                if 'realeco_id' in kwargs:
                    try:
                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                            arkivo=False, publikigo=True)
                    except Realeco.DoesNotExist:
                        errors.append(ErrorNode(
                            field='realeco_id',
                            message=_('Неверный параллельный мир')
                        ))

            if 'tipo_id' in kwargs:
                if not len(errors):
                    try:
                        tipo = TaskojProjektoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                            arkivo=False, publikigo=True)
                    except TaskojProjektoTipo.DoesNotExist:
                        errors.append(ErrorNode(
                            field='tipo_id',
                            message=_('Неверный тип проектов')
                        ))
            else:
                errors.append(ErrorNode(
                    field='tipo_id',
                    message=_('Поле обязательно для заполнения')
                ))

            if 'statuso_id' in kwargs:
                if not len(errors):
                    try:
                        statuso = TaskojProjektoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                            arkivo=False, publikigo=True)
                    except TaskojProjektoStatuso.DoesNotExist:
                        errors.append(ErrorNode(
                            field='statuso_id',
                            message=_('Неверный статус проектов')
                        ))
            else:
                errors.append(ErrorNode(
                    field='statuso_id',
                    message=_('Поле обязательно для заполнения')
                ))

            if not len(errors):
                if 'objekto_uuid' in kwargs:
                    try:
                        objekto = Objekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                            arkivo=False, publikigo=True)
                    except Objekto.DoesNotExist:
                        errors.append(ErrorNode(
                            field='objekto_uuid',
                            message=_('Неверный объект')
                        ))

            if not len(errors):
                projekto = TaskojProjekto.objects.create(
                    forigo=False,
                    arkivo=False,
                    realeco = realeco,
                    tipo = tipo,
                    statuso = statuso,
                    objekto = objekto,
                    kom_koordinato_x = kwargs.get('kom_koordinato_x', None),
                    kom_koordinato_y = kwargs.get('kom_koordinato_y', None),
                    kom_koordinato_z = kwargs.get('kom_koordinato_z', None),
                    fin_koordinato_x = kwargs.get('fin_koordinato_x', None),
                    fin_koordinato_y = kwargs.get('fin_koordinato_y', None),
                    fin_koordinato_z = kwargs.get('fin_koordinato_z', None),
                    pozicio = kwargs.get('pozicio', None),
                    publikigo = kwargs.get('publikigo', False),
                    publikiga_dato = timezone.now()
                )

                if (kwargs.get('nomo', False)):
                    set_enhavo(projekto.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                if (kwargs.get('priskribo', False)):
                    set_enhavo(projekto.priskribo, 
                                kwargs.get('priskribo'), 
                                info.context.LANGUAGE_CODE)
                if 'kategorio' in kwargs:
                    if kategorio:
                        projekto.kategorio.set(kategorio)
                    else:
                        projekto.kategorio.clear()

                projekto.save()

                status = True
                # далее блок добавления владельца
                if (kwargs.get('posedanto_tipo_id', False)):
                    kvanto = len(kwargs.get('posedanto_tipo_id')) # количество владельцев
                    indico = 0 # индекс
                    for posedanto_tipo_id in kwargs.get('posedanto_tipo_id'):
                        if not len(errors):
                            try:
                                posedanto_tipo = TaskojProjektoPosedantoTipo.objects.get(id=posedanto_tipo_id, forigo=False,
                                                                arkivo=False, publikigo=True)
                            except TaskojProjektoPosedantoTipo.DoesNotExist:
                                errors.append(ErrorNode(
                                    field='posedanto_tipo_id',
                                    message=_('Неверный тип владельца проекта')
                                ))

                        if (kwargs.get('posedanto_statuso_id', False)):
                            if len(kwargs.get('posedanto_statuso_id')) != kvanto:
                                errors.append(ErrorNode(
                                    field='posedanto_statuso_id',
                                    message=_('Количество элементов расходится с количеством в posedanto_tipo_id')
                                ))
                            if not len(errors):
                                try:
                                    posedanto_statuso = TaskojProjektoPosedantoStatuso.objects.get(
                                        id=kwargs.get('posedanto_statuso_id')[indico], forigo=False,
                                        arkivo=False, publikigo=True)
                                except TaskojProjektoPosedantoStatuso.DoesNotExist:
                                    errors.append(ErrorNode(
                                        field='posedanto_statuso_id',
                                        message=_('Неверный статус владельца проекта')
                                    ))
                        else:
                            errors.append(ErrorNode(
                                field='posedanto_statuso_id',
                                message=_('Поле обязательно для заполнения')
                            ))

                        posedanto_uzanto = None
                        if 'posedanto_uzanto_id' in kwargs:
                            if len(kwargs.get('posedanto_uzanto_id')) != kvanto:
                                errors.append(ErrorNode(
                                    field='posedanto_uzanto_id',
                                    message=_('Количество элементов расходится с количеством в posedanto_tipo_id')
                                ))
                            if not len(errors) and kwargs.get('posedanto_uzanto_id')[indico]:
                                try:
                                    posedanto_uzanto = Uzanto.objects.get(
                                        id=kwargs.get('posedanto_uzanto_id')[indico], 
                                        konfirmita=True
                                    )
                                except Uzanto.DoesNotExist:
                                    errors.append(ErrorNode(
                                        field='posedanto_uzanto_id',
                                        message=_('Неверный владелец/пользователь проекта')
                                    ))
                            else:
                                print('=== нет пользователя шаг = ', indico)

                        posedanto_organizo = None
                        if 'posedanto_organizo_uuid' in kwargs:
                            if len(kwargs.get('posedanto_organizo_uuid')) != kvanto:
                                errors.append(ErrorNode(
                                    field='posedanto_organizo_uuid',
                                    message=_('Количество элементов расходится с количеством в posedanto_tipo_id')
                                ))
                            if not len(errors) and kwargs.get('posedanto_organizo_uuid')[indico]:
                                try:
                                    posedanto_organizo = Organizo.objects.get(
                                        uuid=kwargs.get('posedanto_organizo_uuid')[indico],
                                        forigo=False,
                                        arkivo=False,  
                                        publikigo=True
                                    )
                                except Organizo.DoesNotExist:
                                    errors.append(ErrorNode(
                                        field='posedanto_organizo_uuid',
                                        message=_('Неверная организация владелец проекта')
                                    ))

                        posedanto_objekto = None
                        if 'posedanto_objekto_uuid' in kwargs:
                            if len(kwargs.get('posedanto_objekto_uuid')) != kvanto:
                                errors.append(ErrorNode(
                                    field='posedanto_objekto_uuid',
                                    message=_('Количество элементов расходится с количеством в posedanto_tipo_id')
                                ))
                            if not len(errors) and kwargs.get('posedanto_objekto_uuid')[indico]:
                                try:
                                    posedanto_objekto = Objekto.objects.get(uuid=kwargs.get('posedanto_objekto_uuid')[indico], forigo=False,
                                                                    arkivo=False, publikigo=True)
                                except Objekto.DoesNotExist:
                                    errors.append(ErrorNode(
                                        field='posedanto_objekto_uuid',
                                        message=_('Неверный объект')
                                    ))
                            else:
                                print('=== нет объекта шаг = ', indico)
                        posedanto_dokumento = None
                        if 'posedanto_dokumento_uuid' in kwargs:
                            if len(kwargs.get('posedanto_dokumento_uuid')) != kvanto:
                                errors.append(ErrorNode(
                                    field='posedanto_dokumento_uuid',
                                    message=_('Количество элементов расходится с количеством в posedanto_tipo_id')
                                ))
                            if not len(errors) and kwargs.get('posedanto_dokumento_uuid')[indico]:
                                try:
                                    posedanto_dokumento = Dokumento.objects.get(uuid=kwargs.get('posedanto_dokumento_uuid')[indico], forigo=False,
                                                                    arkivo=False, publikigo=True)
                                except Dokumento.DoesNotExist:
                                    errors.append(ErrorNode(
                                        field='posedanto_dokumento_uuid',
                                        message=_('Неверный документ')
                                    ))

                        if not len(errors):
                            projekto_posedantoj = TaskojProjektoPosedanto.objects.create(
                                forigo=False,
                                arkivo=False,
                                realeco=realeco,
                                projekto=projekto,
                                posedanto_uzanto=posedanto_uzanto,
                                posedanto_organizo=posedanto_organizo,
                                tipo=posedanto_tipo,
                                statuso=posedanto_statuso,
                                posedanto_objekto=posedanto_objekto,
                                posedanto_dokumento=posedanto_dokumento,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            projekto_posedantoj.save()
                        indico += 1
                        if len(errors):
                            # если в цикле была ошибка - выходим
                            break

                    if not len(errors):
                        message = _('Обе записи созданы')

                if not message:
                    message = _('Запись создана')
        else:
            errors.append(ErrorNode(
                modelo='RedaktuTaskojProjekto',
                message=_('Недостаточно прав')
            ))
        if len(errors):
            status = False
            message = _('Nevalida argumentvaloroj')
            for error in errors:
                message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                    error.message, _('в поле'), error.field)
        else:
            status = True
            errors = list()

        return status, message, errors, projekto

    @staticmethod
    def edit(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        projekto = None
        realeco = None
        kategorio = TaskojProjektoKategorio.objects.none()
        tipo = None
        statuso = None
        objekto = None
        uzanto = info.context.user
        if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                or kwargs.get('realeco_id', False) or kwargs.get('kategorio', False)
                or kwargs.get('tipo_id', False)
                or kwargs.get('statuso_id', False) or kwargs.get('pozicio', False)
                or kwargs.get('kom_koordinato_x', False) or kwargs.get('kom_koordinato_y', False)
                or kwargs.get('kom_koordinato_z', False) or kwargs.get('fin_koordinato_x', False)
                or kwargs.get('fin_koordinato_y', False) or kwargs.get('fin_koordinato_z', False)
                or kwargs.get('objekto', False)):
            errors.append(ErrorNode(
                field='',
                message=_('Не задано ни одно поле для изменения')
            ))

        if not len(errors):
            # Ищем запись для изменения
            try:
                projekto = TaskojProjekto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                # владелец объекта, по которому проект имеет право на удаление/редактирование проекта
                # проверяем владельца проекта на владение проекта или объекта-влаедльца проекта
                # находим владельца проекта
                try:
                    perm_posedanto = TaskojProjektoPosedanto.objects.get(
                        projekto=projekto,
                        posedanto_uzanto=uzanto,
                        forigo=False,
                        arkivo=False,
                        publikigo=True
                    )
                except TaskojProjektoPosedanto.DoesNotExist:
                    perm_posedanto = None
                if not perm_posedanto:
                    # если ещё нет прямого владельца проекта, то проверяем владельцев через владельцев-объектов
                    posedantoj = TaskojProjektoPosedanto.objects.filter(
                        projekto=projekto,
                        forigo=False,
                        arkivo=False,
                        publikigo=True
                    )
                    for pose in posedantoj:
                        if pose.posedanto_objekto and not pose.posedanto_uzanto: # если есть владелец-объект и нет прямого владельца-пользователя
                            try:
                                perm_posedanto = ObjektoPosedanto.objects.get(
                                    objekto=pose.posedanto_objekto,
                                    posedanto_uzanto=uzanto,
                                    forigo=False,
                                    arkivo=False,
                                    publikigo=True
                                )
                                if perm_posedanto: # нашли совпадение пользователя и владельца объектом, разрешаем и выходим из цикла
                                    break
                            except ObjektoPosedanto.DoesNotExist:
                                perm_posedanto = None

                if ((not uzanto.has_perm('taskoj.povas_forigi_projektoj') or
                        not perm_posedanto)
                        and kwargs.get('forigo', False)):
                    errors.append(ErrorNode(
                        field='forigo',
                        modelo='RedaktuTaskojProjekto',
                        message=_('Недостаточно прав для удаления')
                    ))
                elif not (uzanto.has_perm('taskoj.povas_shanghi_projektoj') or 
                        perm_posedanto):
                    errors.append(ErrorNode(
                        field='',
                        modelo='RedaktuTaskojProjekto',
                        message=_('Недостаточно прав для изменения')
                    ))

                if not len(errors):
                    if 'kategorio' in kwargs:
                        kategorio_id = set(kwargs.get('kategorio'))
                        if len(kategorio_id):
                            kategorio = TaskojProjektoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                            dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))
                            if len(dif):
                                errors.append(ErrorNode(
                                    field=str(dif),
                                    message=_('Указаны несуществующие категории проектов')
                                ))

                # проверяем наличие записей с таким кодом
                if not len(errors):
                    if 'realeco_id' in kwargs:
                        try:
                            realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                            arkivo=False, publikigo=True)
                        except Realeco.DoesNotExist:
                            errors.append(ErrorNode(
                                field='realeco_id',
                                message=_('Неверный параллельный мир')
                            ))

                if not len(errors):
                    if 'tipo_id' in kwargs:
                        try:
                            tipo = TaskojProjektoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                            arkivo=False, publikigo=True)
                        except TaskojProjektoTipo.DoesNotExist:
                            errors.append(ErrorNode(
                                field='tipo_id',
                                message=_('Неверный тип проектов')
                            ))

                if not len(errors):
                    if 'statuso_id' in kwargs:
                        try:
                            statuso = TaskojProjektoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                            arkivo=False, publikigo=True)
                        except TaskojProjektoStatuso.DoesNotExist:
                            errors.append(ErrorNode(
                                field='statuso_id',
                                message=_('Неверный статус проектов')
                            ))

                if not len(errors):

                    projekto.forigo = kwargs.get('forigo', projekto.forigo)
                    projekto.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                    projekto.arkivo = kwargs.get('arkivo', projekto.arkivo)
                    projekto.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                    projekto.publikigo = kwargs.get('publikigo', projekto.publikigo)
                    projekto.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                    projekto.realeco = realeco if kwargs.get('realeco_id', False) else projekto.realeco
                    projekto.statuso = statuso if kwargs.get('statuso_id', False) else projekto.statuso
                    projekto.tipo = tipo if kwargs.get('tipo_id', False) else projekto.tipo
                    projekto.pozicio = kwargs.get('pozicio', projekto.pozicio)
                    projekto.objekto = objekto if kwargs.get('objekto_uuid', False) else projekto.objekto
                    projekto.kom_koordinato_x = kwargs.get('kom_koordinato_x', projekto.kom_koordinato_x)
                    projekto.kom_koordinato_y = kwargs.get('kom_koordinato_y', projekto.kom_koordinato_y)
                    projekto.kom_koordinato_z = kwargs.get('kom_koordinato_z', projekto.kom_koordinato_z)
                    projekto.fin_koordinato_x = kwargs.get('fin_koordinato_x', projekto.fin_koordinato_x)
                    projekto.fin_koordinato_y = kwargs.get('fin_koordinato_y', projekto.fin_koordinato_y)
                    projekto.fin_koordinato_z = kwargs.get('fin_koordinato_z', projekto.fin_koordinato_z)

                    if (kwargs.get('nomo', False)):
                        set_enhavo(projekto.nomo, kwargs.get('nomo'), 
                                    info.context.LANGUAGE_CODE)
                    if (kwargs.get('priskribo', False)):
                        set_enhavo(projekto.priskribo, kwargs.get('priskribo'), 
                                    info.context.LANGUAGE_CODE)
                    if 'kategorio' in kwargs:
                        if kategorio:
                            projekto.kategorio.set(kategorio)
                        else:
                            projekto.kategorio.clear()

                    projekto.save()
                    message = _('Запись успешно изменена')
            except TaskojProjekto.DoesNotExist:
                errors.append(ErrorNode(
                    field='uuid',
                    message=_('Запись не найдена')
                ))
        if len(errors):
            status = False
            message = _('Nevalida argumentvaloroj')
            for error in errors:
                message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                    error.message, _('в поле'), error.field)
        else:
            status = True
            errors = list()
        return status, message, errors, projekto

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        projekto = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                if not (message or kwargs.get('uuid', False)):
                    # Создаём новую запись
                    status, message, errors, projekto = RedaktuTaskojProjekto.create(root, info, **kwargs)
                elif not message:
                    # Изменяем запись
                    status, message, errors, projekto = RedaktuTaskojProjekto.edit(root, info, **kwargs)
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojProjekto(status=status, message=message, errors=errors, projekto=projekto)


# Модель типов владельцев проектов
class RedaktuTaskojProjektoPosedantoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    projekto_posedantoj_tipoj = graphene.Field(TaskojProjektoPosedantoTipoNode, 
        description=_('Созданный/изменённый тип владельцев проектов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        projekto_posedantoj_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_projektoj_posedantoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            projekto_posedantoj_tipoj = TaskojProjektoPosedantoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(projekto_posedantoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(projekto_posedantoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            projekto_posedantoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            projekto_posedantoj_tipoj = TaskojProjektoPosedantoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_projektoj_posedantoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_projektoj_posedantoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                projekto_posedantoj_tipoj.forigo = kwargs.get('forigo', projekto_posedantoj_tipoj.forigo)
                                projekto_posedantoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                projekto_posedantoj_tipoj.arkivo = kwargs.get('arkivo', projekto_posedantoj_tipoj.arkivo)
                                projekto_posedantoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                projekto_posedantoj_tipoj.publikigo = kwargs.get('publikigo', projekto_posedantoj_tipoj.publikigo)
                                projekto_posedantoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                projekto_posedantoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(projekto_posedantoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(projekto_posedantoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                projekto_posedantoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojProjektoPosedantoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojProjektoPosedantoTipo(status=status, message=message, projekto_posedantoj_tipoj=projekto_posedantoj_tipoj)


# Модель статусов владельцев проектов
class RedaktuTaskojProjektoPosedantoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    projekto_posedantoj_statusoj = graphene.Field(TaskojProjektoPosedantoStatusoNode, 
        description=_('Созданный/изменённый статус владельца проектов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        projekto_posedantoj_statusoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_projektoj_posedantoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            projekto_posedantoj_statusoj = TaskojProjektoPosedantoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(projekto_posedantoj_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(projekto_posedantoj_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            projekto_posedantoj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            projekto_posedantoj_statusoj = TaskojProjektoPosedantoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_projektoj_posedantoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_projektoj_posedantoj_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                projekto_posedantoj_statusoj.forigo = kwargs.get('forigo', projekto_posedantoj_statusoj.forigo)
                                projekto_posedantoj_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                projekto_posedantoj_statusoj.arkivo = kwargs.get('arkivo', projekto_posedantoj_statusoj.arkivo)
                                projekto_posedantoj_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                projekto_posedantoj_statusoj.publikigo = kwargs.get('publikigo', projekto_posedantoj_statusoj.publikigo)
                                projekto_posedantoj_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                projekto_posedantoj_statusoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(projekto_posedantoj_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(projekto_posedantoj_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                projekto_posedantoj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojProjektoPosedantoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojProjektoPosedantoStatuso(status=status, message=message, projekto_posedantoj_statusoj=projekto_posedantoj_statusoj)


# Модель владельцев проектов
class RedaktuTaskojProjektoPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    projekto_posedantoj = graphene.Field(TaskojProjektoPosedantoNode, 
        description=_('Созданный/изменённый владелец проекта'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        projekto_uuid = graphene.String(description=_('Проект'))
        posedanto_uzanto_id = graphene.Int(description=_('Владелец проекта'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец проекта'))
        posedanto_komunumo_uuid = graphene.String(description=_('Сообщество владелец проекта'))
        tipo_id = graphene.Int(description=_('Тип владельца проекта'))
        statuso_id = graphene.Int(description=_('Статус владельца проекта'))
        posedanto_objekto_uuid = graphene.String(description=_('Объект владелец проекта'))
        posedanto_dokumento_uuid = graphene.String(description=_('Документ-владелец проекта'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        projekto_posedantoj = None
        realeco = None
        projekto = None
        posedanto_uzanto = None
        posedanto_organizo = None
        posedanto_komunumo = None
        posedanto_dokumento = None
        tipo = None
        statuso = None
        posedanto_objekto = None
        uzanto = info.context.user
        posedanto_perm = None

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    # если пользователь создаёт владельца проекта по объекту, чьим владельцем является, то разрешаем
                    # проверяем соответствие пользователя-владельца и подключенного пользователя
                    if 'posedanto_uzanto_id' in kwargs:
                        try:
                            posedanto_perm = Uzanto.objects.get(
                                id=kwargs.get('posedanto_uzanto_id'), 
                                konfirmita=True
                            )
                            if uzanto != posedanto_perm:
                                posedanto_perm = None
                        except Uzanto.DoesNotExist:
                            message = _('Неверный владелец/пользователь проекта')
                    # теперь проверяем владельца объекта
                    if not message:
                        if 'posedanto_objekto_uuid' in kwargs:
                            try:
                                objekto = Objekto.objects.get(
                                    uuid=kwargs.get('posedanto_objekto_uuid'), 
                                    forigo=False,
                                    arkivo=False, 
                                    publikigo=True
                                )
                                try:

                                    posedanto_objekto = ObjektoPosedanto.objects.get(objekto=objekto, 
                                                posedanto_uzanto=uzanto,
                                                forigo=False, arkivo=False, publikigo=True)
                                    if posedanto_perm: # если пользователь был указан ранее
                                        if posedanto_perm != posedanto_objekto.posedanto_uzanto: # проверяем его соответствие с владельцем объекта
                                            posedanto_perm = None
                                except ObjektoPosedanto.DoesNotExist:
                                    pass
                            except Objekto.DoesNotExist:
                                message = '{} "{}"'.format(_('Неверный объект владелец проекта'), 'posedanto_objekto_uuid')

                    if uzanto.has_perm('taskoj.povas_krei_projektoj_posedantoj') or posedanto_perm:
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_organizo_uuid' in kwargs:
                                try:
                                    posedanto_organizo = Organizo.objects.get(
                                        uuid=kwargs.get('posedanto_organizo_uuid'),
                                        forigo=False,
                                        arkivo=False,  
                                        publikigo=True
                                    )
                                except Organizo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная организация владелец проекта'),'posedanto_organizo_uuid')

                        if not message:
                            if 'posedanto_komunumo_uuid' in kwargs:
                                try:
                                    posedanto_komunumo = Komunumo.objects.get(
                                        uuid=kwargs.get('posedanto_komunumo_uuid'),
                                        forigo=False,
                                        arkivo=False
                                    )
                                except Komunumo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверное сообщество-владелец проекта'),'posedanto_komunumo_uuid')
                        if not message:
                        # if not len(errors):
                            if 'posedanto_dokumento_uuid' in kwargs:
                                try:
                                    posedanto_dokumento = Dokumento.objects.get(uuid=kwargs.get('posedanto_dokumento_uuid'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                                except Dokumento.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный документ-владелец проекта'),'posedanto_dokumento_uuid')
                                    # errors.append(ErrorNode(
                                    #     field='posedanto_dokumento_uuid',
                                    #     message=_('Неверный документ-владелец проекта')
                                    # ))

                        if not message:
                            if 'projekto_uuid' in kwargs:
                                try:
                                    projekto = TaskojProjekto.objects.get(uuid=kwargs.get('projekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojProjekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный проект'),'projekto_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'projekto_uuid')

                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный параллельный мир'),'realeco_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = TaskojProjektoPosedantoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojProjektoPosedantoTipo.DoesNotExist:
                                    message = _('Неверный тип владельца проектов')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'statuso_id' in kwargs:
                                try:
                                    statuso = TaskojProjektoPosedantoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojProjektoPosedantoStatuso.DoesNotExist:
                                    message = _('Неверный статус владельца проектов')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')

                        if not message:
                            if 'posedanto_objekto_uuid' in kwargs:
                                try:
                                    posedanto_objekto = Objekto.objects.get(
                                        uuid=kwargs.get('posedanto_objekto_uuid'), 
                                        forigo=False,
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except Objekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект владелец проекта'), 'posedanto_objekto_uuid')

                        if 'posedanto_uzanto_id' in kwargs:
                            try:
                                posedanto_uzanto = Uzanto.objects.get(
                                    id=kwargs.get('posedanto_uzanto_id'), 
                                    konfirmita=True
                                )
                            except Uzanto.DoesNotExist:
                                message = _('Неверный владелец/пользователь проекта')

                        if not message:
                            projekto_posedantoj = TaskojProjektoPosedanto.objects.create(
                                forigo = False,
                                arkivo = False,
                                realeco = realeco,
                                projekto = projekto,
                                posedanto_uzanto = posedanto_uzanto,
                                posedanto_organizo = posedanto_organizo,
                                posedanto_komunumo = posedanto_komunumo,
                                tipo = tipo,
                                statuso = statuso,
                                posedanto_objekto = posedanto_objekto,
                                posedanto_dokumento=posedanto_dokumento,
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato = timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(projekto_posedantoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(projekto_posedantoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            projekto_posedantoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('posedanto_uzanto_id', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('posedanto_organizo_uuid', False)
                            or kwargs.get('posedanto_komunumo_uuid', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('projekto_uuid', False)
                            or kwargs.get('posedanto_objekto_uuid', False)
                            or kwargs.get('posedanto_dokumento_uuid', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            projekto_posedantoj = TaskojProjektoPosedanto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            # проверяем на владельца проекта или владельца объекта, по которому проект
                            if projekto_posedantoj.posedanto_uzanto:
                                if projekto_posedantoj.posedanto_uzanto != uzanto: # пользователи не совпадают
                                    posedanto_perm = None
                                else:
                                    posedanto_perm = uzanto
                            if posedanto_perm: # продолжаем проверку по объекту
                                try:
                                    posedanto_objekto2 = ObjektoPosedanto.objects.get(
                                            objekto=projekto_posedantoj.posedanto_objekto, 
                                            posedanto_uzanto=uzanto,
                                            forigo=False, arkivo=False, publikigo=True)
                                    if posedanto_objekto2: # если нашли объект
                                        if uzanto != posedanto_objekto2.posedanto_uzanto: # проверяем его соответствие с владельцем объекта
                                            posedanto_perm = None
                                except ObjektoPosedanto.DoesNotExist:
                                    pass

                            if (not (uzanto.has_perm('taskoj.povas_forigi_projektoj_posedantoj')
                                    or posedanto_perm)
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not (uzanto.has_perm('taskoj.povas_shanghi_projektoj_posedantoj')
                                    or posedanto_perm):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_uzanto_id' in kwargs:
                                    try:
                                        posedanto_uzanto = Uzanto.objects.get(
                                            id=kwargs.get('posedanto_uzanto_id'), 
                                            konfirmita=True
                                        )
                                    except Uzanto.DoesNotExist:
                                        message = _('Неверный владелец/пользователь проекта')

                            if not message:
                                if 'posedanto_organizo_uuid' in kwargs:
                                    try:
                                        posedanto_organizo = Organizo.objects.get(
                                            uuid=kwargs.get('posedanto_organizo_uuid'),
                                            forigo=False,
                                            arkivo=False,  
                                            publikigo=True
                                        )
                                    except Organizo.DoesNotExist:
                                        message = _('Неверная организация владелец проекта')

                            if not message:
                                if 'posedanto_komunumo_uuid' in kwargs:
                                    try:
                                        posedanto_komunumo = Komunumo.objects.get(
                                            uuid=kwargs.get('posedanto_komunumo_uuid'),
                                            forigo=False,
                                            arkivo=False
                                        )
                                    except Komunumo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверное сообщество-владелец проекта'),'posedanto_komunumo_uuid')

                            if not message:
                                if 'projekto_uuid' in kwargs:
                                    try:
                                        projekto = TaskojProjekto.objects.get(uuid=kwargs.get('projekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except TaskojProjekto.DoesNotExist:
                                        message = _('Неверный проект')

                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = _('Неверный параллельный мир')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = TaskojProjektoPosedantoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except TaskojProjektoPosedantoTipo.DoesNotExist:
                                        message = _('Неверный тип владельца проектов')

                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = TaskojProjektoPosedantoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except TaskojProjektoPosedantoStatuso.DoesNotExist:
                                        message = _('Неверный статус владельца проектов')

                            if not message:
                                if 'posedanto_objekto_uuid' in kwargs:
                                    try:
                                        posedanto_objekto = Objekto.objects.get(
                                            uuid=kwargs.get('posedanto_objekto_uuid'), 
                                            forigo=False,
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except Objekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект владелец проекта'), 'posedanto_objekto_uuid')

                            if not message:
                                if 'posedanto_dokumento_uuid' in kwargs:
                                    try:
                                        posedanto_dokumento = Dokumento.objects.get(
                                            uuid=kwargs.get('posedanto_dokumento_uuid'), 
                                            forigo=False,
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except Dokumento.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный документ-владелец проекта'), 'posedanto_dokumento_uuid')

                            if not message:

                                projekto_posedantoj.forigo = kwargs.get('forigo', projekto_posedantoj.forigo)
                                projekto_posedantoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                projekto_posedantoj.arkivo = kwargs.get('arkivo', projekto_posedantoj.arkivo)
                                projekto_posedantoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                projekto_posedantoj.publikigo = kwargs.get('publikigo', projekto_posedantoj.publikigo)
                                projekto_posedantoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                projekto_posedantoj.realeco = realeco if kwargs.get('realeco_id', False) else projekto_posedantoj.realeco
                                projekto_posedantoj.projekto = realeco if kwargs.get('projekto_uuid', False) else projekto_posedantoj.projekto
                                projekto_posedantoj.posedanto_uzanto = posedanto_uzanto if kwargs.get('posedanto_uzanto_id', False) else projekto_posedantoj.posedanto_uzanto
                                projekto_posedantoj.posedanto_organizo = posedanto_organizo if kwargs.get('posedanto_organizo_uuid', False) else projekto_posedantoj.posedanto_organizo
                                projekto_posedantoj.posedanto_komunumo = posedanto_komunumo if kwargs.get('posedanto_komunumo_uuid', False) else projekto_posedantoj.posedanto_komunumo
                                projekto_posedantoj.statuso = statuso if kwargs.get('statuso_id', False) else projekto_posedantoj.statuso
                                projekto_posedantoj.tipo = tipo if kwargs.get('tipo_id', False) else projekto_posedantoj.tipo
                                projekto_posedantoj.posedanto_objekto = posedanto_objekto if kwargs.get('posedanto_objekto_uuid', False) else projekto_posedantoj.posedanto_objekto
                                projekto_posedantoj.posedanto_dokumento = posedanto_dokumento if kwargs.get('posedanto_dokumento_uuid', False) else projekto_posedantoj.posedanto_dokumento

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(projekto_posedantoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(projekto_posedantoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                projekto_posedantoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojProjektoPosedanto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojProjektoPosedanto(status=status, message=message, projekto_posedantoj=projekto_posedantoj)


# Модель категорий задач
class RedaktuTaskojTaskoKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    taskoj_kategorioj = graphene.Field(TaskojTaskoKategorioNode,
        description=_('Созданная/изменённая категория задач'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        taskoj_kategorioj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_taskoj_kategorioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            taskoj_kategorioj = TaskojTaskoKategorio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(taskoj_kategorioj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(taskoj_kategorioj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    taskoj_kategorioj.realeco.set(realeco)
                                else:
                                    taskoj_kategorioj.realeco.clear()

                            taskoj_kategorioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            taskoj_kategorioj = TaskojTaskoKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_taskoj_kategorioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_taskoj_kategorioj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                taskoj_kategorioj.forigo = kwargs.get('forigo', taskoj_kategorioj.forigo)
                                taskoj_kategorioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                taskoj_kategorioj.arkivo = kwargs.get('arkivo', taskoj_kategorioj.arkivo)
                                taskoj_kategorioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                taskoj_kategorioj.publikigo = kwargs.get('publikigo', taskoj_kategorioj.publikigo)
                                taskoj_kategorioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                taskoj_kategorioj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(taskoj_kategorioj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(taskoj_kategorioj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        taskoj_kategorioj.realeco.set(realeco)
                                    else:
                                        taskoj_kategorioj.realeco.clear()

                                taskoj_kategorioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojTaskoKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojTaskoKategorio(status=status, message=message, taskoj_kategorioj=taskoj_kategorioj)


# Модель типов задач
class RedaktuTaskojTaskoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    taskoj_tipoj = graphene.Field(TaskojTaskoTipoNode, 
        description=_('Созданный/изменённый тип задач'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        taskoj_tipoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_taskoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            taskoj_tipoj = TaskojTaskoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(taskoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(taskoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    taskoj_tipoj.realeco.set(realeco)
                                else:
                                    taskoj_tipoj.realeco.clear()

                            taskoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            taskoj_tipoj = TaskojTaskoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_taskoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_taskoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                taskoj_tipoj.forigo = kwargs.get('forigo', taskoj_tipoj.forigo)
                                taskoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                taskoj_tipoj.arkivo = kwargs.get('arkivo', taskoj_tipoj.arkivo)
                                taskoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                taskoj_tipoj.publikigo = kwargs.get('publikigo', taskoj_tipoj.publikigo)
                                taskoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                taskoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(taskoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(taskoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        taskoj_tipoj.realeco.set(realeco)
                                    else:
                                        taskoj_tipoj.realeco.clear()

                                taskoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojTaskoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojTaskoTipo(status=status, message=message, taskoj_tipoj=taskoj_tipoj)


# Модель статусов задач
class RedaktuTaskojTaskoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    taskoj_statusoj = graphene.Field(TaskojTaskoStatusoNode, 
        description=_('Созданный/изменённый статус задач'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        taskoj_statusoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_taskoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            taskoj_statusoj = TaskojTaskoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(taskoj_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(taskoj_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            taskoj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            taskoj_statusoj = TaskojTaskoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_taskoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_taskoj_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                taskoj_statusoj.forigo = kwargs.get('forigo', taskoj_statusoj.forigo)
                                taskoj_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                taskoj_statusoj.arkivo = kwargs.get('arkivo', taskoj_statusoj.arkivo)
                                taskoj_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                taskoj_statusoj.publikigo = kwargs.get('publikigo', taskoj_statusoj.publikigo)
                                taskoj_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                taskoj_statusoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(taskoj_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(taskoj_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                taskoj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojTaskoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojTaskoStatuso(status=status, message=message, taskoj_statusoj=taskoj_statusoj)


# функция обработки запроса на вход в станцию
def eniri_kosmostacio(taskoj, tasko_posedantoj):
    # проверка наличия необходимых данных
    if not taskoj.objekto:
        return
    if not tasko_posedantoj.posedanto_objekto:
        return
    # проверка нахождения в одном кубе
    if tasko_posedantoj.posedanto_objekto.kubo != taskoj.objekto.kubo:
        return
    # проверяем расстояние между объектами
    distance = tasko_posedantoj.posedanto_objekto.distanceTo(taskoj.objekto)
    if distance > 400:
        return
    # ставим связь на нахождение внутри станции
    #  находим нужный тип связи
    try:
        tipo = ObjektoLigiloTipo.objects.get(id=3, forigo=False,
                                            arkivo=False, publikigo=True)
    except ObjektoLigiloTipo.DoesNotExist:
        message = '{} "{}"'.format(_('Неверный тип места хранения ресурсов'),'tipo_id')
        return
    objektoj_ligiloj = ObjektoLigilo.objects.create(
        forigo=False,
        arkivo=False,
        posedanto = taskoj.objekto,
        ligilo = tasko_posedantoj.posedanto_objekto,
        tipo=tipo,
        publikigo=True,
        publikiga_dato=timezone.now()
    )
    objekto_ligilo_save(objektoj_ligiloj)
    # удаляем указатель на куб
    old_kubo = tasko_posedantoj.posedanto_objekto.kubo
    tasko_posedantoj.posedanto_objekto.kubo = None
    objekto_save(tasko_posedantoj.posedanto_objekto, old_kubo)
    # закрываем задачу и проект
    return fermo_projekto_tasko(taskoj)


# функция закрытия задачи
def fermo_tasko(taskoj):
    message = None
    # находим статус закрытия задачи
    try:
        statuso = TaskojTaskoStatuso.objects.get(id=4, forigo=False,
                                            arkivo=False, publikigo=True)
    except TaskojTaskoStatuso.DoesNotExist:
        message = _('Отсутствует статус закрытия задачи TaskojTaskoStatuso')
        return message
    # закрываем задачу
    taskoj.statuso = statuso
    taskoj.save()
    return message


# функция закрытия проекта
def fermo_projekto(projekto):
    message = None
    # находим статус закрытия проекта
    try:
        statuso = TaskojProjektoStatuso.objects.get(id=4, forigo=False,
                                            arkivo=False, publikigo=True)
    except TaskojProjektoStatuso.DoesNotExist:
        message = _('Отсутствует статус закрытия проекта TaskojProjektoStatuso')
        return message
    # закрываем проект движения
    projekto.statuso = statuso
    projekto.save()
    return message


# функция закрытия проекта и задачи
def fermo_projekto_tasko(taskoj):
    message = fermo_projekto(taskoj.projekto)
    if message:
        return message
    message = fermo_tasko(taskoj)
    return message


# функция обработки запроса на выход из станции
def eliro_kosmostacio(taskoj, tasko_posedantoj):
    # проверка наличия необходимых данных
    if not taskoj.objekto:
        return
    if not tasko_posedantoj.posedanto_objekto:
        return
    # находим связь
    try:
        objektoj_ligiloj = ObjektoLigilo.objects.get(
            posedanto=taskoj.objekto,
            ligilo=tasko_posedantoj.posedanto_objekto,
            forigo=False,
            arkivo=False, 
            publikigo=True)
    except ObjektoLigilo.DoesNotExist:
        message = _('Отсутствует связь')
        return message
    # закрываем задачу и проект
    message = fermo_projekto_tasko(taskoj)
    if message:
        return message
    # устанавливаем координы, куб объекту
    tasko_posedantoj.posedanto_objekto.kubo = taskoj.objekto.kubo
    tasko_posedantoj.posedanto_objekto.koordinato_x = taskoj.objekto.koordinato_x + 120
    tasko_posedantoj.posedanto_objekto.koordinato_y = taskoj.objekto.koordinato_y + 120
    tasko_posedantoj.posedanto_objekto.koordinato_z = taskoj.objekto.koordinato_z + 200
    tasko_posedantoj.posedanto_objekto.rotacia_x = 0
    tasko_posedantoj.posedanto_objekto.rotacia_y = 0
    tasko_posedantoj.posedanto_objekto.rotacia_z = 0
    objekto_save(tasko_posedantoj.posedanto_objekto, None)
    # помечаем на удаление связь на нахождение внутри станции
    objektoj_ligiloj.forigo = True
    objekto_ligilo_save(objektoj_ligiloj)


# перемещение объектов между складами
# stokejo - место хранение в месте назначения
def transmeti(taskoj, tasko_posedantoj, stokejo):
    """
        задача перемещения объектов
        новый объект-хранения перемещаемого объекта - taskoj.objekto
        объект, который будет перемещён - tasko_posedantoj
        в какое место хранение будет помещён объект - stokejo
    """
    message = ""
    # проверка наличия необходимых данных
    if not taskoj.objekto:
        return "отсутствует объект назначения"
    if not tasko_posedantoj.posedanto_objekto:
        return ""
    if not stokejo:
        message = 'отсутствует место хранение'
        return message
    # проверяем, принадлежит ли место хранения объекту назначения
    if stokejo.posedanto_objekto != taskoj.objekto:
        message = 'место хранения не принадлежит объекту назначения'
        return message
    
    # проверить объем свободного места в объекте назначения
    if taskoj.objekto.volumeno_interna: # если внутренний объём ограничен, то высчитываем - есть ли место для хранения нового объекта
        if tasko_posedantoj.posedanto_objekto.volumeno_stokado and loza_volumeno(taskoj.objekto) < tasko_posedantoj.posedanto_objekto.volumeno_stokado:
            message = 'свободного места не достаточно в объекте назначения'
            return message
    # else:
    #     print('=== внутренний объём безграничен ===')

    ### помечаем связь объекта нахождения внутри предыдущего склада на удаление 
    # находим тип связи - находится внутри (id = 3)
    try:
        tipo_ligilo = ObjektoLigiloTipo.objects.get(id=3, forigo=False,
                                            arkivo=False, publikigo=True)
    except ObjektoLigiloTipo.DoesNotExist:
        return 'нет типа связи по нахождению внутри'

    # находим связь
    try:
        objektoj_ligiloj = ObjektoLigilo.objects.filter(
            # posedanto=taskoj.objekto, не известно, где храниться
            # tipo=tipo_ligilo, #связь "находится внутри"
            ligilo=tasko_posedantoj.posedanto_objekto,
            forigo=False,
            arkivo=False, 
            publikigo=True)
        for ligilo in objektoj_ligiloj:
            # помечаем связь на удаление
            ligilo.forigo = True
            objekto_ligilo_save(ligilo)
    except ObjektoLigilo.DoesNotExist:
        # message = _('Отсутствует связь')
        # print('Отсутствует связь')
        pass

    ### меняем объекту (posedanto_objekto в tasko_posedantoj) владельца (владельца posedanto_objekto)
    ###   на владельца объекта назначения (владелец objekto в taskoj)
    # находим старого владельца объекта
    try:
        posedantoj = ObjektoPosedanto.objects.filter(
            objekto=tasko_posedantoj.posedanto_objekto,
            forigo=False,
            arkivo=False, 
            publikigo=True)
        # помечаем на удаление записи старых владельцев
        for posedanto_ in posedantoj:
            posedanto_.forigo = True
            posedanto_.save()
    except ObjektoPosedanto.DoesNotExist:
        # print('===владельца не нашли')
        pass
    # находим владельцев-получателей объекта
    posedantoj = ObjektoPosedanto.objects.filter(
        objekto=taskoj.objekto,
        forigo=False,
        arkivo=False, 
        publikigo=True)
    # создаём новых владельцев объекту
    for posedanto_ in posedantoj:
        objektoj_posedantoj = ObjektoPosedanto.objects.create(
            forigo=False,
            arkivo=False,
            posedanto_uzanto = posedanto_.posedanto_uzanto,
            objekto = tasko_posedantoj.posedanto_objekto,
            realeco = posedanto_.realeco,
            posedanto_organizo = posedanto_.posedanto_organizo,
            parto = posedanto_.parto,
            tipo = posedanto_.tipo,
            statuso = posedanto_.statuso,
            publikigo=True,
            publikiga_dato=timezone.now()
        )
        objektoj_posedantoj.save()
    ### Создаём связь объекту с новым складом
    # находим место расположения груза
    objekto_stokejo = ObjektoStokejo.objects.filter(
        posedanto_objekto=taskoj.objekto,
        forigo=False,
        arkivo=False,
        publikigo=True
    )
    stokejo = None
    # на 08.11.2020 берём первое место хранения в списке и помещаем объект туда
    if len(objekto_stokejo)>0:
        stokejo = objekto_stokejo[0]
    # создаём связь
    new_objektoj_ligiloj = ObjektoLigilo.objects.create(
        forigo=False,
        arkivo=False,
        posedanto=taskoj.objekto,
        posedanto_stokejo=stokejo,
        ligilo=tasko_posedantoj.posedanto_objekto,
        tipo=tipo_ligilo,
        publikigo=True,
        publikiga_dato=timezone.now()
    )
    objekto_ligilo_save(new_objektoj_ligiloj)
    # закрываем задачу
    message = fermo_tasko(taskoj)
    return message


# сохраняем задачи
def taskoj_save(taskoj):

    def eventoj():
        if taskoj.realeco:
            if taskoj.objekto and taskoj.objekto.kubo:
                ObjektoEventoj.movado_tasko(taskoj.objekto.kubo, taskoj)
            else:
                # проверяем есть объект-владелец в кубе
                # находим владельца
                tasko_posedantoj = None
                tasko_posedantoj = TaskojTaskoPosedanto.objects.filter(tasko=taskoj, forigo=False,
                                            arkivo=False, publikigo=True)
                for posedanto in tasko_posedantoj:
                    # проверяем наличие объектов
                    if posedanto.posedanto_objekto and posedanto.posedanto_objekto.kubo:
                        ObjektoEventoj.movado_tasko(posedanto.posedanto_objekto.kubo, taskoj)
    taskoj.save()
    transaction.on_commit(eventoj)


# Модель задач
class RedaktuTaskojTasko(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    taskoj = graphene.Field(TaskojTaskoNode, 
        description=_('Созданный/изменённый задача'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        projekto_uuid = graphene.String(description=_('Проект в который входит задача'))
        objekto_uuid = graphene.String(description=_('Объект владелец задачи'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий задач'))
        tipo_id = graphene.Int(description=_('Тип задач'))
        statuso_id = graphene.Int(description=_('Статус задач'))
        kom_koordinato_x = graphene.Float(description=_('Начальные координаты выполнения задачи по оси X в кубе'))
        kom_koordinato_y = graphene.Float(description=_('Начальные координаты выполнения задачи по оси Y в кубе'))
        kom_koordinato_z = graphene.Float(description=_('Начальные координаты выполнения задачи по оси Z в кубе'))
        fin_koordinato_x = graphene.Float(description=_('Конечные координаты выполнения задачи по оси X в кубе'))
        fin_koordinato_y = graphene.Float(description=_('Конечные координаты выполнения задачи по оси Y в кубе'))
        fin_koordinato_z = graphene.Float(description=_('Конечные координаты выполнения задачи по оси z в кубе'))
        pozicio = graphene.Int(description=_('Позиция в списке'))
        kom_dato = graphene.DateTime(description=_('Дата начала выполнения задачи'))
        fin_dato = graphene.DateTime(description=_('Дата окончания выполнения задачи'))
        kom_adreso = graphene.String(description=_('Адрес начала выполнения задачи'))
        fin_adreso = graphene.String(description=_('Адрес окончания выполнения задачи'))

        # параметры для создания владельца
        posedanto_uzanto_id = graphene.Int(description=_('Владелец задачи'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец задачи'))
        posedanto_tipo_id = graphene.Int(description=_('Тип владельца задачи'))
        posedanto_statuso_id = graphene.Int(description=_('Статус владельца задачи'))
        posedanto_objekto_uuid = graphene.String(description=_('Объект владелец задачи')) # если не передаётся, а передаётся objekto_uuid, то это один и тот же объект
        posedanto_dokumento_uuid = graphene.String(description=_('Документ-владелец задачи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        taskoj = None
        realeco = None
        projekto = None
        objekto = None
        kategorio = TaskojTaskoKategorio.objects.none()
        tipo = None
        statuso = None
        posedanto_uzanto = None
        posedanto_organizo = None
        posedanto_tipo = None
        posedanto_statuso = None
        posedanto_objekto = None
        posedanto_dokumento = None
        uzanto = info.context.user
        posedanto_perm = None # если пользователь владелец объекта по проекту

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    # если пользователь владелец объекта, по которому создаётся проект, то можно создавать
                    # проверяем по полю posedanto
                    if not uzanto.has_perm('taskoj.povas_krei_projektoj'):
                        posedanto_perm, message = provado_posedanto_objekto(uzanto, **kwargs)
                    if uzanto.has_perm('taskoj.povas_krei_taskoj') or posedanto_perm:
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            if 'kategorio' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                            elif not len(kwargs.get('kategorio')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'kategorio')
                            else:
                                kategorio_id = set(kwargs.get('kategorio'))

                                if len(kategorio_id):
                                    kategorio = TaskojTaskoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие категории задач'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = _('Неверный параллельный мир')

                        if not message:
                            if 'projekto_uuid' in kwargs:
                                try:
                                    projekto = TaskojProjekto.objects.get(uuid=kwargs.get('projekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojProjekto.DoesNotExist:
                                    message = _('Неверный проект в который входит задача')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'projekto_uuid')

                        if not message:
                            if 'objekto_uuid' in kwargs:
                                try:
                                    objekto = Objekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Objekto.DoesNotExist:
                                    message = _('Неверный объект')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = TaskojTaskoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojTaskoTipo.DoesNotExist:
                                    message = _('Неверный тип задачи')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'statuso_id' in kwargs:
                                try:
                                    statuso = TaskojTaskoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojTaskoStatuso.DoesNotExist:
                                    message = _('Неверный статус задач')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')

                        if not message:
                            taskoj = TaskojTasko.objects.create(
                                forigo=False,
                                arkivo=False,
                                realeco = realeco,
                                projekto = projekto,
                                objekto = objekto,
                                tipo = tipo,
                                statuso = statuso,
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato = timezone.now(),
                                kom_koordinato_x = kwargs.get('kom_koordinato_x', None),
                                kom_koordinato_y = kwargs.get('kom_koordinato_y', None),
                                kom_koordinato_z = kwargs.get('kom_koordinato_z', None),
                                fin_koordinato_x = kwargs.get('fin_koordinato_x', None),
                                fin_koordinato_y = kwargs.get('fin_koordinato_y', None),
                                fin_koordinato_z = kwargs.get('fin_koordinato_z', None),
                                pozicio = kwargs.get('pozicio', None),
                                kom_dato = kwargs.get('kom_dato', None),
                                fin_dato = kwargs.get('fin_dato', None),
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(taskoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(taskoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if (kwargs.get('kom_adreso', False)):
                                set_enhavo(taskoj.kom_adreso, 
                                        kwargs.get('kom_adreso')[i], 
                                        info.context.LANGUAGE_CODE)
                            if (kwargs.get('fin_adreso', False)):
                                set_enhavo(taskoj.fin_adreso, 
                                        kwargs.get('fin_adreso')[i], 
                                        info.context.LANGUAGE_CODE)
                            if 'kategorio' in kwargs:
                                if kategorio:
                                    taskoj.kategorio.set(kategorio)
                                else:
                                    taskoj.kategorio.clear()
                            
                            taskoj_save(taskoj)

                            status = True

                            # далее блок добавления владельца
                            if (kwargs.get('posedanto_tipo_id', False)):
                                if not message:
                                    try:
                                        posedanto_tipo = TaskojTaskoPosedantoTipo.objects.get(id=kwargs.get('posedanto_tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except TaskojTaskoPosedantoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип владельца задачи'),'posedanto_tipo_id')

                                if not message:
                                    if 'posedanto_statuso_id' in kwargs:
                                        try:
                                            posedanto_statuso = TaskojTaskoPosedantoStatuso.objects.get(id=kwargs.get('posedanto_statuso_id'), forigo=False,
                                                                            arkivo=False, publikigo=True)
                                        except TaskojTaskoPosedantoStatuso.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверный статус владельца задач'),'posedanto_statuso_id')
                                    else:
                                        message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_statuso_id')

                                if not message:
                                    if 'posedanto_uzanto_id' in kwargs:
                                        try:
                                            posedanto_uzanto = Uzanto.objects.get(
                                                id=kwargs.get('posedanto_uzanto_id'), 
                                                konfirmita=True
                                            )
                                        except Uzanto.DoesNotExist:
                                            message = _('Неверный владелец/пользователь задачи')

                                if not message:
                                    if 'posedanto_organizo_uuid' in kwargs:
                                        try:
                                            posedanto_organizo = Organizo.objects.get(
                                                uuid=kwargs.get('posedanto_organizo_uuid'),
                                                forigo=False,
                                                arkivo=False,  
                                                publikigo=True
                                            )
                                        except Organizo.DoesNotExist:
                                            message = _('Неверная организация владелец задачи')

                                if not message:
                                    if 'posedanto_objekto_uuid' in kwargs:
                                        try:
                                            posedanto_objekto = Objekto.objects.get(uuid=kwargs.get('posedanto_objekto_uuid'), forigo=False,
                                                                            arkivo=False, publikigo=True)
                                        except Objekto.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверный объект'),'posedanto_objekto_uuid')
                                if not message:
                                    if 'posedanto_dokumento_uuid' in kwargs:
                                        try:
                                            posedanto_dokumento = Dokumento.objects.get(uuid=kwargs.get('posedanto_dokumento_uuid'), forigo=False,
                                                                            arkivo=False, publikigo=True)
                                        except Dokumento.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверный документ-владелец'),'posedanto_dokumento_uuid')

                                if not message:
                                    tasko_posedantoj = TaskojTaskoPosedanto.objects.create(
                                        forigo = False,
                                        arkivo = False,
                                        realeco = realeco,
                                        tasko = taskoj,
                                        posedanto_uzanto = posedanto_uzanto,
                                        posedanto_organizo = posedanto_organizo,
                                        tipo = posedanto_tipo,
                                        statuso = posedanto_statuso,
                                        posedanto_objekto = posedanto_objekto,
                                        posedanto_dokumento=posedanto_dokumento,
                                        publikigo = kwargs.get('publikigo', False),
                                        publikiga_dato = timezone.now()
                                    )

                                    tasko_posedantoj.save()

                                    status = True
                                    message = _('Обе записи созданы')

                            # если текущая задача (в работе)
                            if (kwargs.get('statuso_id') == 2):
                                # если задача захода в станцию
                                if (8 in kategorio_id):
                                    eniri_kosmostacio(taskoj, tasko_posedantoj)
                                elif (9 in kategorio_id): # если задача выхода из станции
                                    eliro_kosmostacio(taskoj, tasko_posedantoj)

                            if not message:
                                message = _('Запись создана')

                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('kategorio', False)
                            or kwargs.get('projekto_uuid', False) or kwargs.get('objekto_uuid', False)
                            or kwargs.get('tipo_id', False)
                            or kwargs.get('kom_koordinato_x', False) or kwargs.get('kom_koordinato_y', False)
                            or kwargs.get('kom_koordinato_z', False) or kwargs.get('fin_koordinato_x', False)
                            or kwargs.get('fin_koordinato_y', False) or kwargs.get('fin_koordinato_z', False)
                            or kwargs.get('kom_dato', False) or kwargs.get('fin_dato', False)
                            or kwargs.get('kom_adreso', False) or kwargs.get('fin_adreso', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('pozicio', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            taskoj = TaskojTasko.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            # владелец объекта, по которому проект имеет право на удаление/редактирование проекта
                            # проверяем объект из проекта и проверяем объект, указанный в параметрах

                            # если пользователь владелец объекта, по которому редактируется задача, то можно редактировать
                            # проверяем по полю posedanto
                            posedanto_objekto = False
                            if 'posedanto_objekto_uuid' in kwargs:
                                posedanto_objekto = kwargs.get('posedanto_objekto_uuid')
                                try:
                                    posedanto_objekto = Objekto.objects.get(uuid=kwargs.get('posedanto_objekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                except Objekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект'),'posedanto_objekto_uuid')
                            else:
                                # берём существующего владельца
                                try:
                                    posedanto_tasko = TaskojTaskoPosedanto.objects.get(tasko=taskoj, 
                                            forigo=False, arkivo=False, publikigo=True)
                                    posedanto_objekto = posedanto_tasko.posedanto_objekto
                                except TaskojTaskoPosedanto.DoesNotExist:
                                    pass
                            if not posedanto_objekto:
                                if 'objekto_uuid' in kwargs: # проверяем объект из параметра
                                    try:
                                        posedanto_objekto = Objekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Objekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект'),'objekto_uuid')
                                else: # в параметре нет объекта, проверяем владельца объекта из базы
                                    posedanto_objekto = taskoj.objekto
                            if posedanto_objekto:
                                try:
                                    posedanto_perm = ObjektoPosedanto.objects.get(objekto=posedanto_objekto, 
                                            posedanto_uzanto=uzanto,
                                            forigo=False, arkivo=False, publikigo=True)
                                except ObjektoPosedanto.DoesNotExist:
                                    pass
                            if (not (uzanto.has_perm('taskoj.povas_forigi_taskoj') or 
                                    posedanto_perm)
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not (uzanto.has_perm('taskoj.povas_shanghi_taskoj') or 
                                    posedanto_perm):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))
                                    if len(kategorio_id):
                                        kategorio = TaskojTaskoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))
                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие категории проектов'),
                                                        str(dif)
                                            )

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'projekto_uuid' in kwargs:
                                    try:
                                        projekto = TaskojProjekto.objects.get(uuid=kwargs.get('projekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except TaskojProjekto.DoesNotExist:
                                        message = _('Неверный проект в который входит задача')

                            if not message:
                                if 'objekto_uuid' in kwargs:
                                    try:
                                        objekto = Objekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Objekto.DoesNotExist:
                                        message = _('Неверный объект')

                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = _('Неверный параллельный мир')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = TaskojTaskoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except TaskojTaskoTipo.DoesNotExist:
                                        message = _('Неверный тип проектов')

                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = TaskojTaskoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except TaskojTaskoStatuso.DoesNotExist:
                                        message = _('Неверный статус проектов')

                            if not message:

                                taskoj.forigo = kwargs.get('forigo', taskoj.forigo)
                                taskoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                taskoj.arkivo = kwargs.get('arkivo', taskoj.arkivo)
                                taskoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                taskoj.publikigo = kwargs.get('publikigo', taskoj.publikigo)
                                taskoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                taskoj.realeco = realeco if kwargs.get('realeco_id', False) else taskoj.realeco
                                taskoj.projekto = projekto if kwargs.get('projekto_uuid', False) else taskoj.projekto
                                taskoj.objekto = objekto if kwargs.get('objekto_uuid', False) else taskoj.objekto
                                taskoj.statuso = statuso if kwargs.get('statuso_id', False) else taskoj.statuso
                                taskoj.tipo = tipo if kwargs.get('tipo_id', False) else taskoj.tipo
                                taskoj.kom_koordinato_x = kwargs.get('kom_koordinato_x', taskoj.kom_koordinato_x)
                                taskoj.kom_koordinato_y = kwargs.get('kom_koordinato_y', taskoj.kom_koordinato_y)
                                taskoj.kom_koordinato_z = kwargs.get('kom_koordinato_z', taskoj.kom_koordinato_z)
                                taskoj.fin_koordinato_x = kwargs.get('fin_koordinato_x', taskoj.fin_koordinato_x)
                                taskoj.fin_koordinato_y = kwargs.get('fin_koordinato_y', taskoj.fin_koordinato_y)
                                taskoj.fin_koordinato_z = kwargs.get('fin_koordinato_z', taskoj.fin_koordinato_z)
                                taskoj.kom_dato = kwargs.get('kom_dato', taskoj.kom_dato)
                                taskoj.fin_dato = kwargs.get('fin_dato', taskoj.fin_dato)
                                taskoj.pozicio = kwargs.get('pozicio', taskoj.pozicio)

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(taskoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(taskoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('kom_adreso', False)):
                                    set_enhavo(taskoj.kom_adreso, 
                                            kwargs.get('kom_adreso'), 
                                            info.context.LANGUAGE_CODE)
                                if (kwargs.get('fin_adreso', False)):
                                    set_enhavo(taskoj.fin_adreso, 
                                            kwargs.get('fin_adreso'), 
                                            info.context.LANGUAGE_CODE)
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        taskoj.kategorio.set(kategorio)
                                    else:
                                        taskoj.kategorio.clear()

                                taskoj_save(taskoj)
                                status = True

                                # если текущая задача (в работе)
                                if (kwargs.get('statuso_id') == 2):
                                    # если запрос на заход в станцию
                                    tasko_posedantoj = None
                                    # находим владельца
                                    tasko_posedantoj = TaskojTaskoPosedanto.objects.filter(tasko=taskoj, forigo=False,
                                                                arkivo=False, publikigo=True)
                                    if (8 in kategorio):
                                        for posedanto in tasko_posedantoj:
                                            eniri_kosmostacio(taskoj, posedanto)
                                    elif (9 in kategorio): # если задача выхода из станции
                                        eliro_kosmostacio(taskoj, tasko_posedantoj)


                                message = _('Запись успешно изменена')
                        except TaskojTasko.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojTasko(status=status, message=message, taskoj=taskoj)


# Модель типов владельцев задач
class RedaktuTaskojTaskoPosedantoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    taskoj_posedantoj_tipoj = graphene.Field(TaskojTaskoPosedantoTipoNode, 
        description=_('Созданный/изменённый тип владельцев задач'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        taskoj_posedantoj_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_taskoj_posedantoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            taskoj_posedantoj_tipoj = TaskojTaskoPosedantoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(taskoj_posedantoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(taskoj_posedantoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            taskoj_posedantoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            taskoj_posedantoj_tipoj = TaskojTaskoPosedantoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_taskoj_posedantoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_taskoj_posedantoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                taskoj_posedantoj_tipoj.forigo = kwargs.get('forigo', taskoj_posedantoj_tipoj.forigo)
                                taskoj_posedantoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                taskoj_posedantoj_tipoj.arkivo = kwargs.get('arkivo', taskoj_posedantoj_tipoj.arkivo)
                                taskoj_posedantoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                taskoj_posedantoj_tipoj.publikigo = kwargs.get('publikigo', taskoj_posedantoj_tipoj.publikigo)
                                taskoj_posedantoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                taskoj_posedantoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(taskoj_posedantoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(taskoj_posedantoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                taskoj_posedantoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojTaskoPosedantoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojTaskoPosedantoTipo(status=status, message=message, taskoj_posedantoj_tipoj=taskoj_posedantoj_tipoj)


# Модель статусов владельцев задач
class RedaktuTaskojTaskoPosedantoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    tasko_posedantoj_statusoj = graphene.Field(TaskojTaskoPosedantoStatusoNode, 
        description=_('Созданный/изменённый статус владельца задач'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tasko_posedantoj_statusoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('taskoj.povas_krei_taskoj_posedantoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            tasko_posedantoj_statusoj = TaskojTaskoPosedantoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(tasko_posedantoj_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(tasko_posedantoj_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            tasko_posedantoj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tasko_posedantoj_statusoj = TaskojTaskoPosedantoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('taskoj.povas_forigi_taskoj_posedantoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('taskoj.povas_shanghi_taskoj_posedantoj_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                tasko_posedantoj_statusoj.forigo = kwargs.get('forigo', tasko_posedantoj_statusoj.forigo)
                                tasko_posedantoj_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tasko_posedantoj_statusoj.arkivo = kwargs.get('arkivo', tasko_posedantoj_statusoj.arkivo)
                                tasko_posedantoj_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tasko_posedantoj_statusoj.publikigo = kwargs.get('publikigo', tasko_posedantoj_statusoj.publikigo)
                                tasko_posedantoj_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                tasko_posedantoj_statusoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(tasko_posedantoj_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(tasko_posedantoj_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                tasko_posedantoj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojTaskoPosedantoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojTaskoPosedantoStatuso(status=status, message=message, tasko_posedantoj_statusoj=tasko_posedantoj_statusoj)


# Модель владельцев задач
class RedaktuTaskojTaskoPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    tasko_posedantoj = graphene.Field(TaskojTaskoPosedantoNode, 
        description=_('Созданный/изменённый владелец задач'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        tasko_uuid = graphene.String(description=_('Задача'))
        posedanto_uzanto_id = graphene.Int(description=_('Владелец задачи'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец задачи'))
        tipo_id = graphene.Int(description=_('Тип владельца задачи'))
        statuso_id = graphene.Int(description=_('Статус владельца задачи'))
        posedanto_objekto_uuid = graphene.String(description=_('Объект владелец задачи'))
        posedanto_dokumento_uuid = graphene.String(description=_('Документ-владелец задачи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tasko_posedantoj = None
        realeco = None
        tasko = None
        posedanto_uzanto = None
        posedanto_organizo = None
        posedanto_dokumento = None
        tipo = None
        statuso = None
        posedanto_objekto = None
        uzanto = info.context.user
        posedanto_perm = None # для проверки владельца объекта/задачи

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    # если пользователь создаёт владельца проекта по объекту, чьим владельцем является, то разрешаем
                    # проверяем соответствие пользователя и подключенного пользователя
                    if 'posedanto_uzanto_id' in kwargs:
                        try:
                            posedanto_uzanto2 = Uzanto.objects.get(
                                id=kwargs.get('posedanto_uzanto_id'), 
                                konfirmita=True
                            )
                            if uzanto != posedanto_uzanto2:
                                posedanto_perm = None
                            else:
                                posedanto_perm = uzanto
                        except Uzanto.DoesNotExist:
                            message = _('Неверный владелец/пользователь проекта')
                    # теперь проверяем владельца объекта
                    if not message:
                        if 'posedanto_objekto_uuid' in kwargs:
                            try:
                                objekto = Objekto.objects.get(
                                    uuid=kwargs.get('posedanto_objekto_uuid'), 
                                    forigo=False,
                                    arkivo=False, 
                                    publikigo=True
                                )
                                try:
                                    posedanto_objekto2 = ObjektoPosedanto.objects.get(objekto=objekto, 
                                                posedanto_uzanto=uzanto,
                                                forigo=False, arkivo=False, publikigo=True)
                                    if posedanto_perm: # если пользователь был указан ранее
                                        if posedanto_perm != posedanto_objekto2.posedanto_uzanto: # проверяем его соответствие с владельцем объекта
                                            posedanto_perm = None
                                except ObjektoPosedanto.DoesNotExist:
                                    pass
                            except Objekto.DoesNotExist:
                                message = '{} "{}"'.format(_('Неверный объект владелец проекта'), 'posedanto_objekto_uuid')

                    if uzanto.has_perm('taskoj.povas_krei_taskoj_posedantoj') or posedanto_perm:
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_uzanto_id' in kwargs:
                                try:
                                    posedanto_uzanto = Uzanto.objects.get(
                                        id=kwargs.get('posedanto_uzanto_id'), 
                                        konfirmita=True
                                    )
                                except Uzanto.DoesNotExist:
                                    message = _('Неверный владелец/пользователь задачи')

                        if not message:
                            if 'posedanto_organizo_uuid' in kwargs:
                                try:
                                    posedanto_organizo = Organizo.objects.get(
                                        uuid=kwargs.get('posedanto_organizo_uuid'),
                                        forigo=False,
                                        arkivo=False,  
                                        publikigo=True
                                    )
                                except Organizo.DoesNotExist:
                                    message = _('Неверная организация владелец задачи')

                        if not message:
                            if 'tasko_uuid' in kwargs:
                                try:
                                    tasko = TaskojTasko.objects.get(uuid=kwargs.get('tasko_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojTasko.DoesNotExist:
                                    message = _('Неверный проект')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tasko_uuid')

                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = _('Неверный параллельный мир')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = TaskojTaskoPosedantoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojTaskoPosedantoTipo.DoesNotExist:
                                    message = _('Неверный тип владельца задачи')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'statuso_id' in kwargs:
                                try:
                                    statuso = TaskojTaskoPosedantoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except TaskojTaskoPosedantoStatuso.DoesNotExist:
                                    message = _('Неверный статус владельца задачи')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')

                        if not message:
                            if 'posedanto_objekto_uuid' in kwargs:
                                try:
                                    posedanto_objekto = Objekto.objects.get(
                                        uuid=kwargs.get('posedanto_objekto_uuid'), 
                                        forigo=False,
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except Objekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект владелец задачи'),'posedanto_objekto_uuid')

                        if not message:
                            if 'posedanto_dokumento_uuid' in kwargs:
                                try:
                                    posedanto_dokumento = Dokumento.objects.get(
                                        uuid=kwargs.get('posedanto_dokumento_uuid'), 
                                        forigo=False,
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except Dokumento.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный документ-владелец задачи'),'posedanto_dokumento_uuid')

                        if not message:
                            tasko_posedantoj = TaskojTaskoPosedanto.objects.create(
                                forigo = False,
                                arkivo = False,
                                realeco = realeco,
                                tasko = tasko,
                                posedanto_uzanto = posedanto_uzanto,
                                posedanto_organizo = posedanto_organizo,
                                tipo = tipo,
                                statuso = statuso,
                                posedanto_objekto = posedanto_objekto,
                                posedanto_dokumento=posedanto_dokumento,
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato = timezone.now()
                            )

                            tasko_posedantoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('posedanto_uzanto_id', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('posedanto_organizo_uuid', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('tasko_uuid', False)
                            or kwargs.get('posedanto_objekto_uuid', False)
                            or kwargs.get('posedanto_dokumento_uuid', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tasko_posedantoj = TaskojTaskoPosedanto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            # проверяем на владельца проекта или владельца объекта, по которому проект
                            if tasko_posedantoj.posedanto_uzanto:
                                if tasko_posedantoj.posedanto_uzanto != uzanto: # пользователи не совпадают
                                    posedanto_perm = None
                                else:
                                    posedanto_perm = uzanto
                            if posedanto_perm: # продолжаем проверку по объекту
                                try:
                                    posedanto_objekto2 = ObjektoPosedanto.objects.get(
                                            objekto=tasko_posedantoj.posedanto_objekto, 
                                            posedanto_uzanto=uzanto,
                                            forigo=False, arkivo=False, publikigo=True)
                                    if posedanto_perm != posedanto_objekto2.posedanto_uzanto: # проверяем его соответствие с владельцем объекта
                                        posedanto_perm = None
                                except ObjektoPosedanto.DoesNotExist:
                                    pass

                            if (not (uzanto.has_perm('taskoj.povas_forigi_taskoj_posedantoj')
                                    or posedanto_perm)
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not (uzanto.has_perm('taskoj.povas_shanghi_taskoj_posedantoj')
                                    or posedanto_perm):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_uzanto_id' in kwargs:
                                    try:
                                        posedanto_uzanto = Uzanto.objects.get(
                                            id=kwargs.get('posedanto_uzanto_id'), 
                                            konfirmita=True
                                        )
                                    except Uzanto.DoesNotExist:
                                        message = _('Неверный владелец/пользователь задачи')

                            if not message:
                                if 'posedanto_organizo_uuid' in kwargs:
                                    try:
                                        posedanto_organizo = Organizo.objects.get(
                                            uuid=kwargs.get('posedanto_organizo_uuid'),
                                            forigo=False,
                                            arkivo=False,  
                                            publikigo=True
                                        )
                                    except Organizo.DoesNotExist:
                                        message = _('Неверная организация владелец задачи')

                            if not message:
                                if 'tasko_uuid' in kwargs:
                                    try:
                                        tasko = TaskojTasko.objects.get(uuid=kwargs.get('tasko_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except TaskojTasko.DoesNotExist:
                                        message = _('Неверная задача')

                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = _('Неверный параллельный мир')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = TaskojTaskoPosedantoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except TaskojTaskoPosedantoTipo.DoesNotExist:
                                        message = _('Неверный тип владельца задачи')

                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = TaskojTaskoPosedantoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except TaskojTaskoPosedantoStatuso.DoesNotExist:
                                        message = _('Неверный статус владельца задачи')

                            if not message:
                                if 'posedanto_objekto_uuid' in kwargs:
                                    try:
                                        posedanto_objekto = Objekto.objects.get(
                                            uuid=kwargs.get('posedanto_objekto_uuid'), 
                                            forigo=False,
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except Objekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект владелец задачи'),'posedanto_objekto_uuid')

                            if not message:
                                if 'posedanto_dokumento_uuid' in kwargs:
                                    try:
                                        posedanto_dokumento = Dokumento.objects.get(
                                            uuid=kwargs.get('posedanto_dokumento_uuid'), 
                                            forigo=False,
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except Dokumento.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный документ-владелец задачи'),'posedanto_dokumento_uuid')

                            if not message:

                                tasko_posedantoj.forigo = kwargs.get('forigo', tasko_posedantoj.forigo)
                                tasko_posedantoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tasko_posedantoj.arkivo = kwargs.get('arkivo', tasko_posedantoj.arkivo)
                                tasko_posedantoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tasko_posedantoj.publikigo = kwargs.get('publikigo', tasko_posedantoj.publikigo)
                                tasko_posedantoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                tasko_posedantoj.realeco = realeco if kwargs.get('realeco_id', False) else tasko_posedantoj.realeco
                                tasko_posedantoj.tasko = realeco if kwargs.get('tasko_uuid', False) else tasko_posedantoj.tasko
                                tasko_posedantoj.posedanto_uzanto = posedanto_uzanto if kwargs.get('posedanto_uzanto_id', False) else tasko_posedantoj.posedanto_uzanto
                                tasko_posedantoj.posedanto_organizo = posedanto_organizo if kwargs.get('posedanto_organizo_uuid', False) else tasko_posedantoj.posedanto_organizo
                                tasko_posedantoj.statuso = statuso if kwargs.get('statuso_id', False) else tasko_posedantoj.statuso
                                tasko_posedantoj.tipo = tipo if kwargs.get('tipo_id', False) else tasko_posedantoj.tipo
                                tasko_posedantoj.posedanto_objekto = posedanto_objekto if kwargs.get('posedanto_objekto_uuid', False) else tasko_posedantoj.posedanto_objekto
                                tasko_posedantoj.posedanto_dokumento = posedanto_dokumento if kwargs.get('posedanto_dokumento_uuid', False) else tasko_posedantoj.posedanto_dokumento

                                tasko_posedantoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except TaskojTaskoPosedanto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuTaskojTaskoPosedanto(status=status, message=message, tasko_posedantoj=tasko_posedantoj)


# Модель добавления множества задач с их владельцами
class RedaktuKreiTaskojTaskojPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    taskoj = graphene.Field(graphene.List(TaskojTaskoNode), 
        description=_('Создание списка задач с владельцами'))

    class Arguments:
        # параметры, одинаковые для всех задач
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        projekto_uuid = graphene.String(description=_('Проект в который входит задача'))
        tipo_id = graphene.Int(description=_('Тип задач'))
        # параметры задач в перечне (могут быть разные для разных задач)
        nomo = graphene.List(graphene.String, description=_('Название'))
        priskribo = graphene.List(graphene.String, description=_('Описание'))
        kategorio = graphene.List(graphene.List(graphene.Int, description=_('Список категорий задач')))
        objekto_uuid = graphene.List(graphene.String, description=_('Объект'))
        statuso_id = graphene.List(graphene.Int, description=_('Статус задач'))
        kom_koordinato_x = graphene.List(graphene.Float, description=_('Начальные координаты выполнения задачи по оси X в кубе'))
        kom_koordinato_y = graphene.List(graphene.Float, description=_('Начальные координаты выполнения задачи по оси Y в кубе'))
        kom_koordinato_z = graphene.List(graphene.Float, description=_('Начальные координаты выполнения задачи по оси Z в кубе'))
        fin_koordinato_x = graphene.List(graphene.Float, description=_('Конечные координаты выполнения задачи по оси X в кубе'))
        fin_koordinato_y = graphene.List(graphene.Float, description=_('Конечные координаты выполнения задачи по оси Y в кубе'))
        fin_koordinato_z = graphene.List(graphene.Float, description=_('Конечные координаты выполнения задачи по оси z в кубе'))
        pozicio = graphene.List(graphene.Int, description=_('Позиция в списке'))
        kom_dato = graphene.List(graphene.DateTime, description=_('Дата начала выполнения задачи'))
        fin_dato = graphene.List(graphene.DateTime, description=_('Дата окончания выполнения задачи'))
        kom_adreso = graphene.List(graphene.String, description=_('Адрес начала выполнения задачи'))
        fin_adreso = graphene.List(graphene.String, description=_('Адрес окончания выполнения задачи'))

        # Владелец задач - объект
        posedanto_tipo_id = graphene.Int(description=_('Тип владельца задач'))
        posedanto_statuso_id = graphene.Int(description=_('Статус владельца задач'))
        posedanto_objekto_uuid = graphene.String(description=_('Объект владелец задачи')) # если не передаётся, а передаётся objekto_uuid, то это один и тот же объект

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        taskoj = None
        masivo_taskoj = []
        realeco = None
        projekto = None
        objekto = []
        posedanto_objekto = None
        kategorio = TaskojTaskoKategorio.objects.none()
        tipo = None
        statuso = None
        posedanto_tipo = None
        posedanto_statuso = None
        uzanto = info.context.user
        posedanto_perm = None # для проверки владельца объекта/задачи

        if uzanto.is_authenticated:
            with transaction.atomic():
                # проверяем владельца объекта, по которому идёт добавление
                # если пользователь создаёт владельца проекта по объекту, чьим владельцем является, то разрешаем
                # проверяем владельца объекта
                if not message:
                    posedanto = False
                    if 'posedanto_objekto_uuid' in kwargs:
                        posedanto = kwargs.get('posedanto_objekto_uuid')
                    if posedanto:
                        try:
                            objekto_perm = Objekto.objects.get(
                                uuid=posedanto, 
                                forigo=False,
                                arkivo=False, 
                                publikigo=True
                            )
                            try:
                                posedanto_perm = ObjektoPosedanto.objects.get(objekto=objekto_perm, 
                                            posedanto_uzanto=uzanto,
                                            forigo=False, arkivo=False, publikigo=True)
                            except ObjektoPosedanto.DoesNotExist:
                                pass
                        except Objekto.DoesNotExist:
                            message = '{} "{}"'.format(_('Неверный объект владелец проекта'), 'posedanto_objekto_uuid или objekto_uuid')

                if uzanto.has_perm('taskoj.povas_krei_taskoj') or posedanto_perm:
                    # Проверяем наличие всех полей
                    if not message:
                        if not kwargs.get('nomo'):
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                    if not message:
                        if not kwargs.get('priskribo'):
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                    if not message:
                        if 'kategorio' not in kwargs:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                        elif not len(kwargs.get('kategorio')):
                            message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'kategorio')
                        else:
                            mas_kategorio = []
                            mas_kategorio.extend(kwargs.get('kategorio'))
                            kategorio_id = set(mas_kategorio)

                            if len(kategorio_id):
                                kategorio = TaskojTaskoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                if len(dif):
                                    message='{}: {}'.format(
                                                _('Указаны несуществующие категории задач'),
                                                str(dif)
                                    )

                    # проверяем наличие записей с таким кодом
                    if not message:
                        if 'realeco_id' in kwargs:
                            try:
                                realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except Realeco.DoesNotExist:
                                message = _('Неверный параллельный мир')

                    if not message:
                        if 'projekto_uuid' in kwargs:
                            try:
                                projekto = TaskojProjekto.objects.get(uuid=kwargs.get('projekto_uuid'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except TaskojProjekto.DoesNotExist:
                                message = _('Неверный проект в который входит задача')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'projekto_uuid')

                    if not message:
                        if 'posedanto_objekto_uuid' in kwargs:
                            try:
                                posedanto_objekto = Objekto.objects.get(uuid=kwargs.get('posedanto_objekto_uuid'), forigo=False,
                                                                arkivo=False, publikigo=True)
                            except Objekto.DoesNotExist:
                                message = '{} "{}"'.format(_('Неверный объект'),'posedanto_objekto_uuid')

                    if not message:
                        if 'tipo_id' in kwargs:
                            try:
                                tipo = TaskojTaskoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except TaskojTaskoTipo.DoesNotExist:
                                message = _('Неверный тип задачи')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')


                    if not message:
                        if 'posedanto_tipo_id' in kwargs:
                            try:
                                posedanto_tipo = TaskojTaskoPosedantoTipo.objects.get(id=kwargs.get('posedanto_tipo_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                            except TaskojTaskoPosedantoTipo.DoesNotExist:
                                message = _('Неверный тип владельца задачи')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_tipo_id')

                    if not message:
                        if 'posedanto_statuso_id' in kwargs:
                            try:
                                posedanto_statuso = TaskojTaskoPosedantoStatuso.objects.get(id=kwargs.get('posedanto_statuso_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                            except TaskojTaskoPosedantoStatuso.DoesNotExist:
                                message = _('Неверный статус владельца задачи')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_statuso_id')

                    if not message:
                        objekto_uuid = set(kwargs.get('objekto_uuid',[]))
                        # удаляем None из списка
                        objekto_uuid.discard(None)
                        if len(objekto_uuid):
                            try:
                                objekto = Objekto.objects.filter(uuid__in=objekto_uuid, forigo=False, arkivo=False, publikigo=True)
                                set_value = set(objekto.values_list('uuid', flat=True))
                                arr_value = []
                                for s in set_value:
                                    arr_value.append(str(s))

                                dif = set(objekto_uuid) - set(arr_value)

                                if len(dif):
                                    message='{}: {}'.format(
                                                    _('Указаны несуществующие объекты'),
                                                    str(dif)
                                    )
                            except Objekto.DoesNotExist:
                                message = '{} "{}"'.format(_('Неверный объект'),objekto_uuid)

                    if not kwargs.get('pozicio',False):
                        message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'pozicio')

                    if (not message) and kwargs.get('pozicio',False):
                        pozicioj = kwargs.get('pozicio')
                        statusoj = kwargs.get('statuso_id')
                        i = 0
                        for pozic in pozicioj:
                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = TaskojTaskoStatuso.objects.get(id=statusoj[i], forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except TaskojTaskoStatuso.DoesNotExist:
                                        message = _('Неверный статус задач')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')
                            else:
                                break

                            objekto = None
                            if ('objekto_uuid' in kwargs) and (kwargs.get('objekto_uuid')[i]):
                                try:
                                    objekto = Objekto.objects.get(uuid=kwargs.get('objekto_uuid')[i], forigo=False,
                                                                        arkivo=False, publikigo=True)
                                except Objekto.DoesNotExist:
                                    message = _('Неверный объект')

                            if not message:
                                taskoj = TaskojTasko.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    realeco = realeco,
                                    projekto = projekto,
                                    objekto = objekto,
                                    tipo = tipo,
                                    statuso = statuso,
                                    publikigo = True,
                                    publikiga_dato = timezone.now(),
                                    kom_koordinato_x = kwargs.get('kom_koordinato_x', None)[i] if len(kwargs.get('kom_koordinato_x', []))>i else None,
                                    kom_koordinato_y = kwargs.get('kom_koordinato_y', None)[i] if len(kwargs.get('kom_koordinato_y', []))>i else None,
                                    kom_koordinato_z = kwargs.get('kom_koordinato_z', None)[i] if len(kwargs.get('kom_koordinato_z', []))>i else None,
                                    fin_koordinato_x = kwargs.get('fin_koordinato_x', None)[i] if len(kwargs.get('fin_koordinato_x', []))>i else None,
                                    fin_koordinato_y = kwargs.get('fin_koordinato_y', None)[i] if len(kwargs.get('fin_koordinato_y', []))>i else None,
                                    fin_koordinato_z = kwargs.get('fin_koordinato_z', None)[i] if len(kwargs.get('fin_koordinato_z', []))>i else None,
                                    kom_dato = kwargs.get('kom_dato', None)[i] if len(kwargs.get('kom_dato', []))>i else None,
                                    fin_dato = kwargs.get('fin_dato', None)[i] if len(kwargs.get('fin_dato', []))>i else None,
                                    pozicio = pozic,
                                )

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(taskoj.nomo, kwargs.get('nomo')[i], info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(taskoj.priskribo, 
                                            kwargs.get('priskribo')[i], 
                                            info.context.LANGUAGE_CODE)
                                if (kwargs.get('kom_adreso', False)):
                                    set_enhavo(taskoj.kom_adreso, 
                                            kwargs.get('kom_adreso')[i], 
                                            info.context.LANGUAGE_CODE)
                                if (kwargs.get('fin_adreso', False)):
                                    set_enhavo(taskoj.fin_adreso, 
                                            kwargs.get('fin_adreso')[i], 
                                            info.context.LANGUAGE_CODE)
                                # категории брать из параметров
                                if ('kategorio' in kwargs) and (kwargs.get('kategorio')[i]):
                                    kategorio = TaskojTaskoKategorio.objects.none()

                                    kategorio_id = set(kwargs.get('kategorio'))
                                    if len(kategorio_id):
                                        kategorio = TaskojTaskoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)

                                    if kategorio:
                                        taskoj.kategorio.set(kategorio)
                                    else:
                                        taskoj.kategorio.clear()

                                taskoj_save(taskoj)
                                masivo_taskoj.append(taskoj)

                                if not message:
                                    tasko_posedantoj = TaskojTaskoPosedanto.objects.create(
                                        forigo = False,
                                        arkivo = False,
                                        realeco = realeco,
                                        tasko = taskoj,
                                        tipo = posedanto_tipo,
                                        statuso = posedanto_statuso,
                                        posedanto_objekto = posedanto_objekto,
                                        publikigo = True,
                                        publikiga_dato = timezone.now()
                                    )

                                    tasko_posedantoj.save()
                                    # если текущая задача (в работе)
                                    if (kwargs.get('statuso_id')[i] == 2):
                                        # если запрос на заход в станцию
                                        if (8 in kategorio_id):
                                            eniri_kosmostacio(taskoj, tasko_posedantoj)
                                        elif (9 in kategorio_id): # если задача выхода из станции
                                            eliro_kosmostacio(taskoj, tasko_posedantoj)


                                status = True
                            
                            i += 1

                        if status:
                            message = _('Запись создана')
                else:
                    message = _('Недостаточно прав')
        else:
            message = _('Требуется авторизация')

        return RedaktuKreiTaskojTaskojPosedanto(status=status, message=message, taskoj=masivo_taskoj)


# Модель добавления проекта с владельцем и множества задач с их владельцами
class RedaktuKreiTaskojProjektoTaskojPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    projekto = graphene.Field(TaskojProjektoNode, 
        description=_('Создание проекта с владельцем и списка задач с владельцами'))
    taskoj = graphene.Field(graphene.List(TaskojTaskoNode), 
        description=_('Создание списка задач с владельцами'))

    class Arguments:
        # параметры, одинаковые для проекта и всех задач
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        # параметры для проекта
        prj_nomo = graphene.String(description=_('Название'))
        prj_priskribo = graphene.String(description=_('Описание'))
        prj_kategorio = graphene.List(graphene.Int,description=_('Список категорий проектов'))
        prj_tipo_id = graphene.Int(description=_('Тип проектов'))
        prj_statuso_id = graphene.Int(description=_('Статус проектов'))
        prj_objekto_uuid = graphene.String(description=_('Объект'))
        prj_kom_koordinato_x = graphene.Float(description=_('Начальные координаты выполнения проекта по оси X в кубе'))
        prj_kom_koordinato_y = graphene.Float(description=_('Начальные координаты выполнения проекта по оси Y в кубе'))
        prj_kom_koordinato_z = graphene.Float(description=_('Начальные координаты выполнения проекта по оси Z в кубе'))
        prj_fin_koordinato_x = graphene.Float(description=_('Конечные координаты выполнения проекта по оси X в кубе'))
        prj_fin_koordinato_y = graphene.Float(description=_('Конечные координаты выполнения проекта по оси Y в кубе'))
        prj_fin_koordinato_z = graphene.Float(description=_('Конечные координаты выполнения проекта по оси z в кубе'))
        # параметры для создания владельца проекта
        prj_posedanto_uzanto_id = graphene.List(graphene.Int, description=_('ID владельца-лица проекта'))
        prj_posedanto_organizo_uuid = graphene.List(graphene.String, description=_('Организация-владелец проекта')) # может быть несколько владельцев
        prj_posedanto_tipo_id = graphene.List(graphene.Int ,description=_('Тип владельца проекта (обязательно для создания владельцев)'))
        prj_posedanto_statuso_id = graphene.List(graphene.Int, description=_('Статус владельца проекта'))
        # параметры, одинаковые для проекта и всех задач
        prj_posedanto_objekto_uuid = graphene.List(graphene.String, description=_('Объект-владелец проекта')) # если не передаётся, а передаётся objekto_uuid, то это один и тот же объект
        posedanto_dokumento_uuid = graphene.List(graphene.String, description=_('Документ-владелец проекта')) # это будет и владельцем всех задач
        # параметры, одинаковые для всех задач
        tipo_id = graphene.Int(description=_('Тип задач'))
        stokejo_uuid = graphene.String(description=_('Место хранения при передаче объектов'))
        # параметры задач в перечне (могут быть разные для разных задач)
        nomo = graphene.List(graphene.String, description=_('Название'))
        priskribo = graphene.List(graphene.String, description=_('Описание'))
        kategorio = graphene.List(graphene.List(graphene.Int, description=_('Список категорий задач')))
        objekto_uuid = graphene.List(graphene.String, description=_('Объект'))
        statuso_id = graphene.List(graphene.Int, description=_('Статус задач'))
        kom_koordinato_x = graphene.List(graphene.Float, description=_('Начальные координаты выполнения задачи по оси X в кубе'))
        kom_koordinato_y = graphene.List(graphene.Float, description=_('Начальные координаты выполнения задачи по оси Y в кубе'))
        kom_koordinato_z = graphene.List(graphene.Float, description=_('Начальные координаты выполнения задачи по оси Z в кубе'))
        fin_koordinato_x = graphene.List(graphene.Float, description=_('Конечные координаты выполнения задачи по оси X в кубе'))
        fin_koordinato_y = graphene.List(graphene.Float, description=_('Конечные координаты выполнения задачи по оси Y в кубе'))
        fin_koordinato_z = graphene.List(graphene.Float, description=_('Конечные координаты выполнения задачи по оси z в кубе'))
        pozicio = graphene.List(graphene.Int, description=_('Позиция в списке'))
        kom_dato = graphene.List(graphene.DateTime, description=_('Дата начала выполнения задачи'))
        fin_dato = graphene.List(graphene.DateTime, description=_('Дата окончания выполнения задачи'))
        kom_adreso = graphene.List(graphene.String, description=_('Адрес начала выполнения задачи'))
        fin_adreso = graphene.List(graphene.String, description=_('Адрес окончания выполнения задачи'))

        # Владелец задач - объект
        # владельцев может быть много
        posedanto_tipo_id = graphene.List(graphene.Int, description=_('Тип владельца задач'))
        posedanto_statuso_id = graphene.List(graphene.Int, description=_('Статус владельца задач'))
        posedanto_objekto_uuid = graphene.List(graphene.String, description=_('Объект владелец задачи')) # если не передаётся, а передаётся objekto_uuid, то это один и тот же объект

    # функция редактирования без проверки прав - для периодических задач
    @staticmethod
    def krei_projekto(errors, language_code, **kwargs):
        message_krei = None
        objekto = None
        realeco = None
        prj_kategorio = TaskojProjektoKategorio.objects.none()
        # Проверяем наличие всех полей
        if not kwargs.get('pozicio',False):
            errors.append(ErrorNode(
                field='pozicio',
                message=_('Поле обязательно для заполнения (должна быть хотя бы одна задача)')
            ))
        if not kwargs.get('prj_nomo'):
            errors.append(ErrorNode(
                field='prj_nomo',
                message=_('Поле обязательно для заполнения')
            ))

        if 'prj_kategorio' not in kwargs:
            errors.append(ErrorNode(
                field='prj_kategorio',
                message=_('Поле обязательно для заполнения')
            ))
        elif not len(kwargs.get('prj_kategorio')):
            errors.append(ErrorNode(
                field='prj_kategorio',
                message=_('Необходимо указать хотя бы одну группу')
            ))
        elif not len(errors): # есть смысл загружать базу только если нет ошибок
            kategorio_id = set(kwargs.get('prj_kategorio'))

            if len(kategorio_id):
                prj_kategorio = TaskojProjektoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                dif = set(kategorio_id) - set(prj_kategorio.values_list('id', flat=True))

                if len(dif):
                    errors.append(ErrorNode(
                        field=str(dif),
                        message=_('Указаны несуществующие категории проектов')
                    ))
        # проверяем наличие записей с таким кодом
        if not len(errors):
            # устанавливаем по умолчанию
            realeco_id = kwargs.get('realeco_id', 1)
            try:
                realeco = Realeco.objects.get(id=realeco_id, forigo=False,
                                                    arkivo=False, publikigo=True)
            except Realeco.DoesNotExist:
                errors.append(ErrorNode(
                    field='realeco_id',
                    message=_('Неверный параллельный мир')
                ))

        if 'prj_tipo_id' in kwargs:
            if not len(errors):
                try:
                    tipo = TaskojProjektoTipo.objects.get(id=kwargs.get('prj_tipo_id'), forigo=False,
                                                        arkivo=False, publikigo=True)
                except TaskojProjektoTipo.DoesNotExist:
                    errors.append(ErrorNode(
                        field='prj_tipo_id',
                        message=_('Неверный тип проектов')
                    ))
        else:
            errors.append(ErrorNode(
                field='prj_tipo_id',
                message=_('Поле обязательно для заполнения')
            ))

        if 'prj_statuso_id' in kwargs:
            if not len(errors):
                try:
                    statuso = TaskojProjektoStatuso.objects.get(id=kwargs.get('prj_statuso_id'), forigo=False,
                                                        arkivo=False, publikigo=True)
                except TaskojProjektoStatuso.DoesNotExist:
                    errors.append(ErrorNode(
                        field='prj_statuso_id',
                        message=_('Неверный статус проектов')
                    ))
        else:
            errors.append(ErrorNode(
                field='prj_statuso_id',
                message=_('Поле обязательно для заполнения')
            ))

        if not len(errors):
            if 'prj_objekto_uuid' in kwargs:
                try:
                    objekto = Objekto.objects.get(uuid=kwargs.get('prj_objekto_uuid'), forigo=False,
                                                        arkivo=False, publikigo=True)
                except Objekto.DoesNotExist:
                    errors.append(ErrorNode(
                        field='prj_objekto_uuid',
                        message=_('Неверный объект')
                    ))

        # создаём проект
        if not len(errors):
            projekto = TaskojProjekto.objects.create(
                forigo=False,
                arkivo=False,
                realeco = realeco,
                tipo = tipo,
                statuso = statuso,
                kom_koordinato_x = kwargs.get('prj_kom_koordinato_x', None),
                kom_koordinato_y = kwargs.get('prj_kom_koordinato_y', None),
                kom_koordinato_z = kwargs.get('prj_kom_koordinato_z', None),
                fin_koordinato_x = kwargs.get('prj_fin_koordinato_x', None),
                fin_koordinato_y = kwargs.get('prj_fin_koordinato_y', None),
                fin_koordinato_z = kwargs.get('prj_fin_koordinato_z', None),
                publikigo = True,
                publikiga_dato = timezone.now()
            )

            if (kwargs.get('prj_nomo', False)):
                set_enhavo(projekto.nomo, kwargs.get('prj_nomo'), language_code)
            if (kwargs.get('prj_priskribo', False)):
                set_enhavo(projekto.priskribo, 
                            kwargs.get('prj_priskribo'), 
                            language_code)
            if objekto:
                projekto.objekto = objekto
            if 'prj_kategorio' in kwargs:
                if prj_kategorio:
                    projekto.kategorio.set(prj_kategorio)
                else:
                    projekto.kategorio.clear()

            projekto.save()

            status = True
            # далее блок добавления владельца
            if ((kwargs.get('prj_posedanto_tipo_id', False)) and 
                    (kwargs.get('prj_posedanto_uzanto_id', False) or 
                    kwargs.get('prj_posedanto_objekto_uuid', False) or 
                    kwargs.get('posedanto_dokumento_uuid', False) or 
                    kwargs.get('prj_posedanto_organizo_uuid', False))):
                kvanto = len(kwargs.get('prj_posedanto_tipo_id')) # количество владельцев
                indico = 0 # индекс
                for prj_posedanto_tipo_id in kwargs.get('prj_posedanto_tipo_id'):

                    posedanto_uzanto = None
                    if 'prj_posedanto_uzanto_id' in kwargs:
                        if len(kwargs.get('prj_posedanto_uzanto_id')) != kvanto:
                            errors.append(ErrorNode(
                                field='prj_posedanto_uzanto_id',
                                message=_('Количество элементов расходится с количеством в prj_posedanto_tipo_id')
                            ))
                        if not len(errors) and kwargs.get('prj_posedanto_uzanto_id')[indico]:
                            try:
                                posedanto_uzanto = Uzanto.objects.get(
                                    id=kwargs.get('prj_posedanto_uzanto_id')[indico], 
                                    konfirmita=True
                                )
                            except Uzanto.DoesNotExist:
                                errors.append(ErrorNode(
                                    field='prj_posedanto_uzanto_id',
                                    message=_('Несуществующий пользователь проекта')
                                ))

                    if not len(errors):
                        try:
                            posedanto_tipo = TaskojProjektoPosedantoTipo.objects.get(id=prj_posedanto_tipo_id, forigo=False,
                                                            arkivo=False, publikigo=True)
                        except TaskojProjektoPosedantoTipo.DoesNotExist:
                            errors.append(ErrorNode(
                                field='prj_posedanto_tipo_id',
                                message=_('Неверный тип владельца проекта')
                            ))

                    if (kwargs.get('prj_posedanto_statuso_id', False)):
                        if len(kwargs.get('prj_posedanto_statuso_id')) != kvanto:
                            errors.append(ErrorNode(
                                field='prj_posedanto_statuso_id',
                                message=_('Количество элементов расходится с количеством в prj_posedanto_tipo_id')
                            ))
                        if not len(errors):
                            try:
                                posedanto_statuso = TaskojProjektoPosedantoStatuso.objects.get(id=kwargs.get('prj_posedanto_statuso_id')[indico], forigo=False,
                                                                arkivo=False, publikigo=True)
                            except TaskojProjektoPosedantoStatuso.DoesNotExist:
                                errors.append(ErrorNode(
                                    field='prj_posedanto_statuso_id',
                                    message=_('Неверный статус владельца проекта')
                                ))
                    else:
                        errors.append(ErrorNode(
                            field='prj_posedanto_statuso_id',
                            message=_('Поле обязательно для заполнения')
                        ))

                    posedanto_organizo = None
                    if 'prj_posedanto_organizo_uuid' in kwargs:
                        if len(kwargs.get('prj_posedanto_organizo_uuid')) != kvanto:
                            errors.append(ErrorNode(
                                field='prj_posedanto_organizo_uuid',
                                message=_('Количество элементов расходится с количеством в prj_posedanto_tipo_id')
                            ))
                        if not len(errors) and kwargs.get('prj_posedanto_organizo_uuid')[indico]:
                            try:
                                posedanto_organizo = Organizo.objects.get(
                                    uuid=kwargs.get('prj_posedanto_organizo_uuid')[indico],
                                    forigo=False,
                                    arkivo=False,  
                                    publikigo=True
                                )
                            except Organizo.DoesNotExist:
                                errors.append(ErrorNode(
                                    field='prj_posedanto_organizo_uuid',
                                    message=_('Неверная организация владелец проекта')
                                ))

                    posedanto_objekto = None
                    if 'prj_posedanto_objekto_uuid' in kwargs:
                        if len(kwargs.get('prj_posedanto_objekto_uuid')) != kvanto:
                            errors.append(ErrorNode(
                                field='prj_posedanto_objekto_uuid',
                                message=_('Количество элементов расходится с количеством в prj_posedanto_tipo_id')
                            ))
                        if not len(errors) and kwargs.get('prj_posedanto_objekto_uuid')[indico]:
                            try:
                                posedanto_objekto = Objekto.objects.get(uuid=kwargs.get('prj_posedanto_objekto_uuid')[indico], forigo=False,
                                                                arkivo=False, publikigo=True)
                            except Objekto.DoesNotExist:
                                errors.append(ErrorNode(
                                    field='prj_posedanto_objekto_uuid',
                                    message=_('Неверный объект')
                                ))

                    posedanto_dokumento = None
                    if 'posedanto_dokumento_uuid' in kwargs:
                        if len(kwargs.get('posedanto_dokumento_uuid')) != kvanto:
                            errors.append(ErrorNode(
                                field='posedanto_dokumento_uuid',
                                message=_('Количество элементов расходится с количеством в prj_posedanto_tipo_id')
                            ))
                        if not len(errors) and kwargs.get('posedanto_dokumento_uuid')[indico]:
                            try:
                                posedanto_dokumento = Dokumento.objects.get(uuid=kwargs.get('posedanto_dokumento_uuid')[indico], forigo=False,
                                                                arkivo=False, publikigo=True)
                            except Dokumento.DoesNotExist:
                                errors.append(ErrorNode(
                                    field='posedanto_dokumento_uuid',
                                    message=_('Неверный документ')
                                ))

                    if not len(errors):
                        projekto_posedantoj = TaskojProjektoPosedanto.objects.create(
                            forigo=False,
                            arkivo=False,
                            realeco=realeco,
                            projekto=projekto,
                            posedanto_organizo = posedanto_organizo,
                            posedanto_uzanto = posedanto_uzanto,
                            tipo = posedanto_tipo,
                            statuso = posedanto_statuso,
                            posedanto_objekto = posedanto_objekto,
                            posedanto_dokumento=posedanto_dokumento,
                            publikigo = True,
                            publikiga_dato = timezone.now()
                        )

                        projekto_posedantoj.save()
                    indico += 1
                    if len(errors):
                        # если в цикле была ошибка - выходим
                        break
                if not len(errors):
                    status = True
                    message_krei = _('Записи проекта и владельца созданы')

            if not message_krei:
                message_krei = _('Запись проекта создана')

        return errors, message_krei, status, realeco, projekto

    @staticmethod
    def krei_taskoj(errors, realeco, projekto, language_code, **kwargs):
        """Добавляем список задач к проекту

        Args:
            errors (_type_): _description_
            # posedanto_dokumento (_type_): _description_
        """
        message_krei = None
        posedanto_statuso = None
        masivo_taskoj = []
        taskoj = None
        stokejo = None
        # Проверяем наличие всех полей
        pozicioj = kwargs.get('pozicio', False)
        if not pozicioj:
            return
        
        if not kwargs.get('nomo'):
            errors.append(ErrorNode(
                field='nomo',
                message=_('Поле обязательно для заполнения')
            ))
        else:
            if len(kwargs.get('nomo'))<len(pozicioj):
                errors.append(ErrorNode(
                    field='{} "{} < {}"'.format(_('nomo'),len(kwargs.get('nomo')),len(pozicioj)),
                    message=_('количество наименований задач меньше количества позиций')
                ))

        if not kwargs.get('priskribo'):
            errors.append(ErrorNode(
                field='priskribo',
                message=_('Поле обязательно для заполнения')
            ))
        elif len(kwargs.get('priskribo'))<len(pozicioj):
            errors.append(ErrorNode(
                field='{} "{} < {}"'.format(_('priskribo'),len(kwargs.get('priskribo')),len(pozicioj)),
                message=_('количество описаний задач меньше количества позиций')
            ))

        if 'kategorio' not in kwargs:
            errors.append(ErrorNode(
                field='kategorio',
                message=_('Поле обязательно для заполнения')
            ))
        elif len(kwargs.get('kategorio'))<len(pozicioj):
            errors.append(ErrorNode(
                field='{} "{} < {}"'.format(_('kategorio'),len(kwargs.get('kategorio')),len(pozicioj)),
                message=_('количество категорий задач меньше количества позиций')
            ))
        elif not len(errors):
            mas_kategorio = []
            mas_kategorio.extend(kwargs.get('kategorio'))
            kategorio_id = set(mas_kategorio)

            if len(kategorio_id):
                kategorio = TaskojTaskoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                if len(dif):
                    errors.append(ErrorNode(
                        field='{} ({})'.format(_('kategorio'), str(dif)),
                        message=_('Указаны несуществующие категории задач')
                    ))

        # проверяем наличие записей с таким кодом
        tipo = None
        if 'tipo_id' in kwargs:
            if not len(errors):
                try:
                    tipo = TaskojTaskoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                        arkivo=False, publikigo=True)
                except TaskojTaskoTipo.DoesNotExist:
                    errors.append(ErrorNode(
                        field='tipo_id',
                        message=_('Неверный тип задачи')
                    ))
        else:
            errors.append(ErrorNode(
                field='tipo_id',
                message=_('Поле обязательно для заполнения')
            ))

        if not len(errors):
            objekto_uuid = set(kwargs.get('objekto_uuid',[]))
            # удаляем None из списка
            objekto_uuid.discard(None)
            if len(objekto_uuid):
                objektoj = Objekto.objects.filter(uuid__in=objekto_uuid, forigo=False, arkivo=False, publikigo=True)
                set_value = set(objektoj.values_list('uuid', flat=True))
                arr_value = []
                for s in set_value:
                    arr_value.append(str(s))

                dif = set(objekto_uuid) - set(arr_value)

                if len(dif):
                    errors.append(ErrorNode(
                        field='{} ({})'.format(_('objekto_uuid'), str(dif)),
                        message=_('Указаны несуществующие объекты')
                    ))

        if not len(errors):
            tasko_posedanto_tipo_id = set(kwargs.get('posedanto_tipo_id',[]))
            if len(tasko_posedanto_tipo_id):
                tasko_posedanto_tipo = TaskojTaskoPosedantoTipo.objects.filter(id__in=tasko_posedanto_tipo_id, forigo=False, arkivo=False, publikigo=True)
                set_value = set(tasko_posedanto_tipo.values_list('id', flat=True))
                arr_value = []
                for s in set_value:
                    arr_value.append(s)
                dif = set(tasko_posedanto_tipo_id) - set(arr_value)
                if len(dif):
                    errors.append(ErrorNode(
                        field='{} ({})'.format(_('posedanto_tipo_id'), str(dif)),
                        message=_('Указаны несуществующие типы владельцев')
                    ))
            else:
                errors.append(ErrorNode(
                    field='posedanto_tipo_id',
                    message=_('Не указаны типы владельцев')
                ))

        posedanto_objekto = None
        if not len(errors):
            posedanto_objekto_uuid = set(kwargs.get('posedanto_objekto_uuid',[]))
            posedanto_dokumento_uuid = set(kwargs.get('posedanto_dokumento_uuid',[]))
            # удаляем None из списка
            posedanto_objekto_uuid.discard(None)
            if not (len(posedanto_objekto_uuid) or posedanto_dokumento_uuid):
                errors.append(ErrorNode(
                    field=_('posedanto_objekto_uuid или posedanto_dokumento_uuid'),
                    message=_('Не указаны объекты-владельцы или документ-владелец')
                ))

            if len(posedanto_objekto_uuid):
                posedanto_objekto = Objekto.objects.filter(uuid__in=posedanto_objekto_uuid, forigo=False, arkivo=False, publikigo=True)
                set_value = set(posedanto_objekto.values_list('uuid', flat=True))
                arr_value = []
                for s in set_value:
                    arr_value.append(str(s))
                dif = set(posedanto_objekto_uuid) - set(arr_value)
                if len(dif):
                    errors.append(ErrorNode(
                        field='{} ({})'.format(_('posedanto_objekto_uuid'), str(dif)),
                        message=_('Указаны несуществующие объекты')
                    ))
                elif len(posedanto_objekto)<len(tasko_posedanto_tipo):
                    errors.append(ErrorNode(
                        field='{} "{} < {}"'.format(_('posedanto_objekto_uuid'),len(posedanto_objekto),len(tasko_posedanto_tipo)),
                        message=_('количество категорий задач меньше количества позиций')
                    ))

        if not len(errors):
            posedanto_statuso_id = set(kwargs.get('posedanto_statuso_id',[]))
            if len(posedanto_statuso_id):
                posedanto_statuso = TaskojTaskoPosedantoStatuso.objects.filter(id__in=posedanto_statuso_id, forigo=False, arkivo=False, publikigo=True)
                set_value = set(posedanto_statuso.values_list('id', flat=True))
                arr_value = []
                for s in set_value:
                    arr_value.append(s)
                dif = set(posedanto_statuso_id) - set(arr_value)
                if len(dif):
                    errors.append(ErrorNode(
                        field='{} ({})'.format(_('posedanto_statuso_id'), str(dif)),
                        message=_('Указаны несуществующие статусы владельцев')
                    ))
                elif len(posedanto_statuso_id)<len(tasko_posedanto_tipo):
                    errors.append(ErrorNode(
                        field='{} "{} < {}"'.format(_('posedanto_statuso_id'),len(posedanto_statuso_id),len(tasko_posedanto_tipo)),
                        message=_('количество статусов владельцев меньше количества типов владельцев задач')
                    ))
                else: #собираем массив статусов владельцев для каждой задачи согласно вошедшего массива, 
                    #  т.к. у нас перечисление, а нужен верно составленный массив
                    posedanto_statuso_masivo = []
                    for st in kwargs.get('posedanto_statuso_id',[]):
                        for p_s in posedanto_statuso:
                            if st == p_s.id:
                                posedanto_statuso_masivo.append(p_s)
                                break
            else:
                errors.append(ErrorNode(
                    field='posedanto_statuso_id',
                    message=_('Не указаны статусы владельцев')
                ))

        if not kwargs.get('pozicio',False):
            errors.append(ErrorNode(
                field='pozicio',
                message=_('Поле обязательно для заполнения')
            ))

        if not kwargs.get('statuso_id',False):
            errors.append(ErrorNode(
                field='statuso_id',
                message=_('Поле обязательно для заполнения')
            ))

        if not len(errors):
            if 'stokejo_uuid' in kwargs:
                try:
                    stokejo = ObjektoStokejo.objects.get(uuid=kwargs.get('stokejo_uuid'), forigo=False,
                                                        arkivo=False, publikigo=True)
                except ObjektoStokejo.DoesNotExist:
                    errors.append(ErrorNode(
                        field='stokejo_uuid',
                        message=_('Неверное место хранения')
                    ))

        statusoj = kwargs.get('statuso_id')
        # проверка на обязательный параметр statuso_id
        
        if len(statusoj)<len(pozicioj):
            errors.append(ErrorNode(
                field='{} "{} < {}"'.format(_('statuso_id'),len(statusoj),len(pozicioj)),
                message=_('количество статусов меньше количества позиций в параметре')
            ))
        # добавляем список задач к проекту
        if (not len(errors)) and kwargs.get('pozicio', False):
            i = 0
            statuso = None
            status = False
            for pozic in pozicioj:
                if len(errors):
                    break

                try:
                    statuso = TaskojTaskoStatuso.objects.get(id=statusoj[i], forigo=False,
                                                    arkivo=False, publikigo=True)
                except TaskojTaskoStatuso.DoesNotExist:
                    errors.append(ErrorNode(
                        field='statuso_id',
                        message=_('Неверный статус задач')
                    ))
                    break

                objekto = None
                if ('objekto_uuid' in kwargs) and (kwargs.get('objekto_uuid')[i]):
                    try:
                        objekto = Objekto.objects.get(uuid=kwargs.get('objekto_uuid')[i], forigo=False,
                                                            arkivo=False, publikigo=True)
                    except Objekto.DoesNotExist:
                        errors.append(ErrorNode(
                            field='objekto_uuid',
                            message=_('Неверный объект')
                        ))

                if not len(errors):
                    taskoj = TaskojTasko.objects.create(
                        forigo=False,
                        arkivo=False,
                        realeco = realeco,
                        projekto = projekto,
                        objekto = objekto,
                        tipo = tipo,
                        statuso = statuso,
                        publikigo = True,
                        publikiga_dato = timezone.now(),
                        kom_koordinato_x = kwargs.get('kom_koordinato_x', [])[i] if len(kwargs.get('kom_koordinato_x', []))>i else None,
                        kom_koordinato_y = kwargs.get('kom_koordinato_y', [])[i] if len(kwargs.get('kom_koordinato_y', []))>i else None,
                        kom_koordinato_z = kwargs.get('kom_koordinato_z', [])[i] if len(kwargs.get('kom_koordinato_z', []))>i else None,
                        fin_koordinato_x = kwargs.get('fin_koordinato_x', None)[i] if len(kwargs.get('fin_koordinato_x', []))>i else None,
                        fin_koordinato_y = kwargs.get('fin_koordinato_y', None)[i] if len(kwargs.get('fin_koordinato_y', []))>i else None,
                        fin_koordinato_z = kwargs.get('fin_koordinato_z', None)[i] if len(kwargs.get('fin_koordinato_z', []))>i else None,
                        kom_dato = kwargs.get('kom_dato', [])[i] if len(kwargs.get('kom_dato', []))>i else None,
                        fin_dato = kwargs.get('fin_dato', [])[i] if len(kwargs.get('fin_dato', []))>i else None,
                        pozicio = pozic,
                    )

                    if (kwargs.get('nomo', False)):
                        set_enhavo(taskoj.nomo, kwargs.get('nomo')[i], language_code)
                    if (kwargs.get('priskribo', False)):
                        set_enhavo(taskoj.priskribo, 
                                kwargs.get('priskribo')[i], 
                                language_code)
                    if (kwargs.get('kom_adreso', False)):
                        set_enhavo(taskoj.kom_adreso, 
                                kwargs.get('kom_adreso')[i], 
                                language_code)
                    if (kwargs.get('fin_adreso', False)):
                        set_enhavo(taskoj.fin_adreso, 
                                kwargs.get('fin_adreso')[i], 
                                language_code)

                    # категории брать из параметров
                    if ('kategorio' in kwargs) and (kwargs.get('kategorio')[i]):
                        kategorio = TaskojTaskoKategorio.objects.none()
                        kategorio_id = set(kwargs.get('kategorio'))
                        if len(kategorio_id):
                            kategorio = TaskojTaskoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)

                        if kategorio:
                            taskoj.kategorio.set(kategorio)
                        else:
                            taskoj.kategorio.clear()

                    taskoj_save(taskoj)
                    masivo_taskoj.append(taskoj)

                    if not len(errors):
                        fermo_projekto_link = None # по окончании цикла закрыть указанный проект
                        indico = 0
                        for posedanto_tipo in tasko_posedanto_tipo:
                            posedanto_objekto_temp = None
                            if posedanto_objekto:
                                posedanto_objekto_temp = posedanto_objekto[i]

                            posedanto_dokumento = None
                            if 'posedanto_dokumento_uuid' in kwargs:
                                if not len(errors) and kwargs.get('posedanto_dokumento_uuid')[indico]:
                                    try:
                                        posedanto_dokumento = Dokumento.objects.get(uuid=kwargs.get('posedanto_dokumento_uuid')[indico], forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Dokumento.DoesNotExist:
                                        errors.append(ErrorNode(
                                            field='posedanto_dokumento_uuid',
                                            message=_('Неверный документ')
                                        ))
                                        break

                            tasko_posedantoj = TaskojTaskoPosedanto.objects.create(
                                forigo = False,
                                arkivo = False,
                                realeco = realeco,
                                tasko = taskoj,
                                tipo = posedanto_tipo,
                                statuso = posedanto_statuso_masivo[i],
                                posedanto_objekto = posedanto_objekto_temp,
                                posedanto_dokumento = posedanto_dokumento,
                                publikigo = True,
                                publikiga_dato = timezone.now()
                            )

                            tasko_posedantoj.save()
                            # если текущая задача (в работе)
                            if (kwargs.get('statuso_id')[i] == 2):
                                # если запрос на заход в станцию
                                if (8 in kategorio_id):
                                    eniri_kosmostacio(taskoj, tasko_posedantoj)
                                    fermo_projekto_link = taskoj.projekto
                                elif (9 in kategorio_id): # если задача выхода из станции
                                    eliro_kosmostacio(taskoj, tasko_posedantoj)
                                    fermo_projekto_link = taskoj.projekto
                                elif (11 in kategorio_id): # если задача перемещения объекта
                                    if not stokejo:
                                        # закрываем задачу, проект и говорим о недопустимости отсутствия места хранения
                                        errors.append(ErrorNode(
                                            field=taskoj.objekto,
                                            message=_('Не указано место хранение у объекта назначения')
                                        ))
                                        break
                                    message_transmeti = transmeti(taskoj, tasko_posedantoj, stokejo)
                                    if message_transmeti:
                                        errors.append(ErrorNode(
                                            field=taskoj.objekto,
                                            message=message_transmeti
                                        ))
                                        break
                                    fermo_projekto_link = taskoj.projekto
                            indico += 1
                        if len(errors):
                            message = fermo_projekto_tasko(taskoj)
                            if message:
                                errors.append(ErrorNode(
                                    message=message
                                ))
                            fermo_projekto_link = None
                            status = False
                        elif fermo_projekto_link:
                            # закрываем проект
                            message = fermo_projekto(fermo_projekto_link)
                            if message:
                                errors.append(ErrorNode(
                                    message=message
                                ))
                    if not len(errors):
                        status = True
                i += 1
                if len(errors):
                    break

            if status and not len(errors):
                message_krei = '{}, {}'.format(message_krei,_('Запись задачи создана'))
        return errors, status, masivo_taskoj, message_krei

    @staticmethod
    def krei(info, **kwargs):
        errors = list()
        language_code = None
        print('======== зашли в krei ========== ')
        if info:
            uzanto = info.context.user
            language_code = info.context.LANGUAGE_CODE
        elif kwargs.get('uzanto', False):
            uzanto = kwargs['uzanto']
            language_code = uzanto.chefa_lingvo.kodo
        else:
            language_code = settings.LANGUAGE_CODE
        errors, message_krei, status, realeco, projekto = RedaktuKreiTaskojProjektoTaskojPosedanto.krei_projekto(
            errors, language_code, **kwargs
        )
        print('=== после создания проекта ==', projekto, '==')
        if len(errors)>0:
            print('=== выход с ошибкой ==' )
            return status, message_krei, errors, projekto, masivo_taskoj
        errors, status, masivo_taskoj, message_krei = RedaktuKreiTaskojProjektoTaskojPosedanto.krei_taskoj(
            errors, realeco, projekto, language_code, **kwargs
        )
        return status, message_krei, errors, projekto, masivo_taskoj

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        message_krei = None
        projekto = None
        masivo_taskoj = []
        # posedanto_dokumento = None
        # kategorio = TaskojTaskoKategorio.objects.none()
        # objekto = None
        # tipo = None
        # statuso = None
        # posedanto_tipo = None
        uzanto = None
        # language_code = None
        if info:
            uzanto = info.context.user
            # language_code = info.context.LANGUAGE_CODE
        elif kwargs.get('uzanto', False):
            uzanto = kwargs['uzanto']
        #     language_code = uzanto.chefa_lingvo.kodo
        # else:
        #     language_code = settings.LANGUAGE_CODE
        # print('=== type language_code = ', type(language_code))
        # print('=== language_code = ', language_code)
            # и ищем пользователя в базе
        print('=== uzanto RedaktuKreiTaskojProjektoTaskojPosedanto.create = ', uzanto)
        posedanto_perm = None # для проверки владельца объекта/задачи
        posedanto_objekto_perm = None
        # stokejo = None
        # проверяем владельца объекта, по которому идёт добавление проекта
        if not (uzanto.has_perm('taskoj.povas_krei_projektoj') or len(errors)):
            posedanto_objekto_perm, message = provado_posedanto_objekto(
                uzanto, 
                posedanto_objekto_uuid = kwargs.get('prj_posedanto_objekto_uuid', False),
                posedanto_uzanto_id = kwargs.get('prj_posedanto_uzanto_id', False),
                posedanto_organizo_uuid = kwargs.get('prj_posedanto_organizo_uuid', False),
                posedanto_dokumento_uuid = kwargs.get('posedanto_dokumento_uuid', False),
                )
            if message: #временная заплатка, т.к. не везде внедрены errors
                errors.append(ErrorNode(
                    field='provado_posedanto_objekto',
                    message=message
                ))
                message = None
        if uzanto.has_perm('taskoj.povas_krei_projektoj') or posedanto_objekto_perm:
            # errors, message_krei, status, posedanto_dokumento, realeco, projekto, stokejo = RedaktuKreiTaskojProjektoTaskojPosedanto.krei_projekto(
            #     errors, language_code, **kwargs
            # )
            pass
        else:
            errors.append(ErrorNode(
                field='',
                modelo='RedaktuKreiTaskojProjektoTaskojPosedanto',
                message=_('Нет прав на создание проекта')
            ))
            
        # проверяем владельца объекта, по которому идёт добавление задачи
        # если пользователь создаёт владельца проекта по объекту, чьим владельцем является, то разрешаем
        # проверяем владельца объекта
        if not (uzanto.has_perm('taskoj.povas_krei_taskoj') or len(errors)):
            posedanto_perm, message = provado_posedanto_objekto(
                uzanto, 
                posedanto_objekto_uuid = kwargs.get('prj_posedanto_objekto_uuid', False),
                posedanto_uzanto_id = kwargs.get('prj_posedanto_uzanto_id', False),
                posedanto_organizo_uuid = kwargs.get('prj_posedanto_organizo_uuid', False),
                posedanto_dokumento_uuid = kwargs.get('posedanto_dokumento_uuid', False),
                )
            if message: #временная заплатка, т.к. не везде внедрены errors
                errors.append(ErrorNode(
                    field='provado_posedanto_objekto',
                    message=message
                ))
                message = None
        pozicioj = kwargs.get('pozicio', False)
        if pozicioj and projekto and (not len(errors)) and (uzanto.has_perm('taskoj.povas_krei_taskoj') or posedanto_perm):
            # errors, status, masivo_taskoj, message_krei = RedaktuKreiTaskojProjektoTaskojPosedanto.krei_taskoj(
            #     errors, posedanto_dokumento, realeco, projekto, language_code, stokejo, **kwargs
            # )
            pass
        # elif (not projekto) and (not len(errors)):
        #     errors.append(ErrorNode(
        #         field='',
        #         modelo='RedaktuKreiTaskojProjektoTaskojPosedanto',
        #         message=_('Проект не создан')
        #     ))
        elif not (uzanto.has_perm('taskoj.povas_krei_taskoj') or posedanto_perm) and (not message):
            errors.append(ErrorNode(
                modelo='RedaktuKreiTaskojProjektoTaskojPosedanto',
                message=_('Недостаточно прав для создания задач')
            ))
            if projekto:
                # закрываем проект
                message = fermo_projekto(projekto)
                if message:
                    errors.append(ErrorNode(
                        message=message
                    ))
        print('======== пошли в krei ========== ', len(errors))
        if not len(errors):
            status, message, errors, projekto, masivo_taskoj = RedaktuKreiTaskojProjektoTaskojPosedanto.krei(
                info, **kwargs
            )
        if len(errors):
            status = False
            message = _('Nevalida argumentvaloroj')
            for error in errors:
                message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                    error.message, _('в поле'), error.field)
            if message_krei:
                message = "{}, и ошибка - {}".format(message_krei, message)
        else:
            status = True
            errors = list()
            message = message_krei

        return status, message, errors, projekto, masivo_taskoj

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        projekto = None
        errors = list()
        masivo_taskoj = []
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                status, message, errors, projekto, masivo_taskoj = RedaktuKreiTaskojProjektoTaskojPosedanto.create(root, info, **kwargs)
        else:
            message = _('Требуется авторизация')

        return RedaktuKreiTaskojProjektoTaskojPosedanto(
            status=status, 
            message=message, 
            errors=errors,
            projekto=projekto, 
            taskoj=masivo_taskoj
        )


# мутация закрытия проектов (статус statoso_forigi_id по умолчанию=4), у которых нет активных задач (со статусом statusoj_ignori по умолчанию (если не переданы значения) !=3 или !=4)
# fermi - закрыть
class RedaktuFermiTaskojProjekto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    projektoj = graphene.Field(graphene.List(TaskojProjektoNode), 
        description=_('Закрытие проектов (статус=statoso_forigi), у которых нет активных задач (со статусом !=statusoj_ignori)'))

    class Arguments:
        # параметры, одинаковые для проекта и всех задач
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        statuso_forigi_id = graphene.Int(description=_('ID статуса, который ставить проектам')) # какой статус ставить проектам
        statusoj_ignori_id = graphene.List(graphene.Int,description=_('ID статусов, которые не изменять (игнорировать при выборке)')) # какие статусы игнорировать (уже всё хорошо)
        statusoj_tasko_id = graphene.List(graphene.Int,description=_('ID статусов задач, при наличии которых закрывать проект нельзя')) 

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None

        realeco = None
        statusoj_ignori = None
        statuso_forigi = None
        statusoj_tasko = None
        uzanto = info.context.user
        projektoj = None

        if uzanto.is_authenticated:
            with transaction.atomic():
                # проверяем права
                if uzanto.has_perm('taskoj.povas_krei_projektoj'):

                    # проверяем наличие записей с таким кодом
                    if not message:
                        if 'realeco_id' in kwargs:
                            try:
                                realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except Realeco.DoesNotExist:
                                message = _('Неверный параллельный мир')

                    if not message:
                        try:
                            statuso_forigi = TaskojProjektoStatuso.objects.get(id=kwargs.get('statuso_forigi_id', 4), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except TaskojProjektoStatuso.DoesNotExist:
                            message = _('Неверный статус проектов')

                    if not message:
                        statusoj_ignori = TaskojProjektoStatuso.objects.filter(id__in=kwargs.get('statusoj_ignori_id',[3,4]), forigo=False,
                                                                arkivo=False, publikigo=True)

                    if not message:
                        statusoj_tasko = TaskojTaskoStatuso.objects.filter(id__in=kwargs.get('statusoj_tasko_id',[1,2,6]), forigo=False,
                                                                arkivo=False, publikigo=True)
                    count = 0
                    if not message:
                        if realeco:
                            projektoj = TaskojProjekto.objects.filter(
                                realeco__in = realeco,
                                forigo = False,
                            ).exclude(statuso__in = statusoj_ignori
                            ).exclude(taskojtasko__statuso__in=statusoj_tasko
                            )
                        else:
                            projektoj = TaskojProjekto.objects.filter(
                                forigo = False,
                            ).exclude(statuso__in = statusoj_ignori
                            ).exclude(taskojtasko__statuso__in=statusoj_tasko
                            )
                        count = projektoj.count()
                        values = babilej_set = set(
                            projektoj.values_list('uuid', flat=True))

                        projektoj.update(statuso = statuso_forigi,)

                        projektoj = TaskojProjekto.objects.filter(uuid__in = values)

                        status = True

                if not message:
                    message = "{} = {}".format(_('Изменено задач проектов'), count)
                else:
                    message = "ошибка - {} {}".format(message)
        else:
            message = _('Требуется авторизация')

        return RedaktuFermiTaskojProjekto(
            status=status, 
            message=message, 
            projektoj=projektoj
        )


# Импорт Catalog_новаСостоянияЗаявокЭкспедирования "Состояния Заявок Экспедирования" (состоя́ние stato,  (заказ) mendo, Экспедирования (ekspedo)) в статусы задач из 1с
class ImportoStatoMendoEkspedo1c(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    importo = graphene.Field(TaskojTaskoStatusoNode)

    class Arguments:
        adreso = graphene.String(description=_('Адрес сервера, откуда производить импорт данных'))

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        importo = None
        uzanto = info.context.user

        url = settings.ODATA_URL+'Catalog_новаСостоянияЗаявокЭкспедирования?$format=application/json'
        login = settings.ODATA_USER
        password = settings.ODATA_PASSWORD

        if uzanto.has_perm('taskoj.povas_krei_taskoj_statusoj'):

            with transaction.atomic():
                r = requests.get(url,auth=(login, password), verify=False)
                kvanto = 0
                jsj = r.json()
                for js in jsj['value']:
                    if len(errors)>0:
                        break
                    uuid = js['Ref_Key']
                    try:
                        importo = TaskojTaskoStatuso.objects.get(uuid=uuid, forigo=False,
                                                            arkivo=False, publikigo=True)
                        next # такой есть, идём к следующему
                    except TaskojTaskoStatuso.DoesNotExist:

                        nomo = js['Description']
                        priskribo = _('Импортировано из 1с')

                        if not len(errors):
                            importo = TaskojTaskoStatuso.objects.create(
                                uuid=uuid,
                                forigo=False,
                                arkivo=False,
                                publikigo=True,
                                publikiga_dato=timezone.now(),
                                autoro=uzanto,
                            )
                            if nomo:
                                set_enhavo(importo.nomo, nomo, 'ru_RU')
                            if priskribo:
                                set_enhavo(importo.priskribo, 'Импортировано из 1с', 'ru_RU')

                            importo.save()

                            importo_projekto = TaskojProjektoStatuso.objects.create(
                                uuid=uuid,
                                forigo=False,
                                arkivo=False,
                                publikigo=True,
                                publikiga_dato=timezone.now(),
                                autoro=uzanto,
                            )
                            if nomo:
                                set_enhavo(importo_projekto.nomo, nomo, 'ru_RU')
                            if priskribo:
                                set_enhavo(importo_projekto.priskribo, 'Импортировано из 1с', 'ru_RU')

                            importo_projekto.save()

                            kvanto += 1

                        else:
                            message = _('Nevalida argumentvaloroj')
                            for error in errors:
                                message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                    error.mesage, _('в поле'), error.field)
                            break
                if not message:
                    status = True
                    errors = list()
                    if kvanto > 0:
                        message =  '{} {} {}'.format(_('Добавлено'), kvanto, _('записей'))
                    else:
                        message =  _('Не найдено новых объектов для добавления')
        else:
            message = _('Nesufiĉaj rajtoj')

        return status, message, errors, importo

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        importo = None
        user = info.context.user

        if user.is_authenticated:
            # Импортируем данные
            status, message, errors, importo = ImportoStatoMendoEkspedo1c.create(root, info, **kwargs)
        else:
            message = _('Bezonata rajtigo')

        return ImportoStatoMendoEkspedo1c(status=status, message=message, errors=errors, importo=importo)


class TaskojMutations(graphene.ObjectType):
    redaktu_taskoj_projekto_kategorio = RedaktuTaskojProjektoKategorio.Field(
        description=_('''Создаёт или редактирует категории проектов''')
    )
    redaktu_taskoj_projekto_tipo = RedaktuTaskojProjektoTipo.Field(
        description=_('''Создаёт или редактирует типы проектов''')
    )
    redaktu_taskoj_projekto_statuso = RedaktuTaskojProjektoStatuso.Field(
        description=_('''Создаёт или редактирует статусы проектов''')
    )
    redaktu_taskoj_projekto = RedaktuTaskojProjekto.Field(
        description=_('''Создаёт или редактирует проекты''')
    )
    redaktu_taskoj_projekto_posedantoj_tipo = RedaktuTaskojProjektoPosedantoTipo.Field(
        description=_('''Создаёт или редактирует типы владельцев проектов''')
    )
    redaktu_taskoj_projekto_posedantoj_statuso = RedaktuTaskojProjektoPosedantoStatuso.Field(
        description=_('''Создаёт или редактирует статусы владельцев проектов''')
    )
    redaktu_taskoj_projekto_posedantoj = RedaktuTaskojProjektoPosedanto.Field(
        description=_('''Создаёт или редактирует владельцев проекты''')
    )
    redaktu_taskoj_tasko_kategorio = RedaktuTaskojTaskoKategorio.Field(
        description=_('''Создаёт или редактирует категории задач''')
    )
    redaktu_taskoj_tasko_tipo = RedaktuTaskojTaskoTipo.Field(
        description=_('''Создаёт или редактирует типы задач''')
    )
    redaktu_taskoj_tasko_statuso = RedaktuTaskojTaskoStatuso.Field(
        description=_('''Создаёт или редактирует статусы задач''')
    )
    redaktu_taskoj_tasko = RedaktuTaskojTasko.Field(
        description=_('''Создаёт или редактирует задачи''')
    )
    redaktu_taskoj_tasko_posedantoj_tipo = RedaktuTaskojTaskoPosedantoTipo.Field(
        description=_('''Создаёт или редактирует типы владельцев задач''')
    )
    redaktu_taskoj_tasko_posedantoj_statuso = RedaktuTaskojTaskoPosedantoStatuso.Field(
        description=_('''Создаёт или редактирует статусы владельцев задач''')
    )
    redaktu_taskoj_tasko_posedantoj = RedaktuTaskojTaskoPosedanto.Field(
        description=_('''Создаёт или редактирует владельцев задачи''')
    )
    redaktu_krei_taskoj_taskoj_posedanto = RedaktuKreiTaskojTaskojPosedanto.Field(
        description=_('''Создаёт список задач с владельцами''')
    )
    redaktu_krei_taskoj_projekto_taskoj_posedanto = RedaktuKreiTaskojProjektoTaskojPosedanto.Field(
        description=_('''Создание проекта с владельцем и списка задач с владельцами''')
    )
    redaktu_fermi_taskoj_projekto = RedaktuFermiTaskojProjekto.Field(
        description=_('''Закрытие проектов (статус=statoso_forigi), у которых нет активных задач (со статусом !=statusoj_ignori)''')
    )
    importo_stato_mendo_ekspedo1c = ImportoStatoMendoEkspedo1c.Field(
        description=_('''Импорт Catalog_новаСостоянияЗаявокЭкспедирования "Состояния Заявок Экспедирования" bp 1c в статусы задач''')
    )

